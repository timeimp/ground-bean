/*
 * GCLoginView.j
 * GroundCoffee
 *
 * Created by James Wilson (timeimp) 2012
 * Copyright 2012, timeimp inc. All rights reserved.
 */
 
 
@import "../Models/GCAuthenticationModel.j"
@import "../Models/GCServerModel.j"
@import "../Frameworks/Custom/CPAnimations.j"
@import "../Frameworks/Custom/RLIntermediateProgressIndicator.j"
@import "../Frameworks/Custom/EKShakeAnimation.j"
 
@implementation  GCLoginView : CPWindow {
    @outlet      CPButton    loginButton;
    @outlet      CPTextField usernameField;
    @outlet      CPSecureTextField passwordField;
    @outlet      CPTextField errorMessageField;
    @outlet      CPTextField loginActivityString;
    @outlet      CPTextField mojangString;
    @outlet      RLIndeterminateProgressIndicator ProgressIndicator;
    id           delegate       @accessors;
}

+(void)initialize {
    console.log("Loaded GCLoginView");
}
-(IBAction)performLogin:(id)sender{
    var payload = [[GCAuthenticationObject alloc] init];
    [payload setDelegate:self];
    [payload authenticateWithUser:usernameField._stringValue andPassword:passwordField._stringValue];
    [errorMessageField setStringValue:@""]; 
}

-(void)hideProgress{
    [ProgressIndicator setHidden:YES];
}

-(void)didSucceedAuthenticating:(id)sender{
    [[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(updateString:) name:"GCServerInformationRetrieved" object:nil];
    [[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(endLoginWindow:) name:"GCLoginAuditFinished" object:nil];

    [loginButton setKeyEquivalent:nil];
    [loginButton removeFromSuperview];
    [usernameField removeFromSuperview];
    [passwordField removeFromSuperview];
    
    [ProgressIndicator setHidden:NO];
    
    [loginActivityString setStringValue:@"Retrieving Server Configuration"];
    [loginActivityString setHidden:NO];
    
    [mojangString removeFromSuperview];
    
    var model = [[GCServerModel alloc] init];
    [model setDelegate:self];
    console.log("Set");
    [model retrieveInformation];
    
}
-(void)changeString:(CPString)aString{
    [loginActivityString setStringValue:aString];
}
-(void)updateString:(CPNotification)aNotification{
    [loginActivityString setStringValue:@"Validating Retrieved Configuration..."];
}
-(void)didFailAuthenticationWithError:(id)sender{
    [errorMessageField setStringValue:sender];
    var shake = [[EKShakeAnimation alloc] initWithView:errorMessageField];
}
-(void)endLoginWindow:(CPNotification)aNotification{
    [self orderOut:self];
}
@end