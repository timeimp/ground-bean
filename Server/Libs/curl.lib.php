<?php
final class curl //version 1.4.
{
        // static
        private static $_chc = array(
                CURLOPT_HEADER => TRUE,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_MAXCONNECTS => 15,
                CURLOPT_CONNECTTIMEOUT => 30,
                CURLOPT_TIMEOUT => 10
        );
        private static $_ch;
        private static $_mh;
        private static $_buf = array();
        private static $_cht = array();
        private static $_chl = 10;
        private static function _init ()
        {
                if(!isset(self::$_ch))
                {
                        self::$_ch = curl_init();
                        curl_setopt_array(self::$_ch, self::$_chc);
                        self::$_mh = curl_multi_init();
                }
        }
        // url cleaner
        static function url ($url, $base = NULL)
        {
                // parse
                $url = parse_url($url);
                $base = parse_url($base);
                $sp = array('scheme','user','pass','host','port');
                // join
                if(!isset($url['scheme']))
                {
                        // server parameters
                        foreach($sp as $p)
                        {
                                if(isset($base[$p]))
                                        $url[$p] = $base[$p];
                        }
                        // path
                        if($url['path'][0]!='/')
                        {
                                if(!isset($base['path']) || strlen($base['path'])==0)
                                        $base['path'] = '/';
                                elseif($base['path'][0]!='/')
                                        $base['path'] = '/' . $base['path'];
                                $url['path'] = substr($base['path'], 0, strrpos($base['path'], '/')+1) . $url['path'];
                        }
                }
                // reduce
                $url['path'] = preg_replace('#\./#', '/', $url['path']);
                $url['path'] = preg_replace('#/+#', '/', $url['path']);
                do
                {
                        $p = $url['path'];
                        $url['path'] = preg_replace('#[^/]*[^\.]/\./#', '', $url['path'], 1);
                }
                while($p != $url['path']);
                $url['path'] = preg_replace('#\./#', '', $url['path']);
                // special reduce
               
                // build&return
                return
                         ((isset($url['scheme'])) ? $url['scheme'] . '://' : '')
                        .((isset($url['user'])) ? $url['user'] . ((isset($url['pass'])) ? ':' . $url['pass'] : '') .'@' : '')
                        .((isset($url['host'])) ? $url['host'] : '')
                        .((isset($url['port'])) ? ':' . $url['port'] : '')
                        .((isset($url['path'])) ? $url['path'] : '')
                        .((isset($url['query'])) ? '?' . $url['query'] : '')
                ;
        }
        // single request
        static function get ($url)
        {
                self::_init();
                curl_setopt(self::$_ch, CURLOPT_URL, $url);
                curl_setopt(self::$_ch, CURLOPT_HTTPGET, TRUE);
                $data = curl_exec(self::$_ch);
                return new curl(self::$_ch, $data, $url);
        }
        static function head ($url)
        {
                self::_init();
                curl_setopt(self::$_ch, CURLOPT_URL, $url);
                curl_setopt(self::$_ch, CURLOPT_NOBODY, TRUE);
                $data = curl_exec(self::$_ch);
                return new curl(self::$_ch, $data, $url);
        }
        // multi request
        static function limit ($l = 10)
        {
                $l = intval($l);
                if($l < 1)
                        $l = 10;
                if($l > 50)
                        $l = 50;
                if($l < 3)
                        trigger_error('['. __CLASS__ .'] less than 3 concurring request in inefficent; consider using unparalleled requests', E_USER_WARNING);
                self::$_chl = $l;
        }
        static function queue ($url)
        {
                if(in_array($url, self::$_buf) || in_array($url, self::$_cht))
                        return FALSE;
               
                self::$_buf[] = $url;
                return TRUE;
        }
        static function fetch ($timeout = 1)
        {
                self::_init();
                // timeout correction
                $timeout = intval($timeout);
                if($timeout < 1)
                        $timeout = 1;
                if($timeout > 10)
                        $timeout = 10;
                // add more handles?
                while( count(self::$_cht)<self::$_chl && count(self::$_buf) )
                {
                        $url = array_shift(self::$_buf);
                        $ch = curl_init();
                        curl_setopt_array($ch, self::$_chc);
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_multi_add_handle(self::$_mh, $ch);
                        self::$_cht[(int)$ch] = $url;
                }
                // run till timeout
                $to = gettimeofday(true) + $timeout;
                do
                {
                        while( curl_multi_exec(self::$_mh, $active)===CURLM_CALL_MULTI_PERFORM ){};
                        if($active)
                                curl_multi_select(self::$_mh, $to - gettimeofday(true));
                        else
                                usleep(($to - gettimeofday(true)) * 1000000 + 1000);
                        if( ($info=curl_multi_info_read(self::$_mh))!==false )
                        {
                                // read result
                                $ch = $info["handle"];
                                $url = self::$_cht[(int)$ch];
                                $data = curl_multi_getcontent($ch);
                                $obj = new curl($ch, $data, $url);
                                // continue logic
                                curl_multi_remove_handle(self::$_mh, $ch);
                                if( count(self::$_buf) )
                                {
                                        $url = array_shift(self::$_buf);
                                        curl_setopt($ch, CURLOPT_URL, $url);
                                        curl_multi_add_handle(self::$_mh, $ch);
                                        self::$_cht[(int)$ch] = $url;
                                }
                                else
                                {
                                        unset(self::$_cht[(int)$ch]);
                                        curl_close($ch);
                                }
                                // return object
                                return $obj;
                        }
                }
                while( $to > gettimeofday(true) );
                return;
        }
        static function active ()
        {
                return count(self::$_cht);
        }
        static function buffered ()
        {
                return count(self::$_buf);
        }
        // creation
    private function __construct($ch, $data, $url)
        {
                $this->_error = curl_error($ch);
                $this->_errno = curl_errno($ch);
                $p = strpos($data, "\r\n\r\n");
                if($p === FALSE)
                        $this->_ret = "";
                else
                {
                        $this->_ret = substr($data, $p+4);
                        $data = substr($data, 0, $p);
                }
                $data = explode("\r\n", $data);
                array_shift($data);
                $this->_header = array();
                foreach($data as $d)
                {
                        $d = trim($d);
                        if(strlen($d)==0)
                                continue;
                        $p = strpos($d, ": ");
                        if($p === FALSE)
                        {
                                $k = "";
                                $v = $d;
                        }
                        else
                        {
                                $k = substr($d, 0, strpos($d, ": "));
                                $v = substr($d, strpos($d, ": ")+2);
                        }
                        if(isset($this->_header[$k]))
                        {
                                if(is_array($this->_header[$k]))
                                        $this->_header[$k][] = $v;
                                else
                                        $this->_header[$k] = array($this->_header[$k], $v);
                        }
                        else
                                $this->_header[$k] = $v;
                }
                $this->_info = curl_getinfo($ch);
                $this->_info['original_url'] = $url;
        }
    private function __clone() {}
        // dyn
        private $_error;
        private $_errno;
        private $_ret;
        private $_header;
        private $_info;
        function __toString()
        {
                return $this->_ret;
        }
        function __invoke()
        {
                return $this->_ret;
        }
        function size ($p = 0)
        {
                if( $this->failed() )
                        return FALSE;
               
                $p = intval($p);
                if($p < 0)
                        $p = 0;
                if($p > 3)
                        $p = 3;
                return $this->_info['download_content_length']/(1<<(10*$p));
        }
        function info ($type)
        {
                if( $this->failed() )
                        return FALSE;
               
                if(is_string($type) && strlen($type) && isset($this->_info[$type]))
                        return $this->_info[$type];
                else
                        return FALSE;
        }
        function header ($key)
        {
                if( $this->failed() )
                        return FALSE;
               
                if(is_string($key) && strlen($key) && isset($this->_header[$key]))
                        return $this->_header[$key];
                else
                        return "";
        }
        function code ()
        {
                if( $this->failed() )
                        return 0;
                else
                        return $this->_info['http_code'];
        }
        function ok ()
        {
                return $this->code()==200;
        }
        function failed ()
        {
                if( $this->errno() )
                        return TRUE;
                else
                        return FALSE;
        }
        function errno ()
        {
                return $this->_errno;
        }
        function error ()
        {
                return $this->_error;
        }
        function dump ()
        {
                print_r($this->_header);
                print_r($this->_info);
                echo $this->error();
        }
}
?>