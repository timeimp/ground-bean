<?php
/*******************
*
*   addClient.php
*   When the user performs "Add Server", this will authenticate them.
*   If successful, we will then respond with "ClientPayload{"key","phrase"}"
*   and the App will then load storeClient.php and add the client to the DB
*******************/

require("../Libs/curl.lib.php");

error_reporting(E_ALL);

header("Content-Type: application/json");

$postdata = file_get_contents("php://input");
$sql_data = json_decode($postdata, true);

$server = $sql_data['server'];
$user   = $sql_data['user'];
$pass   = sha1($sql_data['pass']);

$url = $server;
$inifile = parse_ini_file("../Configurations/Version/storedVersions.ini",true);
$version = $inifile['config_meta_info']['version'];

//$page = $inifile[$version]['url_client_add'];

/* /$url = $server.$page; */

print_r($inifile);  
echo $version;

?>