<?php
/******************
Authenticate the user. Return the items as JSON.
******************/
error_reporting(E_ALL);
require_once("../mysql.conf.php");

$postdata = file_get_contents("php://input");
$sql_data = json_decode($postdata, true);

$user = $sql_data['user'];
$pass = sha1($sql_data['pass']);

# Inform that we are returning JSON data
header("Content-Type: application/json");

# Set the .status property to failed.
$status = "FAILED";
# Prepare the Query
try{
	$statement = $UOW_DBH->prepare("SELECT * FROM users WHERE uname = :user AND passwd = :pass");
	$statement->bindParam(':user', $user);
	$statement->bindParam(':pass', $pass);
	$statement->execute();
	$totalRows = $statement->rowCount();

	$row = $statement->fetchAll();
	foreach($row as $result){
		$r = array("u_fname"=>"Administrator");
    }
	if($totalRows != 1){
		$json = array("status" => $status, "errorMessage" => "Please try again.");
	} else {
		$status = "SUCCESS";
		$q = array("status" => $status);
		$json = array_merge_recursive($q, $r);
 	}

	$UOW_DBH = null;

} catch(PDOException $e) { 
	$errorMessage = array("errorMessage" => $e->getMessage());
	$json = array_merge_recursive(array("status"=>$status), $errorMessage);
 	//echo $e->getMessage();  
}  
echo json_encode($json);
?>