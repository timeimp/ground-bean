<?php

header("Content-Type: application/json");

$arr = array(
        "name" => "timeimpsServer",
        "ip" => "127.0.0.1",
        "hash" => sha1("timeimpsServer"),
        "key" => "elgoog",
        "information" => array(
            "version" => "1.3.2",
            "mods"  => "NO",
            "properties" => array(
                "allow-nether" => true,
                "level-name" => "world",
                "enable-query" => false,
                "allow-flight" => false,
                "server-port"  => 25565,
                "level-type" => "DEFAULT",
                "enable-rcon" => false,
                "level-seed" => "",
                "server-ip" => "",
                "max-build-height" => 256,
                "spawn-npcs" => true,
                "white-list" => false,
                "spawn-animals" => true,
                "snooper-enabled" => true,
                "hardcore" => false,
                "texture-pack" => "",
                "online-mode" => true,
                "pvp" => true,
                "difficulty" => 1,
                "gamemode" => 0,
                "max-players" => 20,
                "spawn-monsters" => true,
                "generate-structures" => true,
                "view-distance" => 10,
                "motd" => "A Minecraft Server"
            )
        )
    );
echo json_encode($arr);
?>