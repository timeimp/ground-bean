/*
 * AppController.j
 * MyUOW
 *
 * Created by You on September 12, 2012.
 * Copyright 2012, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>
@import <AppKit/AppKit.j>
@import <TNKit/TNKit.j>

@import "Controllers/Controllers.j"
@import "Views/Views.j"

@implementation AppController : CPObject
{
    @outlet		 CPWindow            theWindow;
    
  	@outlet      GCLoginView         loginView;
  	@outlet      CPSplitView         gcSplitView;
  	@outlet      CPSplitView         gcActivitySplitView;
  	@outlet      CPView              gcLeftView;
  	@outlet      CPView              gcRightView;
  	@outlet      CPView              gcActivityView;
  	@outlet      CPTabView           gcTabView;
  	@outlet      CPOutlineView       gcOutlineView;
  	@outlet      CPViewController    gcServerOverviewController;
  	@outlet      CPTabView           gcServerOverview;
  	@outlet      CPTabViewItem       gcServerOverviewItem;
  	
  	@outlet      CPView              gcServerOverviewView;
  	
  	@outlet      CPView              gcAddServerView;
  	@outlet      CPView              gcAddUsersView;
  	
  	@outlet      CPView              gcRemoveUsersView;
  	
  	@outlet      CPView              gcSendMessageAllUsersView;
  	@outlet      CPView              gcSendMessageConnectedUsersView;
  	@outlet      CPView              gcSendMessageSomeUsersView;
  	
  	             CPWindow            tempWindow;
    
}

-(void)applicationDidFinishLaunching:(CPNotification)aNotification
{
    // This is called when the application is done loading.
    [CPMenu setMenuBarVisible:NO];
    [[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(launchApp:) name:@"GCLoginAuditSuccess" object:nil];

}
-(void)awakeFromCib
{
    // This is called when the cib is done loading.
    // You can implement this method on any object instantiated from a Cib.
    // It's a useful hook for setting up current UI values, and other things.

    // In this case, we want the window from Cib to become our full browser window
    //[theWindow setFullPlatformWindow:YES];
    [loginView setFullPlatformWindow:NO];
    [gcLeftView setBackgroundColor:[CPColor colorWithHexString:@"E4EBF7"]];
    [loginView setDelegate:self];
    [loginView orderFront:self];

}

-(void)launchApp:(CPNotification)aNotification
{ 
    CappApp = [CPApplication sharedApplication];
    
    [[CPNotificationCenter defaultCenter] postNotificationName:@"GCLoginAuditFinished" object:nil];
    
    [loginView orderOut:self];
    [loginView close];
    
    [theWindow setFullPlatformWindow:YES];
    [CPMenu setMenuBarVisible:YES];
    [theWindow orderFront:self];
    [gcActivityView setBackgroundColor:[CPColor colorWithHexString:@"dce3f2"]];

    // main split views 
    var posx = 245;
    [gcSplitView setPosition:posx ofDividerAtIndex:0];
    
    var leftViewBounds = [gcLeftView bounds];
    leftViewBounds.size.width = posx;
    [gcLeftView setFrame:leftViewBounds]; 
    
    [gcTabView selectTabViewItemAtIndex:0];
    
}

-(IBAction)addServer:(id)sender{
    tempWindow = [[CPWindow alloc] initWithContentRect:CGRectMake(0,0,550,275) styleMask:CPTexturedBackgroundWindowMask];
    [[tempWindow contentView] addSubview:gcAddServerView];
    
    [CappApp beginSheet: tempWindow
               modalForWindow: theWindow
               modalDelegate: self
               didEndSelector: @selector(didEndSheet:returnCode:contextInfo:)
               contextInfo: nil];
}

-(IBAction)addUsers:(id)sender{
    tempWindow = [[CPWindow alloc] initWithContentRect:CGRectMake(0,0,660,360) styleMask:CPTexturedBackgroundWindowMask];
    [[tempWindow contentView] addSubview:gcAddUsersView];

    [CappApp beginSheet: tempWindow
               modalForWindow: theWindow
               modalDelegate: self
               didEndSelector: @selector(didEndSheet:returnCode:contextInfo:)
               contextInfo: nil];  
}

-(IBAction)removeUsers:(id)sender{
    tempWindow = [[CPWindow alloc] initWithContentRect:CGRectMake(0,0,660,360) styleMask:CPTexturedBackgroundWindowMask];
    [[tempWindow contentView] addSubview:gcRemoveUsersView];

    [CappApp beginSheet: tempWindow
               modalForWindow: theWindow
               modalDelegate: self
               didEndSelector: @selector(didEndSheet:returnCode:contextInfo:)
               contextInfo: nil];  
}

-(IBAction)sendMessageToAllUsers:(id)sender{
    tempWindow = [[CPWindow alloc] initWithContentRect:CGRectMake(0,0,660,360) styleMask:CPTexturedBackgroundWindowMask];
    [[tempWindow contentView] addSubview:gcSendMessageAllUsersView];

    [CappApp beginSheet: tempWindow
               modalForWindow: theWindow
               modalDelegate: self
               didEndSelector: @selector(didEndSheet:returnCode:contextInfo:)
               contextInfo: nil];  
}

-(IBAction)sendMessageToConnectedUsers:(id)sender{
    tempWindow = [[CPWindow alloc] initWithContentRect:CGRectMake(0,0,660,360) styleMask:CPTexturedBackgroundWindowMask];
    [[tempWindow contentView] addSubview:gcSendMessageConnectedUsersView];

    [CappApp beginSheet: tempWindow
               modalForWindow: theWindow
               modalDelegate: self
               didEndSelector: @selector(didEndSheet:returnCode:contextInfo:)
               contextInfo: nil];  
}

-(IBAction)sendMessageToSelectUsers:(id)sender{
    tempWindow = [[CPWindow alloc] initWithContentRect:CGRectMake(0,0,660,360) styleMask:CPTexturedBackgroundWindowMask];
    [[tempWindow contentView] addSubview:gcSendMessageSomeUsersView];

    [CappApp beginSheet: tempWindow
               modalForWindow: theWindow
               modalDelegate: self
               didEndSelector: @selector(didEndSheet:returnCode:contextInfo:)
               contextInfo: nil];  
}

-(float)splitView:(CPSplitView)aSplitView constrainMinCoordinate:(float)proposedMin ofSubviewAt:(int)subviewIndex{
	return proposedMin + 245;
}

-(void)didEndSheet:(CPWindow)aSheet returnCode:(int)returnCode contextInfo:(id)contextInfo{
    [aSheet orderOut:self];
    aSheet = nil;
}
- (IBAction)closeTheSheet:(id)sender{
    var CappApp = [CPApplication sharedApplication];
    [CappApp endSheet:tempWindow];
}
@end