@STATIC;1.0;p;12;Foundation.jt;3182;@STATIC;1.0;i;9;CPArray.ji;10;CPBundle.ji;9;CPCoder.ji;8;CPData.ji;8;CPDate.ji;14;CPDictionary.ji;14;CPEnumerator.ji;13;CPException.ji;13;CPFormatter.ji;12;CPGeometry.ji;12;CPIndexSet.ji;14;CPInvocation.ji;17;CPKeyedArchiver.ji;19;CPKeyedUnarchiver.ji;18;CPKeyValueCoding.ji;21;CPKeyValueObserving.ji;16;CPMutableArray.ji;14;CPMutableSet.ji;16;CPNotification.ji;22;CPNotificationCenter.ji;8;CPNull.ji;10;CPNumber.ji;10;CPObject.ji;15;CPObjJRuntime.ji;29;CPPropertyListSerialization.ji;9;CPRange.ji;11;CPRunLoop.ji;7;CPSet.ji;18;CPSortDescriptor.ji;10;CPString.ji;9;CPTimer.ji;15;CPUndoManager.ji;7;CPURL.ji;17;CPURLConnection.ji;14;CPURLRequest.ji;15;CPURLResponse.ji;16;CPUserDefaults.ji;9;CPValue.ji;20;CPValueTransformer.jt;2450;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPBundle.j",YES);
/* objj_executeFile("CPCharacterSet.j",YES) */ (undefined);
objj_executeFile("CPCoder.j",YES);
/* objj_executeFile("CPComparisonPredicate.j",YES) */ (undefined);
/* objj_executeFile("CPCompoundPredicate.j",YES) */ (undefined);
objj_executeFile("CPData.j",YES);
objj_executeFile("CPDate.j",YES);
/* objj_executeFile("CPDateFormatter.j",YES) */ (undefined);
/* objj_executeFile("CPDecimal.j",YES) */ (undefined);
/* objj_executeFile("CPDecimalNumber.j",YES) */ (undefined);
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPEnumerator.j",YES);
objj_executeFile("CPException.j",YES);
/* objj_executeFile("CPExpression.j",YES) */ (undefined);
objj_executeFile("CPFormatter.j",YES);
objj_executeFile("CPGeometry.j",YES);
objj_executeFile("CPIndexSet.j",YES);
/* objj_executeFile("CPIndexPath.j",YES) */ (undefined);
objj_executeFile("CPInvocation.j",YES);
/* objj_executeFile("CPJSONPConnection.j",YES) */ (undefined);
objj_executeFile("CPKeyedArchiver.j",YES);
objj_executeFile("CPKeyedUnarchiver.j",YES);
objj_executeFile("CPKeyValueCoding.j",YES);
objj_executeFile("CPKeyValueObserving.j",YES);
objj_executeFile("CPMutableArray.j",YES);
objj_executeFile("CPMutableSet.j",YES);
objj_executeFile("CPNotification.j",YES);
objj_executeFile("CPNotificationCenter.j",YES);
objj_executeFile("CPNull.j",YES);
objj_executeFile("CPNumber.j",YES);
/* objj_executeFile("CPNumberFormatter.j",YES) */ (undefined);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPObjJRuntime.j",YES);
/* objj_executeFile("CPOperation.j",YES) */ (undefined);
/* objj_executeFile("CPOperationQueue.j",YES) */ (undefined);
/* objj_executeFile("CPPredicate.j",YES) */ (undefined);
objj_executeFile("CPPropertyListSerialization.j",YES);
objj_executeFile("CPRange.j",YES);
objj_executeFile("CPRunLoop.j",YES);
/* objj_executeFile("CPScanner.j",YES) */ (undefined);
objj_executeFile("CPSet.j",YES);
objj_executeFile("CPSortDescriptor.j",YES);
objj_executeFile("CPString.j",YES);
objj_executeFile("CPTimer.j",YES);
objj_executeFile("CPUndoManager.j",YES);
objj_executeFile("CPURL.j",YES);
objj_executeFile("CPURLConnection.j",YES);
objj_executeFile("CPURLRequest.j",YES);
objj_executeFile("CPURLResponse.j",YES);
objj_executeFile("CPUserDefaults.j",YES);
/* objj_executeFile("CPUserSessionManager.j",YES) */ (undefined);
objj_executeFile("CPValue.j",YES);
objj_executeFile("CPValueTransformer.j",YES);
p;10;CPObject.jt;6817;@STATIC;1.0;t;6798;
var _1=objj_allocateClassPair(Nil,"CPObject"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("isa")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("init"),function(_3,_4){
with(_3){
return _3;
}
}),new objj_method(sel_getUid("copy"),function(_5,_6){
with(_5){
return _5;
}
}),new objj_method(sel_getUid("mutableCopy"),function(_7,_8){
with(_7){
return objj_msgSend(_7,"copy");
}
}),new objj_method(sel_getUid("dealloc"),function(_9,_a){
with(_9){
}
}),new objj_method(sel_getUid("class"),function(_b,_c){
with(_b){
return isa;
}
}),new objj_method(sel_getUid("isKindOfClass:"),function(_d,_e,_f){
with(_d){
return objj_msgSend(isa,"isSubclassOfClass:",_f);
}
}),new objj_method(sel_getUid("isMemberOfClass:"),function(_10,_11,_12){
with(_10){
return _10.isa===_12;
}
}),new objj_method(sel_getUid("isProxy"),function(_13,_14){
with(_13){
return NO;
}
}),new objj_method(sel_getUid("respondsToSelector:"),function(_15,_16,_17){
with(_15){
return !!class_getInstanceMethod(isa,_17);
}
}),new objj_method(sel_getUid("implementsSelector:"),function(_18,_19,_1a){
with(_18){
var _1b=class_copyMethodList(isa),_1c=_1b.length;
while(_1c--){
if(method_getName(_1b[_1c])===_1a){
return YES;
}
}
return NO;
}
}),new objj_method(sel_getUid("methodForSelector:"),function(_1d,_1e,_1f){
with(_1d){
return class_getMethodImplementation(isa,_1f);
}
}),new objj_method(sel_getUid("methodSignatureForSelector:"),function(_20,_21,_22){
with(_20){
return nil;
}
}),new objj_method(sel_getUid("description"),function(_23,_24){
with(_23){
return "<"+class_getName(isa)+" 0x"+objj_msgSend(CPString,"stringWithHash:",objj_msgSend(_23,"UID"))+">";
}
}),new objj_method(sel_getUid("performSelector:"),function(_25,_26,_27){
with(_25){
return objj_msgSend(_25,_27);
}
}),new objj_method(sel_getUid("performSelector:withObject:"),function(_28,_29,_2a,_2b){
with(_28){
return objj_msgSend(_28,_2a,_2b);
}
}),new objj_method(sel_getUid("performSelector:withObject:withObject:"),function(_2c,_2d,_2e,_2f,_30){
with(_2c){
return objj_msgSend(_2c,_2e,_2f,_30);
}
}),new objj_method(sel_getUid("performSelector:withObjects:"),function(_31,_32,_33,_34){
with(_31){
var _35=[_31,_33].concat(Array.prototype.slice.apply(arguments,[3]));
return objj_msgSend.apply(this,_35);
}
}),new objj_method(sel_getUid("forwardingTargetForSelector:"),function(_36,_37,_38){
with(_36){
return nil;
}
}),new objj_method(sel_getUid("forwardInvocation:"),function(_39,_3a,_3b){
with(_39){
objj_msgSend(_39,"doesNotRecognizeSelector:",objj_msgSend(_3b,"selector"));
}
}),new objj_method(sel_getUid("doesNotRecognizeSelector:"),function(_3c,_3d,_3e){
with(_3c){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,(class_isMetaClass(isa)?"+":"-")+" ["+objj_msgSend(_3c,"className")+" "+_3e+"] unrecognized selector sent to "+(class_isMetaClass(isa)?"class "+class_getName(isa):"instance 0x"+objj_msgSend(CPString,"stringWithHash:",objj_msgSend(_3c,"UID"))));
}
}),new objj_method(sel_getUid("awakeAfterUsingCoder:"),function(_3f,_40,_41){
with(_3f){
return _3f;
}
}),new objj_method(sel_getUid("classForKeyedArchiver"),function(_42,_43){
with(_42){
return objj_msgSend(_42,"classForCoder");
}
}),new objj_method(sel_getUid("classForCoder"),function(_44,_45){
with(_44){
return objj_msgSend(_44,"class");
}
}),new objj_method(sel_getUid("replacementObjectForArchiver:"),function(_46,_47,_48){
with(_46){
return objj_msgSend(_46,"replacementObjectForCoder:",_48);
}
}),new objj_method(sel_getUid("replacementObjectForKeyedArchiver:"),function(_49,_4a,_4b){
with(_49){
return objj_msgSend(_49,"replacementObjectForCoder:",_4b);
}
}),new objj_method(sel_getUid("replacementObjectForCoder:"),function(_4c,_4d,_4e){
with(_4c){
return _4c;
}
}),new objj_method(sel_getUid("className"),function(_4f,_50){
with(_4f){
return isa.name;
}
}),new objj_method(sel_getUid("autorelease"),function(_51,_52){
with(_51){
return _51;
}
}),new objj_method(sel_getUid("hash"),function(_53,_54){
with(_53){
return objj_msgSend(_53,"UID");
}
}),new objj_method(sel_getUid("UID"),function(_55,_56){
with(_55){
if(typeof _55._UID==="undefined"){
_55._UID=objj_generateObjectUID();
}
return _UID+"";
}
}),new objj_method(sel_getUid("isEqual:"),function(_57,_58,_59){
with(_57){
return _57===_59||objj_msgSend(_57,"UID")===objj_msgSend(_59,"UID");
}
}),new objj_method(sel_getUid("retain"),function(_5a,_5b){
with(_5a){
return _5a;
}
}),new objj_method(sel_getUid("release"),function(_5c,_5d){
with(_5c){
}
}),new objj_method(sel_getUid("self"),function(_5e,_5f){
with(_5e){
return _5e;
}
}),new objj_method(sel_getUid("superclass"),function(_60,_61){
with(_60){
return isa.super_class;
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("load"),function(_62,_63){
with(_62){
}
}),new objj_method(sel_getUid("initialize"),function(_64,_65){
with(_64){
}
}),new objj_method(sel_getUid("new"),function(_66,_67){
with(_66){
return objj_msgSend(objj_msgSend(_66,"alloc"),"init");
}
}),new objj_method(sel_getUid("alloc"),function(_68,_69){
with(_68){
return class_createInstance(_68);
}
}),new objj_method(sel_getUid("allocWithCoder:"),function(_6a,_6b,_6c){
with(_6a){
return objj_msgSend(_6a,"alloc");
}
}),new objj_method(sel_getUid("class"),function(_6d,_6e){
with(_6d){
return _6d;
}
}),new objj_method(sel_getUid("superclass"),function(_6f,_70){
with(_6f){
return super_class;
}
}),new objj_method(sel_getUid("isSubclassOfClass:"),function(_71,_72,_73){
with(_71){
var _74=_71;
for(;_74;_74=_74.super_class){
if(_74===_73){
return YES;
}
}
return NO;
}
}),new objj_method(sel_getUid("isKindOfClass:"),function(_75,_76,_77){
with(_75){
return objj_msgSend(_75,"isSubclassOfClass:",_77);
}
}),new objj_method(sel_getUid("isMemberOfClass:"),function(_78,_79,_7a){
with(_78){
return _78===_7a;
}
}),new objj_method(sel_getUid("instancesRespondToSelector:"),function(_7b,_7c,_7d){
with(_7b){
return !!class_getInstanceMethod(_7b,_7d);
}
}),new objj_method(sel_getUid("instanceMethodForSelector:"),function(_7e,_7f,_80){
with(_7e){
return class_getMethodImplementation(_7e,_80);
}
}),new objj_method(sel_getUid("description"),function(_81,_82){
with(_81){
return class_getName(isa);
}
}),new objj_method(sel_getUid("setVersion:"),function(_83,_84,_85){
with(_83){
class_setVersion(_83,_85);
}
}),new objj_method(sel_getUid("version"),function(_86,_87){
with(_86){
return class_getVersion(_86);
}
})]);
CPDescriptionOfObject=function(_88){
if(_88.isa){
if(objj_msgSend(_88,"isKindOfClass:",CPString)){
return "@\""+objj_msgSend(_88,"description")+"\"";
}
return objj_msgSend(_88,"description");
}
if(typeof (_88)!=="object"){
return String(_88);
}
var _89="JSObject\n{\n";
for(var _8a in _88){
if(_88.hasOwnProperty(_8a)){
_89+="   "+_8a+": "+CPDescriptionOfObject(_88[_8a])+"\n";
}
}
_89+="}";
return _89.split("\n").join("\n\t");
};
p;10;CPString.jt;13409;@STATIC;1.0;i;13;CPException.ji;10;CPObject.ji;15;CPObjJRuntime.ji;9;CPRange.ji;18;CPSortDescriptor.ji;7;CPURL.ji;9;CPValue.jt;13276;
objj_executeFile("CPException.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPObjJRuntime.j",YES);
objj_executeFile("CPRange.j",YES);
objj_executeFile("CPSortDescriptor.j",YES);
objj_executeFile("CPURL.j",YES);
objj_executeFile("CPValue.j",YES);
CPCaseInsensitiveSearch=1;
CPLiteralSearch=2;
CPBackwardsSearch=4;
CPAnchoredSearch=8;
CPNumericSearch=64;
CPDiacriticInsensitiveSearch=128;
var _1=new CFMutableDictionary(),_2=["/",".","*","+","?","|","$","^","(",")","[","]","{","}","\\"],_3=new RegExp("(\\"+_2.join("|\\")+")","g"),_4=new RegExp("(^\\s+|\\s+$)","g");
var _5=objj_allocateClassPair(CPObject,"CPString"),_6=_5.isa;
objj_registerClassPair(_5);
class_addMethods(_5,[new objj_method(sel_getUid("initWithString:"),function(_7,_8,_9){
with(_7){
if(objj_msgSend(_7,"class")===CPString){
return String(_9);
}
var _a=new String(_9);
_a.isa=objj_msgSend(_7,"class");
return _a;
}
}),new objj_method(sel_getUid("initWithFormat:"),function(_b,_c,_d){
with(_b){
if(!_d){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"initWithFormat: the format can't be 'nil'");
}
_b=ObjectiveJ.sprintf.apply(this,Array.prototype.slice.call(arguments,2));
return _b;
}
}),new objj_method(sel_getUid("description"),function(_e,_f){
with(_e){
return _e;
}
}),new objj_method(sel_getUid("length"),function(_10,_11){
with(_10){
return length;
}
}),new objj_method(sel_getUid("characterAtIndex:"),function(_12,_13,_14){
with(_12){
return charAt(_14);
}
}),new objj_method(sel_getUid("stringByAppendingFormat:"),function(_15,_16,_17){
with(_15){
if(!_17){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"initWithFormat: the format can't be 'nil'");
}
return _15+ObjectiveJ.sprintf.apply(this,Array.prototype.slice.call(arguments,2));
}
}),new objj_method(sel_getUid("stringByAppendingString:"),function(_18,_19,_1a){
with(_18){
return _18+_1a;
}
}),new objj_method(sel_getUid("stringByPaddingToLength:withString:startingAtIndex:"),function(_1b,_1c,_1d,_1e,_1f){
with(_1b){
if(length==_1d){
return _1b;
}
if(_1d<length){
return substr(0,_1d);
}
var _20=_1b,_21=_1e.substring(_1f),_22=_1d-length;
while((_22-=_21.length)>=0){
_20+=_21;
}
if(-_22<_21.length){
_20+=_21.substring(0,-_22);
}
return _20;
}
}),new objj_method(sel_getUid("componentsSeparatedByString:"),function(_23,_24,_25){
with(_23){
return split(_25);
}
}),new objj_method(sel_getUid("substringFromIndex:"),function(_26,_27,_28){
with(_26){
return substr(_28);
}
}),new objj_method(sel_getUid("substringWithRange:"),function(_29,_2a,_2b){
with(_29){
if(_2b.location<0||((_2b).location+(_2b).length)>length){
objj_msgSend(CPException,"raise:reason:",CPRangeException,"aRange out of bounds");
}
return substr(_2b.location,_2b.length);
}
}),new objj_method(sel_getUid("substringToIndex:"),function(_2c,_2d,_2e){
with(_2c){
if(_2e>length){
objj_msgSend(CPException,"raise:reason:",CPRangeException,"index out of bounds");
}
return substring(0,_2e);
}
}),new objj_method(sel_getUid("rangeOfString:"),function(_2f,_30,_31){
with(_2f){
return objj_msgSend(_2f,"rangeOfString:options:",_31,0);
}
}),new objj_method(sel_getUid("rangeOfString:options:"),function(_32,_33,_34,_35){
with(_32){
return objj_msgSend(_32,"rangeOfString:options:range:",_34,_35,nil);
}
}),new objj_method(sel_getUid("rangeOfString:options:range:"),function(_36,_37,_38,_39,_3a){
with(_36){
if(!_38){
return CPMakeRange(CPNotFound,0);
}
var _3b=(_3a==nil)?_36:objj_msgSend(_36,"substringWithRange:",_3a),_3c=CPNotFound;
if(_39&CPCaseInsensitiveSearch){
_3b=_3b.toLowerCase();
_38=_38.toLowerCase();
}
if(_39&CPBackwardsSearch){
_3c=_3b.lastIndexOf(_38);
if(_39&CPAnchoredSearch&&_3c+_38.length!=_3b.length){
_3c=CPNotFound;
}
}else{
if(_39&CPAnchoredSearch){
_3c=_3b.substr(0,_38.length).indexOf(_38)!=CPNotFound?0:CPNotFound;
}else{
_3c=_3b.indexOf(_38);
}
}
if(_3c==CPNotFound){
return CPMakeRange(CPNotFound,0);
}
return CPMakeRange(_3c+(_3a?_3a.location:0),_38.length);
}
}),new objj_method(sel_getUid("stringByEscapingRegexControlCharacters"),function(_3d,_3e){
with(_3d){
return _3d.replace(_3,"\\$1");
}
}),new objj_method(sel_getUid("stringByReplacingOccurrencesOfString:withString:"),function(_3f,_40,_41,_42){
with(_3f){
return _3f.replace(new RegExp(objj_msgSend(_41,"stringByEscapingRegexControlCharacters"),"g"),_42);
}
}),new objj_method(sel_getUid("stringByReplacingOccurrencesOfString:withString:options:range:"),function(_43,_44,_45,_46,_47,_48){
with(_43){
var _49=substring(0,_48.location),_4a=substr(_48.location,_48.length),end=substring(_48.location+_48.length,_43.length),_45=objj_msgSend(_45,"stringByEscapingRegexControlCharacters"),_4b;
if(_47&CPCaseInsensitiveSearch){
_4b=new RegExp(_45,"gi");
}else{
_4b=new RegExp(_45,"g");
}
return _49+""+_4a.replace(_4b,_46)+""+end;
}
}),new objj_method(sel_getUid("stringByReplacingCharactersInRange:withString:"),function(_4c,_4d,_4e,_4f){
with(_4c){
return ""+substring(0,_4e.location)+_4f+substring(_4e.location+_4e.length,_4c.length);
}
}),new objj_method(sel_getUid("stringByTrimmingWhitespace"),function(_50,_51){
with(_50){
return _50.replace(_4,"");
}
}),new objj_method(sel_getUid("compare:"),function(_52,_53,_54){
with(_52){
return objj_msgSend(_52,"compare:options:",_54,nil);
}
}),new objj_method(sel_getUid("caseInsensitiveCompare:"),function(_55,_56,_57){
with(_55){
return objj_msgSend(_55,"compare:options:",_57,CPCaseInsensitiveSearch);
}
}),new objj_method(sel_getUid("compare:options:"),function(_58,_59,_5a,_5b){
with(_58){
var lhs=_58,rhs=_5a;
if(_5b&CPCaseInsensitiveSearch){
lhs=lhs.toLowerCase();
rhs=rhs.toLowerCase();
}
if(_5b&CPDiacriticInsensitiveSearch){
lhs=lhs.stripDiacritics();
rhs=rhs.stripDiacritics();
}
if(lhs<rhs){
return CPOrderedAscending;
}
if(lhs>rhs){
return CPOrderedDescending;
}
return CPOrderedSame;
}
}),new objj_method(sel_getUid("compare:options:range:"),function(_5c,_5d,_5e,_5f,_60){
with(_5c){
var lhs=objj_msgSend(_5c,"substringWithRange:",_60),rhs=_5e;
return objj_msgSend(lhs,"compare:options:",rhs,_5f);
}
}),new objj_method(sel_getUid("hasPrefix:"),function(_61,_62,_63){
with(_61){
return _63&&_63!=""&&indexOf(_63)==0;
}
}),new objj_method(sel_getUid("hasSuffix:"),function(_64,_65,_66){
with(_64){
return _66&&_66!=""&&length>=_66.length&&lastIndexOf(_66)==(length-_66.length);
}
}),new objj_method(sel_getUid("isEqual:"),function(_67,_68,_69){
with(_67){
if(_67===_69){
return YES;
}
if(!_69||!objj_msgSend(_69,"isKindOfClass:",objj_msgSend(CPString,"class"))){
return NO;
}
return objj_msgSend(_67,"isEqualToString:",_69);
}
}),new objj_method(sel_getUid("isEqualToString:"),function(_6a,_6b,_6c){
with(_6a){
return _6a==String(_6c);
}
}),new objj_method(sel_getUid("UID"),function(_6d,_6e){
with(_6d){
var UID=_1.valueForKey(_6d);
if(!UID){
UID=objj_generateObjectUID();
_1.setValueForKey(_6d,UID);
}
return UID+"";
}
}),new objj_method(sel_getUid("commonPrefixWithString:"),function(_6f,_70,_71){
with(_6f){
return objj_msgSend(_6f,"commonPrefixWithString:options:",_71,0);
}
}),new objj_method(sel_getUid("commonPrefixWithString:options:"),function(_72,_73,_74,_75){
with(_72){
var len=0,lhs=_72,rhs=_74,min=MIN(objj_msgSend(lhs,"length"),objj_msgSend(rhs,"length"));
if(_75&CPCaseInsensitiveSearch){
lhs=objj_msgSend(lhs,"lowercaseString");
rhs=objj_msgSend(rhs,"lowercaseString");
}
for(;len<min;len++){
if(objj_msgSend(lhs,"characterAtIndex:",len)!==objj_msgSend(rhs,"characterAtIndex:",len)){
break;
}
}
return objj_msgSend(_72,"substringToIndex:",len);
}
}),new objj_method(sel_getUid("capitalizedString"),function(_76,_77){
with(_76){
var _78=_76.split(/\b/g),i=0,_79=_78.length;
for(;i<_79;i++){
if(i==0||(/\s$/).test(_78[i-1])){
_78[i]=_78[i].substring(0,1).toUpperCase()+_78[i].substring(1).toLowerCase();
}else{
_78[i]=_78[i].toLowerCase();
}
}
return _78.join("");
}
}),new objj_method(sel_getUid("lowercaseString"),function(_7a,_7b){
with(_7a){
return toLowerCase();
}
}),new objj_method(sel_getUid("uppercaseString"),function(_7c,_7d){
with(_7c){
return toUpperCase();
}
}),new objj_method(sel_getUid("doubleValue"),function(_7e,_7f){
with(_7e){
return parseFloat(_7e,10);
}
}),new objj_method(sel_getUid("boolValue"),function(_80,_81){
with(_80){
var _82=new RegExp("^\\s*[\\+,\\-]?0*");
return RegExp("^[Y,y,t,T,1-9]").test(_80.replace(_82,""));
}
}),new objj_method(sel_getUid("floatValue"),function(_83,_84){
with(_83){
return parseFloat(_83,10);
}
}),new objj_method(sel_getUid("intValue"),function(_85,_86){
with(_85){
return parseInt(_85,10);
}
}),new objj_method(sel_getUid("pathComponents"),function(_87,_88){
with(_87){
if(length===0){
return [""];
}
if(_87==="/"){
return ["/"];
}
var _89=split("/");
if(_89[0]===""){
_89[0]="/";
}
var _8a=_89.length-1;
if(_8a>0){
if(_89[_8a]===""){
_89[_8a]="/";
}
while(_8a--){
while(_89[_8a]===""){
_89.splice(_8a--,1);
}
}
}
return _89;
}
}),new objj_method(sel_getUid("pathExtension"),function(_8b,_8c){
with(_8b){
if(lastIndexOf(".")===CPNotFound){
return "";
}
return substr(lastIndexOf(".")+1);
}
}),new objj_method(sel_getUid("lastPathComponent"),function(_8d,_8e){
with(_8d){
var _8f=objj_msgSend(_8d,"pathComponents"),_90=_8f.length-1,_91=_8f[_90];
return _90>0&&_91==="/"?_8f[_90-1]:_91;
}
}),new objj_method(sel_getUid("stringByAppendingPathComponent:"),function(_92,_93,_94){
with(_92){
var _95=objj_msgSend(_92,"pathComponents"),_96=_94&&_94!=="/"?objj_msgSend(_94,"pathComponents"):[];
return objj_msgSend(CPString,"pathWithComponents:",_95.concat(_96));
}
}),new objj_method(sel_getUid("stringByAppendingPathExtension:"),function(_97,_98,ext){
with(_97){
if(ext.indexOf("/")>=0||length===0||_97==="/"){
return _97;
}
var _99=objj_msgSend(_97,"pathComponents"),_9a=_99.length-1;
if(_9a>0&&_99[_9a]==="/"){
_99.splice(_9a--,1);
}
_99[_9a]=_99[_9a]+"."+ext;
return objj_msgSend(CPString,"pathWithComponents:",_99);
}
}),new objj_method(sel_getUid("stringByDeletingLastPathComponent"),function(_9b,_9c){
with(_9b){
if(length===0){
return "";
}else{
if(_9b==="/"){
return "/";
}
}
var _9d=objj_msgSend(_9b,"pathComponents"),_9e=_9d.length-1;
if(_9d[_9e]==="/"){
_9e--;
}
_9d.splice(_9e,_9d.length-_9e);
return objj_msgSend(CPString,"pathWithComponents:",_9d);
}
}),new objj_method(sel_getUid("stringByDeletingPathExtension"),function(_9f,_a0){
with(_9f){
var _a1=objj_msgSend(_9f,"pathExtension");
if(_a1===""){
return _9f;
}else{
if(lastIndexOf(".")<1){
return _9f;
}
}
return substr(0,objj_msgSend(_9f,"length")-(_a1.length+1));
}
}),new objj_method(sel_getUid("stringByStandardizingPath"),function(_a2,_a3){
with(_a2){
return objj_msgSend(objj_msgSend(CPURL,"URLWithString:",_a2),"absoluteString");
}
})]);
class_addMethods(_6,[new objj_method(sel_getUid("alloc"),function(_a4,_a5){
with(_a4){
if(objj_msgSend(_a4,"class")!==CPString){
return objj_msgSendSuper({receiver:_a4,super_class:objj_getMetaClass("CPString").super_class},"alloc");
}
return new String;
}
}),new objj_method(sel_getUid("string"),function(_a6,_a7){
with(_a6){
return objj_msgSend(objj_msgSend(_a6,"alloc"),"init");
}
}),new objj_method(sel_getUid("stringWithHash:"),function(_a8,_a9,_aa){
with(_a8){
var _ab=parseInt(_aa,10).toString(16);
return "000000".substring(0,MAX(6-_ab.length,0))+_ab;
}
}),new objj_method(sel_getUid("stringWithString:"),function(_ac,_ad,_ae){
with(_ac){
if(!_ae){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"stringWithString: the string can't be 'nil'");
}
return objj_msgSend(objj_msgSend(_ac,"alloc"),"initWithString:",_ae);
}
}),new objj_method(sel_getUid("stringWithFormat:"),function(_af,_b0,_b1){
with(_af){
if(!_b1){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"initWithFormat: the format can't be 'nil'");
}
return ObjectiveJ.sprintf.apply(this,Array.prototype.slice.call(arguments,2));
}
}),new objj_method(sel_getUid("pathWithComponents:"),function(_b2,_b3,_b4){
with(_b2){
var _b5=_b4.length,_b6="",i=-1,_b7=true,_b8=false;
while(++i<_b5){
var _b9=_b4[i],_ba=_b9.length-1;
if(_ba>=0&&(_b9!=="/"||_b7)){
if(_ba>0&&_b9.indexOf("/",_ba)===_ba){
_b9=_b9.substring(0,_ba);
}
if(_b7){
if(_b9==="/"){
_b8=true;
}
_b7=false;
}else{
if(!_b8){
_b6+="/";
}else{
_b8=false;
}
}
_b6+=_b9;
}
}
return _b6;
}
})]);
var _5=objj_getClass("CPString");
if(!_5){
throw new SyntaxError("*** Could not find definition for class \"CPString\"");
}
var _6=_5.isa;
class_addMethods(_5,[new objj_method(sel_getUid("objectFromJSON"),function(_bb,_bc){
with(_bb){
return JSON.parse(_bb);
}
})]);
class_addMethods(_6,[new objj_method(sel_getUid("JSONFromObject:"),function(_bd,_be,_bf){
with(_bd){
return JSON.stringify(_bf);
}
})]);
var _5=objj_getClass("CPString");
if(!_5){
throw new SyntaxError("*** Could not find definition for class \"CPString\"");
}
var _6=_5.isa;
class_addMethods(_6,[new objj_method(sel_getUid("UUID"),function(_c0,_c1){
with(_c0){
var g="",i=0;
for(;i<32;i++){
g+=FLOOR(RAND()*15).toString(15);
}
return g;
}
})]);
var _c2=[[192,198],[224,230],[231,231],[232,235],[236,239],[242,246],[249,252]],_c3=[65,97,99,101,105,111,117];
String.prototype.stripDiacritics=function(){
var _c4="";
for(var _c5=0;_c5<this.length;_c5++){
var _c6=this.charCodeAt(_c5);
for(var i=0;i<_c2.length;i++){
var _c7=_c2[i];
if(_c6>=_c7[0]&&_c6<=_c7[_c7.length-1]){
_c6=_c3[i];
break;
}
}
_c4+=String.fromCharCode(_c6);
}
return _c4;
};
String.prototype.isa=CPString;
p;13;CPException.jt;4359;@STATIC;1.0;i;9;CPCoder.ji;10;CPObject.ji;10;CPString.jt;4297;
objj_executeFile("CPCoder.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPString.j",YES);
CPInvalidArgumentException="CPInvalidArgumentException";
CPUnsupportedMethodException="CPUnsupportedMethodException";
CPRangeException="CPRangeException";
CPInternalInconsistencyException="CPInternalInconsistencyException";
var _1=objj_allocateClassPair(CPObject,"CPException"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_userInfo")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithName:reason:userInfo:"),function(_3,_4,_5,_6,_7){
with(_3){
_3=objj_msgSendSuper({receiver:_3,super_class:objj_getClass("CPException").super_class},"init");
if(_3){
name=_5;
message=_6;
_userInfo=_7;
}
return _3;
}
}),new objj_method(sel_getUid("name"),function(_8,_9){
with(_8){
return name;
}
}),new objj_method(sel_getUid("reason"),function(_a,_b){
with(_a){
return message;
}
}),new objj_method(sel_getUid("userInfo"),function(_c,_d){
with(_c){
return _userInfo;
}
}),new objj_method(sel_getUid("description"),function(_e,_f){
with(_e){
return message;
}
}),new objj_method(sel_getUid("raise"),function(_10,_11){
with(_10){
throw _10;
}
}),new objj_method(sel_getUid("isEqual:"),function(_12,_13,_14){
with(_12){
if(!_14||!_14.isa){
return NO;
}
return objj_msgSend(_14,"isKindOfClass:",CPException)&&name===objj_msgSend(_14,"name")&&message===objj_msgSend(_14,"message")&&(_userInfo===objj_msgSend(_14,"userInfo")||(objj_msgSend(_userInfo,"isEqual:",objj_msgSend(_14,"userInfo"))));
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("alloc"),function(_15,_16){
with(_15){
var _17=new Error();
_17.isa=objj_msgSend(_15,"class");
return _17;
}
}),new objj_method(sel_getUid("raise:reason:"),function(_18,_19,_1a,_1b){
with(_18){
objj_msgSend(objj_msgSend(_18,"exceptionWithName:reason:userInfo:",_1a,_1b,nil),"raise");
}
}),new objj_method(sel_getUid("exceptionWithName:reason:userInfo:"),function(_1c,_1d,_1e,_1f,_20){
with(_1c){
return objj_msgSend(objj_msgSend(_1c,"alloc"),"initWithName:reason:userInfo:",_1e,_1f,_20);
}
})]);
var _1=objj_getClass("CPException");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPException\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("copy"),function(_21,_22){
with(_21){
return objj_msgSend(objj_msgSend(_21,"class"),"exceptionWithName:reason:userInfo:",name,message,_userInfo);
}
})]);
var _23="CPExceptionNameKey",_24="CPExceptionReasonKey",_25="CPExceptionUserInfoKey";
var _1=objj_getClass("CPException");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPException\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("initWithCoder:"),function(_26,_27,_28){
with(_26){
if(_26=objj_msgSendSuper({receiver:_26,super_class:objj_getClass("CPException").super_class},"init")){
name=objj_msgSend(_28,"decodeObjectForKey:",_23);
message=objj_msgSend(_28,"decodeObjectForKey:",_24);
_userInfo=objj_msgSend(_28,"decodeObjectForKey:",_25);
}
return _26;
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_29,_2a,_2b){
with(_29){
objj_msgSend(_2b,"encodeObject:forKey:",name,_23);
objj_msgSend(_2b,"encodeObject:forKey:",message,_24);
objj_msgSend(_2b,"encodeObject:forKey:",_userInfo,_25);
}
})]);
Error.prototype.isa=CPException;
Error.prototype._userInfo=null;
objj_msgSend(CPException,"initialize");
_CPRaiseInvalidAbstractInvocation=function(_2c,_2d){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"*** -"+sel_getName(_2d)+" cannot be sent to an abstract object of class "+objj_msgSend(_2c,"className")+": Create a concrete instance!");
};
_CPRaiseInvalidArgumentException=function(_2e,_2f,_30){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,((class_isMetaClass(_2e.isa)?"+":"-")+"["+objj_msgSend(_2e,"className")+" "+_2f+"]: ")+_30);
};
_CPRaiseRangeException=function(_31,_32,_33,_34){
objj_msgSend(CPException,"raise:reason:",CPRangeException,((class_isMetaClass(_31.isa)?"+":"-")+"["+objj_msgSend(_31,"className")+" "+_32+"]: ")+"index ("+_33+") beyond bounds ("+_34+")");
};
_CPReportLenientDeprecation=function(_35,_36,_37){
CPLog.warn("["+CPStringFromClass(_35)+" "+CPStringFromSelector(_36)+"] is deprecated, using "+CPStringFromSelector(_37)+" instead.");
};
p;9;CPCoder.jt;1914;@STATIC;1.0;i;13;CPException.ji;10;CPObject.jt;1862;
objj_executeFile("CPException.j",YES);
objj_executeFile("CPObject.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPCoder"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("allowsKeyedCoding"),function(_3,_4){
with(_3){
return NO;
}
}),new objj_method(sel_getUid("encodeValueOfObjCType:at:"),function(_5,_6,_7,_8){
with(_5){
_CPRaiseInvalidAbstractInvocation(_5,_6);
}
}),new objj_method(sel_getUid("encodeDataObject:"),function(_9,_a,_b){
with(_9){
_CPRaiseInvalidAbstractInvocation(_9,_a);
}
}),new objj_method(sel_getUid("encodeObject:"),function(_c,_d,_e){
with(_c){
}
}),new objj_method(sel_getUid("encodePoint:"),function(_f,_10,_11){
with(_f){
objj_msgSend(_f,"encodeNumber:",_11.x);
objj_msgSend(_f,"encodeNumber:",_11.y);
}
}),new objj_method(sel_getUid("encodeRect:"),function(_12,_13,_14){
with(_12){
objj_msgSend(_12,"encodePoint:",_14.origin);
objj_msgSend(_12,"encodeSize:",_14.size);
}
}),new objj_method(sel_getUid("encodeSize:"),function(_15,_16,_17){
with(_15){
objj_msgSend(_15,"encodeNumber:",_17.width);
objj_msgSend(_15,"encodeNumber:",_17.height);
}
}),new objj_method(sel_getUid("encodePropertyList:"),function(_18,_19,_1a){
with(_18){
}
}),new objj_method(sel_getUid("encodeRootObject:"),function(_1b,_1c,_1d){
with(_1b){
objj_msgSend(_1b,"encodeObject:",_1d);
}
}),new objj_method(sel_getUid("encodeBycopyObject:"),function(_1e,_1f,_20){
with(_1e){
objj_msgSend(_1e,"encodeObject:",_20);
}
}),new objj_method(sel_getUid("encodeConditionalObject:"),function(_21,_22,_23){
with(_21){
objj_msgSend(_21,"encodeObject:",_23);
}
})]);
var _1=objj_getClass("CPObject");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPObject\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("awakeAfterUsingCoder:"),function(_24,_25,_26){
with(_24){
return _24;
}
})]);
p;15;CPObjJRuntime.jt;348;@STATIC;1.0;t;330;
CPStringFromSelector=function(_1){
return sel_getName(_1);
};
CPSelectorFromString=function(_2){
return sel_registerName(_2);
};
CPClassFromString=function(_3){
return objj_getClass(_3);
};
CPStringFromClass=function(_4){
return class_getName(_4);
};
CPOrderedAscending=-1;
CPOrderedSame=0;
CPOrderedDescending=1;
CPNotFound=-1;
p;9;CPRange.jt;1201;@STATIC;1.0;t;1182;
CPMakeRange=function(_1,_2){
return {location:(_1),length:_2};
};
CPMakeRangeCopy=function(_3){
return {location:(_3).location,length:(_3).length};
};
CPEmptyRange=function(_4){
return ((_4).length===0);
};
CPMaxRange=function(_5){
return ((_5).location+(_5).length);
};
CPEqualRanges=function(_6,_7){
return ((_6.location===_7.location)&&(_6.length===_7.length));
};
CPLocationInRange=function(_8,_9){
return (((_8)>=(_9).location)&&((_8)<((_9).location+(_9).length)));
};
CPUnionRange=function(_a,_b){
var _c=MIN(_a.location,_b.location);
return CPMakeRange(_c,MAX(CPMaxRange(_a),CPMaxRange(_b))-_c);
};
CPIntersectionRange=function(_d,_e){
if(CPMaxRange(_d)<_e.location||CPMaxRange(_e)<_d.location){
return CPMakeRange(0,0);
}
var _f=MAX(_d.location,_e.location);
return CPMakeRange(_f,MIN(CPMaxRange(_d),CPMaxRange(_e))-_f);
};
CPRangeInRange=function(_10,_11){
return (_10.location<=_11.location&&CPMaxRange(_10)>=CPMaxRange(_11));
};
CPStringFromRange=function(_12){
return "{"+_12.location+", "+_12.length+"}";
};
CPRangeFromString=function(_13){
var _14=_13.indexOf(",");
return {location:parseInt(_13.substr(1,_14-1)),length:parseInt(_13.substring(_14+1,_13.length))};
};
p;18;CPSortDescriptor.jt;3227;@STATIC;1.0;i;10;CPObject.ji;15;CPObjJRuntime.ji;10;CPString.jt;3158;
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPObjJRuntime.j",YES);
objj_executeFile("CPString.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPSortDescriptor"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_key"),new objj_ivar("_selector"),new objj_ivar("_ascending")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithKey:ascending:"),function(_3,_4,_5,_6){
with(_3){
return objj_msgSend(_3,"initWithKey:ascending:selector:",_5,_6,sel_getUid("compare:"));
}
}),new objj_method(sel_getUid("initWithKey:ascending:selector:"),function(_7,_8,_9,_a,_b){
with(_7){
_7=objj_msgSendSuper({receiver:_7,super_class:objj_getClass("CPSortDescriptor").super_class},"init");
if(_7){
_key=_9;
_ascending=_a;
_selector=_b;
}
return _7;
}
}),new objj_method(sel_getUid("ascending"),function(_c,_d){
with(_c){
return _ascending;
}
}),new objj_method(sel_getUid("key"),function(_e,_f){
with(_e){
return _key;
}
}),new objj_method(sel_getUid("selector"),function(_10,_11){
with(_10){
return _selector;
}
}),new objj_method(sel_getUid("compareObject:withObject:"),function(_12,_13,_14,_15){
with(_12){
return (_ascending?1:-1)*objj_msgSend(objj_msgSend(_14,"valueForKeyPath:",_key),"performSelector:withObject:",_selector,objj_msgSend(_15,"valueForKeyPath:",_key));
}
}),new objj_method(sel_getUid("reversedSortDescriptor"),function(_16,_17){
with(_16){
return objj_msgSend(objj_msgSend(objj_msgSend(_16,"class"),"alloc"),"initWithKey:ascending:selector:",_key,!_ascending,_selector);
}
}),new objj_method(sel_getUid("description"),function(_18,_19){
with(_18){
return objj_msgSend(CPString,"stringWithFormat:","(%@, %@, %@)",objj_msgSend(_18,"key"),objj_msgSend(_18,"ascending")?"ascending":"descending",CPStringFromSelector(objj_msgSend(_18,"selector")));
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("sortDescriptorWithKey:ascending:"),function(_1a,_1b,_1c,_1d){
with(_1a){
return objj_msgSend(objj_msgSend(_1a,"alloc"),"initWithKey:ascending:",_1c,_1d);
}
}),new objj_method(sel_getUid("sortDescriptorWithKey:ascending:selector:"),function(_1e,_1f,_20,_21,_22){
with(_1e){
return objj_msgSend(objj_msgSend(_1e,"alloc"),"initWithKey:ascending:selector:",_20,_21,_22);
}
})]);
var _23="CPSortDescriptorKeyKey",_24="CPSortDescriptorAscendingKey",_25="CPSortDescriptorSelectorKey";
var _1=objj_getClass("CPSortDescriptor");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPSortDescriptor\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("initWithCoder:"),function(_26,_27,_28){
with(_26){
if(_26=objj_msgSendSuper({receiver:_26,super_class:objj_getClass("CPSortDescriptor").super_class},"init")){
_key=objj_msgSend(_28,"decodeObjectForKey:",_23);
_ascending=objj_msgSend(_28,"decodeBoolForKey:",_24);
_selector=CPSelectorFromString(objj_msgSend(_28,"decodeObjectForKey:",_25));
}
return _26;
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_29,_2a,_2b){
with(_29){
objj_msgSend(_2b,"encodeObject:forKey:",_key,_23);
objj_msgSend(_2b,"encodeBool:forKey:",_ascending,_24);
objj_msgSend(_2b,"encodeObject:forKey:",CPStringFromSelector(_selector),_25);
}
})]);
p;7;CPURL.jt;6448;@STATIC;1.0;i;10;CPObject.ji;10;CPString.jt;6399;
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPString.j",YES);
CPURLNameKey="CPURLNameKey";
CPURLLocalizedNameKey="CPURLLocalizedNameKey";
CPURLIsRegularFileKey="CPURLIsRegularFileKey";
CPURLIsDirectoryKey="CPURLIsDirectoryKey";
CPURLIsSymbolicLinkKey="CPURLIsSymbolicLinkKey";
CPURLIsVolumeKey="CPURLIsVolumeKey";
CPURLIsPackageKey="CPURLIsPackageKey";
CPURLIsSystemImmutableKey="CPURLIsSystemImmutableKey";
CPURLIsUserImmutableKey="CPURLIsUserImmutableKey";
CPURLIsHiddenKey="CPURLIsHiddenKey";
CPURLHasHiddenExtensionKey="CPURLHasHiddenExtensionKey";
CPURLCreationDateKey="CPURLCreationDateKey";
CPURLContentAccessDateKey="CPURLContentAccessDateKey";
CPURLContentModificationDateKey="CPURLContentModificationDateKey";
CPURLAttributeModificationDateKey="CPURLAttributeModificationDateKey";
CPURLLinkCountKey="CPURLLinkCountKey";
CPURLParentDirectoryURLKey="CPURLParentDirectoryURLKey";
CPURLVolumeURLKey="CPURLTypeIdentifierKey";
CPURLTypeIdentifierKey="CPURLTypeIdentifierKey";
CPURLLocalizedTypeDescriptionKey="CPURLLocalizedTypeDescriptionKey";
CPURLLabelNumberKey="CPURLLabelNumberKey";
CPURLLabelColorKey="CPURLLabelColorKey";
CPURLLocalizedLabelKey="CPURLLocalizedLabelKey";
CPURLEffectiveIconKey="CPURLEffectiveIconKey";
CPURLCustomIconKey="CPURLCustomIconKey";
var _1=objj_allocateClassPair(CPObject,"CPURL"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("init"),function(_3,_4){
with(_3){
return nil;
}
}),new objj_method(sel_getUid("initWithScheme:host:path:"),function(_5,_6,_7,_8,_9){
with(_5){
var _a=(_7?_7+":":"")+(_8?_8+"//":"")+(_9||"");
return objj_msgSend(_5,"initWithString:",_a);
}
}),new objj_method(sel_getUid("initWithString:"),function(_b,_c,_d){
with(_b){
return objj_msgSend(_b,"initWithString:relativeToURL:",_d,nil);
}
}),new objj_method(sel_getUid("initWithString:relativeToURL:"),function(_e,_f,_10,_11){
with(_e){
var _12=new CFURL(_10,_11);
_12.isa=objj_msgSend(_e,"class");
return _12;
}
}),new objj_method(sel_getUid("absoluteURL"),function(_13,_14){
with(_13){
return _13.absoluteURL();
}
}),new objj_method(sel_getUid("baseURL"),function(_15,_16){
with(_15){
return _15.baseURL();
}
}),new objj_method(sel_getUid("absoluteString"),function(_17,_18){
with(_17){
return _17.absoluteString();
}
}),new objj_method(sel_getUid("relativeString"),function(_19,_1a){
with(_19){
return _19.string();
}
}),new objj_method(sel_getUid("path"),function(_1b,_1c){
with(_1b){
return objj_msgSend(_1b,"absoluteURL").path();
}
}),new objj_method(sel_getUid("pathComponents"),function(_1d,_1e){
with(_1d){
var _1f=_1d.pathComponents();
return objj_msgSend(_1f,"copy");
}
}),new objj_method(sel_getUid("relativePath"),function(_20,_21){
with(_20){
return _20.path();
}
}),new objj_method(sel_getUid("scheme"),function(_22,_23){
with(_22){
return _22.scheme();
}
}),new objj_method(sel_getUid("user"),function(_24,_25){
with(_24){
return objj_msgSend(_24,"absoluteURL").user();
}
}),new objj_method(sel_getUid("password"),function(_26,_27){
with(_26){
return objj_msgSend(_26,"absoluteURL").password();
}
}),new objj_method(sel_getUid("host"),function(_28,_29){
with(_28){
return objj_msgSend(_28,"absoluteURL").domain();
}
}),new objj_method(sel_getUid("port"),function(_2a,_2b){
with(_2a){
var _2c=objj_msgSend(_2a,"absoluteURL").portNumber();
if(_2c===-1){
return nil;
}
return _2c;
}
}),new objj_method(sel_getUid("parameterString"),function(_2d,_2e){
with(_2d){
return _2d.queryString();
}
}),new objj_method(sel_getUid("fragment"),function(_2f,_30){
with(_2f){
return _2f.fragment();
}
}),new objj_method(sel_getUid("isEqual:"),function(_31,_32,_33){
with(_31){
if(_31===_33){
return YES;
}
if(!_33||!objj_msgSend(_33,"isKindOfClass:",objj_msgSend(CPURL,"class"))){
return NO;
}
return objj_msgSend(_31,"isEqualToURL:",_33);
}
}),new objj_method(sel_getUid("isEqualToURL:"),function(_34,_35,_36){
with(_34){
if(_34===_36){
return YES;
}
return objj_msgSend(objj_msgSend(_34,"absoluteString"),"isEqual:",objj_msgSend(_36,"absoluteString"));
}
}),new objj_method(sel_getUid("lastPathComponent"),function(_37,_38){
with(_37){
return objj_msgSend(_37,"absoluteURL").lastPathComponent();
}
}),new objj_method(sel_getUid("pathExtension"),function(_39,_3a){
with(_39){
return _39.pathExtension();
}
}),new objj_method(sel_getUid("URLByDeletingLastPathComponent"),function(_3b,_3c){
with(_3b){
var _3d=_3b.createCopyDeletingLastPathComponent();
_3d.isa=objj_msgSend(_3b,"class");
return _3d;
}
}),new objj_method(sel_getUid("standardizedURL"),function(_3e,_3f){
with(_3e){
return _3e.standardizedURL();
}
}),new objj_method(sel_getUid("isFileURL"),function(_40,_41){
with(_40){
return objj_msgSend(_40,"scheme")==="file";
}
}),new objj_method(sel_getUid("description"),function(_42,_43){
with(_42){
return objj_msgSend(_42,"absoluteString");
}
}),new objj_method(sel_getUid("resourceValueForKey:"),function(_44,_45,_46){
with(_44){
return _44.resourcePropertyForKey(_46);
}
}),new objj_method(sel_getUid("setResourceValue:forKey:"),function(_47,_48,_49,_4a){
with(_47){
return _47.setResourcePropertyForKey(_4a,_49);
}
}),new objj_method(sel_getUid("staticResourceData"),function(_4b,_4c){
with(_4b){
return _4b.staticResourceData();
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("alloc"),function(_4d,_4e){
with(_4d){
var _4f=new CFURL();
_4f.isa=objj_msgSend(_4d,"class");
return _4f;
}
}),new objj_method(sel_getUid("URLWithString:"),function(_50,_51,_52){
with(_50){
return objj_msgSend(objj_msgSend(_50,"alloc"),"initWithString:",_52);
}
}),new objj_method(sel_getUid("URLWithString:relativeToURL:"),function(_53,_54,_55,_56){
with(_53){
return objj_msgSend(objj_msgSend(_53,"alloc"),"initWithString:relativeToURL:",_55,_56);
}
})]);
var _57="CPURLURLStringKey",_58="CPURLBaseURLKey";
var _1=objj_getClass("CPURL");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPURL\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("initWithCoder:"),function(_59,_5a,_5b){
with(_59){
return objj_msgSend(_59,"initWithString:relativeToURL:",objj_msgSend(_5b,"decodeObjectForKey:",_57),objj_msgSend(_5b,"decodeObjectForKey:",_58));
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_5c,_5d,_5e){
with(_5c){
objj_msgSend(_5e,"encodeObject:forKey:",_baseURL,_58);
objj_msgSend(_5e,"encodeObject:forKey:",_string,_57);
}
})]);
CFURL.prototype.isa=objj_msgSend(CPURL,"class");
p;9;CPValue.jt;1690;@STATIC;1.0;i;9;CPCoder.ji;10;CPObject.jt;1643;
objj_executeFile("CPCoder.j",YES);
objj_executeFile("CPObject.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPValue"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_JSObject")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithJSObject:"),function(_3,_4,_5){
with(_3){
_3=objj_msgSendSuper({receiver:_3,super_class:objj_getClass("CPValue").super_class},"init");
if(_3){
_JSObject=_5;
}
return _3;
}
}),new objj_method(sel_getUid("JSObject"),function(_6,_7){
with(_6){
return _JSObject;
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("valueWithJSObject:"),function(_8,_9,_a){
with(_8){
return objj_msgSend(objj_msgSend(_8,"alloc"),"initWithJSObject:",_a);
}
})]);
var _b="CPValueValueKey";
var _1=objj_getClass("CPValue");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPValue\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("initWithCoder:"),function(_c,_d,_e){
with(_c){
_c=objj_msgSendSuper({receiver:_c,super_class:objj_getClass("CPValue").super_class},"init");
if(_c){
_JSObject=JSON.parse(objj_msgSend(_e,"decodeObjectForKey:",_b));
}
return _c;
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_f,_10,_11){
with(_f){
objj_msgSend(_11,"encodeObject:forKey:",JSON.stringify(_JSObject),_b);
}
})]);
CPJSObjectCreateJSON=function(_12){
CPLog.warn("CPJSObjectCreateJSON deprecated, use JSON.stringify() or CPString's objectFromJSON");
return JSON.stringify(_12);
};
CPJSObjectCreateWithJSON=function(_13){
CPLog.warn("CPJSObjectCreateWithJSON deprecated, use JSON.parse() or CPString's JSONFromObject");
return JSON.parse(_13);
};
p;10;CPBundle.jt;4159;@STATIC;1.0;i;14;CPDictionary.ji;16;CPNotification.ji;22;CPNotificationCenter.ji;10;CPObject.jt;4058;
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPNotification.j",YES);
objj_executeFile("CPNotificationCenter.j",YES);
objj_executeFile("CPObject.j",YES);
CPBundleDidLoadNotification="CPBundleDidLoadNotification";
var _1={};
var _2=objj_allocateClassPair(CPObject,"CPBundle"),_3=_2.isa;
class_addIvars(_2,[new objj_ivar("_bundle"),new objj_ivar("_delegate")]);
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("initWithURL:"),function(_4,_5,_6){
with(_4){
_6=new CFURL(_6);
var _7=_6.absoluteString(),_8=_1[_7];
if(_8){
return _8;
}
_4=objj_msgSendSuper({receiver:_4,super_class:objj_getClass("CPBundle").super_class},"init");
if(_4){
_bundle=new CFBundle(_6);
_1[_7]=_4;
}
return _4;
}
}),new objj_method(sel_getUid("initWithPath:"),function(_9,_a,_b){
with(_9){
return objj_msgSend(_9,"initWithURL:",_b);
}
}),new objj_method(sel_getUid("classNamed:"),function(_c,_d,_e){
with(_c){
}
}),new objj_method(sel_getUid("bundleURL"),function(_f,_10){
with(_f){
return _bundle.bundleURL();
}
}),new objj_method(sel_getUid("bundlePath"),function(_11,_12){
with(_11){
return objj_msgSend(objj_msgSend(_11,"bundleURL"),"path");
}
}),new objj_method(sel_getUid("resourcePath"),function(_13,_14){
with(_13){
return objj_msgSend(objj_msgSend(_13,"resourceURL"),"path");
}
}),new objj_method(sel_getUid("resourceURL"),function(_15,_16){
with(_15){
return _bundle.resourcesDirectoryURL();
}
}),new objj_method(sel_getUid("principalClass"),function(_17,_18){
with(_17){
var _19=objj_msgSend(_17,"objectForInfoDictionaryKey:","CPPrincipalClass");
return _19?CPClassFromString(_19):Nil;
}
}),new objj_method(sel_getUid("bundleIdentifier"),function(_1a,_1b){
with(_1a){
return objj_msgSend(_1a,"objectForInfoDictionaryKey:","CPBundleIdentifier");
}
}),new objj_method(sel_getUid("isLoaded"),function(_1c,_1d){
with(_1c){
return _bundle.isLoaded();
}
}),new objj_method(sel_getUid("pathForResource:"),function(_1e,_1f,_20){
with(_1e){
return _bundle.pathForResource(_20);
}
}),new objj_method(sel_getUid("infoDictionary"),function(_21,_22){
with(_21){
return _bundle.infoDictionary();
}
}),new objj_method(sel_getUid("objectForInfoDictionaryKey:"),function(_23,_24,_25){
with(_23){
return _bundle.valueForInfoDictionaryKey(_25);
}
}),new objj_method(sel_getUid("loadWithDelegate:"),function(_26,_27,_28){
with(_26){
_delegate=_28;
_bundle.addEventListener("load",function(){
objj_msgSend(_delegate,"bundleDidFinishLoading:",_26);
objj_msgSend(objj_msgSend(CPNotificationCenter,"defaultCenter"),"postNotificationName:object:userInfo:",CPBundleDidLoadNotification,_26,nil);
});
_bundle.addEventListener("error",function(){
CPLog.error("Could not find bundle: "+_26);
});
_bundle.load(YES);
}
}),new objj_method(sel_getUid("staticResourceURLs"),function(_29,_2a){
with(_29){
var _2b=[],_2c=_bundle.staticResources(),_2d=0,_2e=objj_msgSend(_2c,"count");
for(;_2d<_2e;++_2d){
objj_msgSend(_2b,"addObject:",_2c[_2d].URL());
}
return _2b;
}
}),new objj_method(sel_getUid("environments"),function(_2f,_30){
with(_2f){
return _bundle.environments();
}
}),new objj_method(sel_getUid("mostEligibleEnvironment"),function(_31,_32){
with(_31){
return _bundle.mostEligibleEnvironment();
}
}),new objj_method(sel_getUid("description"),function(_33,_34){
with(_33){
return objj_msgSendSuper({receiver:_33,super_class:objj_getClass("CPBundle").super_class},"description")+"("+objj_msgSend(_33,"bundlePath")+")";
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("bundleWithURL:"),function(_35,_36,_37){
with(_35){
return objj_msgSend(objj_msgSend(_35,"alloc"),"initWithURL:",_37);
}
}),new objj_method(sel_getUid("bundleWithPath:"),function(_38,_39,_3a){
with(_38){
return objj_msgSend(_38,"bundleWithURL:",_3a);
}
}),new objj_method(sel_getUid("bundleForClass:"),function(_3b,_3c,_3d){
with(_3b){
return objj_msgSend(_3b,"bundleWithURL:",CFBundle.bundleForClass(_3d).bundleURL());
}
}),new objj_method(sel_getUid("mainBundle"),function(_3e,_3f){
with(_3e){
return objj_msgSend(CPBundle,"bundleWithPath:",CFBundle.mainBundle().bundleURL());
}
})]);
p;14;CPDictionary.jt;10365;@STATIC;1.0;i;9;CPArray.ji;14;CPEnumerator.ji;13;CPException.ji;8;CPNull.ji;10;CPObject.jt;10268;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPEnumerator.j",YES);
objj_executeFile("CPException.j",YES);
objj_executeFile("CPNull.j",YES);
objj_executeFile("CPObject.j",YES);
var _1=objj_allocateClassPair(CPEnumerator,"_CPDictionaryValueEnumerator"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_keyEnumerator"),new objj_ivar("_dictionary")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithDictionary:"),function(_3,_4,_5){
with(_3){
_3=objj_msgSendSuper({receiver:_3,super_class:objj_getClass("_CPDictionaryValueEnumerator").super_class},"init");
if(_3){
_keyEnumerator=objj_msgSend(_5,"keyEnumerator");
_dictionary=_5;
}
return _3;
}
}),new objj_method(sel_getUid("nextObject"),function(_6,_7){
with(_6){
var _8=objj_msgSend(_keyEnumerator,"nextObject");
if(_8===nil){
return nil;
}
return objj_msgSend(_dictionary,"objectForKey:",_8);
}
})]);
var _1=objj_allocateClassPair(CPObject,"CPDictionary"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithDictionary:"),function(_9,_a,_b){
with(_9){
var _c="",_d=objj_msgSend(objj_msgSend(CPDictionary,"alloc"),"init");
for(_c in _b._buckets){
objj_msgSend(_d,"setObject:forKey:",objj_msgSend(_b,"objectForKey:",_c),_c);
}
return _d;
}
}),new objj_method(sel_getUid("initWithObjects:forKeys:"),function(_e,_f,_10,_11){
with(_e){
_e=objj_msgSendSuper({receiver:_e,super_class:objj_getClass("CPDictionary").super_class},"init");
if(objj_msgSend(_10,"count")!=objj_msgSend(_11,"count")){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"Counts are different.("+objj_msgSend(_10,"count")+"!="+objj_msgSend(_11,"count")+")");
}
if(_e){
var i=objj_msgSend(_11,"count");
while(i--){
objj_msgSend(_e,"setObject:forKey:",_10[i],_11[i]);
}
}
return _e;
}
}),new objj_method(sel_getUid("initWithObjectsAndKeys:"),function(_12,_13,_14){
with(_12){
var _15=arguments.length;
if(_15%2!==0){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"Key-value count is mismatched. ("+_15+" arguments passed)");
}
_12=objj_msgSendSuper({receiver:_12,super_class:objj_getClass("CPDictionary").super_class},"init");
if(_12){
var _16=2;
for(;_16<_15;_16+=2){
var _17=arguments[_16];
if(_17===nil){
break;
}
objj_msgSend(_12,"setObject:forKey:",_17,arguments[_16+1]);
}
}
return _12;
}
}),new objj_method(sel_getUid("copy"),function(_18,_19){
with(_18){
return objj_msgSend(CPDictionary,"dictionaryWithDictionary:",_18);
}
}),new objj_method(sel_getUid("count"),function(_1a,_1b){
with(_1a){
return _count;
}
}),new objj_method(sel_getUid("allKeys"),function(_1c,_1d){
with(_1c){
return objj_msgSend(_keys,"copy");
}
}),new objj_method(sel_getUid("allValues"),function(_1e,_1f){
with(_1e){
var _20=_keys.length,_21=[];
while(_20--){
_21.push(_1e.valueForKey(_keys[_20]));
}
return _21;
}
}),new objj_method(sel_getUid("allKeysForObject:"),function(_22,_23,_24){
with(_22){
var _25=_keys.length,_26=0,_27=[],key=nil,_28=nil;
for(;_26<_25;++_26){
key=_keys[_26];
_28=_buckets[key];
if(_28.isa&&_24&&_24.isa&&objj_msgSend(_28,"respondsToSelector:",sel_getUid("isEqual:"))&&objj_msgSend(_28,"isEqual:",_24)){
_27.push(key);
}else{
if(_28===_24){
_27.push(key);
}
}
}
return _27;
}
}),new objj_method(sel_getUid("keysOfEntriesPassingTest:"),function(_29,_2a,_2b){
with(_29){
return objj_msgSend(_29,"keysOfEntriesWithOptions:passingTest:",CPEnumerationNormal,_2b);
}
}),new objj_method(sel_getUid("keysOfEntriesWithOptions:passingTest:"),function(_2c,_2d,_2e,_2f){
with(_2c){
if(_2e&CPEnumerationReverse){
var _30=objj_msgSend(_keys,"count")-1,_31=-1,_32=-1;
}else{
var _30=0,_31=objj_msgSend(_keys,"count"),_32=1;
}
var _33=[],key=nil,_34=nil,_35=NO,_36=function(_37){
if(arguments.length){
return _35=_37;
}
return _35;
};
for(;_30!==_31;_30+=_32){
key=_keys[_30];
_34=_buckets[key];
if(_2f(key,_34,_36)){
_33.push(key);
}
if(_35){
break;
}
}
return _33;
}
}),new objj_method(sel_getUid("keysSortedByValueUsingComparator:"),function(_38,_39,_3a){
with(_38){
return objj_msgSend(objj_msgSend(_38,"allKeys"),"sortedArrayUsingFunction:",function(a,b){
a=objj_msgSend(_38,"objectForKey:",a);
b=objj_msgSend(_38,"objectForKey:",b);
return _3a(a,b);
});
}
}),new objj_method(sel_getUid("keysSortedByValueUsingSelector:"),function(_3b,_3c,_3d){
with(_3b){
return objj_msgSend(objj_msgSend(_3b,"allKeys"),"sortedArrayUsingFunction:",function(a,b){
a=objj_msgSend(_3b,"objectForKey:",a);
b=objj_msgSend(_3b,"objectForKey:",b);
return objj_msgSend(a,"performSelector:withObject:",_3d,b);
});
}
}),new objj_method(sel_getUid("keyEnumerator"),function(_3e,_3f){
with(_3e){
return objj_msgSend(_keys,"objectEnumerator");
}
}),new objj_method(sel_getUid("objectEnumerator"),function(_40,_41){
with(_40){
return objj_msgSend(objj_msgSend(_CPDictionaryValueEnumerator,"alloc"),"initWithDictionary:",_40);
}
}),new objj_method(sel_getUid("isEqualToDictionary:"),function(_42,_43,_44){
with(_42){
if(_42===_44){
return YES;
}
var _45=objj_msgSend(_42,"count");
if(_45!==objj_msgSend(_44,"count")){
return NO;
}
var _46=_45;
while(_46--){
var _47=_keys[_46],_48=_buckets[_47],_49=_44._buckets[_47];
if(_48===_49){
continue;
}
if(_48&&_48.isa&&_49&&_49.isa&&objj_msgSend(_48,"respondsToSelector:",sel_getUid("isEqual:"))&&objj_msgSend(_48,"isEqual:",_49)){
continue;
}
return NO;
}
return YES;
}
}),new objj_method(sel_getUid("isEqual:"),function(_4a,_4b,_4c){
with(_4a){
if(_4a===_4c){
return YES;
}
if(!objj_msgSend(_4c,"isKindOfClass:",objj_msgSend(CPDictionary,"class"))){
return NO;
}
return objj_msgSend(_4a,"isEqualToDictionary:",_4c);
}
}),new objj_method(sel_getUid("objectForKey:"),function(_4d,_4e,_4f){
with(_4d){
var _50=_buckets[_4f];
return (_50===undefined)?nil:_50;
}
}),new objj_method(sel_getUid("removeAllObjects"),function(_51,_52){
with(_51){
_51.removeAllValues();
}
}),new objj_method(sel_getUid("removeObjectForKey:"),function(_53,_54,_55){
with(_53){
_53.removeValueForKey(_55);
}
}),new objj_method(sel_getUid("removeObjectsForKeys:"),function(_56,_57,_58){
with(_56){
var _59=_58.length;
while(_59--){
objj_msgSend(_56,"removeObjectForKey:",_58[_59]);
}
}
}),new objj_method(sel_getUid("setObject:forKey:"),function(_5a,_5b,_5c,_5d){
with(_5a){
_5a.setValueForKey(_5d,_5c);
}
}),new objj_method(sel_getUid("addEntriesFromDictionary:"),function(_5e,_5f,_60){
with(_5e){
if(!_60){
return;
}
var _61=objj_msgSend(_60,"allKeys"),_62=objj_msgSend(_61,"count");
while(_62--){
var key=_61[_62];
objj_msgSend(_5e,"setObject:forKey:",objj_msgSend(_60,"objectForKey:",key),key);
}
}
}),new objj_method(sel_getUid("description"),function(_63,_64){
with(_63){
var _65="@{\n",_66=_keys,_67=0,_68=_count;
for(;_67<_68;++_67){
var key=_66[_67],_69=valueForKey(key);
_65+="\t"+key+": "+CPDescriptionOfObject(_69).split("\n").join("\n\t")+",\n";
}
return _65+"}";
}
}),new objj_method(sel_getUid("containsKey:"),function(_6a,_6b,_6c){
with(_6a){
var _6d=objj_msgSend(_6a,"objectForKey:",_6c);
return ((_6d!==nil)&&(_6d!==undefined));
}
}),new objj_method(sel_getUid("enumerateKeysAndObjectsUsingBlock:"),function(_6e,_6f,_70){
with(_6e){
var _71=NO,_72=function(_73){
if(arguments.length){
return _71=_73;
}
return _71;
};
for(var _74=0;_74<_count;_74++){
var key=_keys[_74],_75=valueForKey(key);
_70(key,_75,_72);
if(_71){
return;
}
}
}
}),new objj_method(sel_getUid("enumerateKeysAndObjectsWithOptions:usingBlock:"),function(_76,_77,_78,_79){
with(_76){
objj_msgSend(_76,"enumerateKeysAndObjectsUsingBlock:",_79);
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("alloc"),function(_7a,_7b){
with(_7a){
var _7c=new CFMutableDictionary();
_7c.isa=objj_msgSend(_7a,"class");
return _7c;
}
}),new objj_method(sel_getUid("dictionary"),function(_7d,_7e){
with(_7d){
return objj_msgSend(objj_msgSend(_7d,"alloc"),"init");
}
}),new objj_method(sel_getUid("dictionaryWithDictionary:"),function(_7f,_80,_81){
with(_7f){
return objj_msgSend(objj_msgSend(_7f,"alloc"),"initWithDictionary:",_81);
}
}),new objj_method(sel_getUid("dictionaryWithObject:forKey:"),function(_82,_83,_84,_85){
with(_82){
return objj_msgSend(objj_msgSend(_82,"alloc"),"initWithObjects:forKeys:",[_84],[_85]);
}
}),new objj_method(sel_getUid("dictionaryWithObjects:forKeys:"),function(_86,_87,_88,_89){
with(_86){
return objj_msgSend(objj_msgSend(_86,"alloc"),"initWithObjects:forKeys:",_88,_89);
}
}),new objj_method(sel_getUid("dictionaryWithJSObject:"),function(_8a,_8b,_8c){
with(_8a){
return objj_msgSend(_8a,"dictionaryWithJSObject:recursively:",_8c,NO);
}
}),new objj_method(sel_getUid("dictionaryWithJSObject:recursively:"),function(_8d,_8e,_8f,_90){
with(_8d){
var key="",_91=objj_msgSend(objj_msgSend(_8d,"alloc"),"init");
for(key in _8f){
if(!_8f.hasOwnProperty(key)){
continue;
}
var _92=_8f[key];
if(_92===null){
objj_msgSend(_91,"setObject:forKey:",objj_msgSend(CPNull,"null"),key);
continue;
}
if(_90){
if(_92.constructor===Object){
_92=objj_msgSend(CPDictionary,"dictionaryWithJSObject:recursively:",_92,YES);
}else{
if(objj_msgSend(_92,"isKindOfClass:",CPArray)){
var _93=[],i=0,_94=_92.length;
for(;i<_94;i++){
var _95=_92[i];
if(_95===null){
_93.push(objj_msgSend(CPNull,"null"));
}else{
if(_95.constructor===Object){
_93.push(objj_msgSend(CPDictionary,"dictionaryWithJSObject:recursively:",_95,YES));
}else{
_93.push(_95);
}
}
}
_92=_93;
}
}
}
objj_msgSend(_91,"setObject:forKey:",_92,key);
}
return _91;
}
}),new objj_method(sel_getUid("dictionaryWithObjectsAndKeys:"),function(_96,_97,_98){
with(_96){
arguments[0]=objj_msgSend(_96,"alloc");
arguments[1]=sel_getUid("initWithObjectsAndKeys:");
return objj_msgSend.apply(this,arguments);
}
})]);
var _1=objj_getClass("CPDictionary");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPDictionary\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("initWithCoder:"),function(_99,_9a,_9b){
with(_99){
return objj_msgSend(_9b,"_decodeDictionaryOfObjectsForKey:","CP.objects");
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_9c,_9d,_9e){
with(_9c){
objj_msgSend(_9e,"_encodeDictionaryOfObjects:forKey:",_9c,"CP.objects");
}
})]);
var _1=objj_allocateClassPair(CPDictionary,"CPMutableDictionary"),_2=_1.isa;
objj_registerClassPair(_1);
CFDictionary.prototype.isa=CPDictionary;
CFMutableDictionary.prototype.isa=CPMutableDictionary;
p;9;CPArray.jt;17315;@STATIC;1.0;i;14;CPEnumerator.ji;13;CPException.ji;10;CPObject.ji;9;CPRange.ji;18;CPSortDescriptor.ji;20;_CPJavaScriptArray.jt;17182;
objj_executeFile("CPEnumerator.j",YES);
objj_executeFile("CPException.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPRange.j",YES);
objj_executeFile("CPSortDescriptor.j",YES);
CPEnumerationNormal=0;
CPEnumerationConcurrent=1<<0;
CPEnumerationReverse=1<<1;
CPBinarySearchingFirstEqual=1<<8;
CPBinarySearchingLastEqual=1<<9;
CPBinarySearchingInsertionIndex=1<<10;
var _1=Array.prototype.concat,_2=Array.prototype.join,_3=Array.prototype.push;
var _4=objj_allocateClassPair(CPObject,"CPArray"),_5=_4.isa;
objj_registerClassPair(_4);
class_addMethods(_4,[new objj_method(sel_getUid("init"),function(_6,_7){
with(_6){
if(_6===_8){
arguments[0]=objj_msgSend(_CPJavaScriptArray,"alloc");
return objj_msgSend.apply(this,arguments);
}
return objj_msgSendSuper({receiver:_6,super_class:objj_getClass("CPArray").super_class},"init");
}
}),new objj_method(sel_getUid("initWithArray:"),function(_9,_a,_b){
with(_9){
if(_9===_8){
arguments[0]=objj_msgSend(_CPJavaScriptArray,"alloc");
return objj_msgSend.apply(this,arguments);
}
return objj_msgSendSuper({receiver:_9,super_class:objj_getClass("CPArray").super_class},"init");
}
}),new objj_method(sel_getUid("initWithArray:copyItems:"),function(_c,_d,_e,_f){
with(_c){
if(_c===_8){
arguments[0]=objj_msgSend(_CPJavaScriptArray,"alloc");
return objj_msgSend.apply(this,arguments);
}
return objj_msgSendSuper({receiver:_c,super_class:objj_getClass("CPArray").super_class},"init");
}
}),new objj_method(sel_getUid("initWithObjects:"),function(_10,_11,_12){
with(_10){
if(_10===_8){
arguments[0]=objj_msgSend(_CPJavaScriptArray,"alloc");
return objj_msgSend.apply(this,arguments);
}
return objj_msgSendSuper({receiver:_10,super_class:objj_getClass("CPArray").super_class},"init");
}
}),new objj_method(sel_getUid("initWithObjects:count:"),function(_13,_14,_15,_16){
with(_13){
if(_13===_8){
arguments[0]=objj_msgSend(_CPJavaScriptArray,"alloc");
return objj_msgSend.apply(this,arguments);
}
return objj_msgSendSuper({receiver:_13,super_class:objj_getClass("CPArray").super_class},"init");
}
}),new objj_method(sel_getUid("initWithCapacity:"),function(_17,_18,_19){
with(_17){
if(_17===_8){
arguments[0]=objj_msgSend(_CPJavaScriptArray,"alloc");
return objj_msgSend.apply(this,arguments);
}
return objj_msgSendSuper({receiver:_17,super_class:objj_getClass("CPArray").super_class},"init");
}
}),new objj_method(sel_getUid("containsObject:"),function(_1a,_1b,_1c){
with(_1a){
return objj_msgSend(_1a,"indexOfObject:",_1c)!==CPNotFound;
}
}),new objj_method(sel_getUid("containsObjectIdenticalTo:"),function(_1d,_1e,_1f){
with(_1d){
return objj_msgSend(_1d,"indexOfObjectIdenticalTo:",_1f)!==CPNotFound;
}
}),new objj_method(sel_getUid("count"),function(_20,_21){
with(_20){
_CPRaiseInvalidAbstractInvocation(_20,_21);
}
}),new objj_method(sel_getUid("firstObject"),function(_22,_23){
with(_22){
var _24=objj_msgSend(_22,"count");
if(_24>0){
return objj_msgSend(_22,"objectAtIndex:",0);
}
return nil;
}
}),new objj_method(sel_getUid("lastObject"),function(_25,_26){
with(_25){
var _27=objj_msgSend(_25,"count");
if(_27<=0){
return nil;
}
return objj_msgSend(_25,"objectAtIndex:",_27-1);
}
}),new objj_method(sel_getUid("objectAtIndex:"),function(_28,_29,_2a){
with(_28){
_CPRaiseInvalidAbstractInvocation(_28,_29);
}
}),new objj_method(sel_getUid("objectsAtIndexes:"),function(_2b,_2c,_2d){
with(_2b){
var _2e=CPNotFound,_2f=[];
while((_2e=objj_msgSend(_2d,"indexGreaterThanIndex:",_2e))!==CPNotFound){
_2f.push(objj_msgSend(_2b,"objectAtIndex:",_2e));
}
return _2f;
}
}),new objj_method(sel_getUid("objectEnumerator"),function(_30,_31){
with(_30){
return objj_msgSend(objj_msgSend(_CPArrayEnumerator,"alloc"),"initWithArray:",_30);
}
}),new objj_method(sel_getUid("reverseObjectEnumerator"),function(_32,_33){
with(_32){
return objj_msgSend(objj_msgSend(_CPReverseArrayEnumerator,"alloc"),"initWithArray:",_32);
}
}),new objj_method(sel_getUid("indexOfObject:"),function(_34,_35,_36){
with(_34){
return objj_msgSend(_34,"indexOfObject:inRange:",_36,nil);
}
}),new objj_method(sel_getUid("indexOfObject:inRange:"),function(_37,_38,_39,_3a){
with(_37){
if(_39&&_39.isa){
var _3b=_3a?_3a.location:0,_3c=_3a?CPMaxRange(_3a):objj_msgSend(_37,"count");
for(;_3b<_3c;++_3b){
if(objj_msgSend(objj_msgSend(_37,"objectAtIndex:",_3b),"isEqual:",_39)){
return _3b;
}
}
return CPNotFound;
}
return objj_msgSend(_37,"indexOfObjectIdenticalTo:inRange:",_39,_3a);
}
}),new objj_method(sel_getUid("indexOfObjectIdenticalTo:"),function(_3d,_3e,_3f){
with(_3d){
return objj_msgSend(_3d,"indexOfObjectIdenticalTo:inRange:",_3f,nil);
}
}),new objj_method(sel_getUid("indexOfObjectIdenticalTo:inRange:"),function(_40,_41,_42,_43){
with(_40){
var _44=_43?_43.location:0,_45=_43?CPMaxRange(_43):objj_msgSend(_40,"count");
for(;_44<_45;++_44){
if(objj_msgSend(_40,"objectAtIndex:",_44)===_42){
return _44;
}
}
return CPNotFound;
}
}),new objj_method(sel_getUid("indexOfObjectPassingTest:"),function(_46,_47,_48){
with(_46){
return objj_msgSend(_46,"indexOfObjectWithOptions:passingTest:context:",CPEnumerationNormal,_48,undefined);
}
}),new objj_method(sel_getUid("indexOfObjectPassingTest:context:"),function(_49,_4a,_4b,_4c){
with(_49){
return objj_msgSend(_49,"indexOfObjectWithOptions:passingTest:context:",CPEnumerationNormal,_4b,_4c);
}
}),new objj_method(sel_getUid("indexOfObjectWithOptions:passingTest:"),function(_4d,_4e,_4f,_50){
with(_4d){
return objj_msgSend(_4d,"indexOfObjectWithOptions:passingTest:context:",_4f,_50,undefined);
}
}),new objj_method(sel_getUid("indexOfObjectWithOptions:passingTest:context:"),function(_51,_52,_53,_54,_55){
with(_51){
if(_53&CPEnumerationReverse){
var _56=objj_msgSend(_51,"count")-1,_57=-1,_58=-1;
}else{
var _56=0,_57=objj_msgSend(_51,"count"),_58=1;
}
for(;_56!==_57;_56+=_58){
if(_54(objj_msgSend(_51,"objectAtIndex:",_56),_56,_55)){
return _56;
}
}
return CPNotFound;
}
}),new objj_method(sel_getUid("indexOfObject:inSortedRange:options:usingComparator:"),function(_59,_5a,_5b,_5c,_5d,_5e){
with(_59){
if(!_5e){
_CPRaiseInvalidArgumentException(_59,_5a,"comparator is nil");
}
if((_5d&CPBinarySearchingFirstEqual)&&(_5d&CPBinarySearchingLastEqual)){
_CPRaiseInvalidArgumentException(_59,_5a,"both CPBinarySearchingFirstEqual and CPBinarySearchingLastEqual options cannot be specified");
}
var _5f=objj_msgSend(_59,"count");
if(_5f<=0){
return (_5d&CPBinarySearchingInsertionIndex)?0:CPNotFound;
}
var _60=_5c?_5c.location:0,_61=(_5c?CPMaxRange(_5c):objj_msgSend(_59,"count"))-1;
if(_60<0){
_CPRaiseRangeException(_59,_5a,_60,_5f);
}
if(_61>=_5f){
_CPRaiseRangeException(_59,_5a,_61,_5f);
}
while(_60<=_61){
var _62=FLOOR((_60+_61)/2),_63=_5e(_5b,objj_msgSend(_59,"objectAtIndex:",_62));
if(_63>0){
_60=_62+1;
}else{
if(_63<0){
_61=_62-1;
}else{
if(_5d&CPBinarySearchingFirstEqual){
while(_62>_60&&_5e(_5b,objj_msgSend(_59,"objectAtIndex:",_62-1))===CPOrderedSame){
--_62;
}
}else{
if(_5d&CPBinarySearchingLastEqual){
while(_62<_61&&_5e(_5b,objj_msgSend(_59,"objectAtIndex:",_62+1))===CPOrderedSame){
++_62;
}
if(_5d&CPBinarySearchingInsertionIndex){
++_62;
}
}
}
return _62;
}
}
}
if(_5d&CPBinarySearchingInsertionIndex){
return MAX(_60,0);
}
return CPNotFound;
}
}),new objj_method(sel_getUid("indexesOfObjectsPassingTest:"),function(_64,_65,_66){
with(_64){
return objj_msgSend(_64,"indexesOfObjectsWithOptions:passingTest:context:",CPEnumerationNormal,_66,undefined);
}
}),new objj_method(sel_getUid("indexesOfObjectsPassingTest:context:"),function(_67,_68,_69,_6a){
with(_67){
return objj_msgSend(_67,"indexesOfObjectsWithOptions:passingTest:context:",CPEnumerationNormal,_69,_6a);
}
}),new objj_method(sel_getUid("indexesOfObjectsWithOptions:passingTest:"),function(_6b,_6c,_6d,_6e){
with(_6b){
return objj_msgSend(_6b,"indexesOfObjectsWithOptions:passingTest:context:",_6d,_6e,undefined);
}
}),new objj_method(sel_getUid("indexesOfObjectsWithOptions:passingTest:context:"),function(_6f,_70,_71,_72,_73){
with(_6f){
if(_71&CPEnumerationReverse){
var _74=objj_msgSend(_6f,"count")-1,_75=-1,_76=-1;
}else{
var _74=0,_75=objj_msgSend(_6f,"count"),_76=1;
}
var _77=objj_msgSend(CPIndexSet,"indexSet");
for(;_74!==_75;_74+=_76){
if(_72(objj_msgSend(_6f,"objectAtIndex:",_74),_74,_73)){
objj_msgSend(_77,"addIndex:",_74);
}
}
return _77;
}
}),new objj_method(sel_getUid("makeObjectsPerformSelector:"),function(_78,_79,_7a){
with(_78){
objj_msgSend(_78,"makeObjectsPerformSelector:withObjects:",_7a,nil);
}
}),new objj_method(sel_getUid("makeObjectsPerformSelector:withObject:"),function(_7b,_7c,_7d,_7e){
with(_7b){
return objj_msgSend(_7b,"makeObjectsPerformSelector:withObjects:",_7d,[_7e]);
}
}),new objj_method(sel_getUid("makeObjectsPerformSelector:withObjects:"),function(_7f,_80,_81,_82){
with(_7f){
if(!_81){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"makeObjectsPerformSelector:withObjects: 'aSelector' can't be nil");
}
var _83=0,_84=objj_msgSend(_7f,"count");
if(objj_msgSend(_82,"count")){
var _85=objj_msgSend([nil,_81],"arrayByAddingObjectsFromArray:",_82);
for(;_83<_84;++_83){
_85[0]=objj_msgSend(_7f,"objectAtIndex:",_83);
objj_msgSend.apply(this,_85);
}
}else{
for(;_83<_84;++_83){
objj_msgSend(objj_msgSend(_7f,"objectAtIndex:",_83),_81);
}
}
}
}),new objj_method(sel_getUid("enumerateObjectsUsingBlock:"),function(_86,_87,_88){
with(_86){
var _89=0,_8a=objj_msgSend(_86,"count"),_8b=NO,_8c=function(_8d){
if(arguments.length){
return _8b=_8d;
}
return _8b;
};
for(;_89<_8a;++_89){
_88(objj_msgSend(_86,"objectAtIndex:",_89),_89,_8c);
if(_8b){
return;
}
}
}
}),new objj_method(sel_getUid("enumerateObjectsWithOptions:usingBlock:"),function(_8e,_8f,_90,_91){
with(_8e){
var _92=NO;
if(_90&CPEnumerationReverse){
var _93=objj_msgSend(_8e,"count")-1,_94=-1,_95=-1;
}else{
var _93=0,_94=objj_msgSend(_8e,"count"),_95=1;
}
for(;_93!==_94;_93+=_95){
_91(objj_msgSend(_8e,"objectAtIndex:",_93),_93,function(_96){
if(arguments.length){
return _92=_96;
}
return _92;
});
if(_92){
return;
}
}
}
}),new objj_method(sel_getUid("firstObjectCommonWithArray:"),function(_97,_98,_99){
with(_97){
var _9a=objj_msgSend(_97,"count");
if(!objj_msgSend(_99,"count")||!_9a){
return nil;
}
var _9b=0;
for(;_9b<_9a;++_9b){
var _9c=objj_msgSend(_97,"objectAtIndex:",_9b);
if(objj_msgSend(_99,"containsObject:",_9c)){
return _9c;
}
}
return nil;
}
}),new objj_method(sel_getUid("isEqualToArray:"),function(_9d,_9e,_9f){
with(_9d){
if(_9d===_9f){
return YES;
}
if(!objj_msgSend(_9f,"isKindOfClass:",CPArray)){
return NO;
}
var _a0=objj_msgSend(_9d,"count"),_a1=objj_msgSend(_9f,"count");
if(_9f===nil||_a0!==_a1){
return NO;
}
var _a2=0;
for(;_a2<_a0;++_a2){
var lhs=objj_msgSend(_9d,"objectAtIndex:",_a2),rhs=objj_msgSend(_9f,"objectAtIndex:",_a2);
if(lhs!==rhs&&(lhs&&!lhs.isa||rhs&&!rhs.isa||!objj_msgSend(lhs,"isEqual:",rhs))){
return NO;
}
}
return YES;
}
}),new objj_method(sel_getUid("isEqual:"),function(_a3,_a4,_a5){
with(_a3){
return (_a3===_a5)||objj_msgSend(_a3,"isEqualToArray:",_a5);
}
}),new objj_method(sel_getUid("_javaScriptArrayCopy"),function(_a6,_a7){
with(_a6){
var _a8=0,_a9=objj_msgSend(_a6,"count"),_aa=[];
for(;_a8<_a9;++_a8){
_3.call(_aa,objj_msgSend(_a6,"objectAtIndex:",_a8));
}
return _aa;
}
}),new objj_method(sel_getUid("arrayByAddingObject:"),function(_ab,_ac,_ad){
with(_ab){
var _ae=objj_msgSend(_ab,"_javaScriptArrayCopy");
_3.call(_ae,_ad);
return objj_msgSend(objj_msgSend(_ab,"class"),sel_getUid("arrayWithArray:"),_ae);
}
}),new objj_method(sel_getUid("arrayByAddingObjectsFromArray:"),function(_af,_b0,_b1){
with(_af){
if(!_b1){
return objj_msgSend(_af,"copy");
}
var _b1=_b1.isa===_CPJavaScriptArray?_b1:objj_msgSend(_b1,"_javaScriptArrayCopy"),_b2=_1.call(objj_msgSend(_af,"_javaScriptArrayCopy"),_b1);
return objj_msgSend(objj_msgSend(_af,"class"),sel_getUid("arrayWithArray:"),_b2);
}
}),new objj_method(sel_getUid("subarrayWithRange:"),function(_b3,_b4,_b5){
with(_b3){
if(!_b5){
return objj_msgSend(_b3,"copy");
}
if(_b5.location<0||CPMaxRange(_b5)>_b3.length){
objj_msgSend(CPException,"raise:reason:",CPRangeException,"subarrayWithRange: aRange out of bounds");
}
var _b6=_b5.location,_b7=CPMaxRange(_b5),_b8=[];
for(;_b6<_b7;++_b6){
_3.call(_b8,objj_msgSend(_b3,"objectAtIndex:",_b6));
}
return objj_msgSend(objj_msgSend(_b3,"class"),sel_getUid("arrayWithArray:"),_b8);
}
}),new objj_method(sel_getUid("sortedArrayUsingDescriptors:"),function(_b9,_ba,_bb){
with(_b9){
var _bc=objj_msgSend(_b9,"copy");
objj_msgSend(_bc,"sortUsingDescriptors:",_bb);
return _bc;
}
}),new objj_method(sel_getUid("sortedArrayUsingFunction:"),function(_bd,_be,_bf){
with(_bd){
return objj_msgSend(_bd,"sortedArrayUsingFunction:context:",_bf,nil);
}
}),new objj_method(sel_getUid("sortedArrayUsingFunction:context:"),function(_c0,_c1,_c2,_c3){
with(_c0){
var _c4=objj_msgSend(_c0,"copy");
objj_msgSend(_c4,"sortUsingFunction:context:",_c2,_c3);
return _c4;
}
}),new objj_method(sel_getUid("sortedArrayUsingSelector:"),function(_c5,_c6,_c7){
with(_c5){
var _c8=objj_msgSend(_c5,"copy");
objj_msgSend(_c8,"sortUsingSelector:",_c7);
return _c8;
}
}),new objj_method(sel_getUid("componentsJoinedByString:"),function(_c9,_ca,_cb){
with(_c9){
return _2.call(objj_msgSend(_c9,"_javaScriptArrayCopy"),_cb);
}
}),new objj_method(sel_getUid("description"),function(_cc,_cd){
with(_cc){
var _ce=0,_cf=objj_msgSend(_cc,"count"),_d0="@[";
for(;_ce<_cf;++_ce){
if(_ce===0){
_d0+="\n\t";
}
var _d1=objj_msgSend(_cc,"objectAtIndex:",_ce);
_d0+=CPDescriptionOfObject(_d1);
if(_ce!==_cf-1){
_d0+=",\n\t";
}else{
_d0+="\n";
}
}
return _d0+"]";
}
}),new objj_method(sel_getUid("pathsMatchingExtensions:"),function(_d2,_d3,_d4){
with(_d2){
var _d5=0,_d6=objj_msgSend(_d2,"count"),_d7=[];
for(;_d5<_d6;++_d5){
if(_d2[_d5].isa&&objj_msgSend(_d2[_d5],"isKindOfClass:",objj_msgSend(CPString,"class"))&&objj_msgSend(_d4,"containsObject:",objj_msgSend(_d2[_d5],"pathExtension"))){
_d7.push(_d2[_d5]);
}
}
return _d7;
}
}),new objj_method(sel_getUid("copy"),function(_d8,_d9){
with(_d8){
return objj_msgSend(objj_msgSend(_d8,"class"),"arrayWithArray:",_d8);
}
})]);
class_addMethods(_5,[new objj_method(sel_getUid("alloc"),function(_da,_db){
with(_da){
if(_da===CPArray||_da===CPMutableArray){
return objj_msgSend(_CPPlaceholderArray,"alloc");
}
return objj_msgSendSuper({receiver:_da,super_class:objj_getMetaClass("CPArray").super_class},"alloc");
}
}),new objj_method(sel_getUid("array"),function(_dc,_dd){
with(_dc){
return objj_msgSend(objj_msgSend(_dc,"alloc"),"init");
}
}),new objj_method(sel_getUid("arrayWithArray:"),function(_de,_df,_e0){
with(_de){
return objj_msgSend(objj_msgSend(_de,"alloc"),"initWithArray:",_e0);
}
}),new objj_method(sel_getUid("arrayWithObject:"),function(_e1,_e2,_e3){
with(_e1){
return objj_msgSend(objj_msgSend(_e1,"alloc"),"initWithObjects:",_e3);
}
}),new objj_method(sel_getUid("arrayWithObjects:"),function(_e4,_e5,_e6){
with(_e4){
arguments[0]=objj_msgSend(_e4,"alloc");
arguments[1]=sel_getUid("initWithObjects:");
return objj_msgSend.apply(this,arguments);
}
}),new objj_method(sel_getUid("arrayWithObjects:count:"),function(_e7,_e8,_e9,_ea){
with(_e7){
return objj_msgSend(objj_msgSend(_e7,"alloc"),"initWithObjects:count:",_e9,_ea);
}
})]);
var _4=objj_getClass("CPArray");
if(!_4){
throw new SyntaxError("*** Could not find definition for class \"CPArray\"");
}
var _5=_4.isa;
class_addMethods(_4,[new objj_method(sel_getUid("initWithCoder:"),function(_eb,_ec,_ed){
with(_eb){
return objj_msgSend(_ed,"decodeObjectForKey:","CP.objects");
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_ee,_ef,_f0){
with(_ee){
objj_msgSend(_f0,"_encodeArrayOfObjects:forKey:",_ee,"CP.objects");
}
})]);
var _4=objj_allocateClassPair(CPEnumerator,"_CPArrayEnumerator"),_5=_4.isa;
class_addIvars(_4,[new objj_ivar("_array"),new objj_ivar("_index")]);
objj_registerClassPair(_4);
class_addMethods(_4,[new objj_method(sel_getUid("initWithArray:"),function(_f1,_f2,_f3){
with(_f1){
_f1=objj_msgSendSuper({receiver:_f1,super_class:objj_getClass("_CPArrayEnumerator").super_class},"init");
if(_f1){
_array=_f3;
_index=-1;
}
return _f1;
}
}),new objj_method(sel_getUid("nextObject"),function(_f4,_f5){
with(_f4){
if(++_index>=objj_msgSend(_array,"count")){
return nil;
}
return objj_msgSend(_array,"objectAtIndex:",_index);
}
})]);
var _4=objj_allocateClassPair(CPEnumerator,"_CPReverseArrayEnumerator"),_5=_4.isa;
class_addIvars(_4,[new objj_ivar("_array"),new objj_ivar("_index")]);
objj_registerClassPair(_4);
class_addMethods(_4,[new objj_method(sel_getUid("initWithArray:"),function(_f6,_f7,_f8){
with(_f6){
_f6=objj_msgSendSuper({receiver:_f6,super_class:objj_getClass("_CPReverseArrayEnumerator").super_class},"init");
if(_f6){
_array=_f8;
_index=objj_msgSend(_array,"count");
}
return _f6;
}
}),new objj_method(sel_getUid("nextObject"),function(_f9,_fa){
with(_f9){
if(--_index<0){
return nil;
}
return objj_msgSend(_array,"objectAtIndex:",_index);
}
})]);
var _8=nil;
var _4=objj_allocateClassPair(CPArray,"_CPPlaceholderArray"),_5=_4.isa;
objj_registerClassPair(_4);
class_addMethods(_5,[new objj_method(sel_getUid("alloc"),function(_fb,_fc){
with(_fb){
if(!_8){
_8=objj_msgSendSuper({receiver:_fb,super_class:objj_getMetaClass("_CPPlaceholderArray").super_class},"alloc");
}
return _8;
}
})]);
objj_executeFile("_CPJavaScriptArray.j",YES);
p;14;CPEnumerator.jt;357;@STATIC;1.0;i;10;CPObject.jt;324;
objj_executeFile("CPObject.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPEnumerator"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("nextObject"),function(_3,_4){
with(_3){
return nil;
}
}),new objj_method(sel_getUid("allObjects"),function(_5,_6){
with(_5){
return [];
}
})]);
p;20;_CPJavaScriptArray.jt;7130;@STATIC;1.0;i;16;CPMutableArray.jt;7090;
objj_executeFile("CPMutableArray.j",YES);
var _1=Array.prototype.concat,_2=Array.prototype.indexOf,_3=Array.prototype.join,_4=Array.prototype.pop,_5=Array.prototype.push,_6=Array.prototype.slice,_7=Array.prototype.splice;
var _8=objj_allocateClassPair(CPMutableArray,"_CPJavaScriptArray"),_9=_8.isa;
objj_registerClassPair(_8);
class_addMethods(_8,[new objj_method(sel_getUid("initWithArray:"),function(_a,_b,_c){
with(_a){
return objj_msgSend(_a,"initWithArray:copyItems:",_c,NO);
}
}),new objj_method(sel_getUid("initWithArray:copyItems:"),function(_d,_e,_f,_10){
with(_d){
if(!_10&&objj_msgSend(_f,"isKindOfClass:",_CPJavaScriptArray)){
return _6.call(_f,0);
}
_d=objj_msgSendSuper({receiver:_d,super_class:objj_getClass("_CPJavaScriptArray").super_class},"init");
var _11=0;
if(objj_msgSend(_f,"isKindOfClass:",_CPJavaScriptArray)){
var _12=_f.length;
for(;_11<_12;++_11){
var _13=_f[_11];
_d[_11]=(_13&&_13.isa)?objj_msgSend(_13,"copy"):_13;
}
return _d;
}
var _12=objj_msgSend(_f,"count");
for(;_11<_12;++_11){
var _13=objj_msgSend(_f,"objectAtIndex:",_11);
_d[_11]=(_10&&_13&&_13.isa)?objj_msgSend(_13,"copy"):_13;
}
return _d;
}
}),new objj_method(sel_getUid("initWithObjects:"),function(_14,_15,_16){
with(_14){
var _17=2,_18=arguments.length;
for(;_17<_18;++_17){
if(arguments[_17]===nil){
break;
}
}
return _6.call(arguments,2,_17);
}
}),new objj_method(sel_getUid("initWithObjects:count:"),function(_19,_1a,_1b,_1c){
with(_19){
if(objj_msgSend(_1b,"isKindOfClass:",_CPJavaScriptArray)){
return _6.call(_1b,0);
}
var _1d=[],_1e=0;
for(;_1e<_1c;++_1e){
_5.call(_1d,objj_msgSend(_1b,"objectAtIndex:",_1e));
}
return _1d;
}
}),new objj_method(sel_getUid("initWithCapacity:"),function(_1f,_20,_21){
with(_1f){
return _1f;
}
}),new objj_method(sel_getUid("count"),function(_22,_23){
with(_22){
return _22.length;
}
}),new objj_method(sel_getUid("objectAtIndex:"),function(_24,_25,_26){
with(_24){
if(_26>=_24.length||_26<0){
_CPRaiseRangeException(_24,_25,_26,_24.length);
}
return _24[_26];
}
}),new objj_method(sel_getUid("indexOfObject:inRange:"),function(_27,_28,_29,_2a){
with(_27){
if(_29&&_29.isa){
var _2b=_2a?_2a.location:0,_2c=_2a?CPMaxRange(_2a):_27.length;
for(;_2b<_2c;++_2b){
if(objj_msgSend(_27[_2b],"isEqual:",_29)){
return _2b;
}
}
return CPNotFound;
}
return objj_msgSend(_27,"indexOfObjectIdenticalTo:inRange:",_29,_2a);
}
}),new objj_method(sel_getUid("indexOfObjectIdenticalTo:inRange:"),function(_2d,_2e,_2f,_30){
with(_2d){
if(_2&&!_30){
return _2.call(_2d,_2f);
}
var _31=_30?_30.location:0,_32=_30?CPMaxRange(_30):_2d.length;
for(;_31<_32;++_31){
if(_2d[_31]===_2f){
return _31;
}
}
return CPNotFound;
}
}),new objj_method(sel_getUid("makeObjectsPerformSelector:withObjects:"),function(_33,_34,_35,_36){
with(_33){
if(!_35){
_CPRaiseInvalidArgumentException(_33,_34,"attempt to pass a nil selector");
}
var _37=0,_38=_33.length;
if(objj_msgSend(_36,"count")){
var _39=objj_msgSend([nil,_35],"arrayByAddingObjectsFromArray:",_36);
for(;_37<_38;++_37){
_39[0]=_33[_37];
objj_msgSend.apply(this,_39);
}
}else{
for(;_37<_38;++_37){
objj_msgSend(_33[_37],_35);
}
}
}
}),new objj_method(sel_getUid("arrayByAddingObject:"),function(_3a,_3b,_3c){
with(_3a){
if(_3c&&_3c.isa&&objj_msgSend(_3c,"isKindOfClass:",_CPJavaScriptArray)){
return _1.call(_3a,[_3c]);
}
return _1.call(_3a,_3c);
}
}),new objj_method(sel_getUid("arrayByAddingObjectsFromArray:"),function(_3d,_3e,_3f){
with(_3d){
if(!_3f){
return objj_msgSend(_3d,"copy");
}
return _1.call(_3d,objj_msgSend(_3f,"isKindOfClass:",_CPJavaScriptArray)?_3f:objj_msgSend(_3f,"_javaScriptArrayCopy"));
}
}),new objj_method(sel_getUid("subarrayWithRange:"),function(_40,_41,_42){
with(_40){
if(_42.location<0||CPMaxRange(_42)>_40.length){
objj_msgSend(CPException,"raise:reason:",CPRangeException,_41+" aRange out of bounds");
}
return _6.call(_40,_42.location,CPMaxRange(_42));
}
}),new objj_method(sel_getUid("componentsJoinedByString:"),function(_43,_44,_45){
with(_43){
return _3.call(_43,_45);
}
}),new objj_method(sel_getUid("insertObject:atIndex:"),function(_46,_47,_48,_49){
with(_46){
if(_49>_46.length||_49<0){
_CPRaiseRangeException(_46,_47,_49,_46.length);
}
_7.call(_46,_49,0,_48);
}
}),new objj_method(sel_getUid("removeObjectAtIndex:"),function(_4a,_4b,_4c){
with(_4a){
if(_4c>=_4a.length||_4c<0){
_CPRaiseRangeException(_4a,_4b,_4c,_4a.length);
}
_7.call(_4a,_4c,1);
}
}),new objj_method(sel_getUid("addObject:"),function(_4d,_4e,_4f){
with(_4d){
_5.call(_4d,_4f);
}
}),new objj_method(sel_getUid("removeAllObjects"),function(_50,_51){
with(_50){
_7.call(_50,0,_50.length);
}
}),new objj_method(sel_getUid("removeLastObject"),function(_52,_53){
with(_52){
_4.call(_52);
}
}),new objj_method(sel_getUid("removeObjectsInRange:"),function(_54,_55,_56){
with(_54){
if(_56.location<0||CPMaxRange(_56)>_54.length){
objj_msgSend(CPException,"raise:reason:",CPRangeException,_55+" aRange out of bounds");
}
_7.call(_54,_56.location,_56.length);
}
}),new objj_method(sel_getUid("replaceObjectAtIndex:withObject:"),function(_57,_58,_59,_5a){
with(_57){
if(_59>=_57.length||_59<0){
_CPRaiseRangeException(_57,_58,_59,_57.length);
}
_57[_59]=_5a;
}
}),new objj_method(sel_getUid("replaceObjectsInRange:withObjectsFromArray:range:"),function(_5b,_5c,_5d,_5e,_5f){
with(_5b){
if(_5d.location<0||CPMaxRange(_5d)>_5b.length){
objj_msgSend(CPException,"raise:reason:",CPRangeException,_5c+" aRange out of bounds");
}
if(_5f&&(_5f.location<0||CPMaxRange(_5f)>_5e.length)){
objj_msgSend(CPException,"raise:reason:",CPRangeException,_5c+" otherRange out of bounds");
}
if(_5f&&(_5f.location!==0||_5f.length!==objj_msgSend(_5e,"count"))){
_5e=objj_msgSend(_5e,"subarrayWithRange:",_5f);
}
if(_5e.isa!==_CPJavaScriptArray){
_5e=objj_msgSend(_5e,"_javaScriptArrayCopy");
}
_7.apply(_5b,[_5d.location,_5d.length].concat(_5e));
}
}),new objj_method(sel_getUid("setArray:"),function(_60,_61,_62){
with(_60){
if(objj_msgSend(_62,"isKindOfClass:",_CPJavaScriptArray)){
_7.apply(_60,[0,_60.length].concat(_62));
}else{
objj_msgSendSuper({receiver:_60,super_class:objj_getClass("_CPJavaScriptArray").super_class},"setArray:",_62);
}
}
}),new objj_method(sel_getUid("addObjectsFromArray:"),function(_63,_64,_65){
with(_63){
if(objj_msgSend(_65,"isKindOfClass:",_CPJavaScriptArray)){
_7.apply(_63,[_63.length,0].concat(_65));
}else{
objj_msgSendSuper({receiver:_63,super_class:objj_getClass("_CPJavaScriptArray").super_class},"addObjectsFromArray:",_65);
}
}
}),new objj_method(sel_getUid("copy"),function(_66,_67){
with(_66){
return _6.call(_66,0);
}
}),new objj_method(sel_getUid("classForCoder"),function(_68,_69){
with(_68){
return CPArray;
}
})]);
class_addMethods(_9,[new objj_method(sel_getUid("alloc"),function(_6a,_6b){
with(_6a){
return [];
}
}),new objj_method(sel_getUid("array"),function(_6c,_6d){
with(_6c){
return [];
}
}),new objj_method(sel_getUid("arrayWithArray:"),function(_6e,_6f,_70){
with(_6e){
return objj_msgSend(objj_msgSend(_6e,"alloc"),"initWithArray:",_70);
}
}),new objj_method(sel_getUid("arrayWithObject:"),function(_71,_72,_73){
with(_71){
return [_73];
}
})]);
Array.prototype.isa=_CPJavaScriptArray;
p;16;CPMutableArray.jt;8140;@STATIC;1.0;i;9;CPArray.jt;8108;
objj_executeFile("CPArray.j",YES);
var _1=objj_allocateClassPair(CPArray,"CPMutableArray"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("addObject:"),function(_3,_4,_5){
with(_3){
_CPRaiseInvalidAbstractInvocation(_3,_4);
}
}),new objj_method(sel_getUid("addObjectsFromArray:"),function(_6,_7,_8){
with(_6){
var _9=0,_a=objj_msgSend(_8,"count");
for(;_9<_a;++_9){
objj_msgSend(_6,"addObject:",objj_msgSend(_8,"objectAtIndex:",_9));
}
}
}),new objj_method(sel_getUid("insertObject:atIndex:"),function(_b,_c,_d,_e){
with(_b){
_CPRaiseInvalidAbstractInvocation(_b,_c);
}
}),new objj_method(sel_getUid("insertObjects:atIndexes:"),function(_f,_10,_11,_12){
with(_f){
var _13=objj_msgSend(_12,"count"),_14=objj_msgSend(_11,"count");
if(_13!==_14){
objj_msgSend(CPException,"raise:reason:",CPRangeException,"the counts of the passed-in array ("+_14+") and index set ("+_13+") must be identical.");
}
var _15=objj_msgSend(_12,"lastIndex");
if(_15>=objj_msgSend(_f,"count")+_13){
objj_msgSend(CPException,"raise:reason:",CPRangeException,"the last index ("+_15+") must be less than the sum of the original count ("+objj_msgSend(_f,"count")+") and the insertion count ("+_13+").");
}
var _16=0,_17=objj_msgSend(_12,"firstIndex");
for(;_16<_14;++_16,_17=objj_msgSend(_12,"indexGreaterThanIndex:",_17)){
objj_msgSend(_f,"insertObject:atIndex:",objj_msgSend(_11,"objectAtIndex:",_16),_17);
}
}
}),new objj_method(sel_getUid("insertObject:inArraySortedByDescriptors:"),function(_18,_19,_1a,_1b){
with(_18){
var _1c,_1d=objj_msgSend(_1b,"count");
if(_1d){
_1c=objj_msgSend(_18,"indexOfObject:inSortedRange:options:usingComparator:",_1a,nil,CPBinarySearchingInsertionIndex,function(lhs,rhs){
var _1e=0,_1f=CPOrderedSame;
while(_1e<_1d&&((_1f=objj_msgSend(objj_msgSend(_1b,"objectAtIndex:",_1e),"compareObject:withObject:",lhs,rhs))===CPOrderedSame)){
++_1e;
}
return _1f;
});
}else{
_1c=objj_msgSend(_18,"count");
}
objj_msgSend(_18,"insertObject:atIndex:",_1a,_1c);
return _1c;
}
}),new objj_method(sel_getUid("replaceObjectAtIndex:withObject:"),function(_20,_21,_22,_23){
with(_20){
_CPRaiseInvalidAbstractInvocation(_20,_21);
}
}),new objj_method(sel_getUid("replaceObjectsAtIndexes:withObjects:"),function(_24,_25,_26,_27){
with(_24){
var i=0,_28=objj_msgSend(_26,"firstIndex");
while(_28!==CPNotFound){
objj_msgSend(_24,"replaceObjectAtIndex:withObject:",_28,objj_msgSend(_27,"objectAtIndex:",i++));
_28=objj_msgSend(_26,"indexGreaterThanIndex:",_28);
}
}
}),new objj_method(sel_getUid("replaceObjectsInRange:withObjectsFromArray:range:"),function(_29,_2a,_2b,_2c,_2d){
with(_29){
objj_msgSend(_29,"removeObjectsInRange:",_2b);
if(_2d&&(_2d.location!==0||_2d.length!==objj_msgSend(_2c,"count"))){
_2c=objj_msgSend(_2c,"subarrayWithRange:",_2d);
}
var _2e=objj_msgSend(CPIndexSet,"indexSetWithIndexesInRange:",CPMakeRange(_2b.location,objj_msgSend(_2c,"count")));
objj_msgSend(_29,"insertObjects:atIndexes:",_2c,_2e);
}
}),new objj_method(sel_getUid("replaceObjectsInRange:withObjectsFromArray:"),function(_2f,_30,_31,_32){
with(_2f){
objj_msgSend(_2f,"replaceObjectsInRange:withObjectsFromArray:range:",_31,_32,nil);
}
}),new objj_method(sel_getUid("setArray:"),function(_33,_34,_35){
with(_33){
if(_33===_35){
return;
}
objj_msgSend(_33,"removeAllObjects");
objj_msgSend(_33,"addObjectsFromArray:",_35);
}
}),new objj_method(sel_getUid("removeAllObjects"),function(_36,_37){
with(_36){
while(objj_msgSend(_36,"count")){
objj_msgSend(_36,"removeLastObject");
}
}
}),new objj_method(sel_getUid("removeLastObject"),function(_38,_39){
with(_38){
_CPRaiseInvalidAbstractInvocation(_38,_39);
}
}),new objj_method(sel_getUid("removeObject:"),function(_3a,_3b,_3c){
with(_3a){
objj_msgSend(_3a,"removeObject:inRange:",_3c,CPMakeRange(0,length));
}
}),new objj_method(sel_getUid("removeObject:inRange:"),function(_3d,_3e,_3f,_40){
with(_3d){
var _41;
while((_41=objj_msgSend(_3d,"indexOfObject:inRange:",_3f,_40))!=CPNotFound){
objj_msgSend(_3d,"removeObjectAtIndex:",_41);
_40=CPIntersectionRange(CPMakeRange(_41,length-_41),_40);
}
}
}),new objj_method(sel_getUid("removeObjectAtIndex:"),function(_42,_43,_44){
with(_42){
_CPRaiseInvalidAbstractInvocation(_42,_43);
}
}),new objj_method(sel_getUid("removeObjectsAtIndexes:"),function(_45,_46,_47){
with(_45){
var _48=objj_msgSend(_47,"lastIndex");
while(_48!==CPNotFound){
objj_msgSend(_45,"removeObjectAtIndex:",_48);
_48=objj_msgSend(_47,"indexLessThanIndex:",_48);
}
}
}),new objj_method(sel_getUid("removeObjectIdenticalTo:"),function(_49,_4a,_4b){
with(_49){
objj_msgSend(_49,"removeObjectIdenticalTo:inRange:",_4b,CPMakeRange(0,objj_msgSend(_49,"count")));
}
}),new objj_method(sel_getUid("removeObjectIdenticalTo:inRange:"),function(_4c,_4d,_4e,_4f){
with(_4c){
var _50,_51=objj_msgSend(_4c,"count");
while((_50=objj_msgSend(_4c,"indexOfObjectIdenticalTo:inRange:",_4e,_4f))!==CPNotFound){
objj_msgSend(_4c,"removeObjectAtIndex:",_50);
_4f=CPIntersectionRange(CPMakeRange(_50,(--_51)-_50),_4f);
}
}
}),new objj_method(sel_getUid("removeObjectsInArray:"),function(_52,_53,_54){
with(_52){
var _55=0,_56=objj_msgSend(_54,"count");
for(;_55<_56;++_55){
objj_msgSend(_52,"removeObject:",objj_msgSend(_54,"objectAtIndex:",_55));
}
}
}),new objj_method(sel_getUid("removeObjectsInRange:"),function(_57,_58,_59){
with(_57){
var _5a=_59.location,_5b=CPMaxRange(_59);
while(_5b-->_5a){
objj_msgSend(_57,"removeObjectAtIndex:",_5a);
}
}
}),new objj_method(sel_getUid("exchangeObjectAtIndex:withObjectAtIndex:"),function(_5c,_5d,_5e,_5f){
with(_5c){
if(_5e===_5f){
return;
}
var _60=objj_msgSend(_5c,"objectAtIndex:",_5e);
objj_msgSend(_5c,"replaceObjectAtIndex:withObject:",_5e,objj_msgSend(_5c,"objectAtIndex:",_5f));
objj_msgSend(_5c,"replaceObjectAtIndex:withObject:",_5f,_60);
}
}),new objj_method(sel_getUid("sortUsingDescriptors:"),function(_61,_62,_63){
with(_61){
var i=objj_msgSend(_63,"count"),_64=[];
while(i--){
var d=objj_msgSend(_63,"objectAtIndex:",i);
objj_msgSend(_64,"addObject:",{"k":objj_msgSend(d,"key"),"a":objj_msgSend(d,"ascending"),"s":objj_msgSend(d,"selector")});
}
_65(_61,_64);
}
}),new objj_method(sel_getUid("sortUsingFunction:context:"),function(_66,_67,_68,_69){
with(_66){
_6a(_66,_68,_69);
}
}),new objj_method(sel_getUid("sortUsingSelector:"),function(_6b,_6c,_6d){
with(_6b){
_6a(_6b,_6e,_6d);
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("arrayWithCapacity:"),function(_6f,_70,_71){
with(_6f){
return objj_msgSend(objj_msgSend(_6f,"alloc"),"initWithCapacity:",_71);
}
})]);
var _1=objj_getClass("CPArray");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPArray\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("mutableCopy"),function(_72,_73){
with(_72){
var r=objj_msgSend(CPMutableArray,"new");
objj_msgSend(r,"addObjectsFromArray:",_72);
return r;
}
})]);
var _6e=function(_74,_75,_76){
return objj_msgSend(_74,"performSelector:withObject:",_76,_75);
};
var _6a=function(_77,_78,_79){
var h,i,j,k,l,m,n=_77.length,o;
var A,B=[];
for(h=1;h<n;h+=h){
for(m=n-1-h;m>=0;m-=h+h){
l=m-h+1;
if(l<0){
l=0;
}
for(i=0,j=l;j<=m;i++,j++){
B[i]=_77[j];
}
for(i=0,k=l;k<j&&j<=m+h;k++){
A=_77[j];
o=_78(A,B[i],_79);
if(o>=0){
_77[k]=B[i++];
}else{
_77[k]=A;
j++;
}
}
while(k<j){
_77[k++]=B[i++];
}
}
}
};
var _65=function(a,d){
var h,i,j,k,l,m,n=a.length,dl=d.length-1,o,c={};
var A,B=[],C1,C2,cn,_7a,_7b,key,dd;
if(dl<0){
return;
}
for(h=1;h<n;h+=h){
for(m=n-1-h;m>=0;m-=h+h){
l=m-h+1;
if(l<0){
l=0;
}
for(i=0,j=l;j<=m;i++,j++){
B[i]=a[j];
}
for(i=0,k=l;k<j&&j<=m+h;k++){
A=a[j];
_7a=A._UID;
if(!_7a){
_7a=objj_msgSend(A,"UID");
}
C1=c[_7a];
if(!C1){
C1={};
cn=dl;
do{
key=d[cn].k;
C1[key]=objj_msgSend(A,"valueForKeyPath:",key);
}while(cn--);
c[_7a]=C1;
}
_7b=B[i]._UID;
if(!_7b){
_7b=objj_msgSend(B[i],"UID");
}
C2=c[_7b];
if(!C2){
C2={};
cn=dl;
do{
key=d[cn].k;
C2[key]=objj_msgSend(B[i],"valueForKeyPath:",key);
}while(cn--);
c[_7b]=C2;
}
cn=dl;
do{
dd=d[cn];
key=dd.k;
o=objj_msgSend(C1[key],dd.s,C2[key]);
if(o&&!dd.a){
o=-o;
}
}while(cn--&&o==CPOrderedSame);
if(o>=0){
a[k]=B[i++];
}else{
a[k]=A;
j++;
}
}
while(k<j){
a[k++]=B[i++];
}
}
}
};
p;12;CPIndexSet.jt;16324;@STATIC;1.0;i;9;CPArray.ji;10;CPObject.ji;9;CPRange.jt;16263;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPRange.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPIndexSet"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_count"),new objj_ivar("_ranges")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("init"),function(_3,_4){
with(_3){
return objj_msgSend(_3,"initWithIndexesInRange:",{location:(0),length:0});
}
}),new objj_method(sel_getUid("initWithIndex:"),function(_5,_6,_7){
with(_5){
return objj_msgSend(_5,"initWithIndexesInRange:",{location:(_7),length:1});
}
}),new objj_method(sel_getUid("initWithIndexesInRange:"),function(_8,_9,_a){
with(_8){
_8=objj_msgSendSuper({receiver:_8,super_class:objj_getClass("CPIndexSet").super_class},"init");
if(_8){
_count=MAX(0,_a.length);
if(_count>0){
_ranges=[_a];
}else{
_ranges=[];
}
}
return _8;
}
}),new objj_method(sel_getUid("initWithIndexSet:"),function(_b,_c,_d){
with(_b){
_b=objj_msgSendSuper({receiver:_b,super_class:objj_getClass("CPIndexSet").super_class},"init");
if(_b){
_count=objj_msgSend(_d,"count");
_ranges=[];
var _e=_d._ranges,_f=_e.length;
while(_f--){
_ranges[_f]={location:(_e[_f]).location,length:(_e[_f]).length};
}
}
return _b;
}
}),new objj_method(sel_getUid("isEqual:"),function(_10,_11,_12){
with(_10){
if(_10===_12){
return YES;
}
if(!_12||!objj_msgSend(_12,"isKindOfClass:",objj_msgSend(CPIndexSet,"class"))){
return NO;
}
return objj_msgSend(_10,"isEqualToIndexSet:",_12);
}
}),new objj_method(sel_getUid("isEqualToIndexSet:"),function(_13,_14,_15){
with(_13){
if(!_15){
return NO;
}
if(_13===_15){
return YES;
}
var _16=_ranges.length,_17=_15._ranges;
if(_16!==_17.length||_count!==_15._count){
return NO;
}
while(_16--){
if(!CPEqualRanges(_ranges[_16],_17[_16])){
return NO;
}
}
return YES;
}
}),new objj_method(sel_getUid("isEqual:"),function(_18,_19,_1a){
with(_18){
return _18===_1a||objj_msgSend(_1a,"isKindOfClass:",objj_msgSend(_18,"class"))&&objj_msgSend(_18,"isEqualToIndexSet:",_1a);
}
}),new objj_method(sel_getUid("containsIndex:"),function(_1b,_1c,_1d){
with(_1b){
return _1e(_ranges,_1d)!==CPNotFound;
}
}),new objj_method(sel_getUid("containsIndexesInRange:"),function(_1f,_20,_21){
with(_1f){
if(_21.length<=0){
return NO;
}
if(_count<_21.length){
return NO;
}
var _22=_1e(_ranges,_21.location);
if(_22===CPNotFound){
return NO;
}
var _23=_ranges[_22];
return CPIntersectionRange(_23,_21).length===_21.length;
}
}),new objj_method(sel_getUid("containsIndexes:"),function(_24,_25,_26){
with(_24){
var _27=_26._count;
if(_27<=0){
return YES;
}
if(_count<_27){
return NO;
}
var _28=_26._ranges,_29=_28.length;
while(_29--){
if(!objj_msgSend(_24,"containsIndexesInRange:",_28[_29])){
return NO;
}
}
return YES;
}
}),new objj_method(sel_getUid("intersectsIndexesInRange:"),function(_2a,_2b,_2c){
with(_2a){
if(_count<=0){
return NO;
}
var _2d=_2e(_ranges,_2c.location);
if(FLOOR(_2d)===_2d){
return YES;
}
var _2f=_2e(_ranges,((_2c).location+(_2c).length)-1);
if(FLOOR(_2f)===_2f){
return YES;
}
return _2d!==_2f;
}
}),new objj_method(sel_getUid("count"),function(_30,_31){
with(_30){
return _count;
}
}),new objj_method(sel_getUid("firstIndex"),function(_32,_33){
with(_32){
if(_count>0){
return _ranges[0].location;
}
return CPNotFound;
}
}),new objj_method(sel_getUid("lastIndex"),function(_34,_35){
with(_34){
if(_count>0){
return ((_ranges[_ranges.length-1]).location+(_ranges[_ranges.length-1]).length)-1;
}
return CPNotFound;
}
}),new objj_method(sel_getUid("indexGreaterThanIndex:"),function(_36,_37,_38){
with(_36){
++_38;
var _39=_2e(_ranges,_38);
if(_39===CPNotFound){
return CPNotFound;
}
_39=CEIL(_39);
if(_39>=_ranges.length){
return CPNotFound;
}
var _3a=_ranges[_39];
if((((_38)>=(_3a).location)&&((_38)<((_3a).location+(_3a).length)))){
return _38;
}
return _3a.location;
}
}),new objj_method(sel_getUid("indexLessThanIndex:"),function(_3b,_3c,_3d){
with(_3b){
--_3d;
var _3e=_2e(_ranges,_3d);
if(_3e===CPNotFound){
return CPNotFound;
}
_3e=FLOOR(_3e);
if(_3e<0){
return CPNotFound;
}
var _3f=_ranges[_3e];
if((((_3d)>=(_3f).location)&&((_3d)<((_3f).location+(_3f).length)))){
return _3d;
}
return ((_3f).location+(_3f).length)-1;
}
}),new objj_method(sel_getUid("indexGreaterThanOrEqualToIndex:"),function(_40,_41,_42){
with(_40){
return objj_msgSend(_40,"indexGreaterThanIndex:",_42-1);
}
}),new objj_method(sel_getUid("indexLessThanOrEqualToIndex:"),function(_43,_44,_45){
with(_43){
return objj_msgSend(_43,"indexLessThanIndex:",_45+1);
}
}),new objj_method(sel_getUid("getIndexes:maxCount:inIndexRange:"),function(_46,_47,_48,_49,_4a){
with(_46){
if(!_count||_49===0||_4a&&!_4a.length){
if(_4a){
_4a.length=0;
}
return 0;
}
var _4b=0;
if(_4a){
var _4c=_4a.location,_4d=((_4a).location+(_4a).length)-1,_4e=CEIL(_2e(_ranges,_4c)),_4f=FLOOR(_2e(_ranges,_4d));
}else{
var _4c=objj_msgSend(_46,"firstIndex"),_4d=objj_msgSend(_46,"lastIndex"),_4e=0,_4f=_ranges.length-1;
}
while(_4e<=_4f){
var _50=_ranges[_4e],_51=MAX(_4c,_50.location),_52=MIN(_4d+1,((_50).location+(_50).length));
for(;_51<_52;++_51){
_48[_4b++]=_51;
if(_4b===_49){
if(_4a){
_4a.location=_51+1;
_4a.length=_4d+1-_51-1;
}
return _49;
}
}
++_4e;
}
if(_4a){
_4a.location=CPNotFound;
_4a.length=0;
}
return _4b;
}
}),new objj_method(sel_getUid("description"),function(_53,_54){
with(_53){
var _55=objj_msgSendSuper({receiver:_53,super_class:objj_getClass("CPIndexSet").super_class},"description");
if(_count){
var _56=0,_57=_ranges.length;
_55+="[number of indexes: "+_count+" (in "+_57;
if(_57===1){
_55+=" range), indexes: (";
}else{
_55+=" ranges), indexes: (";
}
for(;_56<_57;++_56){
var _58=_ranges[_56];
_55+=_58.location;
if(_58.length>1){
_55+="-"+(CPMaxRange(_58)-1);
}
if(_56+1<_57){
_55+=" ";
}
}
_55+=")]";
}else{
_55+="(no indexes)";
}
return _55;
}
}),new objj_method(sel_getUid("enumerateIndexesUsingBlock:"),function(_59,_5a,_5b){
with(_59){
objj_msgSend(_59,"enumerateIndexesWithOptions:usingBlock:",CPEnumerationNormal,_5b);
}
}),new objj_method(sel_getUid("enumerateIndexesWithOptions:usingBlock:"),function(_5c,_5d,_5e,_5f){
with(_5c){
if(!_count){
return;
}
objj_msgSend(_5c,"enumerateIndexesInRange:options:usingBlock:",CPMakeRange(0,((_ranges[_ranges.length-1]).location+(_ranges[_ranges.length-1]).length)),_5e,_5f);
}
}),new objj_method(sel_getUid("enumerateIndexesInRange:options:usingBlock:"),function(_60,_61,_62,_63,_64){
with(_60){
if(!_count||((_62).length===0)){
return;
}
var _65=NO,_66,_67,_68;
if(_63&CPEnumerationReverse){
_66=_ranges.length-1,_67=-1,_68=-1;
}else{
_66=0;
_67=_ranges.length;
_68=1;
}
for(;_66!==_67;_66+=_68){
var _69=_ranges[_66],_6a,_6b,_6c;
if(_63&CPEnumerationReverse){
_6a=((_69).location+(_69).length)-1;
_6b=_69.location-1;
_6c=-1;
}else{
_6a=_69.location;
_6b=((_69).location+(_69).length);
_6c=1;
}
for(;_6a!==_6b;_6a+=_6c){
if((((_6a)>=(_62).location)&&((_6a)<((_62).location+(_62).length)))){
_64(_6a,function(_6d){
if(arguments.length){
return _65=_6d;
}
return _65;
});
if(_65){
return;
}
}
}
}
}
}),new objj_method(sel_getUid("indexPassingTest:"),function(_6e,_6f,_70){
with(_6e){
return objj_msgSend(_6e,"indexWithOptions:passingTest:",CPEnumerationNormal,_70);
}
}),new objj_method(sel_getUid("indexesPassingTest:"),function(_71,_72,_73){
with(_71){
return objj_msgSend(_71,"indexesWithOptions:passingTest:",CPEnumerationNormal,_73);
}
}),new objj_method(sel_getUid("indexWithOptions:passingTest:"),function(_74,_75,_76,_77){
with(_74){
if(!_count){
return CPNotFound;
}
return objj_msgSend(_74,"indexInRange:options:passingTest:",{location:(0),length:((_ranges[_ranges.length-1]).location+(_ranges[_ranges.length-1]).length)},_76,_77);
}
}),new objj_method(sel_getUid("indexesWithOptions:passingTest:"),function(_78,_79,_7a,_7b){
with(_78){
if(!_count){
return objj_msgSend(CPIndexSet,"indexSet");
}
return objj_msgSend(_78,"indexesInRange:options:passingTest:",{location:(0),length:((_ranges[_ranges.length-1]).location+(_ranges[_ranges.length-1]).length)},_7a,_7b);
}
}),new objj_method(sel_getUid("indexInRange:options:passingTest:"),function(_7c,_7d,_7e,_7f,_80){
with(_7c){
if(!_count||((_7e).length===0)){
return CPNotFound;
}
var _81=NO,_82,_83,_84;
if(_7f&CPEnumerationReverse){
_82=_ranges.length-1,_83=-1,_84=-1;
}else{
_82=0;
_83=_ranges.length;
_84=1;
}
for(;_82!==_83;_82+=_84){
var _85=_ranges[_82],_86,_87,_88;
if(_7f&CPEnumerationReverse){
_86=((_85).location+(_85).length)-1;
_87=_85.location-1;
_88=-1;
}else{
_86=_85.location;
_87=((_85).location+(_85).length);
_88=1;
}
for(;_86!==_87;_86+=_88){
if((((_86)>=(_7e).location)&&((_86)<((_7e).location+(_7e).length)))){
if(_80(_86,function(_89){
if(arguments.length){
return _81=_89;
}
return _81;
})){
return _86;
}
if(_81){
return CPNotFound;
}
}
}
}
return CPNotFound;
}
}),new objj_method(sel_getUid("indexesInRange:options:passingTest:"),function(_8a,_8b,_8c,_8d,_8e){
with(_8a){
if(!_count||((_8c).length===0)){
return objj_msgSend(CPIndexSet,"indexSet");
}
var _8f=NO,_90,_91,_92;
if(_8d&CPEnumerationReverse){
_90=_ranges.length-1,_91=-1,_92=-1;
}else{
_90=0;
_91=_ranges.length;
_92=1;
}
var _93=objj_msgSend(CPMutableIndexSet,"indexSet");
for(;_90!==_91;_90+=_92){
var _94=_ranges[_90],_95,_96,_97;
if(_8d&CPEnumerationReverse){
_95=((_94).location+(_94).length)-1;
_96=_94.location-1;
_97=-1;
}else{
_95=_94.location;
_96=((_94).location+(_94).length);
_97=1;
}
for(;_95!==_96;_95+=_97){
if((((_95)>=(_8c).location)&&((_95)<((_8c).location+(_8c).length)))){
if(_8e(_95,function(_98){
if(arguments.length){
return _8f=_98;
}
return _8f;
})){
objj_msgSend(_93,"addIndex:",_95);
}
if(_8f){
return _93;
}
}
}
}
return _93;
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("indexSet"),function(_99,_9a){
with(_99){
return objj_msgSend(objj_msgSend(_99,"alloc"),"init");
}
}),new objj_method(sel_getUid("indexSetWithIndex:"),function(_9b,_9c,_9d){
with(_9b){
return objj_msgSend(objj_msgSend(_9b,"alloc"),"initWithIndex:",_9d);
}
}),new objj_method(sel_getUid("indexSetWithIndexesInRange:"),function(_9e,_9f,_a0){
with(_9e){
return objj_msgSend(objj_msgSend(_9e,"alloc"),"initWithIndexesInRange:",_a0);
}
})]);
var _1=objj_getClass("CPIndexSet");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPIndexSet\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("addIndex:"),function(_a1,_a2,_a3){
with(_a1){
objj_msgSend(_a1,"addIndexesInRange:",{location:(_a3),length:1});
}
}),new objj_method(sel_getUid("addIndexes:"),function(_a4,_a5,_a6){
with(_a4){
var _a7=_a6._ranges,_a8=_a7.length;
while(_a8--){
objj_msgSend(_a4,"addIndexesInRange:",_a7[_a8]);
}
}
}),new objj_method(sel_getUid("addIndexesInRange:"),function(_a9,_aa,_ab){
with(_a9){
if(_ab.length<=0){
return;
}
if(_count<=0){
_count=_ab.length;
_ranges=[_ab];
return;
}
var _ac=_ranges.length,_ad=_2e(_ranges,_ab.location-1),_ae=CEIL(_ad);
if(_ae===_ad&&_ae<_ac){
_ab=CPUnionRange(_ab,_ranges[_ae]);
}
var _af=_2e(_ranges,((_ab).location+(_ab).length)),_b0=FLOOR(_af);
if(_b0===_af&&_b0>=0){
_ab=CPUnionRange(_ab,_ranges[_b0]);
}
var _b1=_b0-_ae+1;
if(_b1===_ranges.length){
_ranges=[_ab];
_count=_ab.length;
}else{
if(_b1===1){
if(_ae<_ranges.length){
_count-=_ranges[_ae].length;
}
_count+=_ab.length;
_ranges[_ae]=_ab;
}else{
if(_b1>0){
var _b2=_ae,_b3=_ae+_b1-1;
for(;_b2<=_b3;++_b2){
_count-=_ranges[_b2].length;
}
objj_msgSend(_ranges,"removeObjectsInRange:",{location:(_ae),length:_b1});
}
objj_msgSend(_ranges,"insertObject:atIndex:",_ab,_ae);
_count+=_ab.length;
}
}
}
}),new objj_method(sel_getUid("removeIndex:"),function(_b4,_b5,_b6){
with(_b4){
objj_msgSend(_b4,"removeIndexesInRange:",{location:(_b6),length:1});
}
}),new objj_method(sel_getUid("removeIndexes:"),function(_b7,_b8,_b9){
with(_b7){
var _ba=_b9._ranges,_bb=_ba.length;
while(_bb--){
objj_msgSend(_b7,"removeIndexesInRange:",_ba[_bb]);
}
}
}),new objj_method(sel_getUid("removeAllIndexes"),function(_bc,_bd){
with(_bc){
_ranges=[];
_count=0;
}
}),new objj_method(sel_getUid("removeIndexesInRange:"),function(_be,_bf,_c0){
with(_be){
if(_c0.length<=0){
return;
}
if(_count<=0){
return;
}
var _c1=_ranges.length,_c2=_2e(_ranges,_c0.location),_c3=CEIL(_c2);
if(_c2===_c3&&_c3<_c1){
var _c4=_ranges[_c3];
if(_c0.location!==_c4.location){
var _c5=((_c0).location+(_c0).length),_c6=((_c4).location+(_c4).length);
_c4.length=_c0.location-_c4.location;
if(_c5<_c6){
_count-=_c0.length;
objj_msgSend(_ranges,"insertObject:atIndex:",{location:(_c5),length:_c6-_c5},_c3+1);
return;
}else{
_count-=_c6-_c0.location;
_c3+=1;
}
}
}
var _c7=_2e(_ranges,((_c0).location+(_c0).length)-1),_c8=FLOOR(_c7);
if(_c7===_c8&&_c8>=0){
var _c5=((_c0).location+(_c0).length),_c4=_ranges[_c8],_c6=((_c4).location+(_c4).length);
if(_c5!==_c6){
_count-=_c5-_c4.location;
_c8-=1;
_c4.location=_c5;
_c4.length=_c6-_c5;
}
}
var _c9=_c8-_c3+1;
if(_c9>0){
var _ca=_c3,_cb=_c3+_c9-1;
for(;_ca<=_cb;++_ca){
_count-=_ranges[_ca].length;
}
objj_msgSend(_ranges,"removeObjectsInRange:",{location:(_c3),length:_c9});
}
}
}),new objj_method(sel_getUid("shiftIndexesStartingAtIndex:by:"),function(_cc,_cd,_ce,_cf){
with(_cc){
if(!_count||_cf==0){
return;
}
var i=_ranges.length-1,_d0=CPMakeRange(CPNotFound,0);
for(;i>=0;--i){
var _d1=_ranges[i],_d2=((_d1).location+(_d1).length);
if(_ce>=_d2){
break;
}
if(_ce>_d1.location){
_d0=CPMakeRange(_ce+_cf,_d2-_ce);
_d1.length=_ce-_d1.location;
if(_cf>0){
objj_msgSend(_ranges,"insertObject:atIndex:",_d0,i+1);
}else{
if(_d0.location<0){
_d0.length=((_d0).location+(_d0).length);
_d0.location=0;
}
}
break;
}
if((_d1.location+=_cf)<0){
_count-=_d1.length-((_d1).location+(_d1).length);
_d1.length=((_d1).location+(_d1).length);
_d1.location=0;
}
}
if(_cf<0){
var j=i+1,_d3=_ranges.length,_d4=[];
for(;j<_d3;++j){
objj_msgSend(_d4,"addObject:",_ranges[j]);
_count-=_ranges[j].length;
}
if((j=i+1)<_d3){
objj_msgSend(_ranges,"removeObjectsInRange:",{location:(j),length:_d3-j});
for(j=0,_d3=_d4.length;j<_d3;++j){
objj_msgSend(_cc,"addIndexesInRange:",_d4[j]);
}
}
if(_d0.location!=CPNotFound){
objj_msgSend(_cc,"addIndexesInRange:",_d0);
}
}
}
})]);
var _d5="CPIndexSetCountKey",_d6="CPIndexSetRangeStringsKey";
var _1=objj_getClass("CPIndexSet");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPIndexSet\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("initWithCoder:"),function(_d7,_d8,_d9){
with(_d7){
_d7=objj_msgSendSuper({receiver:_d7,super_class:objj_getClass("CPIndexSet").super_class},"init");
if(_d7){
_count=objj_msgSend(_d9,"decodeIntForKey:",_d5);
_ranges=[];
var _da=objj_msgSend(_d9,"decodeObjectForKey:",_d6),_db=0,_dc=_da.length;
for(;_db<_dc;++_db){
_ranges.push(CPRangeFromString(_da[_db]));
}
}
return _d7;
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_dd,_de,_df){
with(_dd){
objj_msgSend(_df,"encodeInt:forKey:",_count,_d5);
var _e0=0,_e1=_ranges.length,_e2=[];
for(;_e0<_e1;++_e0){
_e2[_e0]=CPStringFromRange(_ranges[_e0]);
}
objj_msgSend(_df,"encodeObject:forKey:",_e2,_d6);
}
})]);
var _1=objj_getClass("CPIndexSet");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPIndexSet\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("copy"),function(_e3,_e4){
with(_e3){
return objj_msgSend(objj_msgSend(objj_msgSend(_e3,"class"),"alloc"),"initWithIndexSet:",_e3);
}
}),new objj_method(sel_getUid("mutableCopy"),function(_e5,_e6){
with(_e5){
return objj_msgSend(objj_msgSend(objj_msgSend(_e5,"class"),"alloc"),"initWithIndexSet:",_e5);
}
})]);
var _1=objj_allocateClassPair(CPIndexSet,"CPMutableIndexSet"),_2=_1.isa;
objj_registerClassPair(_1);
var _1e=function(_e7,_e8){
var low=0,_e9=_e7.length-1;
while(low<=_e9){
var _ea=FLOOR(low+(_e9-low)/2),_eb=_e7[_ea];
if(_e8<_eb.location){
_e9=_ea-1;
}else{
if(_e8>=((_eb).location+(_eb).length)){
low=_ea+1;
}else{
return _ea;
}
}
}
return CPNotFound;
};
var _2e=function(_ec,_ed){
var _ee=_ec.length;
if(_ee<=0){
return CPNotFound;
}
var low=0,_ef=_ee*2;
while(low<=_ef){
var _f0=FLOOR(low+(_ef-low)/2),_f1=_f0/2,_f2=FLOOR(_f1);
if(_f1===_f2){
if(_f2-1>=0&&_ed<((_ec[_f2-1]).location+(_ec[_f2-1]).length)){
_ef=_f0-1;
}else{
if(_f2<_ee&&_ed>=_ec[_f2].location){
low=_f0+1;
}else{
return _f2-0.5;
}
}
}else{
var _f3=_ec[_f2];
if(_ed<_f3.location){
_ef=_f0-1;
}else{
if(_ed>=((_f3).location+(_f3).length)){
low=_f0+1;
}else{
return _f2;
}
}
}
}
return CPNotFound;
};
p;8;CPNull.jt;731;@STATIC;1.0;i;10;CPObject.jt;698;
objj_executeFile("CPObject.j",YES);
var _1=nil;
var _2=objj_allocateClassPair(CPObject,"CPNull"),_3=_2.isa;
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("isEqual:"),function(_4,_5,_6){
with(_4){
if(_4===_6){
return YES;
}
return objj_msgSend(_6,"isKindOfClass:",objj_msgSend(CPNull,"class"));
}
}),new objj_method(sel_getUid("initWithCoder:"),function(_7,_8,_9){
with(_7){
return objj_msgSend(CPNull,"null");
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_a,_b,_c){
with(_a){
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("null"),function(_d,_e){
with(_d){
if(!_1){
_1=objj_msgSend(objj_msgSend(CPNull,"alloc"),"init");
}
return _1;
}
})]);
p;16;CPNotification.jt;1472;@STATIC;1.0;i;13;CPException.ji;10;CPObject.jt;1420;
objj_executeFile("CPException.j",YES);
objj_executeFile("CPObject.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPNotification"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_name"),new objj_ivar("_object"),new objj_ivar("_userInfo")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("init"),function(_3,_4){
with(_3){
objj_msgSend(CPException,"raise:reason:",CPUnsupportedMethodException,"CPNotification's init method should not be used");
}
}),new objj_method(sel_getUid("initWithName:object:userInfo:"),function(_5,_6,_7,_8,_9){
with(_5){
_5=objj_msgSendSuper({receiver:_5,super_class:objj_getClass("CPNotification").super_class},"init");
if(_5){
_name=_7;
_object=_8;
_userInfo=_9;
}
return _5;
}
}),new objj_method(sel_getUid("name"),function(_a,_b){
with(_a){
return _name;
}
}),new objj_method(sel_getUid("object"),function(_c,_d){
with(_c){
return _object;
}
}),new objj_method(sel_getUid("userInfo"),function(_e,_f){
with(_e){
return _userInfo;
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("notificationWithName:object:userInfo:"),function(_10,_11,_12,_13,_14){
with(_10){
return objj_msgSend(objj_msgSend(_10,"alloc"),"initWithName:object:userInfo:",_12,_13,_14);
}
}),new objj_method(sel_getUid("notificationWithName:object:"),function(_15,_16,_17,_18){
with(_15){
return objj_msgSend(objj_msgSend(_15,"alloc"),"initWithName:object:userInfo:",_17,_18,nil);
}
})]);
p;22;CPNotificationCenter.jt;6589;@STATIC;1.0;i;9;CPArray.ji;14;CPDictionary.ji;13;CPException.ji;16;CPNotification.ji;8;CPNull.jt;6487;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPException.j",YES);
objj_executeFile("CPNotification.j",YES);
objj_executeFile("CPNull.j",YES);
var _1=nil;
var _2=objj_allocateClassPair(CPObject,"CPNotificationCenter"),_3=_2.isa;
class_addIvars(_2,[new objj_ivar("_namedRegistries"),new objj_ivar("_unnamedRegistry")]);
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("init"),function(_4,_5){
with(_4){
_4=objj_msgSendSuper({receiver:_4,super_class:objj_getClass("CPNotificationCenter").super_class},"init");
if(_4){
_namedRegistries=objj_msgSend(CPDictionary,"dictionary");
_unnamedRegistry=objj_msgSend(objj_msgSend(_CPNotificationRegistry,"alloc"),"init");
}
return _4;
}
}),new objj_method(sel_getUid("addObserver:selector:name:object:"),function(_6,_7,_8,_9,_a,_b){
with(_6){
var _c,_d=objj_msgSend(objj_msgSend(_CPNotificationObserver,"alloc"),"initWithObserver:selector:",_8,_9);
if(_a==nil){
_c=_unnamedRegistry;
}else{
if(!(_c=objj_msgSend(_namedRegistries,"objectForKey:",_a))){
_c=objj_msgSend(objj_msgSend(_CPNotificationRegistry,"alloc"),"init");
objj_msgSend(_namedRegistries,"setObject:forKey:",_c,_a);
}
}
objj_msgSend(_c,"addObserver:object:",_d,_b);
}
}),new objj_method(sel_getUid("removeObserver:"),function(_e,_f,_10){
with(_e){
var _11=nil,_12=objj_msgSend(_namedRegistries,"keyEnumerator");
while((_11=objj_msgSend(_12,"nextObject"))!==nil){
objj_msgSend(objj_msgSend(_namedRegistries,"objectForKey:",_11),"removeObserver:object:",_10,nil);
}
objj_msgSend(_unnamedRegistry,"removeObserver:object:",_10,nil);
}
}),new objj_method(sel_getUid("removeObserver:name:object:"),function(_13,_14,_15,_16,_17){
with(_13){
if(_16==nil){
var _18=nil,_19=objj_msgSend(_namedRegistries,"keyEnumerator");
while((_18=objj_msgSend(_19,"nextObject"))!==nil){
objj_msgSend(objj_msgSend(_namedRegistries,"objectForKey:",_18),"removeObserver:object:",_15,_17);
}
objj_msgSend(_unnamedRegistry,"removeObserver:object:",_15,_17);
}else{
objj_msgSend(objj_msgSend(_namedRegistries,"objectForKey:",_16),"removeObserver:object:",_15,_17);
}
}
}),new objj_method(sel_getUid("postNotification:"),function(_1a,_1b,_1c){
with(_1a){
if(!_1c){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"postNotification: does not except 'nil' notifications");
}
_1d(_1a,_1c);
}
}),new objj_method(sel_getUid("postNotificationName:object:userInfo:"),function(_1e,_1f,_20,_21,_22){
with(_1e){
_1d(_1e,objj_msgSend(objj_msgSend(CPNotification,"alloc"),"initWithName:object:userInfo:",_20,_21,_22));
}
}),new objj_method(sel_getUid("postNotificationName:object:"),function(_23,_24,_25,_26){
with(_23){
_1d(_23,objj_msgSend(objj_msgSend(CPNotification,"alloc"),"initWithName:object:userInfo:",_25,_26,nil));
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("defaultCenter"),function(_27,_28){
with(_27){
if(!_1){
_1=objj_msgSend(objj_msgSend(CPNotificationCenter,"alloc"),"init");
}
return _1;
}
})]);
var _1d=function(_29,_2a){
objj_msgSend(_29._unnamedRegistry,"postNotification:",_2a);
objj_msgSend(objj_msgSend(_29._namedRegistries,"objectForKey:",objj_msgSend(_2a,"name")),"postNotification:",_2a);
};
var _2=objj_allocateClassPair(CPObject,"_CPNotificationRegistry"),_3=_2.isa;
class_addIvars(_2,[new objj_ivar("_objectObservers")]);
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("init"),function(_2b,_2c){
with(_2b){
_2b=objj_msgSendSuper({receiver:_2b,super_class:objj_getClass("_CPNotificationRegistry").super_class},"init");
if(_2b){
_objectObservers=objj_msgSend(CPDictionary,"dictionary");
}
return _2b;
}
}),new objj_method(sel_getUid("addObserver:object:"),function(_2d,_2e,_2f,_30){
with(_2d){
if(!_30){
_30=objj_msgSend(CPNull,"null");
}
var _31=objj_msgSend(_objectObservers,"objectForKey:",objj_msgSend(_30,"UID"));
if(!_31){
_31=objj_msgSend(CPMutableSet,"set");
objj_msgSend(_objectObservers,"setObject:forKey:",_31,objj_msgSend(_30,"UID"));
}
objj_msgSend(_31,"addObject:",_2f);
}
}),new objj_method(sel_getUid("removeObserver:object:"),function(_32,_33,_34,_35){
with(_32){
var _36=[];
if(_35==nil){
var key=nil,_37=objj_msgSend(_objectObservers,"keyEnumerator");
while((key=objj_msgSend(_37,"nextObject"))!==nil){
var _38=objj_msgSend(_objectObservers,"objectForKey:",key),_39=nil,_3a=objj_msgSend(_38,"objectEnumerator");
while((_39=objj_msgSend(_3a,"nextObject"))!==nil){
if(objj_msgSend(_39,"observer")==_34){
objj_msgSend(_38,"removeObject:",_39);
}
}
if(!objj_msgSend(_38,"count")){
_36.push(key);
}
}
}else{
var key=objj_msgSend(_35,"UID"),_38=objj_msgSend(_objectObservers,"objectForKey:",key),_39=nil,_3a=objj_msgSend(_38,"objectEnumerator");
while((_39=objj_msgSend(_3a,"nextObject"))!==nil){
if(objj_msgSend(_39,"observer")==_34){
objj_msgSend(_38,"removeObject:",_39);
}
}
if(!objj_msgSend(_38,"count")){
_36.push(key);
}
}
var _3b=_36.length;
while(_3b--){
objj_msgSend(_objectObservers,"removeObjectForKey:",_36[_3b]);
}
}
}),new objj_method(sel_getUid("postNotification:"),function(_3c,_3d,_3e){
with(_3c){
var _3f=objj_msgSend(_3e,"object"),_40=nil;
if(_3f!=nil&&(_40=objj_msgSend(_objectObservers,"objectForKey:",objj_msgSend(_3f,"UID")))){
var _41=objj_msgSend(_40,"copy"),_42=nil,_43=objj_msgSend(_41,"objectEnumerator");
while((_42=objj_msgSend(_43,"nextObject"))!==nil){
if(objj_msgSend(_40,"containsObject:",_42)){
objj_msgSend(_42,"postNotification:",_3e);
}
}
}
_40=objj_msgSend(_objectObservers,"objectForKey:",objj_msgSend(objj_msgSend(CPNull,"null"),"UID"));
if(!_40){
return;
}
var _41=objj_msgSend(_40,"copy"),_43=objj_msgSend(_41,"objectEnumerator");
while((_42=objj_msgSend(_43,"nextObject"))!==nil){
if(objj_msgSend(_40,"containsObject:",_42)){
objj_msgSend(_42,"postNotification:",_3e);
}
}
}
}),new objj_method(sel_getUid("count"),function(_44,_45){
with(_44){
return objj_msgSend(_objectObservers,"count");
}
})]);
var _2=objj_allocateClassPair(CPObject,"_CPNotificationObserver"),_3=_2.isa;
class_addIvars(_2,[new objj_ivar("_observer"),new objj_ivar("_selector")]);
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("initWithObserver:selector:"),function(_46,_47,_48,_49){
with(_46){
if(_46){
_observer=_48;
_selector=_49;
}
return _46;
}
}),new objj_method(sel_getUid("observer"),function(_4a,_4b){
with(_4a){
return _observer;
}
}),new objj_method(sel_getUid("postNotification:"),function(_4c,_4d,_4e){
with(_4c){
objj_msgSend(_observer,"performSelector:withObject:",_selector,_4e);
}
})]);
p;14;CPMutableSet.jt;2517;@STATIC;1.0;i;7;CPSet.ji;23;_CPConcreteMutableSet.jt;2459;
objj_executeFile("CPSet.j",YES);
var _1=objj_allocateClassPair(CPSet,"CPMutableSet"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithCapacity:"),function(_3,_4,_5){
with(_3){
return objj_msgSend(_3,"init");
}
}),new objj_method(sel_getUid("filterUsingPredicate:"),function(_6,_7,_8){
with(_6){
var _9,_a=objj_msgSend(_6,"objectEnumerator");
while((_9=objj_msgSend(_a,"nextObject"))!==nil){
if(!objj_msgSend(_8,"evaluateWithObject:",_9)){
objj_msgSend(_6,"removeObject:",_9);
}
}
}
}),new objj_method(sel_getUid("removeObject:"),function(_b,_c,_d){
with(_b){
_CPRaiseInvalidAbstractInvocation(_b,_c);
}
}),new objj_method(sel_getUid("removeObjectsInArray:"),function(_e,_f,_10){
with(_e){
var _11=0,_12=objj_msgSend(_10,"count");
for(;_11<_12;++_11){
objj_msgSend(_e,"removeObject:",objj_msgSend(_10,"objectAtIndex:",_11));
}
}
}),new objj_method(sel_getUid("removeAllObjects"),function(_13,_14){
with(_13){
var _15,_16=objj_msgSend(_13,"objectEnumerator");
while((_15=objj_msgSend(_16,"nextObject"))!==nil){
objj_msgSend(_13,"removeObject:",_15);
}
}
}),new objj_method(sel_getUid("addObjectsFromArray:"),function(_17,_18,_19){
with(_17){
var _1a=objj_msgSend(_19,"count");
while(_1a--){
objj_msgSend(_17,"addObject:",_19[_1a]);
}
}
}),new objj_method(sel_getUid("unionSet:"),function(_1b,_1c,_1d){
with(_1b){
var _1e,_1f=objj_msgSend(_1d,"objectEnumerator");
while((_1e=objj_msgSend(_1f,"nextObject"))!==nil){
objj_msgSend(_1b,"addObject:",_1e);
}
}
}),new objj_method(sel_getUid("minusSet:"),function(_20,_21,_22){
with(_20){
var _23,_24=objj_msgSend(_22,"objectEnumerator");
while((_23=objj_msgSend(_24,"nextObject"))!==nil){
objj_msgSend(_20,"removeObject:",_23);
}
}
}),new objj_method(sel_getUid("intersectSet:"),function(_25,_26,_27){
with(_25){
var _28,_29=objj_msgSend(_25,"objectEnumerator"),_2a=[];
while((_28=objj_msgSend(_29,"nextObject"))!==nil){
if(!objj_msgSend(_27,"containsObject:",_28)){
_2a.push(_28);
}
}
var _2b=objj_msgSend(_2a,"count");
while(_2b--){
objj_msgSend(_25,"removeObject:",_2a[_2b]);
}
}
}),new objj_method(sel_getUid("setSet:"),function(_2c,_2d,_2e){
with(_2c){
objj_msgSend(_2c,"removeAllObjects");
objj_msgSend(_2c,"unionSet:",_2e);
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("setWithCapacity:"),function(_2f,_30,_31){
with(_2f){
return objj_msgSend(objj_msgSend(_2f,"alloc"),"initWithCapacity:",_31);
}
})]);
objj_executeFile("_CPConcreteMutableSet.j",YES);
p;7;CPSet.jt;9420;@STATIC;1.0;i;9;CPArray.ji;14;CPEnumerator.ji;10;CPNumber.ji;10;CPObject.ji;14;CPMutableSet.jt;9320;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPEnumerator.j",YES);
objj_executeFile("CPNumber.j",YES);
objj_executeFile("CPObject.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPSet"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("setByAddingObject:"),function(_3,_4,_5){
with(_3){
return objj_msgSend(objj_msgSend(_3,"class"),"setWithArray:",objj_msgSend(objj_msgSend(_3,"allObjects"),"arrayByAddingObject:",_5));
}
}),new objj_method(sel_getUid("setByAddingObjectsFromSet:"),function(_6,_7,_8){
with(_6){
return objj_msgSend(_6,"setByAddingObjectsFromArray:",objj_msgSend(_8,"allObjects"));
}
}),new objj_method(sel_getUid("setByAddingObjectsFromArray:"),function(_9,_a,_b){
with(_9){
return objj_msgSend(objj_msgSend(_9,"class"),"setWithArray:",objj_msgSend(objj_msgSend(_9,"allObjects"),"arrayByAddingObjectsFromArray:",_b));
}
}),new objj_method(sel_getUid("init"),function(_c,_d){
with(_c){
return objj_msgSend(_c,"initWithObjects:count:",nil,0);
}
}),new objj_method(sel_getUid("initWithArray:"),function(_e,_f,_10){
with(_e){
return objj_msgSend(_e,"initWithObjects:count:",_10,objj_msgSend(_10,"count"));
}
}),new objj_method(sel_getUid("initWithObjects:"),function(_11,_12,_13){
with(_11){
var _14=2,_15=arguments.length;
for(;_14<_15;++_14){
if(arguments[_14]===nil){
break;
}
}
return objj_msgSend(_11,"initWithObjects:count:",Array.prototype.slice.call(arguments,2,_14),_14-2);
}
}),new objj_method(sel_getUid("initWithObjects:count:"),function(_16,_17,_18,_19){
with(_16){
if(_16===_1a){
return objj_msgSend(objj_msgSend(_CPConcreteMutableSet,"alloc"),"initWithObjects:count:",_18,_19);
}
return objj_msgSendSuper({receiver:_16,super_class:objj_getClass("CPSet").super_class},"init");
}
}),new objj_method(sel_getUid("initWithSet:"),function(_1b,_1c,_1d){
with(_1b){
return objj_msgSend(_1b,"initWithArray:",objj_msgSend(_1d,"allObjects"));
}
}),new objj_method(sel_getUid("initWithSet:copyItems:"),function(_1e,_1f,_20,_21){
with(_1e){
if(_21){
return objj_msgSend(_20,"valueForKey:","copy");
}
return objj_msgSend(_1e,"initWithSet:",_20);
}
}),new objj_method(sel_getUid("count"),function(_22,_23){
with(_22){
_CPRaiseInvalidAbstractInvocation(_22,_23);
}
}),new objj_method(sel_getUid("allObjects"),function(_24,_25){
with(_24){
var _26=[],_27,_28=objj_msgSend(_24,"objectEnumerator");
while((_27=objj_msgSend(_28,"nextObject"))!==nil){
_26.push(_27);
}
return _26;
}
}),new objj_method(sel_getUid("anyObject"),function(_29,_2a){
with(_29){
return objj_msgSend(objj_msgSend(_29,"objectEnumerator"),"nextObject");
}
}),new objj_method(sel_getUid("containsObject:"),function(_2b,_2c,_2d){
with(_2b){
return objj_msgSend(_2b,"member:",_2d)!==nil;
}
}),new objj_method(sel_getUid("filteredSetUsingPredicate:"),function(_2e,_2f,_30){
with(_2e){
var _31=[],_32,_33=objj_msgSend(_2e,"objectEnumerator");
while((_32=objj_msgSend(_33,"nextObject"))!==nil){
if(objj_msgSend(_30,"evaluateWithObject:",_32)){
_31.push(_32);
}
}
return objj_msgSend(objj_msgSend(objj_msgSend(_2e,"class"),"alloc"),"initWithArray:",_31);
}
}),new objj_method(sel_getUid("makeObjectsPerformSelector:"),function(_34,_35,_36){
with(_34){
objj_msgSend(_34,"makeObjectsPerformSelector:withObjects:",_36,nil);
}
}),new objj_method(sel_getUid("makeObjectsPerformSelector:withObject:"),function(_37,_38,_39,_3a){
with(_37){
objj_msgSend(_37,"makeObjectsPerformSelector:withObjects:",_39,[_3a]);
}
}),new objj_method(sel_getUid("makeObjectsPerformSelector:withObjects:"),function(_3b,_3c,_3d,_3e){
with(_3b){
var _3f,_40=objj_msgSend(_3b,"objectEnumerator"),_41=[nil,_3d].concat(_3e||[]);
while((_3f=objj_msgSend(_40,"nextObject"))!==nil){
_41[0]=_3f;
objj_msgSend.apply(this,_41);
}
}
}),new objj_method(sel_getUid("member:"),function(_42,_43,_44){
with(_42){
_CPRaiseInvalidAbstractInvocation(_42,_43);
}
}),new objj_method(sel_getUid("objectEnumerator"),function(_45,_46){
with(_45){
_CPRaiseInvalidAbstractInvocation(_45,_46);
}
}),new objj_method(sel_getUid("enumerateObjectsUsingBlock:"),function(_47,_48,_49){
with(_47){
var _4a,_4b=objj_msgSend(_47,"objectEnumerator");
while((_4a=objj_msgSend(_4b,"nextObject"))!==nil){
if(_49(_4a)){
break;
}
}
}
}),new objj_method(sel_getUid("objectsPassingTest:"),function(_4c,_4d,_4e){
with(_4c){
var _4f=[],_50=nil,_51=objj_msgSend(_4c,"objectEnumerator");
while((_50=objj_msgSend(_51,"nextObject"))!==nil){
if(_4e(_50)){
_4f.push(_50);
}
}
return objj_msgSend(objj_msgSend(objj_msgSend(_4c,"class"),"alloc"),"initWithArray:",_4f);
}
}),new objj_method(sel_getUid("isSubsetOfSet:"),function(_52,_53,_54){
with(_52){
var _55=nil,_56=objj_msgSend(_52,"objectEnumerator");
while((_55=objj_msgSend(_56,"nextObject"))!==nil){
if(!objj_msgSend(_54,"containsObject:",_55)){
return NO;
}
}
return YES;
}
}),new objj_method(sel_getUid("intersectsSet:"),function(_57,_58,_59){
with(_57){
if(_57===_59){
return objj_msgSend(_57,"count")>0;
}
var _5a=nil,_5b=objj_msgSend(_57,"objectEnumerator");
while((_5a=objj_msgSend(_5b,"nextObject"))!==nil){
if(objj_msgSend(_59,"containsObject:",_5a)){
return YES;
}
}
return NO;
}
}),new objj_method(sel_getUid("sortedArrayUsingDescriptors:"),function(_5c,_5d,_5e){
with(_5c){
return objj_msgSend(objj_msgSend(_5c,"allObjects"),"sortedArrayUsingDescriptors:",_5e);
}
}),new objj_method(sel_getUid("isEqualToSet:"),function(_5f,_60,_61){
with(_5f){
return objj_msgSend(_5f,"isEqual:",_61);
}
}),new objj_method(sel_getUid("isEqual:"),function(_62,_63,_64){
with(_62){
return _62===_64||objj_msgSend(_64,"isKindOfClass:",objj_msgSend(CPSet,"class"))&&(objj_msgSend(_62,"count")===objj_msgSend(_64,"count")&&objj_msgSend(_64,"isSubsetOfSet:",_62));
}
}),new objj_method(sel_getUid("description"),function(_65,_66){
with(_65){
var _67="{(\n",_68=objj_msgSend(_65,"allObjects"),_69=0,_6a=objj_msgSend(_68,"count");
for(;_69<_6a;++_69){
var _6b=_68[_69];
_67+="\t"+String(_6b).split("\n").join("\n\t")+"\n";
}
return _67+")}";
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("alloc"),function(_6c,_6d){
with(_6c){
if(_6c===objj_msgSend(CPSet,"class")||_6c===objj_msgSend(CPMutableSet,"class")){
return objj_msgSend(_CPPlaceholderSet,"alloc");
}
return objj_msgSendSuper({receiver:_6c,super_class:objj_getMetaClass("CPSet").super_class},"alloc");
}
}),new objj_method(sel_getUid("set"),function(_6e,_6f){
with(_6e){
return objj_msgSend(objj_msgSend(_6e,"alloc"),"init");
}
}),new objj_method(sel_getUid("setWithArray:"),function(_70,_71,_72){
with(_70){
return objj_msgSend(objj_msgSend(_70,"alloc"),"initWithArray:",_72);
}
}),new objj_method(sel_getUid("setWithObject:"),function(_73,_74,_75){
with(_73){
return objj_msgSend(objj_msgSend(_73,"alloc"),"initWithObjects:",_75);
}
}),new objj_method(sel_getUid("setWithObjects:count:"),function(_76,_77,_78,_79){
with(_76){
return objj_msgSend(objj_msgSend(_76,"alloc"),"initWithObjects:count:",_78,_79);
}
}),new objj_method(sel_getUid("setWithObjects:"),function(_7a,_7b,_7c){
with(_7a){
var _7d=Array.prototype.slice.apply(arguments);
_7d[0]=objj_msgSend(_7a,"alloc");
_7d[1]=sel_getUid("initWithObjects:");
return objj_msgSend.apply(this,_7d);
}
}),new objj_method(sel_getUid("setWithSet:"),function(_7e,_7f,set){
with(_7e){
return objj_msgSend(objj_msgSend(_7e,"alloc"),"initWithSet:",set);
}
})]);
var _1=objj_getClass("CPSet");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPSet\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("copy"),function(_80,_81){
with(_80){
return objj_msgSend(objj_msgSend(_80,"class"),"setWithSet:",_80);
}
}),new objj_method(sel_getUid("mutableCopy"),function(_82,_83){
with(_82){
return objj_msgSend(_82,"copy");
}
})]);
var _84="CPSetObjectsKey";
var _1=objj_getClass("CPSet");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPSet\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("initWithCoder:"),function(_85,_86,_87){
with(_85){
return objj_msgSend(_85,"initWithArray:",objj_msgSend(_87,"decodeObjectForKey:",_84));
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_88,_89,_8a){
with(_88){
objj_msgSend(_8a,"encodeObject:forKey:",objj_msgSend(_88,"allObjects"),_84);
}
})]);
var _1=objj_getClass("CPSet");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPSet\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("valueForKey:"),function(_8b,_8c,_8d){
with(_8b){
if(_8d==="@count"){
return objj_msgSend(_8b,"count");
}
var _8e=objj_msgSend(CPSet,"set"),_8f,_90=objj_msgSend(_8b,"objectEnumerator");
while((_8f=objj_msgSend(_90,"nextObject"))!==nil){
var _91=objj_msgSend(_8f,"valueForKey:",_8d);
objj_msgSend(_8e,"addObject:",_91);
}
return _8e;
}
}),new objj_method(sel_getUid("setValue:forKey:"),function(_92,_93,_94,_95){
with(_92){
var _96,_97=objj_msgSend(_92,"objectEnumerator");
while((_96=objj_msgSend(_97,"nextObject"))!==nil){
objj_msgSend(_96,"setValue:forKey:",_94,_95);
}
}
})]);
var _1a=nil;
var _1=objj_allocateClassPair(CPSet,"_CPPlaceholderSet"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_2,[new objj_method(sel_getUid("alloc"),function(_98,_99){
with(_98){
if(!_1a){
_1a=objj_msgSendSuper({receiver:_98,super_class:objj_getMetaClass("_CPPlaceholderSet").super_class},"alloc");
}
return _1a;
}
})]);
objj_executeFile("CPMutableSet.j",YES);
p;10;CPNumber.jt;6088;@STATIC;1.0;i;10;CPObject.ji;15;CPObjJRuntime.jt;6034;
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPObjJRuntime.j",YES);
var _1=new CFMutableDictionary();
var _2=objj_allocateClassPair(CPObject,"CPNumber"),_3=_2.isa;
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("initWithBool:"),function(_4,_5,_6){
with(_4){
return _6;
}
}),new objj_method(sel_getUid("initWithChar:"),function(_7,_8,_9){
with(_7){
if(_9.charCodeAt){
return _9.charCodeAt(0);
}
return _9;
}
}),new objj_method(sel_getUid("initWithDouble:"),function(_a,_b,_c){
with(_a){
return _c;
}
}),new objj_method(sel_getUid("initWithFloat:"),function(_d,_e,_f){
with(_d){
return _f;
}
}),new objj_method(sel_getUid("initWithInt:"),function(_10,_11,_12){
with(_10){
return _12;
}
}),new objj_method(sel_getUid("initWithLong:"),function(_13,_14,_15){
with(_13){
return _15;
}
}),new objj_method(sel_getUid("initWithLongLong:"),function(_16,_17,_18){
with(_16){
return _18;
}
}),new objj_method(sel_getUid("initWithShort:"),function(_19,_1a,_1b){
with(_19){
return _1b;
}
}),new objj_method(sel_getUid("initWithUnsignedChar:"),function(_1c,_1d,_1e){
with(_1c){
if(_1e.charCodeAt){
return _1e.charCodeAt(0);
}
return _1e;
}
}),new objj_method(sel_getUid("initWithUnsignedInt:"),function(_1f,_20,_21){
with(_1f){
return _21;
}
}),new objj_method(sel_getUid("initWithUnsignedLong:"),function(_22,_23,_24){
with(_22){
return _24;
}
}),new objj_method(sel_getUid("initWithUnsignedShort:"),function(_25,_26,_27){
with(_25){
return _27;
}
}),new objj_method(sel_getUid("UID"),function(_28,_29){
with(_28){
var UID=_1.valueForKey(_28);
if(!UID){
UID=objj_generateObjectUID();
_1.setValueForKey(_28,UID);
}
return UID+"";
}
}),new objj_method(sel_getUid("boolValue"),function(_2a,_2b){
with(_2a){
return _2a?true:false;
}
}),new objj_method(sel_getUid("charValue"),function(_2c,_2d){
with(_2c){
return String.fromCharCode(_2c);
}
}),new objj_method(sel_getUid("decimalValue"),function(_2e,_2f){
with(_2e){
throw new Error("decimalValue: NOT YET IMPLEMENTED");
}
}),new objj_method(sel_getUid("descriptionWithLocale:"),function(_30,_31,_32){
with(_30){
if(!_32){
return toString();
}
throw new Error("descriptionWithLocale: NOT YET IMPLEMENTED");
}
}),new objj_method(sel_getUid("description"),function(_33,_34){
with(_33){
return objj_msgSend(_33,"descriptionWithLocale:",nil);
}
}),new objj_method(sel_getUid("doubleValue"),function(_35,_36){
with(_35){
if(typeof _35=="boolean"){
return _35?1:0;
}
return _35;
}
}),new objj_method(sel_getUid("floatValue"),function(_37,_38){
with(_37){
if(typeof _37=="boolean"){
return _37?1:0;
}
return _37;
}
}),new objj_method(sel_getUid("intValue"),function(_39,_3a){
with(_39){
if(typeof _39=="boolean"){
return _39?1:0;
}
return _39;
}
}),new objj_method(sel_getUid("longLongValue"),function(_3b,_3c){
with(_3b){
if(typeof _3b=="boolean"){
return _3b?1:0;
}
return _3b;
}
}),new objj_method(sel_getUid("longValue"),function(_3d,_3e){
with(_3d){
if(typeof _3d=="boolean"){
return _3d?1:0;
}
return _3d;
}
}),new objj_method(sel_getUid("shortValue"),function(_3f,_40){
with(_3f){
if(typeof _3f=="boolean"){
return _3f?1:0;
}
return _3f;
}
}),new objj_method(sel_getUid("stringValue"),function(_41,_42){
with(_41){
return toString();
}
}),new objj_method(sel_getUid("unsignedCharValue"),function(_43,_44){
with(_43){
return String.fromCharCode(_43);
}
}),new objj_method(sel_getUid("unsignedIntValue"),function(_45,_46){
with(_45){
if(typeof _45=="boolean"){
return _45?1:0;
}
return _45;
}
}),new objj_method(sel_getUid("unsignedLongValue"),function(_47,_48){
with(_47){
if(typeof _47=="boolean"){
return _47?1:0;
}
return _47;
}
}),new objj_method(sel_getUid("unsignedShortValue"),function(_49,_4a){
with(_49){
if(typeof _49=="boolean"){
return _49?1:0;
}
return _49;
}
}),new objj_method(sel_getUid("compare:"),function(_4b,_4c,_4d){
with(_4b){
if(_4b>_4d){
return CPOrderedDescending;
}else{
if(_4b<_4d){
return CPOrderedAscending;
}
}
return CPOrderedSame;
}
}),new objj_method(sel_getUid("isEqualToNumber:"),function(_4e,_4f,_50){
with(_4e){
return _4e==_50;
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("alloc"),function(_51,_52){
with(_51){
var _53=new Number();
_53.isa=objj_msgSend(_51,"class");
return _53;
}
}),new objj_method(sel_getUid("numberWithBool:"),function(_54,_55,_56){
with(_54){
return _56;
}
}),new objj_method(sel_getUid("numberWithChar:"),function(_57,_58,_59){
with(_57){
if(_59.charCodeAt){
return _59.charCodeAt(0);
}
return _59;
}
}),new objj_method(sel_getUid("numberWithDouble:"),function(_5a,_5b,_5c){
with(_5a){
return _5c;
}
}),new objj_method(sel_getUid("numberWithFloat:"),function(_5d,_5e,_5f){
with(_5d){
return _5f;
}
}),new objj_method(sel_getUid("numberWithInt:"),function(_60,_61,_62){
with(_60){
return _62;
}
}),new objj_method(sel_getUid("numberWithLong:"),function(_63,_64,_65){
with(_63){
return _65;
}
}),new objj_method(sel_getUid("numberWithLongLong:"),function(_66,_67,_68){
with(_66){
return _68;
}
}),new objj_method(sel_getUid("numberWithShort:"),function(_69,_6a,_6b){
with(_69){
return _6b;
}
}),new objj_method(sel_getUid("numberWithUnsignedChar:"),function(_6c,_6d,_6e){
with(_6c){
if(_6e.charCodeAt){
return _6e.charCodeAt(0);
}
return _6e;
}
}),new objj_method(sel_getUid("numberWithUnsignedInt:"),function(_6f,_70,_71){
with(_6f){
return _71;
}
}),new objj_method(sel_getUid("numberWithUnsignedLong:"),function(_72,_73,_74){
with(_72){
return _74;
}
}),new objj_method(sel_getUid("numberWithUnsignedShort:"),function(_75,_76,_77){
with(_75){
return _77;
}
})]);
var _2=objj_getClass("CPNumber");
if(!_2){
throw new SyntaxError("*** Could not find definition for class \"CPNumber\"");
}
var _3=_2.isa;
class_addMethods(_2,[new objj_method(sel_getUid("initWithCoder:"),function(_78,_79,_7a){
with(_78){
return objj_msgSend(_7a,"decodeNumber");
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_7b,_7c,_7d){
with(_7b){
objj_msgSend(_7d,"encodeNumber:forKey:",_7b,"self");
}
})]);
Number.prototype.isa=CPNumber;
Boolean.prototype.isa=CPNumber;
objj_msgSend(CPNumber,"initialize");
p;23;_CPConcreteMutableSet.jt;2327;@STATIC;1.0;i;14;CPMutableSet.jt;2289;
objj_executeFile("CPMutableSet.j",YES);
var _1=Object.prototype.hasOwnProperty;
var _2=objj_allocateClassPair(CPMutableSet,"_CPConcreteMutableSet"),_3=_2.isa;
class_addIvars(_2,[new objj_ivar("_contents"),new objj_ivar("_count")]);
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("initWithObjects:count:"),function(_4,_5,_6,_7){
with(_4){
_4=objj_msgSendSuper({receiver:_4,super_class:objj_getClass("_CPConcreteMutableSet").super_class},"initWithObjects:count:",_6,_7);
if(_4){
_count=0;
_contents={};
var _8=0,_9=MIN(objj_msgSend(_6,"count"),_7);
for(;_8<_9;++_8){
objj_msgSend(_4,"addObject:",_6[_8]);
}
}
return _4;
}
}),new objj_method(sel_getUid("count"),function(_a,_b){
with(_a){
return _count;
}
}),new objj_method(sel_getUid("member:"),function(_c,_d,_e){
with(_c){
var _f=objj_msgSend(_e,"UID");
if(_1.call(_contents,_f)){
return _contents[_f];
}else{
for(var _10 in _contents){
if(!_1.call(_contents,_10)){
continue;
}
var _11=_contents[_10];
if(_11===_e||objj_msgSend(_11,"isEqual:",_e)){
return _11;
}
}
}
return nil;
}
}),new objj_method(sel_getUid("allObjects"),function(_12,_13){
with(_12){
var _14=[],_15;
for(_15 in _contents){
if(_1.call(_contents,_15)){
_14.push(_contents[_15]);
}
}
return _14;
}
}),new objj_method(sel_getUid("objectEnumerator"),function(_16,_17){
with(_16){
return objj_msgSend(objj_msgSend(_16,"allObjects"),"objectEnumerator");
}
}),new objj_method(sel_getUid("addObject:"),function(_18,_19,_1a){
with(_18){
if(_1a===nil||_1a===undefined){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"attempt to insert nil or undefined");
}
if(objj_msgSend(_18,"containsObject:",_1a)){
return;
}
_contents[objj_msgSend(_1a,"UID")]=_1a;
_count++;
}
}),new objj_method(sel_getUid("removeObject:"),function(_1b,_1c,_1d){
with(_1b){
if(_1d===nil||_1d===undefined){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"attempt to remove nil or undefined");
}
var _1e=objj_msgSend(_1b,"member:",_1d);
if(_1e!==nil){
delete _contents[objj_msgSend(_1e,"UID")];
_count--;
}
}
}),new objj_method(sel_getUid("removeAllObjects"),function(_1f,_20){
with(_1f){
_contents={};
_count=0;
}
}),new objj_method(sel_getUid("classForCoder"),function(_21,_22){
with(_21){
return objj_msgSend(CPSet,"class");
}
})]);
p;12;CPGeometry.jt;1467;@STATIC;1.0;i;13;_CGGeometry.jt;1430;
objj_executeFile("_CGGeometry.j",YES);
CPMinXEdge=0;
CPMinYEdge=1;
CPMaxXEdge=2;
CPMaxYEdge=3;
CPMakePoint=CGPointMake;
CPMakeSize=CGSizeMake;
CPMakeRect=CGRectMake;
CPPointCreateCopy=CGPointMakeCopy;
CPPointEqualToPoint=CGPointEqualToPoint;
CPRectEqualToRect=CGRectEqualToRect;
CPRectIsEmpty=CGRectIsEmpty;
CPRectContainsRect=CGRectContainsRect;
CPRectIntersection=CGRectIntersection;
CPPointMake=CGPointMake;
CPRectInset=CGRectInset;
CPRectIntegral=CGRectIntegral;
CPRectCreateCopy=CGRectCreateCopy;
CPRectMake=CGRectMake;
CPRectOffset=CGRectOffset;
CPRectStandardize=CGRectStandardize;
CPRectUnion=CGRectUnion;
CPSizeCreateCopy=CGSizeCreateCopy;
CPSizeMake=CGSizeMake;
CPRectContainsPoint=CGRectContainsPoint;
CPRectGetHeight=CGRectGetHeight;
CPRectGetMaxX=CGRectGetMaxX;
CPRectGetMaxY=CGRectGetMaxY;
CPRectGetMidX=CGRectGetMidX;
CPRectGetMidY=CGRectGetMidY;
CPRectGetMinX=CGRectGetMinX;
CPRectGetMinY=CGRectGetMinY;
CPRectGetWidth=CGRectGetWidth;
CPRectIntersectsRect=CGRectIntersectsRect;
CPRectIsNull=CGRectIsNull;
CPDivideRect=CGRectDivide;
CPSizeEqualToSize=CGSizeEqualToSize;
CPStringFromPoint=CGStringFromPoint;
CPStringFromSize=CGStringFromSize;
CPStringFromRect=CGStringFromRect;
CPPointFromString=CGPointFromString;
CPSizeFromString=CGSizeFromString;
CPRectFromString=CGRectFromString;
CPPointFromEvent=CGPointFromEvent;
CPSizeMakeZero=CGSizeMakeZero;
CPRectMakeZero=CGRectMakeZero;
CPPointMakeZero=CGPointMakeZero;
p;13;_CGGeometry.jt;7411;@STATIC;1.0;t;7392;
CGPointMake=function(x,y){
return {x:x,y:y};
};
CGPointMakeZero=function(){
return {x:0,y:0};
};
CGPointMakeCopy=function(_1){
return {x:_1.x,y:_1.y};
};
CGPointCreateCopy=function(_2){
return {x:_2.x,y:_2.y};
};
CGPointEqualToPoint=function(_3,_4){
return (_3.x==_4.x&&_3.y==_4.y);
};
CGStringFromPoint=function(_5){
return ("{"+_5.x+", "+_5.y+"}");
};
CGSizeMake=function(_6,_7){
return {width:_6,height:_7};
};
CGSizeMakeZero=function(){
return {width:0,height:0};
};
CGSizeMakeCopy=function(_8){
return {width:_8.width,height:_8.height};
};
CGSizeCreateCopy=function(_9){
return {width:_9.width,height:_9.height};
};
CGSizeEqualToSize=function(_a,_b){
return (_a.width==_b.width&&_a.height==_b.height);
};
CGStringFromSize=function(_c){
return ("{"+_c.width+", "+_c.height+"}");
};
CGRectMake=function(x,y,_d,_e){
return {origin:{x:x,y:y},size:{width:_d,height:_e}};
};
CGRectMakeZero=function(){
return {origin:{x:0,y:0},size:{width:0,height:0}};
};
CGRectMakeCopy=function(_f){
return {origin:{x:_f.origin.x,y:_f.origin.y},size:{width:_f.size.width,height:_f.size.height}};
};
CGRectCreateCopy=function(_10){
return {origin:{x:_10.origin.x,y:_10.origin.y},size:{width:_10.size.width,height:_10.size.height}};
};
CGRectEqualToRect=function(_11,_12){
return ((_11.origin.x==_12.origin.x&&_11.origin.y==_12.origin.y)&&(_11.size.width==_12.size.width&&_11.size.height==_12.size.height));
};
CGStringFromRect=function(_13){
return ("{"+("{"+_13.origin.x+", "+_13.origin.y+"}")+", "+("{"+_13.size.width+", "+_13.size.height+"}")+"}");
};
CGRectOffset=function(_14,dX,dY){
return {origin:{x:_14.origin.x+dX,y:_14.origin.y+dY},size:{width:_14.size.width,height:_14.size.height}};
};
CGRectInset=function(_15,dX,dY){
return {origin:{x:_15.origin.x+dX,y:_15.origin.y+dY},size:{width:_15.size.width-2*dX,height:_15.size.height-2*dY}};
};
CGRectGetHeight=function(_16){
return (_16.size.height);
};
CGRectGetMaxX=function(_17){
return (_17.origin.x+_17.size.width);
};
CGRectGetMaxY=function(_18){
return (_18.origin.y+_18.size.height);
};
CGRectGetMidX=function(_19){
return (_19.origin.x+(_19.size.width)/2);
};
CGRectGetMidY=function(_1a){
return (_1a.origin.y+(_1a.size.height)/2);
};
CGRectGetMinX=function(_1b){
return (_1b.origin.x);
};
CGRectGetMinY=function(_1c){
return (_1c.origin.y);
};
CGRectGetWidth=function(_1d){
return (_1d.size.width);
};
CGRectIsEmpty=function(_1e){
return (_1e.size.width<=0||_1e.size.height<=0);
};
CGRectIsNull=function(_1f){
return (_1f.size.width<=0||_1f.size.height<=0);
};
CGRectContainsPoint=function(_20,_21){
return (_21.x>=(_20.origin.x)&&_21.y>=(_20.origin.y)&&_21.x<(_20.origin.x+_20.size.width)&&_21.y<(_20.origin.y+_20.size.height));
};
CGInsetMake=function(top,_22,_23,_24){
return {top:(top),right:(_22),bottom:(_23),left:(_24)};
};
CGInsetMakeZero=function(){
return {top:(0),right:(0),bottom:(0),left:(0)};
};
CGInsetMakeCopy=function(_25){
return {top:(_25.top),right:(_25.right),bottom:(_25.bottom),left:(_25.left)};
};
CGInsetMakeInvertedCopy=function(_26){
return {top:(-_26.top),right:(-_26.right),bottom:(-_26.bottom),left:(-_26.left)};
};
CGInsetIsEmpty=function(_27){
return ((_27).top===0&&(_27).right===0&&(_27).bottom===0&&(_27).left===0);
};
CGInsetEqualToInset=function(_28,_29){
return ((_28).top===(_29).top&&(_28).right===(_29).right&&(_28).bottom===(_29).bottom&&(_28).left===(_29).left);
};
CGMinXEdge=0;
CGMinYEdge=1;
CGMaxXEdge=2;
CGMaxYEdge=3;
CGRectNull={origin:{x:Infinity,y:Infinity},size:{width:0,height:0}};
CGRectDivide=function(_2a,_2b,rem,_2c,_2d){
_2b.origin={x:_2a.origin.x,y:_2a.origin.y};
_2b.size={width:_2a.size.width,height:_2a.size.height};
rem.origin={x:_2a.origin.x,y:_2a.origin.y};
rem.size={width:_2a.size.width,height:_2a.size.height};
switch(_2d){
case CGMinXEdge:
_2b.size.width=_2c;
rem.origin.x+=_2c;
rem.size.width-=_2c;
break;
case CGMaxXEdge:
_2b.origin.x=(_2b.origin.x+_2b.size.width)-_2c;
_2b.size.width=_2c;
rem.size.width-=_2c;
break;
case CGMinYEdge:
_2b.size.height=_2c;
rem.origin.y+=_2c;
rem.size.height-=_2c;
break;
case CGMaxYEdge:
_2b.origin.y=(_2b.origin.y+_2b.size.height)-_2c;
_2b.size.height=_2c;
rem.size.height-=_2c;
}
};
CGRectContainsRect=function(_2e,_2f){
var _30=CGRectUnion(_2e,_2f);
return ((_30.origin.x==_2e.origin.x&&_30.origin.y==_2e.origin.y)&&(_30.size.width==_2e.size.width&&_30.size.height==_2e.size.height));
};
CGRectIntersectsRect=function(_31,_32){
var _33=CGRectIntersection(_31,_32);
return !(_33.size.width<=0||_33.size.height<=0);
};
CGRectIntegral=function(_34){
_34=CGRectStandardize(_34);
var x=FLOOR((_34.origin.x)),y=FLOOR((_34.origin.y));
_34.size.width=CEIL((_34.origin.x+_34.size.width))-x;
_34.size.height=CEIL((_34.origin.y+_34.size.height))-y;
_34.origin.x=x;
_34.origin.y=y;
return _34;
};
CGRectIntersection=function(_35,_36){
var _37={origin:{x:MAX((_35.origin.x),(_36.origin.x)),y:MAX((_35.origin.y),(_36.origin.y))},size:{width:0,height:0}};
_37.size.width=MIN((_35.origin.x+_35.size.width),(_36.origin.x+_36.size.width))-(_37.origin.x);
_37.size.height=MIN((_35.origin.y+_35.size.height),(_36.origin.y+_36.size.height))-(_37.origin.y);
return (_37.size.width<=0||_37.size.height<=0)?{origin:{x:0,y:0},size:{width:0,height:0}}:_37;
};
CGRectStandardize=function(_38){
var _39=(_38.size.width),_3a=(_38.size.height),_3b={origin:{x:_38.origin.x,y:_38.origin.y},size:{width:_38.size.width,height:_38.size.height}};
if(_39<0){
_3b.origin.x+=_39;
_3b.size.width=-_39;
}
if(_3a<0){
_3b.origin.y+=_3a;
_3b.size.height=-_3a;
}
return _3b;
};
CGRectUnion=function(_3c,_3d){
var _3e=!_3c||_3c===CGRectNull,_3f=!_3d||_3d===CGRectNull;
if(_3e){
return _3f?CGRectNull:_3d;
}
if(_3f){
return _3e?CGRectNull:_3c;
}
var _40=MIN((_3c.origin.x),(_3d.origin.x)),_41=MIN((_3c.origin.y),(_3d.origin.y)),_42=MAX((_3c.origin.x+_3c.size.width),(_3d.origin.x+_3d.size.width)),_43=MAX((_3c.origin.y+_3c.size.height),(_3d.origin.y+_3d.size.height));
return {origin:{x:_40,y:_41},size:{width:_42-_40,height:_43-_41}};
};
CGRectInsetByInset=function(_44,_45){
return {origin:{x:(_44).origin.x+(_45).left,y:(_44).origin.y+(_45).top},size:{width:(_44).size.width-(_45).left-(_45).right,height:(_44).size.height-(_45).top-(_45).bottom}};
};
CGPointFromString=function(_46){
var _47=_46.indexOf(",");
return {x:parseFloat(_46.substr(1,_47-1)),y:parseFloat(_46.substring(_47+1,_46.length))};
};
CGSizeFromString=function(_48){
var _49=_48.indexOf(",");
return {width:parseFloat(_48.substr(1,_49-1)),height:parseFloat(_48.substring(_49+1,_48.length))};
};
CGRectFromString=function(_4a){
var _4b=_4a.indexOf(",",_4a.indexOf(",")+1);
return {origin:CGPointFromString(_4a.substr(1,_4b-1)),size:CGSizeFromString(_4a.substring(_4b+2,_4a.length))};
};
CGPointFromEvent=function(_4c){
return {x:_4c.clientX,y:_4c.clientY};
};
CGInsetUnion=function(_4d,_4e){
return {top:(_4d.top+_4e.top),right:(_4d.right+_4e.right),bottom:(_4d.bottom+_4e.bottom),left:(_4d.left+_4e.left)};
};
CGInsetDifference=function(_4f,_50){
return {top:(_4f.top-_50.top),right:(_4f.right-_50.right),bottom:(_4f.bottom-_50.bottom),left:(_4f.left-_50.left)};
};
CGInsetFromString=function(_51){
var _52=_51.substr(1,_51.length-2).split(",");
return {top:(parseFloat(_52[0])),right:(parseFloat(_52[1])),bottom:(parseFloat(_52[2])),left:(parseFloat(_52[3]))};
};
CGInsetFromCPString=CGInsetFromString;
CPStringFromCGInset=function(_53){
return "{"+_53.top+", "+_53.left+", "+_53.bottom+", "+_53.right+"}";
};
p;13;CPFormatter.jt;1511;@STATIC;1.0;i;13;CPException.ji;10;CPObject.jt;1459;
objj_executeFile("CPException.j",YES);
objj_executeFile("CPObject.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPFormatter"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("stringForObjectValue:"),function(_3,_4,_5){
with(_3){
_CPRaiseInvalidAbstractInvocation(_3,_4);
return nil;
}
}),new objj_method(sel_getUid("editingStringForObjectValue:"),function(_6,_7,_8){
with(_6){
return objj_msgSend(_6,"stringForObjectValue:",_8);
}
}),new objj_method(sel_getUid("getObjectValue:forString:errorDescription:"),function(_9,_a,_b,_c,_d){
with(_9){
_CPRaiseInvalidAbstractInvocation(_9,_a);
return NO;
}
}),new objj_method(sel_getUid("isPartialStringValid:newEditingString:errorDescription:"),function(_e,_f,_10,_11,_12){
with(_e){
_10(nil);
if(_12){
_12(nil);
}
return YES;
}
}),new objj_method(sel_getUid("isPartialStringValid:proposedSelectedRange:originalString:originalSelectedRange:errorDescription:"),function(_13,_14,_15,_16,_17,_18,_19){
with(_13){
var _1a=nil,_1b=objj_msgSend(_13,"isPartialStringValid:newEditingString:errorDescription:",_15,function(_1c){
if(arguments.length){
return _1a=_1c;
}
return _1a;
},_19);
if(!_1b){
_15(_1a);
if(_1a!==nil){
_16(CPMakeRange(_1a.length,0));
}
}
return _1b;
}
}),new objj_method(sel_getUid("initWithCoder:"),function(_1d,_1e,_1f){
with(_1d){
return objj_msgSend(_1d,"init");
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_20,_21,_22){
with(_20){
}
})]);
p;11;CPRunLoop.jt;6982;@STATIC;1.0;i;9;CPArray.ji;8;CPDate.ji;10;CPObject.ji;10;CPString.jt;6908;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPDate.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPString.j",YES);
CPDefaultRunLoopMode="CPDefaultRunLoopMode";
_CPRunLoopPerformCompare=function(_1,_2){
return objj_msgSend(_2,"order")-objj_msgSend(_1,"order");
};
var _3=[],_4=5;
var _5=objj_allocateClassPair(CPObject,"_CPRunLoopPerform"),_6=_5.isa;
class_addIvars(_5,[new objj_ivar("_target"),new objj_ivar("_selector"),new objj_ivar("_argument"),new objj_ivar("_order"),new objj_ivar("_runLoopModes"),new objj_ivar("_isValid")]);
objj_registerClassPair(_5);
class_addMethods(_5,[new objj_method(sel_getUid("initWithSelector:target:argument:order:modes:"),function(_7,_8,_9,_a,_b,_c,_d){
with(_7){
_7=objj_msgSendSuper({receiver:_7,super_class:objj_getClass("_CPRunLoopPerform").super_class},"init");
if(_7){
_selector=_9;
_target=_a;
_argument=_b;
_order=_c;
_runLoopModes=_d;
_isValid=YES;
}
return _7;
}
}),new objj_method(sel_getUid("selector"),function(_e,_f){
with(_e){
return _selector;
}
}),new objj_method(sel_getUid("target"),function(_10,_11){
with(_10){
return _target;
}
}),new objj_method(sel_getUid("argument"),function(_12,_13){
with(_12){
return _argument;
}
}),new objj_method(sel_getUid("order"),function(_14,_15){
with(_14){
return _order;
}
}),new objj_method(sel_getUid("fireInMode:"),function(_16,_17,_18){
with(_16){
if(!_isValid){
return YES;
}
if(objj_msgSend(_runLoopModes,"containsObject:",_18)){
objj_msgSend(_target,"performSelector:withObject:",_selector,_argument);
return YES;
}
return NO;
}
}),new objj_method(sel_getUid("invalidate"),function(_19,_1a){
with(_19){
_isValid=NO;
}
})]);
class_addMethods(_6,[new objj_method(sel_getUid("_poolPerform:"),function(_1b,_1c,_1d){
with(_1b){
if(!_1d||_3.length>=_4){
return;
}
_3.push(_1d);
}
}),new objj_method(sel_getUid("performWithSelector:target:argument:order:modes:"),function(_1e,_1f,_20,_21,_22,_23,_24){
with(_1e){
if(_3.length){
var _25=_3.pop();
_25._target=_21;
_25._selector=_20;
_25._argument=_22;
_25._order=_23;
_25._runLoopModes=_24;
_25._isValid=YES;
return _25;
}
return objj_msgSend(objj_msgSend(_1e,"alloc"),"initWithSelector:target:argument:order:modes:",_20,_21,_22,_23,_24);
}
})]);
var _26=0;
var _5=objj_allocateClassPair(CPObject,"CPRunLoop"),_6=_5.isa;
class_addIvars(_5,[new objj_ivar("_runLoopLock"),new objj_ivar("_timersForModes"),new objj_ivar("_nativeTimersForModes"),new objj_ivar("_nextTimerFireDatesForModes"),new objj_ivar("_didAddTimer"),new objj_ivar("_effectiveDate"),new objj_ivar("_orderedPerforms"),new objj_ivar("_runLoopInsuranceTimer")]);
objj_registerClassPair(_5);
class_addMethods(_5,[new objj_method(sel_getUid("init"),function(_27,_28){
with(_27){
_27=objj_msgSendSuper({receiver:_27,super_class:objj_getClass("CPRunLoop").super_class},"init");
if(_27){
_orderedPerforms=[];
_timersForModes={};
_nativeTimersForModes={};
_nextTimerFireDatesForModes={};
}
return _27;
}
}),new objj_method(sel_getUid("performSelector:target:argument:order:modes:"),function(_29,_2a,_2b,_2c,_2d,_2e,_2f){
with(_29){
var _30=objj_msgSend(_CPRunLoopPerform,"performWithSelector:target:argument:order:modes:",_2b,_2c,_2d,_2e,_2f),_31=_orderedPerforms.length;
while(_31--){
if(_2e<objj_msgSend(_orderedPerforms[_31],"order")){
break;
}
}
_orderedPerforms.splice(_31+1,0,_30);
}
}),new objj_method(sel_getUid("cancelPerformSelector:target:argument:"),function(_32,_33,_34,_35,_36){
with(_32){
var _37=_orderedPerforms.length;
while(_37--){
var _38=_orderedPerforms[_37];
if(objj_msgSend(_38,"selector")===_34&&objj_msgSend(_38,"target")==_35&&objj_msgSend(_38,"argument")==_36){
objj_msgSend(_orderedPerforms[_37],"invalidate");
}
}
}
}),new objj_method(sel_getUid("performSelectors"),function(_39,_3a){
with(_39){
objj_msgSend(_39,"limitDateForMode:",CPDefaultRunLoopMode);
}
}),new objj_method(sel_getUid("addTimer:forMode:"),function(_3b,_3c,_3d,_3e){
with(_3b){
if(_timersForModes[_3e]){
_timersForModes[_3e].push(_3d);
}else{
_timersForModes[_3e]=[_3d];
}
_didAddTimer=YES;
if(!_3d._lastNativeRunLoopsForModes){
_3d._lastNativeRunLoopsForModes={};
}
_3d._lastNativeRunLoopsForModes[_3e]=_26;
if(objj_msgSend(CFBundle.environments(),"indexOfObject:",("Browser"))!==CPNotFound){
if(!_runLoopInsuranceTimer){
_runLoopInsuranceTimer=window.setNativeTimeout(function(){
objj_msgSend(_3b,"limitDateForMode:",CPDefaultRunLoopMode);
},0);
}
}
}
}),new objj_method(sel_getUid("limitDateForMode:"),function(_3f,_40,_41){
with(_3f){
if(_runLoopLock){
return;
}
_runLoopLock=YES;
if(objj_msgSend(CFBundle.environments(),"indexOfObject:",("Browser"))!==CPNotFound){
if(_runLoopInsuranceTimer){
window.clearNativeTimeout(_runLoopInsuranceTimer);
_runLoopInsuranceTimer=nil;
}
}
var now=_effectiveDate?objj_msgSend(_effectiveDate,"laterDate:",objj_msgSend(CPDate,"date")):objj_msgSend(CPDate,"date"),_42=nil,_43=_nextTimerFireDatesForModes[_41];
if(_didAddTimer||_43&&_43<=now){
_didAddTimer=NO;
if(_nativeTimersForModes[_41]!==nil){
window.clearNativeTimeout(_nativeTimersForModes[_41]);
_nativeTimersForModes[_41]=nil;
}
var _44=_timersForModes[_41],_45=_44.length;
_timersForModes[_41]=nil;
while(_45--){
var _46=_44[_45];
if(_46._lastNativeRunLoopsForModes[_41]<_26&&_46._isValid&&_46._fireDate<=now){
objj_msgSend(_46,"fire");
}
if(_46._isValid){
_42=(_42===nil)?_46._fireDate:objj_msgSend(_42,"earlierDate:",_46._fireDate);
}else{
_46._lastNativeRunLoopsForModes[_41]=0;
_44.splice(_45,1);
}
}
var _47=_timersForModes[_41];
if(_47&&_47.length){
_45=_47.length;
while(_45--){
var _46=_47[_45];
if(objj_msgSend(_46,"isValid")){
_42=(_42===nil)?_46._fireDate:objj_msgSend(_42,"earlierDate:",_46._fireDate);
}else{
_47.splice(_45,1);
}
}
_timersForModes[_41]=_47.concat(_44);
}else{
_timersForModes[_41]=_44;
}
_nextTimerFireDatesForModes[_41]=_42;
if(_nextTimerFireDatesForModes[_41]!==nil){
_nativeTimersForModes[_41]=window.setNativeTimeout(function(){
_effectiveDate=_42;
_nativeTimersForModes[_41]=nil;
++_26;
objj_msgSend(_3f,"limitDateForMode:",_41);
_effectiveDate=nil;
},MAX(0,objj_msgSend(_42,"timeIntervalSinceNow")*1000));
}
}
var _48=_orderedPerforms,_45=_48.length;
_orderedPerforms=[];
while(_45--){
var _49=_48[_45];
if(objj_msgSend(_49,"fireInMode:",CPDefaultRunLoopMode)){
objj_msgSend(_CPRunLoopPerform,"_poolPerform:",_49);
_48.splice(_45,1);
}
}
if(_orderedPerforms.length){
_orderedPerforms=_orderedPerforms.concat(_48);
_orderedPerforms.sort(_CPRunLoopPerformCompare);
}else{
_orderedPerforms=_48;
}
_runLoopLock=NO;
return _42;
}
})]);
class_addMethods(_6,[new objj_method(sel_getUid("initialize"),function(_4a,_4b){
with(_4a){
if(_4a!==objj_msgSend(CPRunLoop,"class")){
return;
}
CPMainRunLoop=objj_msgSend(objj_msgSend(CPRunLoop,"alloc"),"init");
}
}),new objj_method(sel_getUid("currentRunLoop"),function(_4c,_4d){
with(_4c){
return CPMainRunLoop;
}
}),new objj_method(sel_getUid("mainRunLoop"),function(_4e,_4f){
with(_4e){
return CPMainRunLoop;
}
})]);
p;8;CPDate.jt;5957;@STATIC;1.0;i;10;CPObject.ji;10;CPString.ji;13;CPException.jt;5890;
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPString.j",YES);
objj_executeFile("CPException.j",YES);
var _1=new Date(Date.UTC(2001,0,1,0,0,0,0));
var _2=objj_allocateClassPair(CPObject,"CPDate"),_3=_2.isa;
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("initWithTimeIntervalSinceNow:"),function(_4,_5,_6){
with(_4){
_4=new Date((new Date()).getTime()+_6*1000);
return _4;
}
}),new objj_method(sel_getUid("initWithTimeIntervalSince1970:"),function(_7,_8,_9){
with(_7){
_7=new Date(_9*1000);
return _7;
}
}),new objj_method(sel_getUid("initWithTimeIntervalSinceReferenceDate:"),function(_a,_b,_c){
with(_a){
_a=objj_msgSend(_a,"initWithTimeInterval:sinceDate:",_c,_1);
return _a;
}
}),new objj_method(sel_getUid("initWithTimeInterval:sinceDate:"),function(_d,_e,_f,_10){
with(_d){
_d=new Date(_10.getTime()+_f*1000);
return _d;
}
}),new objj_method(sel_getUid("initWithString:"),function(_11,_12,_13){
with(_11){
var _14=/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}) ([-+])(\d{2})(\d{2})/,d=_13.match(new RegExp(_14));
if(!d||d.length!=10){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"initWithString: the string must be in YYYY-MM-DD HH:MM:SS ±HHMM format");
}
var _15=new Date(d[1],d[2]-1,d[3]),_16=(Number(d[8])*60+Number(d[9]))*(d[7]==="-"?1:-1);
_15.setHours(d[4]);
_15.setMinutes(d[5]);
_15.setSeconds(d[6]);
_11=new Date(_15.getTime()+(_16-_15.getTimezoneOffset())*60*1000);
return _11;
}
}),new objj_method(sel_getUid("timeIntervalSinceDate:"),function(_17,_18,_19){
with(_17){
return (_17.getTime()-_19.getTime())/1000;
}
}),new objj_method(sel_getUid("timeIntervalSinceNow"),function(_1a,_1b){
with(_1a){
return objj_msgSend(_1a,"timeIntervalSinceDate:",objj_msgSend(CPDate,"date"));
}
}),new objj_method(sel_getUid("timeIntervalSince1970"),function(_1c,_1d){
with(_1c){
return _1c.getTime()/1000;
}
}),new objj_method(sel_getUid("timeIntervalSinceReferenceDate"),function(_1e,_1f){
with(_1e){
return (_1e.getTime()-_1.getTime())/1000;
}
}),new objj_method(sel_getUid("isEqual:"),function(_20,_21,_22){
with(_20){
if(_20===_22){
return YES;
}
if(!_22||!objj_msgSend(_22,"isKindOfClass:",objj_msgSend(CPDate,"class"))){
return NO;
}
return objj_msgSend(_20,"isEqualToDate:",_22);
}
}),new objj_method(sel_getUid("isEqualToDate:"),function(_23,_24,_25){
with(_23){
if(!_25){
return NO;
}
return !(_23<_25||_23>_25);
}
}),new objj_method(sel_getUid("compare:"),function(_26,_27,_28){
with(_26){
return (_26>_28)?CPOrderedDescending:((_26<_28)?CPOrderedAscending:CPOrderedSame);
}
}),new objj_method(sel_getUid("earlierDate:"),function(_29,_2a,_2b){
with(_29){
return (_29<_2b)?_29:_2b;
}
}),new objj_method(sel_getUid("laterDate:"),function(_2c,_2d,_2e){
with(_2c){
return (_2c>_2e)?_2c:_2e;
}
}),new objj_method(sel_getUid("description"),function(_2f,_30){
with(_2f){
return objj_msgSend(CPString,"stringWithFormat:","%04d-%02d-%02d %02d:%02d:%02d %s",_2f.getFullYear(),_2f.getMonth()+1,_2f.getDate(),_2f.getHours(),_2f.getMinutes(),_2f.getSeconds(),objj_msgSend(CPDate,"timezoneOffsetString:",_2f.getTimezoneOffset()));
}
}),new objj_method(sel_getUid("copy"),function(_31,_32){
with(_31){
return new Date(_31.getTime());
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("alloc"),function(_33,_34){
with(_33){
var _35=new Date;
_35.isa=objj_msgSend(_33,"class");
return _35;
}
}),new objj_method(sel_getUid("date"),function(_36,_37){
with(_36){
return objj_msgSend(objj_msgSend(_36,"alloc"),"init");
}
}),new objj_method(sel_getUid("dateWithTimeIntervalSinceNow:"),function(_38,_39,_3a){
with(_38){
return objj_msgSend(objj_msgSend(CPDate,"alloc"),"initWithTimeIntervalSinceNow:",_3a);
}
}),new objj_method(sel_getUid("dateWithTimeIntervalSince1970:"),function(_3b,_3c,_3d){
with(_3b){
return objj_msgSend(objj_msgSend(CPDate,"alloc"),"initWithTimeIntervalSince1970:",_3d);
}
}),new objj_method(sel_getUid("dateWithTimeIntervalSinceReferenceDate:"),function(_3e,_3f,_40){
with(_3e){
return objj_msgSend(objj_msgSend(CPDate,"alloc"),"initWithTimeIntervalSinceReferenceDate:",_40);
}
}),new objj_method(sel_getUid("distantPast"),function(_41,_42){
with(_41){
return objj_msgSend(CPDate,"dateWithTimeIntervalSinceReferenceDate:",-63113817600);
}
}),new objj_method(sel_getUid("distantFuture"),function(_43,_44){
with(_43){
return objj_msgSend(CPDate,"dateWithTimeIntervalSinceReferenceDate:",63113990400);
}
}),new objj_method(sel_getUid("timeIntervalSinceReferenceDate"),function(_45,_46){
with(_45){
return objj_msgSend(objj_msgSend(CPDate,"date"),"timeIntervalSinceReferenceDate");
}
}),new objj_method(sel_getUid("timezoneOffsetString:"),function(_47,_48,_49){
with(_47){
var _4a=-_49,_4b=_4a>=0,_4c=_4b?FLOOR(_4a/60):CEIL(_4a/60),_4d=_4a-_4c*60;
return objj_msgSend(CPString,"stringWithFormat:","%s%02d%02d",_4b?"+":"-",ABS(_4c),ABS(_4d));
}
})]);
var _4e="CPDateTimeKey";
var _2=objj_getClass("CPDate");
if(!_2){
throw new SyntaxError("*** Could not find definition for class \"CPDate\"");
}
var _3=_2.isa;
class_addMethods(_2,[new objj_method(sel_getUid("initWithCoder:"),function(_4f,_50,_51){
with(_4f){
if(_4f){
_4f.setTime(objj_msgSend(_51,"decodeIntForKey:",_4e));
}
return _4f;
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_52,_53,_54){
with(_52){
objj_msgSend(_54,"encodeInt:forKey:",_52.getTime(),_4e);
}
})]);
var _55=[1,4,5,6,7,10,11];
Date.parseISO8601=function(_56){
var _57,_58,_59=0;
_57=Date.parse(_56);
if(isNaN(_57)&&(_58=/^(\d{4}|[+\-]\d{6})(?:-(\d{2})(?:-(\d{2}))?)?(?:T(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3}))?)?(?:(Z)|([+\-])(\d{2})(?::(\d{2}))?)?)?$/.exec(_56))){
for(var i=0,k;(k=_55[i]);++i){
_58[k]=+_58[k]||0;
}
_58[2]=(+_58[2]||1)-1;
_58[3]=+_58[3]||1;
if(_58[8]!=="Z"&&_58[9]!==undefined){
_59=_58[10]*60+_58[11];
if(_58[9]==="+"){
_59=0-_59;
}
}
return Date.UTC(_58[1],_58[2],_58[3],_58[4],_58[5]+_59,_58[6],_58[7]);
}
return _57;
};
Date.prototype.isa=CPDate;
p;8;CPData.jt;4872;@STATIC;1.0;i;10;CPObject.ji;10;CPString.jt;4823;
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPString.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPData"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithRawString:"),function(_3,_4,_5){
with(_3){
_3=objj_msgSendSuper({receiver:_3,super_class:objj_getClass("CPData").super_class},"init");
if(_3){
objj_msgSend(_3,"setRawString:",_5);
}
return _3;
}
}),new objj_method(sel_getUid("initWithPlistObject:"),function(_6,_7,_8){
with(_6){
_6=objj_msgSendSuper({receiver:_6,super_class:objj_getClass("CPData").super_class},"init");
if(_6){
objj_msgSend(_6,"setPlistObject:",_8);
}
return _6;
}
}),new objj_method(sel_getUid("initWithPlistObject:format:"),function(_9,_a,_b,_c){
with(_9){
_9=objj_msgSendSuper({receiver:_9,super_class:objj_getClass("CPData").super_class},"init");
if(_9){
objj_msgSend(_9,"setPlistObject:format:",_b,_c);
}
return _9;
}
}),new objj_method(sel_getUid("initWithJSONObject:"),function(_d,_e,_f){
with(_d){
_d=objj_msgSendSuper({receiver:_d,super_class:objj_getClass("CPData").super_class},"init");
if(_d){
objj_msgSend(_d,"setJSONObject:",_f);
}
return _d;
}
}),new objj_method(sel_getUid("rawString"),function(_10,_11){
with(_10){
return _10.rawString();
}
}),new objj_method(sel_getUid("plistObject"),function(_12,_13){
with(_12){
return _12.propertyList();
}
}),new objj_method(sel_getUid("JSONObject"),function(_14,_15){
with(_14){
return _14.JSONObject();
}
}),new objj_method(sel_getUid("bytes"),function(_16,_17){
with(_16){
return _16.bytes();
}
}),new objj_method(sel_getUid("base64"),function(_18,_19){
with(_18){
return _18.base64();
}
}),new objj_method(sel_getUid("length"),function(_1a,_1b){
with(_1a){
return objj_msgSend(objj_msgSend(_1a,"rawString"),"length");
}
}),new objj_method(sel_getUid("description"),function(_1c,_1d){
with(_1c){
return _1c.toString();
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("alloc"),function(_1e,_1f){
with(_1e){
var _20=new CFMutableData();
_20.isa=objj_msgSend(_1e,"class");
return _20;
}
}),new objj_method(sel_getUid("data"),function(_21,_22){
with(_21){
return objj_msgSend(objj_msgSend(_21,"alloc"),"init");
}
}),new objj_method(sel_getUid("dataWithRawString:"),function(_23,_24,_25){
with(_23){
return objj_msgSend(objj_msgSend(_23,"alloc"),"initWithRawString:",_25);
}
}),new objj_method(sel_getUid("dataWithPlistObject:"),function(_26,_27,_28){
with(_26){
return objj_msgSend(objj_msgSend(_26,"alloc"),"initWithPlistObject:",_28);
}
}),new objj_method(sel_getUid("dataWithPlistObject:format:"),function(_29,_2a,_2b,_2c){
with(_29){
return objj_msgSend(objj_msgSend(_29,"alloc"),"initWithPlistObject:format:",_2b,_2c);
}
}),new objj_method(sel_getUid("dataWithJSONObject:"),function(_2d,_2e,_2f){
with(_2d){
return objj_msgSend(objj_msgSend(_2d,"alloc"),"initWithJSONObject:",_2f);
}
}),new objj_method(sel_getUid("dataWithBytes:"),function(_30,_31,_32){
with(_30){
var _33=objj_msgSend(objj_msgSend(_30,"alloc"),"init");
_33.setBytes(_32);
return _33;
}
}),new objj_method(sel_getUid("dataWithBase64:"),function(_34,_35,_36){
with(_34){
var _37=objj_msgSend(objj_msgSend(_34,"alloc"),"init");
_37.setBase64String(_36);
return _37;
}
})]);
var _1=objj_getClass("CPData");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPData\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("setRawString:"),function(_38,_39,_3a){
with(_38){
_38.setRawString(_3a);
}
}),new objj_method(sel_getUid("setPlistObject:"),function(_3b,_3c,_3d){
with(_3b){
_3b.setPropertyList(_3d);
}
}),new objj_method(sel_getUid("setPlistObject:format:"),function(_3e,_3f,_40,_41){
with(_3e){
_3e.setPropertyList(_40,_41);
}
}),new objj_method(sel_getUid("setJSONObject:"),function(_42,_43,_44){
with(_42){
_42.setJSONObject(_44);
}
})]);
var _1=objj_getClass("CPData");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPData\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("initWithString:"),function(_45,_46,_47){
with(_45){
_CPReportLenientDeprecation(_45,_46,sel_getUid("initWithRawString:"));
return objj_msgSend(_45,"initWithRawString:",_47);
}
}),new objj_method(sel_getUid("setString:"),function(_48,_49,_4a){
with(_48){
_CPReportLenientDeprecation(_48,_49,sel_getUid("setRawString:"));
objj_msgSend(_48,"setRawString:",_4a);
}
}),new objj_method(sel_getUid("string"),function(_4b,_4c){
with(_4b){
_CPReportLenientDeprecation(_4b,_4c,sel_getUid("rawString"));
return objj_msgSend(_4b,"rawString");
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("dataWithString:"),function(_4d,_4e,_4f){
with(_4d){
_CPReportLenientDeprecation(_4d,_4e,sel_getUid("dataWithRawString:"));
return objj_msgSend(_4d,"dataWithRawString:",_4f);
}
})]);
CFData.prototype.isa=CPData;
CFMutableData.prototype.isa=CPData;
p;19;CPKeyedUnarchiver.jt;8619;@STATIC;1.0;i;9;CPArray.ji;9;CPCoder.ji;8;CPData.ji;14;CPDictionary.ji;13;CPException.ji;17;CPKeyedArchiver.ji;8;CPNull.ji;10;CPNumber.ji;10;CPString.jt;8461;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPCoder.j",YES);
objj_executeFile("CPData.j",YES);
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPException.j",YES);
objj_executeFile("CPKeyedArchiver.j",YES);
objj_executeFile("CPNull.j",YES);
objj_executeFile("CPNumber.j",YES);
objj_executeFile("CPString.j",YES);
CPInvalidUnarchiveOperationException="CPInvalidUnarchiveOperationException";
var _1=1<<0,_2=1<<1,_3=1<<2,_4=1<<3,_5=1<<4,_6=1<<5;
var _7="$null",_8="CP$UID",_9="$top",_a="$objects",_b="$archiver",_c="$version",_d="$classname",_e="$classes",_f="$class";
var _10=Nil,_11=Nil,_12=Nil,_13=Nil,_14=Nil,_15=Nil,_16=Nil,_17=Nil;
var _18=objj_allocateClassPair(CPCoder,"CPKeyedUnarchiver"),_19=_18.isa;
class_addIvars(_18,[new objj_ivar("_delegate"),new objj_ivar("_delegateSelectors"),new objj_ivar("_data"),new objj_ivar("_replacementClasses"),new objj_ivar("_objects"),new objj_ivar("_archive"),new objj_ivar("_plistObject"),new objj_ivar("_plistObjects")]);
objj_registerClassPair(_18);
class_addMethods(_18,[new objj_method(sel_getUid("initForReadingWithData:"),function(_1a,_1b,_1c){
with(_1a){
_1a=objj_msgSendSuper({receiver:_1a,super_class:objj_getClass("CPKeyedUnarchiver").super_class},"init");
if(_1a){
_archive=objj_msgSend(_1c,"plistObject");
_objects=[objj_msgSend(CPNull,"null")];
_plistObject=objj_msgSend(_archive,"objectForKey:",_9);
_plistObjects=objj_msgSend(_archive,"objectForKey:",_a);
_replacementClasses=new CFMutableDictionary();
}
return _1a;
}
}),new objj_method(sel_getUid("containsValueForKey:"),function(_1d,_1e,_1f){
with(_1d){
return _plistObject.valueForKey(_1f)!=nil;
}
}),new objj_method(sel_getUid("_decodeDictionaryOfObjectsForKey:"),function(_20,_21,_22){
with(_20){
var _23=_plistObject.valueForKey(_22),_24=(_23!=nil)&&_23.isa;
if(_24===_13||_24===_14){
var _25=_23.keys(),_26=0,_27=_25.length,_28=new CFMutableDictionary();
for(;_26<_27;++_26){
var key=_25[_26];
_28.setValueForKey(key,_29(_20,_23.valueForKey(key).valueForKey(_8)));
}
return _28;
}
return nil;
}
}),new objj_method(sel_getUid("decodeBoolForKey:"),function(_2a,_2b,_2c){
with(_2a){
return !!objj_msgSend(_2a,"decodeObjectForKey:",_2c);
}
}),new objj_method(sel_getUid("decodeFloatForKey:"),function(_2d,_2e,_2f){
with(_2d){
var f=objj_msgSend(_2d,"decodeObjectForKey:",_2f);
return f===nil?0:f;
}
}),new objj_method(sel_getUid("decodeDoubleForKey:"),function(_30,_31,_32){
with(_30){
var d=objj_msgSend(_30,"decodeObjectForKey:",_32);
return d===nil?0:d;
}
}),new objj_method(sel_getUid("decodeIntForKey:"),function(_33,_34,_35){
with(_33){
var i=objj_msgSend(_33,"decodeObjectForKey:",_35);
return i===nil?0:i;
}
}),new objj_method(sel_getUid("decodePointForKey:"),function(_36,_37,_38){
with(_36){
var _39=objj_msgSend(_36,"decodeObjectForKey:",_38);
if(_39){
return CGPointFromString(_39);
}else{
return CGPointMakeZero();
}
}
}),new objj_method(sel_getUid("decodeRectForKey:"),function(_3a,_3b,_3c){
with(_3a){
var _3d=objj_msgSend(_3a,"decodeObjectForKey:",_3c);
if(_3d){
return CGRectFromString(_3d);
}else{
return CGRectMakeZero();
}
}
}),new objj_method(sel_getUid("decodeSizeForKey:"),function(_3e,_3f,_40){
with(_3e){
var _41=objj_msgSend(_3e,"decodeObjectForKey:",_40);
if(_41){
return CGSizeFromString(_41);
}else{
return CGSizeMakeZero();
}
}
}),new objj_method(sel_getUid("decodeObjectForKey:"),function(_42,_43,_44){
with(_42){
var _45=_plistObject.valueForKey(_44),_46=(_45!=nil)&&_45.isa;
if(_46===_13||_46===_14){
return _29(_42,_45.valueForKey(_8));
}else{
if(_46===_15||_46===_16||_46===_12){
return _45;
}else{
if(_46===_CPJavaScriptArray){
var _47=0,_48=_45.length,_49=[];
for(;_47<_48;++_47){
_49[_47]=_29(_42,_45[_47].valueForKey(_8));
}
return _49;
}
}
}
return nil;
}
}),new objj_method(sel_getUid("decodeBytesForKey:"),function(_4a,_4b,_4c){
with(_4a){
var _4d=objj_msgSend(_4a,"decodeObjectForKey:",_4c);
if(!_4d){
return nil;
}
var _4e=_4d.isa;
if(_4e===_16){
return _4d.bytes();
}
return nil;
}
}),new objj_method(sel_getUid("finishDecoding"),function(_4f,_50){
with(_4f){
if(_delegateSelectors&_4){
objj_msgSend(_delegate,"unarchiverWillFinish:",_4f);
}
if(_delegateSelectors&_5){
objj_msgSend(_delegate,"unarchiverDidFinish:",_4f);
}
}
}),new objj_method(sel_getUid("delegate"),function(_51,_52){
with(_51){
return _delegate;
}
}),new objj_method(sel_getUid("setDelegate:"),function(_53,_54,_55){
with(_53){
_delegate=_55;
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("unarchiver:cannotDecodeObjectOfClassName:originalClasses:"))){
_delegateSelectors|=_1;
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("unarchiver:didDecodeObject:"))){
_delegateSelectors|=_2;
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("unarchiver:willReplaceObject:withObject:"))){
_delegateSelectors|=_3;
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("unarchiverWillFinish:"))){
_delegateSelectors|=_CPKeyedUnarchiverWilFinishSelector;
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("unarchiverDidFinish:"))){
_delegateSelectors|=_5;
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("unarchiver:cannotDecodeObjectOfClassName:originalClasses:"))){
_delegateSelectors|=_6;
}
}
}),new objj_method(sel_getUid("setClass:forClassName:"),function(_56,_57,_58,_59){
with(_56){
_replacementClasses.setValueForKey(_59,_58);
}
}),new objj_method(sel_getUid("classForClassName:"),function(_5a,_5b,_5c){
with(_5a){
return _replacementClasses.valueForKey(_5c);
}
}),new objj_method(sel_getUid("allowsKeyedCoding"),function(_5d,_5e){
with(_5d){
return YES;
}
})]);
class_addMethods(_19,[new objj_method(sel_getUid("initialize"),function(_5f,_60){
with(_5f){
if(_5f!==objj_msgSend(CPKeyedUnarchiver,"class")){
return;
}
_10=objj_msgSend(CPArray,"class");
_11=objj_msgSend(CPMutableArray,"class");
_12=objj_msgSend(CPString,"class");
_13=objj_msgSend(CPDictionary,"class");
_14=objj_msgSend(CPMutableDictionary,"class");
_15=objj_msgSend(CPNumber,"class");
_16=objj_msgSend(CPData,"class");
_17=objj_msgSend(_CPKeyedArchiverValue,"class");
}
}),new objj_method(sel_getUid("unarchiveObjectWithData:"),function(_61,_62,_63){
with(_61){
if(!_63){
CPLog.error("Null data passed to -[CPKeyedUnarchiver unarchiveObjectWithData:].");
return nil;
}
var _64=objj_msgSend(objj_msgSend(_61,"alloc"),"initForReadingWithData:",_63),_65=objj_msgSend(_64,"decodeObjectForKey:","root");
objj_msgSend(_64,"finishDecoding");
return _65;
}
}),new objj_method(sel_getUid("unarchiveObjectWithFile:"),function(_66,_67,_68){
with(_66){
}
}),new objj_method(sel_getUid("unarchiveObjectWithFile:asynchronously:"),function(_69,_6a,_6b,_6c){
with(_69){
}
})]);
var _29=function(_6d,_6e){
var _6f=_6d._objects[_6e];
if(_6f){
if(_6f===_6d._objects[0]){
return nil;
}
}else{
var _70=_6d._plistObjects[_6e],_71=_70.isa;
if(_71===_13||_71===_14){
var _72=_6d._plistObjects[_70.valueForKey(_f).valueForKey(_8)],_73=_72.valueForKey(_d),_74=_72.valueForKey(_e),_75=objj_msgSend(_6d,"classForClassName:",_73);
if(!_75){
_75=CPClassFromString(_73);
}
if(!_75&&(_6d._delegateSelectors&_6)){
_75=objj_msgSend(_delegate,"unarchiver:cannotDecodeObjectOfClassName:originalClasses:",_6d,_73,_74);
}
if(!_75){
objj_msgSend(CPException,"raise:reason:",CPInvalidUnarchiveOperationException,"-[CPKeyedUnarchiver decodeObjectForKey:]: cannot decode object of class ("+_73+")");
}
var _76=_6d._plistObject;
_6d._plistObject=_70;
_6f=objj_msgSend(_75,"allocWithCoder:",_6d);
_6d._objects[_6e]=_6f;
var _77=objj_msgSend(_6f,"initWithCoder:",_6d);
_6d._plistObject=_76;
if(_77!==_6f){
if(_6d._delegateSelectors&_3){
objj_msgSend(_6d._delegate,"unarchiver:willReplaceObject:withObject:",_6d,_6f,_77);
}
_6f=_77;
_6d._objects[_6e]=_77;
}
_77=objj_msgSend(_6f,"awakeAfterUsingCoder:",_6d);
if(_77!==_6f){
if(_6d._delegateSelectors&_3){
objj_msgSend(_6d._delegate,"unarchiver:willReplaceObject:withObject:",_6d,_6f,_77);
}
_6f=_77;
_6d._objects[_6e]=_77;
}
if(_6d._delegate){
if(_6d._delegateSelectors&_2){
_77=objj_msgSend(_6d._delegate,"unarchiver:didDecodeObject:",_6d,_6f);
}
if(_77&&_77!=_6f){
if(_6d._delegateSelectors&_3){
objj_msgSend(_6d._delegate,"unarchiver:willReplaceObject:withObject:",_6d,_6f,_77);
}
_6f=_77;
_6d._objects[_6e]=_77;
}
}
}else{
_6d._objects[_6e]=_6f=_70;
if(objj_msgSend(_6f,"class")===_12){
if(_6f===_7){
_6d._objects[_6e]=_6d._objects[0];
return nil;
}else{
_6d._objects[_6e]=_6f=_70;
}
}
}
}
if((_6f!=nil)&&(_6f.isa===_17)){
_6f=objj_msgSend(_6f,"JSObject");
}
return _6f;
};
p;17;CPKeyedArchiver.jt;10260;@STATIC;1.0;i;9;CPArray.ji;9;CPCoder.ji;8;CPData.ji;14;CPDictionary.ji;10;CPNumber.ji;10;CPString.ji;9;CPValue.jt;10140;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPCoder.j",YES);
objj_executeFile("CPData.j",YES);
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPNumber.j",YES);
objj_executeFile("CPString.j",YES);
objj_executeFile("CPValue.j",YES);
var _1=nil;
var _2=1,_3=2,_4=4,_5=8,_6=16;
var _7="$null",_8=nil,_9="CP$UID",_a="$top",_b="$objects",_c="$archiver",_d="$version",_e="$classname",_f="$classes",_10="$class";
var _11=Nil,_12=Nil;
var _13=objj_allocateClassPair(CPValue,"_CPKeyedArchiverValue"),_14=_13.isa;
objj_registerClassPair(_13);
var _13=objj_allocateClassPair(CPCoder,"CPKeyedArchiver"),_14=_13.isa;
class_addIvars(_13,[new objj_ivar("_delegate"),new objj_ivar("_delegateSelectors"),new objj_ivar("_data"),new objj_ivar("_objects"),new objj_ivar("_UIDs"),new objj_ivar("_conditionalUIDs"),new objj_ivar("_replacementObjects"),new objj_ivar("_replacementClassNames"),new objj_ivar("_plistObject"),new objj_ivar("_plistObjects"),new objj_ivar("_outputFormat")]);
objj_registerClassPair(_13);
class_addMethods(_13,[new objj_method(sel_getUid("initForWritingWithMutableData:"),function(_15,_16,_17){
with(_15){
_15=objj_msgSendSuper({receiver:_15,super_class:objj_getClass("CPKeyedArchiver").super_class},"init");
if(_15){
_data=_17;
_objects=[];
_UIDs=objj_msgSend(CPDictionary,"dictionary");
_conditionalUIDs=objj_msgSend(CPDictionary,"dictionary");
_replacementObjects=objj_msgSend(CPDictionary,"dictionary");
_plistObject=objj_msgSend(CPDictionary,"dictionary");
_plistObjects=objj_msgSend(CPArray,"arrayWithObject:",_7);
}
return _15;
}
}),new objj_method(sel_getUid("finishEncoding"),function(_18,_19){
with(_18){
if(_delegate&&_delegateSelectors&_6){
objj_msgSend(_delegate,"archiverWillFinish:",_18);
}
var i=0,_1a=_plistObject,_1b=[];
for(;i<_objects.length;++i){
var _1c=_objects[i];
_plistObject=_plistObjects[objj_msgSend(_UIDs,"objectForKey:",objj_msgSend(_1c,"UID"))];
objj_msgSend(_1c,"encodeWithCoder:",_18);
if(_delegate&&_delegateSelectors&_2){
objj_msgSend(_delegate,"archiver:didEncodeObject:",_18,_1c);
}
}
_plistObject=objj_msgSend(CPDictionary,"dictionary");
objj_msgSend(_plistObject,"setObject:forKey:",_1a,_a);
objj_msgSend(_plistObject,"setObject:forKey:",_plistObjects,_b);
objj_msgSend(_plistObject,"setObject:forKey:",objj_msgSend(_18,"className"),_c);
objj_msgSend(_plistObject,"setObject:forKey:","100000",_d);
objj_msgSend(_data,"setPlistObject:",_plistObject);
if(_delegate&&_delegateSelectors&_5){
objj_msgSend(_delegate,"archiverDidFinish:",_18);
}
}
}),new objj_method(sel_getUid("outputFormat"),function(_1d,_1e){
with(_1d){
return _outputFormat;
}
}),new objj_method(sel_getUid("setOutputFormat:"),function(_1f,_20,_21){
with(_1f){
_outputFormat=_21;
}
}),new objj_method(sel_getUid("encodeBool:forKey:"),function(_22,_23,_24,_25){
with(_22){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_22,_24,NO),_25);
}
}),new objj_method(sel_getUid("encodeDouble:forKey:"),function(_27,_28,_29,_2a){
with(_27){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_27,_29,NO),_2a);
}
}),new objj_method(sel_getUid("encodeFloat:forKey:"),function(_2b,_2c,_2d,_2e){
with(_2b){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_2b,_2d,NO),_2e);
}
}),new objj_method(sel_getUid("encodeInt:forKey:"),function(_2f,_30,_31,_32){
with(_2f){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_2f,_31,NO),_32);
}
}),new objj_method(sel_getUid("setDelegate:"),function(_33,_34,_35){
with(_33){
_delegate=_35;
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("archiver:didEncodeObject:"))){
_delegateSelectors|=_2;
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("archiver:willEncodeObject:"))){
_delegateSelectors|=_3;
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("archiver:willReplaceObject:withObject:"))){
_delegateSelectors|=_4;
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("archiver:didFinishEncoding:"))){
_delegateSelectors|=_CPKeyedArchiverDidFinishEncodingSelector;
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("archiver:willFinishEncoding:"))){
_delegateSelectors|=_CPKeyedArchiverWillFinishEncodingSelector;
}
}
}),new objj_method(sel_getUid("delegate"),function(_36,_37){
with(_36){
return _delegate;
}
}),new objj_method(sel_getUid("encodePoint:forKey:"),function(_38,_39,_3a,_3b){
with(_38){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_38,CPStringFromPoint(_3a),NO),_3b);
}
}),new objj_method(sel_getUid("encodeRect:forKey:"),function(_3c,_3d,_3e,_3f){
with(_3c){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_3c,CPStringFromRect(_3e),NO),_3f);
}
}),new objj_method(sel_getUid("encodeSize:forKey:"),function(_40,_41,_42,_43){
with(_40){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_40,CPStringFromSize(_42),NO),_43);
}
}),new objj_method(sel_getUid("encodeConditionalObject:forKey:"),function(_44,_45,_46,_47){
with(_44){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_44,_46,YES),_47);
}
}),new objj_method(sel_getUid("encodeNumber:forKey:"),function(_48,_49,_4a,_4b){
with(_48){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_48,_4a,NO),_4b);
}
}),new objj_method(sel_getUid("encodeObject:forKey:"),function(_4c,_4d,_4e,_4f){
with(_4c){
objj_msgSend(_plistObject,"setObject:forKey:",_26(_4c,_4e,NO),_4f);
}
}),new objj_method(sel_getUid("_encodeArrayOfObjects:forKey:"),function(_50,_51,_52,_53){
with(_50){
var i=0,_54=_52.length,_55=[];
for(;i<_54;++i){
objj_msgSend(_55,"addObject:",_26(_50,_52[i],NO));
}
objj_msgSend(_plistObject,"setObject:forKey:",_55,_53);
}
}),new objj_method(sel_getUid("_encodeDictionaryOfObjects:forKey:"),function(_56,_57,_58,_59){
with(_56){
var key,_5a=objj_msgSend(_58,"keyEnumerator"),_5b=objj_msgSend(CPDictionary,"dictionary");
while((key=objj_msgSend(_5a,"nextObject"))!==nil){
objj_msgSend(_5b,"setObject:forKey:",_26(_56,objj_msgSend(_58,"objectForKey:",key),NO),key);
}
objj_msgSend(_plistObject,"setObject:forKey:",_5b,_59);
}
}),new objj_method(sel_getUid("setClassName:forClass:"),function(_5c,_5d,_5e,_5f){
with(_5c){
if(!_replacementClassNames){
_replacementClassNames=objj_msgSend(CPDictionary,"dictionary");
}
objj_msgSend(_replacementClassNames,"setObject:forKey:",_5e,CPStringFromClass(_5f));
}
}),new objj_method(sel_getUid("classNameForClass:"),function(_60,_61,_62){
with(_60){
if(!_replacementClassNames){
return _62.name;
}
var _63=objj_msgSend(_replacementClassNames,"objectForKey:",CPStringFromClass(aClassName));
return _63?_63:_62.name;
}
})]);
class_addMethods(_14,[new objj_method(sel_getUid("initialize"),function(_64,_65){
with(_64){
if(_64!==objj_msgSend(CPKeyedArchiver,"class")){
return;
}
_11=objj_msgSend(CPString,"class");
_12=objj_msgSend(CPNumber,"class");
_8=objj_msgSend(CPDictionary,"dictionaryWithObject:forKey:",0,_9);
}
}),new objj_method(sel_getUid("allowsKeyedCoding"),function(_66,_67){
with(_66){
return YES;
}
}),new objj_method(sel_getUid("archivedDataWithRootObject:"),function(_68,_69,_6a){
with(_68){
var _6b=objj_msgSend(CPData,"dataWithPlistObject:",nil),_6c=objj_msgSend(objj_msgSend(_68,"alloc"),"initForWritingWithMutableData:",_6b);
objj_msgSend(_6c,"encodeObject:forKey:",_6a,"root");
objj_msgSend(_6c,"finishEncoding");
return _6b;
}
}),new objj_method(sel_getUid("setClassName:forClass:"),function(_6d,_6e,_6f,_70){
with(_6d){
if(!_1){
_1=objj_msgSend(CPDictionary,"dictionary");
}
objj_msgSend(_1,"setObject:forKey:",_6f,CPStringFromClass(_70));
}
}),new objj_method(sel_getUid("classNameForClass:"),function(_71,_72,_73){
with(_71){
if(!_1){
return _73.name;
}
var _74=objj_msgSend(_1,"objectForKey:",CPStringFromClass(_73));
return _74?_74:_73.name;
}
})]);
var _26=function(_75,_76,_77){
if(_76!==nil&&_76!==undefined&&!_76.isa){
_76=objj_msgSend(_CPKeyedArchiverValue,"valueWithJSObject:",_76);
}
var _78=objj_msgSend(_76,"UID"),_79=objj_msgSend(_75._replacementObjects,"objectForKey:",_78);
if(_79===nil){
_79=objj_msgSend(_76,"replacementObjectForKeyedArchiver:",_75);
if(_75._delegate){
if(_79!==_76&&_75._delegateSelectors&_4){
objj_msgSend(_75._delegate,"archiver:willReplaceObject:withObject:",_75,_76,_79);
}
if(_75._delegateSelectors&_3){
_76=objj_msgSend(_75._delegate,"archiver:willEncodeObject:",_75,_79);
if(_76!==_79&&_75._delegateSelectors&_4){
objj_msgSend(_75._delegate,"archiver:willReplaceObject:withObject:",_75,_79,_76);
}
_79=_76;
}
}
objj_msgSend(_75._replacementObjects,"setObject:forKey:",_79,_78);
}
if(_79===nil){
return _8;
}
var UID=objj_msgSend(_75._UIDs,"objectForKey:",_78=objj_msgSend(_79,"UID"));
if(UID===nil){
if(_77){
if((UID=objj_msgSend(_75._conditionalUIDs,"objectForKey:",_78))===nil){
objj_msgSend(_75._conditionalUIDs,"setObject:forKey:",UID=objj_msgSend(_75._plistObjects,"count"),_78);
objj_msgSend(_75._plistObjects,"addObject:",_7);
}
}else{
var _7a=objj_msgSend(_79,"classForKeyedArchiver"),_7b=nil;
if((_7a===_11)||(_7a===_12)){
_7b=_79;
}else{
_7b=objj_msgSend(CPDictionary,"dictionary");
objj_msgSend(_75._objects,"addObject:",_79);
var _7c=objj_msgSend(_75,"classNameForClass:",_7a);
if(!_7c){
_7c=objj_msgSend(objj_msgSend(_75,"class"),"classNameForClass:",_7a);
}
if(!_7c){
_7c=_7a.name;
}else{
_7a=CPClassFromString(_7c);
}
var _7d=objj_msgSend(_75._UIDs,"objectForKey:",_7c);
if(!_7d){
var _7e=objj_msgSend(CPDictionary,"dictionary"),_7f=[];
objj_msgSend(_7e,"setObject:forKey:",_7c,_e);
do{
objj_msgSend(_7f,"addObject:",CPStringFromClass(_7a));
}while(_7a=objj_msgSend(_7a,"superclass"));
objj_msgSend(_7e,"setObject:forKey:",_7f,_f);
_7d=objj_msgSend(_75._plistObjects,"count");
objj_msgSend(_75._plistObjects,"addObject:",_7e);
objj_msgSend(_75._UIDs,"setObject:forKey:",_7d,_7c);
}
objj_msgSend(_7b,"setObject:forKey:",objj_msgSend(CPDictionary,"dictionaryWithObject:forKey:",_7d,_9),_10);
}
UID=objj_msgSend(_75._conditionalUIDs,"objectForKey:",_78);
if(UID!==nil){
objj_msgSend(_75._UIDs,"setObject:forKey:",UID,_78);
objj_msgSend(_75._plistObjects,"replaceObjectAtIndex:withObject:",UID,_7b);
}else{
objj_msgSend(_75._UIDs,"setObject:forKey:",UID=objj_msgSend(_75._plistObjects,"count"),_78);
objj_msgSend(_75._plistObjects,"addObject:",_7b);
}
}
}
return objj_msgSend(CPDictionary,"dictionaryWithObject:forKey:",UID,_9);
};
p;9;CPTimer.jt;5564;@STATIC;1.0;i;8;CPDate.ji;14;CPInvocation.ji;10;CPObject.ji;11;CPRunLoop.jt;5483;
objj_executeFile("CPDate.j",YES);
objj_executeFile("CPInvocation.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPRunLoop.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPTimer"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_timeInterval"),new objj_ivar("_invocation"),new objj_ivar("_callback"),new objj_ivar("_repeats"),new objj_ivar("_isValid"),new objj_ivar("_fireDate"),new objj_ivar("_userInfo")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithFireDate:interval:invocation:repeats:"),function(_3,_4,_5,_6,_7,_8){
with(_3){
_3=objj_msgSendSuper({receiver:_3,super_class:objj_getClass("CPTimer").super_class},"init");
if(_3){
_timeInterval=_6;
_invocation=_7;
_repeats=_8;
_isValid=YES;
_fireDate=_5;
}
return _3;
}
}),new objj_method(sel_getUid("initWithFireDate:interval:target:selector:userInfo:repeats:"),function(_9,_a,_b,_c,_d,_e,_f,_10){
with(_9){
var _11=objj_msgSend(CPInvocation,"invocationWithMethodSignature:",1);
objj_msgSend(_11,"setTarget:",_d);
objj_msgSend(_11,"setSelector:",_e);
objj_msgSend(_11,"setArgument:atIndex:",_9,2);
_9=objj_msgSend(_9,"initWithFireDate:interval:invocation:repeats:",_b,_c,_11,_10);
if(_9){
_userInfo=_f;
}
return _9;
}
}),new objj_method(sel_getUid("initWithFireDate:interval:callback:repeats:"),function(_12,_13,_14,_15,_16,_17){
with(_12){
_12=objj_msgSendSuper({receiver:_12,super_class:objj_getClass("CPTimer").super_class},"init");
if(_12){
_timeInterval=_15;
_callback=_16;
_repeats=_17;
_isValid=YES;
_fireDate=_14;
}
return _12;
}
}),new objj_method(sel_getUid("timeInterval"),function(_18,_19){
with(_18){
return _timeInterval;
}
}),new objj_method(sel_getUid("fireDate"),function(_1a,_1b){
with(_1a){
return _fireDate;
}
}),new objj_method(sel_getUid("setFireDate:"),function(_1c,_1d,_1e){
with(_1c){
_fireDate=_1e;
}
}),new objj_method(sel_getUid("fire"),function(_1f,_20){
with(_1f){
if(!_isValid){
return;
}
if(_callback){
_callback();
}else{
objj_msgSend(_invocation,"invoke");
}
if(!_isValid){
return;
}
if(_repeats){
_fireDate=objj_msgSend(CPDate,"dateWithTimeIntervalSinceNow:",_timeInterval);
}else{
objj_msgSend(_1f,"invalidate");
}
}
}),new objj_method(sel_getUid("isValid"),function(_21,_22){
with(_21){
return _isValid;
}
}),new objj_method(sel_getUid("invalidate"),function(_23,_24){
with(_23){
_isValid=NO;
_userInfo=nil;
_invocation=nil;
_callback=nil;
}
}),new objj_method(sel_getUid("userInfo"),function(_25,_26){
with(_25){
return _userInfo;
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("scheduledTimerWithTimeInterval:invocation:repeats:"),function(_27,_28,_29,_2a,_2b){
with(_27){
var _2c=objj_msgSend(objj_msgSend(_27,"alloc"),"initWithFireDate:interval:invocation:repeats:",objj_msgSend(CPDate,"dateWithTimeIntervalSinceNow:",_29),_29,_2a,_2b);
objj_msgSend(objj_msgSend(CPRunLoop,"currentRunLoop"),"addTimer:forMode:",_2c,CPDefaultRunLoopMode);
return _2c;
}
}),new objj_method(sel_getUid("scheduledTimerWithTimeInterval:target:selector:userInfo:repeats:"),function(_2d,_2e,_2f,_30,_31,_32,_33){
with(_2d){
var _34=objj_msgSend(objj_msgSend(_2d,"alloc"),"initWithFireDate:interval:target:selector:userInfo:repeats:",objj_msgSend(CPDate,"dateWithTimeIntervalSinceNow:",_2f),_2f,_30,_31,_32,_33);
objj_msgSend(objj_msgSend(CPRunLoop,"currentRunLoop"),"addTimer:forMode:",_34,CPDefaultRunLoopMode);
return _34;
}
}),new objj_method(sel_getUid("scheduledTimerWithTimeInterval:callback:repeats:"),function(_35,_36,_37,_38,_39){
with(_35){
var _3a=objj_msgSend(objj_msgSend(_35,"alloc"),"initWithFireDate:interval:callback:repeats:",objj_msgSend(CPDate,"dateWithTimeIntervalSinceNow:",_37),_37,_38,_39);
objj_msgSend(objj_msgSend(CPRunLoop,"currentRunLoop"),"addTimer:forMode:",_3a,CPDefaultRunLoopMode);
return _3a;
}
}),new objj_method(sel_getUid("timerWithTimeInterval:invocation:repeats:"),function(_3b,_3c,_3d,_3e,_3f){
with(_3b){
return objj_msgSend(objj_msgSend(_3b,"alloc"),"initWithFireDate:interval:invocation:repeats:",objj_msgSend(CPDate,"dateWithTimeIntervalSinceNow:",_3d),_3d,_3e,_3f);
}
}),new objj_method(sel_getUid("timerWithTimeInterval:target:selector:userInfo:repeats:"),function(_40,_41,_42,_43,_44,_45,_46){
with(_40){
return objj_msgSend(objj_msgSend(_40,"alloc"),"initWithFireDate:interval:target:selector:userInfo:repeats:",objj_msgSend(CPDate,"dateWithTimeIntervalSinceNow:",_42),_42,_43,_44,_45,_46);
}
}),new objj_method(sel_getUid("timerWithTimeInterval:callback:repeats:"),function(_47,_48,_49,_4a,_4b){
with(_47){
return objj_msgSend(objj_msgSend(_47,"alloc"),"initWithFireDate:interval:callback:repeats:",objj_msgSend(CPDate,"dateWithTimeIntervalSinceNow:",_49),_49,_4a,_4b);
}
})]);
var _4c=1000,_4d={};
var _4e=function(_4f,_50,_51,_52){
var _53=_4c++,_54=nil;
if(typeof _4f==="string"){
_54=function(){
new Function(_4f)();
if(!_51){
_4d[_53]=nil;
}
};
}else{
if(!_52){
_52=[];
}
_54=function(){
_4f.apply(window,_52);
if(!_51){
_4d[_53]=nil;
}
};
}
_4d[_53]=objj_msgSend(CPTimer,"scheduledTimerWithTimeInterval:callback:repeats:",_50/1000,_54,_51);
return _53;
};
if(typeof (window)!=="undefined"){
window.setTimeout=function(_55,_56){
return _4e(_55,_56,NO,Array.prototype.slice.apply(arguments,[2]));
};
window.clearTimeout=function(_57){
var _58=_4d[_57];
if(_58){
objj_msgSend(_58,"invalidate");
}
_4d[_57]=nil;
};
window.setInterval=function(_59,_5a,_5b){
return _4e(_59,_5a,YES,Array.prototype.slice.apply(arguments,[2]));
};
window.clearInterval=function(_5c){
window.clearTimeout(_5c);
};
}
p;14;CPInvocation.jt;2602;@STATIC;1.0;i;10;CPObject.jt;2568;
objj_executeFile("CPObject.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPInvocation"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_returnValue"),new objj_ivar("_arguments"),new objj_ivar("_methodSignature")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithMethodSignature:"),function(_3,_4,_5){
with(_3){
_3=objj_msgSendSuper({receiver:_3,super_class:objj_getClass("CPInvocation").super_class},"init");
if(_3){
_arguments=[];
_methodSignature=_5;
}
return _3;
}
}),new objj_method(sel_getUid("setSelector:"),function(_6,_7,_8){
with(_6){
_arguments[1]=_8;
}
}),new objj_method(sel_getUid("selector"),function(_9,_a){
with(_9){
return _arguments[1];
}
}),new objj_method(sel_getUid("setTarget:"),function(_b,_c,_d){
with(_b){
_arguments[0]=_d;
}
}),new objj_method(sel_getUid("target"),function(_e,_f){
with(_e){
return _arguments[0];
}
}),new objj_method(sel_getUid("setArgument:atIndex:"),function(_10,_11,_12,_13){
with(_10){
_arguments[_13]=_12;
}
}),new objj_method(sel_getUid("argumentAtIndex:"),function(_14,_15,_16){
with(_14){
return _arguments[_16];
}
}),new objj_method(sel_getUid("setReturnValue:"),function(_17,_18,_19){
with(_17){
_returnValue=_19;
}
}),new objj_method(sel_getUid("returnValue"),function(_1a,_1b){
with(_1a){
return _returnValue;
}
}),new objj_method(sel_getUid("invoke"),function(_1c,_1d){
with(_1c){
_returnValue=objj_msgSend.apply(objj_msgSend,_arguments);
}
}),new objj_method(sel_getUid("invokeWithTarget:"),function(_1e,_1f,_20){
with(_1e){
_arguments[0]=_20;
_returnValue=objj_msgSend.apply(objj_msgSend,_arguments);
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("invocationWithMethodSignature:"),function(_21,_22,_23){
with(_21){
return objj_msgSend(objj_msgSend(_21,"alloc"),"initWithMethodSignature:",_23);
}
})]);
var _24="CPInvocationArguments",_25="CPInvocationReturnValue";
var _1=objj_getClass("CPInvocation");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPInvocation\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("initWithCoder:"),function(_26,_27,_28){
with(_26){
_26=objj_msgSendSuper({receiver:_26,super_class:objj_getClass("CPInvocation").super_class},"init");
if(_26){
_returnValue=objj_msgSend(_28,"decodeObjectForKey:",_25);
_arguments=objj_msgSend(_28,"decodeObjectForKey:",_24);
}
return _26;
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_29,_2a,_2b){
with(_29){
objj_msgSend(_2b,"encodeObject:forKey:",_returnValue,_25);
objj_msgSend(_2b,"encodeObject:forKey:",_arguments,_24);
}
})]);
p;14;CPCountedSet.jt;1347;@STATIC;1.0;i;10;CPObject.ji;7;CPSet.jt;1302;
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPSet.j",YES);
var _1=objj_allocateClassPair(_CPConcreteMutableSet,"CPCountedSet"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_counts")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("addObject:"),function(_3,_4,_5){
with(_3){
if(!_counts){
_counts={};
}
objj_msgSendSuper({receiver:_3,super_class:objj_getClass("CPCountedSet").super_class},"addObject:",_5);
var _6=objj_msgSend(_5,"UID");
if(_counts[_6]===undefined){
_counts[_6]=1;
}else{
++_counts[_6];
}
}
}),new objj_method(sel_getUid("removeObject:"),function(_7,_8,_9){
with(_7){
if(!_counts){
return;
}
var _a=objj_msgSend(_9,"UID");
if(_counts[_a]===undefined){
return;
}else{
--_counts[_a];
if(_counts[_a]===0){
delete _counts[_a];
objj_msgSendSuper({receiver:_7,super_class:objj_getClass("CPCountedSet").super_class},"removeObject:",_9);
}
}
}
}),new objj_method(sel_getUid("removeAllObjects"),function(_b,_c){
with(_b){
objj_msgSendSuper({receiver:_b,super_class:objj_getClass("CPCountedSet").super_class},"removeAllObjects");
_counts={};
}
}),new objj_method(sel_getUid("countForObject:"),function(_d,_e,_f){
with(_d){
if(!_counts){
_counts={};
}
var UID=objj_msgSend(_f,"UID");
if(_counts[UID]===undefined){
return 0;
}
return _counts[UID];
}
})]);
p;15;CPUndoManager.jt;17844;@STATIC;1.0;i;9;CPArray.ji;13;CPException.ji;14;CPInvocation.ji;22;CPNotificationCenter.ji;10;CPObject.ji;9;CPProxy.ji;21;CPKeyValueObserving.ji;11;CPRunLoop.jt;17677;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPException.j",YES);
objj_executeFile("CPInvocation.j",YES);
objj_executeFile("CPNotificationCenter.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPProxy.j",YES);
objj_executeFile("CPKeyValueObserving.j",YES);
objj_executeFile("CPRunLoop.j",YES);
var _1=0,_2=1,_3=2;
CPUndoManagerCheckpointNotification="CPUndoManagerCheckpointNotification";
CPUndoManagerDidOpenUndoGroupNotification="CPUndoManagerDidOpenUndoGroupNotification";
CPUndoManagerDidCloseUndoGroupNotification="CPUndoManagerDidCloseUndoGroupNotification";
CPUndoManagerDidRedoChangeNotification="CPUndoManagerDidRedoChangeNotification";
CPUndoManagerDidUndoChangeNotification="CPUndoManagerDidUndoChangeNotification";
CPUndoManagerWillCloseUndoGroupNotification="CPUndoManagerWillCloseUndoGroupNotification";
CPUndoManagerWillRedoChangeNotification="CPUndoManagerWillRedoChangeNotification";
CPUndoManagerWillUndoChangeNotification="CPUndoManagerWillUndoChangeNotification";
CPUndoCloseGroupingRunLoopOrdering=350000;
var _4=[],_5=5;
var _6=objj_allocateClassPair(CPObject,"_CPUndoGrouping"),_7=_6.isa;
class_addIvars(_6,[new objj_ivar("_parent"),new objj_ivar("_invocations"),new objj_ivar("_actionName")]);
objj_registerClassPair(_6);
class_addMethods(_6,[new objj_method(sel_getUid("initWithParent:"),function(_8,_9,_a){
with(_8){
_8=objj_msgSendSuper({receiver:_8,super_class:objj_getClass("_CPUndoGrouping").super_class},"init");
if(_8){
_parent=_a;
_invocations=[];
_actionName="";
}
return _8;
}
}),new objj_method(sel_getUid("parent"),function(_b,_c){
with(_b){
return _parent;
}
}),new objj_method(sel_getUid("addInvocation:"),function(_d,_e,_f){
with(_d){
_invocations.push(_f);
}
}),new objj_method(sel_getUid("addInvocationsFromArray:"),function(_10,_11,_12){
with(_10){
objj_msgSend(_invocations,"addObjectsFromArray:",_12);
}
}),new objj_method(sel_getUid("removeInvocationsWithTarget:"),function(_13,_14,_15){
with(_13){
var _16=_invocations.length;
while(_16--){
if(objj_msgSend(_invocations[_16],"target")==_15){
_invocations.splice(_16,1);
}
}
}
}),new objj_method(sel_getUid("invocations"),function(_17,_18){
with(_17){
return _invocations;
}
}),new objj_method(sel_getUid("invoke"),function(_19,_1a){
with(_19){
var _1b=_invocations.length;
while(_1b--){
objj_msgSend(_invocations[_1b],"invoke");
}
}
}),new objj_method(sel_getUid("setActionName:"),function(_1c,_1d,_1e){
with(_1c){
_actionName=_1e;
}
}),new objj_method(sel_getUid("actionName"),function(_1f,_20){
with(_1f){
return _actionName;
}
})]);
class_addMethods(_7,[new objj_method(sel_getUid("_poolUndoGrouping:"),function(_21,_22,_23){
with(_21){
if(!_23||_4.length>=_5){
return;
}
_4.push(_23);
}
}),new objj_method(sel_getUid("undoGroupingWithParent:"),function(_24,_25,_26){
with(_24){
if(_4.length){
var _27=_4.pop();
_27._parent=_26;
if(_27._invocations.length){
_27._invocations=[];
}
return _27;
}
return objj_msgSend(objj_msgSend(_24,"alloc"),"initWithParent:",_26);
}
})]);
var _28="_CPUndoGroupingParentKey",_29="_CPUndoGroupingInvocationsKey",_2a="_CPUndoGroupingActionNameKey";
var _6=objj_getClass("_CPUndoGrouping");
if(!_6){
throw new SyntaxError("*** Could not find definition for class \"_CPUndoGrouping\"");
}
var _7=_6.isa;
class_addMethods(_6,[new objj_method(sel_getUid("initWithCoder:"),function(_2b,_2c,_2d){
with(_2b){
_2b=objj_msgSendSuper({receiver:_2b,super_class:objj_getClass("_CPUndoGrouping").super_class},"init");
if(_2b){
_parent=objj_msgSend(_2d,"decodeObjectForKey:",_28);
_invocations=objj_msgSend(_2d,"decodeObjectForKey:",_29);
_actionName=objj_msgSend(_2d,"decodeObjectForKey:",_2a);
}
return _2b;
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_2e,_2f,_30){
with(_2e){
objj_msgSend(_30,"encodeObject:forKey:",_parent,_28);
objj_msgSend(_30,"encodeObject:forKey:",_invocations,_29);
objj_msgSend(_30,"encodeObject:forKey:",_actionName,_2a);
}
})]);
var _6=objj_allocateClassPair(CPObject,"CPUndoManager"),_7=_6.isa;
class_addIvars(_6,[new objj_ivar("_redoStack"),new objj_ivar("_undoStack"),new objj_ivar("_groupsByEvent"),new objj_ivar("_disableCount"),new objj_ivar("_levelsOfUndo"),new objj_ivar("_currentGrouping"),new objj_ivar("_state"),new objj_ivar("_preparedTarget"),new objj_ivar("_undoManagerProxy"),new objj_ivar("_runLoopModes"),new objj_ivar("_registeredWithRunLoop")]);
objj_registerClassPair(_6);
class_addMethods(_6,[new objj_method(sel_getUid("init"),function(_31,_32){
with(_31){
_31=objj_msgSendSuper({receiver:_31,super_class:objj_getClass("CPUndoManager").super_class},"init");
if(_31){
_redoStack=[];
_undoStack=[];
_state=_1;
objj_msgSend(_31,"setRunLoopModes:",[CPDefaultRunLoopMode]);
objj_msgSend(_31,"setGroupsByEvent:",YES);
_undoManagerProxy=objj_msgSend(_CPUndoManagerProxy,"alloc");
_undoManagerProxy._undoManager=_31;
}
return _31;
}
}),new objj_method(sel_getUid("_addUndoInvocation:"),function(_33,_34,_35){
with(_33){
if(!_currentGrouping){
if(objj_msgSend(_33,"groupsByEvent")){
objj_msgSend(_33,"_beginUndoGroupingForEvent");
}else{
objj_msgSend(CPException,"raise:reason:",CPInternalInconsistencyException,"No undo group is currently open");
}
}
objj_msgSend(_currentGrouping,"addInvocation:",_35);
if(_state===_1){
objj_msgSend(_redoStack,"removeAllObjects");
}
}
}),new objj_method(sel_getUid("registerUndoWithTarget:selector:object:"),function(_36,_37,_38,_39,_3a){
with(_36){
if(_disableCount>0){
return;
}
var _3b=objj_msgSend(CPInvocation,"invocationWithMethodSignature:",nil);
objj_msgSend(_3b,"setTarget:",_38);
objj_msgSend(_3b,"setSelector:",_39);
objj_msgSend(_3b,"setArgument:atIndex:",_3a,2);
objj_msgSend(_36,"_addUndoInvocation:",_3b);
}
}),new objj_method(sel_getUid("prepareWithInvocationTarget:"),function(_3c,_3d,_3e){
with(_3c){
_preparedTarget=_3e;
return _undoManagerProxy;
}
}),new objj_method(sel_getUid("_methodSignatureOfPreparedTargetForSelector:"),function(_3f,_40,_41){
with(_3f){
if(objj_msgSend(_preparedTarget,"respondsToSelector:",_41)){
return 1;
}
return nil;
}
}),new objj_method(sel_getUid("_forwardInvocationToPreparedTarget:"),function(_42,_43,_44){
with(_42){
if(_disableCount>0){
return;
}
objj_msgSend(_44,"setTarget:",_preparedTarget);
objj_msgSend(_42,"_addUndoInvocation:",_44);
_preparedTarget=nil;
}
}),new objj_method(sel_getUid("canRedo"),function(_45,_46){
with(_45){
objj_msgSend(objj_msgSend(CPNotificationCenter,"defaultCenter"),"postNotificationName:object:",CPUndoManagerCheckpointNotification,_45);
return objj_msgSend(_redoStack,"count")>0;
}
}),new objj_method(sel_getUid("canUndo"),function(_47,_48){
with(_47){
if(_undoStack.length>0){
return YES;
}
return objj_msgSend(objj_msgSend(_currentGrouping,"invocations"),"count")>0;
}
}),new objj_method(sel_getUid("undo"),function(_49,_4a){
with(_49){
if(objj_msgSend(_49,"groupingLevel")===1){
objj_msgSend(_49,"endUndoGrouping");
}
objj_msgSend(_49,"undoNestedGroup");
}
}),new objj_method(sel_getUid("undoNestedGroup"),function(_4b,_4c){
with(_4b){
if(objj_msgSend(_undoStack,"count")<=0){
return;
}
var _4d=objj_msgSend(CPNotificationCenter,"defaultCenter");
objj_msgSend(_4d,"postNotificationName:object:",CPUndoManagerCheckpointNotification,_4b);
objj_msgSend(_4d,"postNotificationName:object:",CPUndoManagerWillUndoChangeNotification,_4b);
var _4e=_undoStack.pop(),_4f=objj_msgSend(_4e,"actionName");
_state=_2;
objj_msgSend(_4b,"_beginUndoGrouping");
objj_msgSend(_4e,"invoke");
objj_msgSend(_4b,"endUndoGrouping");
objj_msgSend(_CPUndoGrouping,"_poolUndoGrouping:",_4e);
_state=_1;
objj_msgSend(objj_msgSend(_redoStack,"lastObject"),"setActionName:",_4f);
objj_msgSend(_4d,"postNotificationName:object:",CPUndoManagerDidUndoChangeNotification,_4b);
}
}),new objj_method(sel_getUid("redo"),function(_50,_51){
with(_50){
if(objj_msgSend(_redoStack,"count")<=0){
return;
}
var _52=objj_msgSend(CPNotificationCenter,"defaultCenter");
objj_msgSend(_52,"postNotificationName:object:",CPUndoManagerCheckpointNotification,_50);
objj_msgSend(_52,"postNotificationName:object:",CPUndoManagerWillRedoChangeNotification,_50);
var _53=_currentGrouping,_54=_redoStack.pop(),_55=objj_msgSend(_54,"actionName");
_currentGrouping=nil;
_state=_3;
objj_msgSend(_50,"_beginUndoGrouping");
objj_msgSend(_54,"invoke");
objj_msgSend(_50,"endUndoGrouping");
objj_msgSend(_CPUndoGrouping,"_poolUndoGrouping:",_54);
_currentGrouping=_53;
_state=_1;
objj_msgSend(objj_msgSend(_undoStack,"lastObject"),"setActionName:",_55);
objj_msgSend(_52,"postNotificationName:object:",CPUndoManagerDidRedoChangeNotification,_50);
}
}),new objj_method(sel_getUid("beginUndoGrouping"),function(_56,_57){
with(_56){
if(!_currentGrouping&&objj_msgSend(_56,"groupsByEvent")){
objj_msgSend(_56,"_beginUndoGroupingForEvent");
}
objj_msgSend(objj_msgSend(CPNotificationCenter,"defaultCenter"),"postNotificationName:object:",CPUndoManagerCheckpointNotification,_56);
objj_msgSend(_56,"_beginUndoGrouping");
}
}),new objj_method(sel_getUid("_beginUndoGroupingForEvent"),function(_58,_59){
with(_58){
objj_msgSend(_58,"_beginUndoGrouping");
objj_msgSend(_58,"_registerWithRunLoop");
}
}),new objj_method(sel_getUid("_beginUndoGrouping"),function(_5a,_5b){
with(_5a){
_currentGrouping=objj_msgSend(_CPUndoGrouping,"undoGroupingWithParent:",_currentGrouping);
}
}),new objj_method(sel_getUid("endUndoGrouping"),function(_5c,_5d){
with(_5c){
if(!_currentGrouping){
objj_msgSend(CPException,"raise:reason:",CPInternalInconsistencyException,"endUndoGrouping. No undo group is currently open.");
}
var _5e=objj_msgSend(CPNotificationCenter,"defaultCenter");
objj_msgSend(_5e,"postNotificationName:object:",CPUndoManagerCheckpointNotification,_5c);
var _5f=objj_msgSend(_currentGrouping,"parent");
if(!_5f&&objj_msgSend(_currentGrouping,"invocations").length>0){
objj_msgSend(_5e,"postNotificationName:object:",CPUndoManagerWillCloseUndoGroupNotification,_5c);
var _60=_state===_2?_redoStack:_undoStack;
_60.push(_currentGrouping);
if(_levelsOfUndo>0&&_60.length>_levelsOfUndo){
_60.splice(0,1);
}
objj_msgSend(_5e,"postNotificationName:object:",CPUndoManagerDidCloseUndoGroupNotification,_5c);
}else{
objj_msgSend(_5f,"addInvocationsFromArray:",objj_msgSend(_currentGrouping,"invocations"));
objj_msgSend(_CPUndoGrouping,"_poolUndoGrouping:",_currentGrouping);
}
_currentGrouping=_5f;
}
}),new objj_method(sel_getUid("enableUndoRegistration"),function(_61,_62){
with(_61){
if(_disableCount<=0){
objj_msgSend(CPException,"raise:reason:",CPInternalInconsistencyException,"enableUndoRegistration. There are no disable messages in effect right now.");
}
_disableCount--;
}
}),new objj_method(sel_getUid("groupsByEvent"),function(_63,_64){
with(_63){
return _groupsByEvent;
}
}),new objj_method(sel_getUid("setGroupsByEvent:"),function(_65,_66,_67){
with(_65){
_67=!!_67;
if(_groupsByEvent===_67){
return;
}
_groupsByEvent=_67;
if(!objj_msgSend(_65,"groupsByEvent")){
objj_msgSend(_65,"_unregisterWithRunLoop");
}
}
}),new objj_method(sel_getUid("groupingLevel"),function(_68,_69){
with(_68){
var _6a=_currentGrouping,_6b=_currentGrouping?1:0;
while(_6a=objj_msgSend(_6a,"parent")){
++_6b;
}
return _6b;
}
}),new objj_method(sel_getUid("disableUndoRegistration"),function(_6c,_6d){
with(_6c){
++_disableCount;
}
}),new objj_method(sel_getUid("isUndoRegistrationEnabled"),function(_6e,_6f){
with(_6e){
return _disableCount==0;
}
}),new objj_method(sel_getUid("isUndoing"),function(_70,_71){
with(_70){
return _state===_2;
}
}),new objj_method(sel_getUid("isRedoing"),function(_72,_73){
with(_72){
return _state===_3;
}
}),new objj_method(sel_getUid("removeAllActions"),function(_74,_75){
with(_74){
while(_currentGrouping){
objj_msgSend(_74,"endUndoGrouping");
}
objj_msgSend(_74,"_unregisterWithRunLoop");
_state=_1;
_redoStack=[];
_undoStack=[];
_disableCount=0;
}
}),new objj_method(sel_getUid("removeAllActionsWithTarget:"),function(_76,_77,_78){
with(_76){
objj_msgSend(_currentGrouping,"removeInvocationsWithTarget:",_78);
var _79=_redoStack.length;
while(_79--){
var _7a=_redoStack[_79];
objj_msgSend(_7a,"removeInvocationsWithTarget:",_78);
if(!objj_msgSend(_7a,"invocations").length){
_redoStack.splice(_79,1);
}
}
_79=_undoStack.length;
while(_79--){
var _7a=_undoStack[_79];
objj_msgSend(_7a,"removeInvocationsWithTarget:",_78);
if(!objj_msgSend(_7a,"invocations").length){
_undoStack.splice(_79,1);
}
}
}
}),new objj_method(sel_getUid("setActionName:"),function(_7b,_7c,_7d){
with(_7b){
if(_7d!==nil&&_currentGrouping){
objj_msgSend(_currentGrouping,"setActionName:",_7d);
}
}
}),new objj_method(sel_getUid("redoActionName"),function(_7e,_7f){
with(_7e){
if(!objj_msgSend(_7e,"canRedo")){
return nil;
}
return objj_msgSend(objj_msgSend(_redoStack,"lastObject"),"actionName");
}
}),new objj_method(sel_getUid("redoMenuItemTitle"),function(_80,_81){
with(_80){
return objj_msgSend(_80,"redoMenuTitleForUndoActionName:",objj_msgSend(_80,"redoActionName"));
}
}),new objj_method(sel_getUid("redoMenuTitleForUndoActionName:"),function(_82,_83,_84){
with(_82){
if(_84||_84===0){
return "Redo "+_84;
}
return "Redo";
}
}),new objj_method(sel_getUid("undoActionName"),function(_85,_86){
with(_85){
if(!objj_msgSend(_85,"canUndo")){
return nil;
}
return objj_msgSend(objj_msgSend(_undoStack,"lastObject"),"actionName");
}
}),new objj_method(sel_getUid("undoMenuItemTitle"),function(_87,_88){
with(_87){
return objj_msgSend(_87,"undoMenuTitleForUndoActionName:",objj_msgSend(_87,"undoActionName"));
}
}),new objj_method(sel_getUid("undoMenuTitleForUndoActionName:"),function(_89,_8a,_8b){
with(_89){
if(_8b||_8b===0){
return "Undo "+_8b;
}
return "Undo";
}
}),new objj_method(sel_getUid("runLoopModes"),function(_8c,_8d){
with(_8c){
return _runLoopModes;
}
}),new objj_method(sel_getUid("setRunLoopModes:"),function(_8e,_8f,_90){
with(_8e){
_runLoopModes=objj_msgSend(_90,"copy");
if(_registeredWithRunLoop){
objj_msgSend(_8e,"_unregisterWithRunLoop");
objj_msgSend(_8e,"_registerWithRunLoop");
}
}
}),new objj_method(sel_getUid("_runLoopEndUndoGrouping"),function(_91,_92){
with(_91){
objj_msgSend(_91,"endUndoGrouping");
_registeredWithRunLoop=NO;
}
}),new objj_method(sel_getUid("_registerWithRunLoop"),function(_93,_94){
with(_93){
if(_registeredWithRunLoop){
return;
}
_registeredWithRunLoop=YES;
objj_msgSend(objj_msgSend(CPRunLoop,"currentRunLoop"),"performSelector:target:argument:order:modes:",sel_getUid("_runLoopEndUndoGrouping"),_93,nil,CPUndoCloseGroupingRunLoopOrdering,_runLoopModes);
}
}),new objj_method(sel_getUid("_unregisterWithRunLoop"),function(_95,_96){
with(_95){
if(!_registeredWithRunLoop){
return;
}
_registeredWithRunLoop=NO;
objj_msgSend(objj_msgSend(CPRunLoop,"currentRunLoop"),"cancelPerformSelector:target:argument:",sel_getUid("_runLoopEndUndoGrouping"),_95,nil);
}
}),new objj_method(sel_getUid("observeChangesForKeyPath:ofObject:"),function(_97,_98,_99,_9a){
with(_97){
objj_msgSend(_9a,"addObserver:forKeyPath:options:context:",_97,_99,CPKeyValueObservingOptionOld|CPKeyValueObservingOptionNew,NULL);
}
}),new objj_method(sel_getUid("stopObservingChangesForKeyPath:ofObject:"),function(_9b,_9c,_9d,_9e){
with(_9b){
objj_msgSend(_9e,"removeObserver:forKeyPath:",_9b,_9d);
}
}),new objj_method(sel_getUid("observeValueForKeyPath:ofObject:change:context:"),function(_9f,_a0,_a1,_a2,_a3,_a4){
with(_9f){
var _a5=objj_msgSend(_a3,"valueForKey:",CPKeyValueChangeOldKey),_a6=objj_msgSend(_a3,"valueForKey:",CPKeyValueChangeNewKey);
if(_a5===_a6||(_a5!==nil&&_a5.isa&&(_a6===nil||_a6.isa)&&objj_msgSend(_a5,"isEqual:",_a6))){
return;
}
objj_msgSend(objj_msgSend(_9f,"prepareWithInvocationTarget:",_a2),"applyChange:toKeyPath:",objj_msgSend(_a3,"inverseChangeDictionary"),_a1);
}
})]);
var _a7="CPUndoManagerRedoStackKey",_a8="CPUndoManagerUndoStackKey",_a9="CPUndoManagerLevelsOfUndoKey",_aa="CPUndoManagerActionNameKey",_ab="CPUndoManagerCurrentGroupingKey",_ac="CPUndoManagerRunLoopModesKey",_ad="CPUndoManagerGroupsByEventKey";
var _6=objj_getClass("CPUndoManager");
if(!_6){
throw new SyntaxError("*** Could not find definition for class \"CPUndoManager\"");
}
var _7=_6.isa;
class_addMethods(_6,[new objj_method(sel_getUid("initWithCoder:"),function(_ae,_af,_b0){
with(_ae){
_ae=objj_msgSendSuper({receiver:_ae,super_class:objj_getClass("CPUndoManager").super_class},"init");
if(_ae){
_redoStack=objj_msgSend(_b0,"decodeObjectForKey:",_a7);
_undoStack=objj_msgSend(_b0,"decodeObjectForKey:",_a8);
_levelsOfUndo=objj_msgSend(_b0,"decodeObjectForKey:",_a9);
_actionName=objj_msgSend(_b0,"decodeObjectForKey:",_aa);
_currentGrouping=objj_msgSend(_b0,"decodeObjectForKey:",_ab);
_state=_1;
objj_msgSend(_ae,"setRunLoopModes:",objj_msgSend(_b0,"decodeObjectForKey:",_ac));
objj_msgSend(_ae,"setGroupsByEvent:",objj_msgSend(_b0,"decodeBoolForKey:",_ad));
}
return _ae;
}
}),new objj_method(sel_getUid("encodeWithCoder:"),function(_b1,_b2,_b3){
with(_b1){
objj_msgSend(_b3,"encodeObject:forKey:",_redoStack,_a7);
objj_msgSend(_b3,"encodeObject:forKey:",_undoStack,_a8);
objj_msgSend(_b3,"encodeInt:forKey:",_levelsOfUndo,_a9);
objj_msgSend(_b3,"encodeObject:forKey:",_actionName,_aa);
objj_msgSend(_b3,"encodeObject:forKey:",_currentGrouping,_ab);
objj_msgSend(_b3,"encodeObject:forKey:",_runLoopModes,_ac);
objj_msgSend(_b3,"encodeBool:forKey:",_groupsByEvent,_ad);
}
})]);
var _6=objj_allocateClassPair(CPProxy,"_CPUndoManagerProxy"),_7=_6.isa;
class_addIvars(_6,[new objj_ivar("_undoManager")]);
objj_registerClassPair(_6);
class_addMethods(_6,[new objj_method(sel_getUid("methodSignatureForSelector:"),function(_b4,_b5,_b6){
with(_b4){
return objj_msgSend(_undoManager,"_methodSignatureOfPreparedTargetForSelector:",_b6);
}
}),new objj_method(sel_getUid("forwardInvocation:"),function(_b7,_b8,_b9){
with(_b7){
objj_msgSend(_undoManager,"_forwardInvocationToPreparedTarget:",_b9);
}
})]);
p;9;CPProxy.jt;3618;@STATIC;1.0;i;13;CPException.ji;14;CPInvocation.ji;10;CPObject.ji;10;CPString.jt;3532;
objj_executeFile("CPException.j",YES);
objj_executeFile("CPInvocation.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPString.j",YES);
var _1=objj_allocateClassPair(Nil,"CPProxy"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("methodSignatureForSelector:"),function(_3,_4,_5){
with(_3){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"-methodSignatureForSelector: called on abstract CPProxy class.");
}
}),new objj_method(sel_getUid("forwardInvocation:"),function(_6,_7,_8){
with(_6){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"-forwardInvocation: called on abstract CPProxy class.");
}
}),new objj_method(sel_getUid("forward::"),function(_9,_a,_b,_c){
with(_9){
return objj_msgSend(CPObject,"methodForSelector:",_a)(_9,_a,_b,_c);
}
}),new objj_method(sel_getUid("hash"),function(_d,_e){
with(_d){
return objj_msgSend(_d,"UID");
}
}),new objj_method(sel_getUid("UID"),function(_f,_10){
with(_f){
if(typeof _f._UID==="undefined"){
_f._UID=objj_generateObjectUID();
}
return _UID;
}
}),new objj_method(sel_getUid("isEqual:"),function(_11,_12,_13){
with(_11){
return _11===object;
}
}),new objj_method(sel_getUid("self"),function(_14,_15){
with(_14){
return _14;
}
}),new objj_method(sel_getUid("class"),function(_16,_17){
with(_16){
return isa;
}
}),new objj_method(sel_getUid("superclass"),function(_18,_19){
with(_18){
return class_getSuperclass(isa);
}
}),new objj_method(sel_getUid("performSelector:"),function(_1a,_1b,_1c){
with(_1a){
return objj_msgSend(_1a,_1c);
}
}),new objj_method(sel_getUid("performSelector:withObject:"),function(_1d,_1e,_1f,_20){
with(_1d){
return objj_msgSend(_1d,_1f,_20);
}
}),new objj_method(sel_getUid("performSelector:withObject:withObject:"),function(_21,_22,_23,_24,_25){
with(_21){
return objj_msgSend(_21,_23,_24,_25);
}
}),new objj_method(sel_getUid("isProxy"),function(_26,_27){
with(_26){
return YES;
}
}),new objj_method(sel_getUid("isKindOfClass:"),function(_28,_29,_2a){
with(_28){
var _2b=objj_msgSend(_28,"methodSignatureForSelector:",_29),_2c=objj_msgSend(CPInvocation,"invocationWithMethodSignature:",_2b);
objj_msgSend(_28,"forwardInvocation:",_2c);
return objj_msgSend(_2c,"returnValue");
}
}),new objj_method(sel_getUid("isMemberOfClass:"),function(_2d,_2e,_2f){
with(_2d){
var _30=objj_msgSend(_2d,"methodSignatureForSelector:",_2e),_31=objj_msgSend(CPInvocation,"invocationWithMethodSignature:",_30);
objj_msgSend(_2d,"forwardInvocation:",_31);
return objj_msgSend(_31,"returnValue");
}
}),new objj_method(sel_getUid("respondsToSelector:"),function(_32,_33,_34){
with(_32){
var _35=objj_msgSend(_32,"methodSignatureForSelector:",_33),_36=objj_msgSend(CPInvocation,"invocationWithMethodSignature:",_35);
objj_msgSend(_32,"forwardInvocation:",_36);
return objj_msgSend(_36,"returnValue");
}
}),new objj_method(sel_getUid("description"),function(_37,_38){
with(_37){
return "<"+class_getName(isa)+" 0x"+objj_msgSend(CPString,"stringWithHash:",objj_msgSend(_37,"UID"))+">";
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("load"),function(_39,_3a){
with(_39){
}
}),new objj_method(sel_getUid("initialize"),function(_3b,_3c){
with(_3b){
}
}),new objj_method(sel_getUid("class"),function(_3d,_3e){
with(_3d){
return _3d;
}
}),new objj_method(sel_getUid("alloc"),function(_3f,_40){
with(_3f){
return class_createInstance(_3f);
}
}),new objj_method(sel_getUid("respondsToSelector:"),function(_41,_42,_43){
with(_41){
return !!class_getInstanceMethod(isa,aSelector);
}
})]);
p;21;CPKeyValueObserving.jt;30040;@STATIC;1.0;i;9;CPArray.ji;14;CPDictionary.ji;13;CPException.ji;8;CPNull.ji;10;CPObject.ji;7;CPSet.ji;13;CPArray+KVO.ji;11;CPSet+KVO.jt;29898;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPException.j",YES);
objj_executeFile("CPNull.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPSet.j",YES);
var _1=objj_getClass("CPObject");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPObject\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("willChangeValueForKey:"),function(_3,_4,_5){
with(_3){
if(!_5){
return;
}
if(!_3[_6]){
if(!_3._willChangeMessageCounter){
_3._willChangeMessageCounter=new Object();
}
if(!_3._willChangeMessageCounter[_5]){
_3._willChangeMessageCounter[_5]=1;
}else{
_3._willChangeMessageCounter[_5]+=1;
}
}
}
}),new objj_method(sel_getUid("didChangeValueForKey:"),function(_7,_8,_9){
with(_7){
if(!_9){
return;
}
if(!_7[_6]){
if(_7._willChangeMessageCounter&&_7._willChangeMessageCounter[_9]){
_7._willChangeMessageCounter[_9]-=1;
if(!_7._willChangeMessageCounter[_9]){
delete _7._willChangeMessageCounter[_9];
}
}else{
objj_msgSend(CPException,"raise:reason:","CPKeyValueObservingException","'didChange...' message called without prior call of 'willChange...'");
}
}
}
}),new objj_method(sel_getUid("willChange:valuesAtIndexes:forKey:"),function(_a,_b,_c,_d,_e){
with(_a){
if(!_e){
return;
}
if(!_a[_6]){
if(!_a._willChangeMessageCounter){
_a._willChangeMessageCounter=new Object();
}
if(!_a._willChangeMessageCounter[_e]){
_a._willChangeMessageCounter[_e]=1;
}else{
_a._willChangeMessageCounter[_e]+=1;
}
}
}
}),new objj_method(sel_getUid("didChange:valuesAtIndexes:forKey:"),function(_f,_10,_11,_12,_13){
with(_f){
if(!_13){
return;
}
if(!_f[_6]){
if(_f._willChangeMessageCounter&&_f._willChangeMessageCounter[_13]){
_f._willChangeMessageCounter[_13]-=1;
if(!_f._willChangeMessageCounter[_13]){
delete _f._willChangeMessageCounter[_13];
}
}else{
objj_msgSend(CPException,"raise:reason:","CPKeyValueObservingException","'didChange...' message called without prior call of 'willChange...'");
}
}
}
}),new objj_method(sel_getUid("willChangeValueForKey:withSetMutation:usingObjects:"),function(_14,_15,_16,_17,_18){
with(_14){
if(!_16){
return;
}
if(!_14[_6]){
if(!_14._willChangeMessageCounter){
_14._willChangeMessageCounter=new Object();
}
if(!_14._willChangeMessageCounter[_16]){
_14._willChangeMessageCounter[_16]=1;
}else{
_14._willChangeMessageCounter[_16]+=1;
}
}
}
}),new objj_method(sel_getUid("didChangeValueForKey:withSetMutation:usingObjects:"),function(_19,_1a,_1b,_1c,_1d){
with(_19){
if(!_19[_6]){
if(_19._willChangeMessageCounter&&_19._willChangeMessageCounter[_1b]){
_19._willChangeMessageCounter[_1b]-=1;
if(!_19._willChangeMessageCounter[_1b]){
delete _19._willChangeMessageCounter[_1b];
}
}else{
objj_msgSend(CPException,"raise:reason:","CPKeyValueObservingException","'didChange...' message called without prior call of 'willChange...'");
}
}
}
}),new objj_method(sel_getUid("addObserver:forKeyPath:options:context:"),function(_1e,_1f,_20,_21,_22,_23){
with(_1e){
if(!_20||!_21){
return;
}
objj_msgSend(objj_msgSend(_CPKVOProxy,"proxyForObject:",_1e),"_addObserver:forKeyPath:options:context:",_20,_21,_22,_23);
}
}),new objj_method(sel_getUid("removeObserver:forKeyPath:"),function(_24,_25,_26,_27){
with(_24){
if(!_26||!_27){
return;
}
objj_msgSend(_24[_6],"_removeObserver:forKeyPath:",_26,_27);
}
}),new objj_method(sel_getUid("applyChange:toKeyPath:"),function(_28,_29,_2a,_2b){
with(_28){
var _2c=objj_msgSend(_2a,"objectForKey:",CPKeyValueChangeKindKey),_2d=objj_msgSend(_2a,"objectForKey:",CPKeyValueChangeOldKey),_2e=objj_msgSend(_2a,"objectForKey:",CPKeyValueChangeNewKey);
if(_2e===objj_msgSend(CPNull,"null")){
_2e=nil;
}
if(_2c===CPKeyValueChangeSetting){
return objj_msgSend(_28,"setValue:forKeyPath:",_2e,_2b);
}
var _2f=objj_msgSend(_2a,"objectForKey:",CPKeyValueChangeIndexesKey);
if(_2f){
if(_2c===CPKeyValueChangeInsertion){
objj_msgSend(objj_msgSend(_28,"mutableArrayValueForKeyPath:",_2b),"insertObjects:atIndexes:",_2e,_2f);
}else{
if(_2c===CPKeyValueChangeRemoval){
objj_msgSend(objj_msgSend(_28,"mutableArrayValueForKeyPath:",_2b),"removeObjectsAtIndexes:",_2f);
}else{
if(_2c===CPKeyValueChangeReplacement){
objj_msgSend(objj_msgSend(_28,"mutableArrayValueForKeyPath:",_2b),"replaceObjectAtIndexes:withObjects:",_2f,_2e);
}
}
}
}else{
if(_2c===CPKeyValueChangeInsertion){
objj_msgSend(objj_msgSend(_28,"mutableSetValueForKeyPath:",_2b),"unionSet:",_2e);
}else{
if(_2c===CPKeyValueChangeRemoval){
objj_msgSend(objj_msgSend(_28,"mutableSetValueForKeyPath:",_2b),"minusSet:",_2d);
}else{
if(_2c===CPKeyValueChangeReplacement){
objj_msgSend(objj_msgSend(_28,"mutableSetValueForKeyPath:",_2b),"setSet:",_2e);
}
}
}
}
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("automaticallyNotifiesObserversForKey:"),function(_30,_31,_32){
with(_30){
var _33=_32.charAt(0).toUpperCase()+_32.substring(1),_34="automaticallyNotifiesObserversOf"+_33;
if(objj_msgSend(objj_msgSend(_30,"class"),"respondsToSelector:",_34)){
return objj_msgSend(objj_msgSend(_30,"class"),_34);
}
return YES;
}
}),new objj_method(sel_getUid("keyPathsForValuesAffectingValueForKey:"),function(_35,_36,_37){
with(_35){
var _38=_37.charAt(0).toUpperCase()+_37.substring(1),_39="keyPathsForValuesAffecting"+_38;
if(objj_msgSend(objj_msgSend(_35,"class"),"respondsToSelector:",_39)){
return objj_msgSend(objj_msgSend(_35,"class"),_39);
}
return objj_msgSend(CPSet,"set");
}
})]);
var _1=objj_getClass("CPDictionary");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPDictionary\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("inverseChangeDictionary"),function(_3a,_3b){
with(_3a){
var _3c=objj_msgSend(_3a,"mutableCopy"),_3d=objj_msgSend(_3a,"objectForKey:",CPKeyValueChangeKindKey);
if(_3d===CPKeyValueChangeSetting||_3d===CPKeyValueChangeReplacement){
objj_msgSend(_3c,"setObject:forKey:",objj_msgSend(_3a,"objectForKey:",CPKeyValueChangeOldKey),CPKeyValueChangeNewKey);
objj_msgSend(_3c,"setObject:forKey:",objj_msgSend(_3a,"objectForKey:",CPKeyValueChangeNewKey),CPKeyValueChangeOldKey);
}else{
if(_3d===CPKeyValueChangeInsertion){
objj_msgSend(_3c,"setObject:forKey:",CPKeyValueChangeRemoval,CPKeyValueChangeKindKey);
objj_msgSend(_3c,"setObject:forKey:",objj_msgSend(_3a,"objectForKey:",CPKeyValueChangeNewKey),CPKeyValueChangeOldKey);
objj_msgSend(_3c,"removeObjectForKey:",CPKeyValueChangeNewKey);
}else{
if(_3d===CPKeyValueChangeRemoval){
objj_msgSend(_3c,"setObject:forKey:",CPKeyValueChangeInsertion,CPKeyValueChangeKindKey);
objj_msgSend(_3c,"setObject:forKey:",objj_msgSend(_3a,"objectForKey:",CPKeyValueChangeOldKey),CPKeyValueChangeNewKey);
objj_msgSend(_3c,"removeObjectForKey:",CPKeyValueChangeOldKey);
}
}
}
return _3c;
}
})]);
CPKeyValueObservingOptionNew=1<<0;
CPKeyValueObservingOptionOld=1<<1;
CPKeyValueObservingOptionInitial=1<<2;
CPKeyValueObservingOptionPrior=1<<3;
CPKeyValueChangeKindKey="CPKeyValueChangeKindKey";
CPKeyValueChangeNewKey="CPKeyValueChangeNewKey";
CPKeyValueChangeOldKey="CPKeyValueChangeOldKey";
CPKeyValueChangeIndexesKey="CPKeyValueChangeIndexesKey";
CPKeyValueChangeNotificationIsPriorKey="CPKeyValueChangeNotificationIsPriorKey";
CPKeyValueChangeSetting=1;
CPKeyValueChangeInsertion=2;
CPKeyValueChangeRemoval=3;
CPKeyValueChangeReplacement=4;
CPKeyValueUnionSetMutation=1;
CPKeyValueMinusSetMutation=2;
CPKeyValueIntersectSetMutation=3;
CPKeyValueSetSetMutation=4;
_CPKeyValueChangeSetMutationObjectsKey="_CPKeyValueChangeSetMutationObjectsKey";
_CPKeyValueChangeSetMutationKindKey="_CPKeyValueChangeSetMutationKindKey";
_CPKeyValueChangeSetMutationNewValueKey="_CPKeyValueChangeSetMutationNewValueKey";
var _3e=function(_3f){
switch(_3f){
case CPKeyValueUnionSetMutation:
return CPKeyValueChangeInsertion;
case CPKeyValueMinusSetMutation:
return CPKeyValueChangeRemoval;
case CPKeyValueIntersectSetMutation:
return CPKeyValueChangeRemoval;
case CPKeyValueSetSetMutation:
return CPKeyValueChangeReplacement;
}
};
var _40=CPKeyValueObservingOptionNew|CPKeyValueObservingOptionOld,_41="$KVODEPENDENT",_6="$KVOPROXY";
var _1=objj_allocateClassPair(CPObject,"_CPKVOProxy"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_targetObject"),new objj_ivar("_nativeClass"),new objj_ivar("_changesForKey"),new objj_ivar("_nestingForKey"),new objj_ivar("_observersForKey"),new objj_ivar("_observersForKeyLength"),new objj_ivar("_replacedKeys")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithTarget:"),function(_42,_43,_44){
with(_42){
if(_42=objj_msgSendSuper({receiver:_42,super_class:objj_getClass("_CPKVOProxy").super_class},"init")){
_targetObject=_44;
_nativeClass=objj_msgSend(_44,"class");
_observersForKey={};
_changesForKey={};
_nestingForKey={};
_observersForKeyLength=0;
objj_msgSend(_42,"_replaceClass");
_44[_6]=_42;
}
return _42;
}
}),new objj_method(sel_getUid("_replaceClass"),function(_45,_46){
with(_45){
var _47=_nativeClass,_48="$KVO_"+class_getName(_nativeClass),_49=objj_lookUpClass(_48);
if(_49){
_targetObject.isa=_49;
_replacedKeys=_49._replacedKeys;
return;
}
var _4a=objj_allocateClassPair(_47,_48);
objj_registerClassPair(_4a);
_replacedKeys=objj_msgSend(CPSet,"set");
_4a._replacedKeys=_replacedKeys;
var _4b=class_copyMethodList(_CPKVOModelSubclass);
if(objj_msgSend(_targetObject,"isKindOfClass:",objj_msgSend(CPDictionary,"class"))){
_4b=_4b.concat(class_copyMethodList(_CPKVOModelDictionarySubclass));
}
class_addMethods(_4a,_4b);
_targetObject.isa=_4a;
}
}),new objj_method(sel_getUid("_replaceModifiersForKey:"),function(_4c,_4d,_4e){
with(_4c){
if(objj_msgSend(_replacedKeys,"containsObject:",_4e)||!objj_msgSend(_nativeClass,"automaticallyNotifiesObserversForKey:",_4e)){
return;
}
objj_msgSend(_replacedKeys,"addObject:",_4e);
var _4f=_nativeClass,_50=_targetObject.isa,_51=_4e.charAt(0).toUpperCase()+_4e.substring(1);
var _52=sel_getUid("set"+_51+":"),_53=class_getInstanceMethod(_4f,_52);
if(_53){
var _54=_53.method_imp;
class_addMethod(_50,_52,function(_55,_56,_57){
objj_msgSend(_55,"willChangeValueForKey:",_4e);
_54(_55,_56,_57);
objj_msgSend(_55,"didChangeValueForKey:",_4e);
},"");
}
var _58=sel_getUid("_set"+_51+":"),_59=class_getInstanceMethod(_4f,_58);
if(_59){
var _5a=_59.method_imp;
class_addMethod(_50,_58,function(_5b,_5c,_5d){
objj_msgSend(_5b,"willChangeValueForKey:",_4e);
_5a(_5b,_5c,_5d);
objj_msgSend(_5b,"didChangeValueForKey:",_4e);
},"");
}
var _5e=sel_getUid("insertObject:in"+_51+"AtIndex:"),_5f=class_getInstanceMethod(_4f,_5e),_60=sel_getUid("insert"+_51+":atIndexes:"),_61=class_getInstanceMethod(_4f,_60),_62=sel_getUid("removeObjectFrom"+_51+"AtIndex:"),_63=class_getInstanceMethod(_4f,_62),_64=sel_getUid("remove"+_51+"AtIndexes:"),_65=class_getInstanceMethod(_4f,_64);
if((_5f||_61)&&(_63||_65)){
if(_5f){
var _66=_5f.method_imp;
class_addMethod(_50,_5e,function(_67,_68,_69,_6a){
objj_msgSend(_67,"willChange:valuesAtIndexes:forKey:",CPKeyValueChangeInsertion,objj_msgSend(CPIndexSet,"indexSetWithIndex:",_6a),_4e);
_66(_67,_68,_69,_6a);
objj_msgSend(_67,"didChange:valuesAtIndexes:forKey:",CPKeyValueChangeInsertion,objj_msgSend(CPIndexSet,"indexSetWithIndex:",_6a),_4e);
},"");
}
if(_61){
var _6b=_61.method_imp;
class_addMethod(_50,_60,function(_6c,_6d,_6e,_6f){
objj_msgSend(_6c,"willChange:valuesAtIndexes:forKey:",CPKeyValueChangeInsertion,objj_msgSend(_6f,"copy"),_4e);
_6b(_6c,_6d,_6e,_6f);
objj_msgSend(_6c,"didChange:valuesAtIndexes:forKey:",CPKeyValueChangeInsertion,objj_msgSend(_6f,"copy"),_4e);
},"");
}
if(_63){
var _70=_63.method_imp;
class_addMethod(_50,_62,function(_71,_72,_73){
objj_msgSend(_71,"willChange:valuesAtIndexes:forKey:",CPKeyValueChangeRemoval,objj_msgSend(CPIndexSet,"indexSetWithIndex:",_73),_4e);
_70(_71,_72,_73);
objj_msgSend(_71,"didChange:valuesAtIndexes:forKey:",CPKeyValueChangeRemoval,objj_msgSend(CPIndexSet,"indexSetWithIndex:",_73),_4e);
},"");
}
if(_65){
var _74=_65.method_imp;
class_addMethod(_50,_64,function(_75,_76,_77){
objj_msgSend(_75,"willChange:valuesAtIndexes:forKey:",CPKeyValueChangeRemoval,objj_msgSend(_77,"copy"),_4e);
_74(_75,_76,_77);
objj_msgSend(_75,"didChange:valuesAtIndexes:forKey:",CPKeyValueChangeRemoval,objj_msgSend(_77,"copy"),_4e);
},"");
}
var _78=sel_getUid("replaceObjectIn"+_51+"AtIndex:withObject:"),_79=class_getInstanceMethod(_4f,_78);
if(_79){
var _7a=_79.method_imp;
class_addMethod(_50,_78,function(_7b,_7c,_7d,_7e){
objj_msgSend(_7b,"willChange:valuesAtIndexes:forKey:",CPKeyValueChangeReplacement,objj_msgSend(CPIndexSet,"indexSetWithIndex:",_7d),_4e);
_7a(_7b,_7c,_7d,_7e);
objj_msgSend(_7b,"didChange:valuesAtIndexes:forKey:",CPKeyValueChangeReplacement,objj_msgSend(CPIndexSet,"indexSetWithIndex:",_7d),_4e);
},"");
}
var _7f=sel_getUid("replace"+_51+"AtIndexes:with"+_51+":"),_80=class_getInstanceMethod(_4f,_7f);
if(_80){
var _81=_80.method_imp;
class_addMethod(_50,_7f,function(_82,_83,_84,_85){
objj_msgSend(_82,"willChange:valuesAtIndexes:forKey:",CPKeyValueChangeReplacement,objj_msgSend(_84,"copy"),_4e);
_7a(_82,_83,_84,_85);
objj_msgSend(_82,"didChange:valuesAtIndexes:forKey:",CPKeyValueChangeReplacement,objj_msgSend(_84,"copy"),_4e);
},"");
}
}
var _86=sel_getUid("add"+_51+"Object:"),_87=class_getInstanceMethod(_4f,_86),_88=sel_getUid("add"+_51+":"),_89=class_getInstanceMethod(_4f,_88),_8a=sel_getUid("remove"+_51+"Object:"),_8b=class_getInstanceMethod(_4f,_8a),_8c=sel_getUid("remove"+_51+":"),_8d=class_getInstanceMethod(_4f,_8c);
if((_87||_89)&&(_8b||_8d)){
if(_87){
var _8e=_87.method_imp;
class_addMethod(_50,_86,function(_8f,_90,_91){
objj_msgSend(_8f,"willChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueUnionSetMutation,objj_msgSend(CPSet,"setWithObject:",_91));
_8e(_8f,_90,_91);
objj_msgSend(_8f,"didChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueUnionSetMutation,objj_msgSend(CPSet,"setWithObject:",_91));
},"");
}
if(_89){
var _92=_89.method_imp;
class_addMethod(_50,_88,function(_93,_94,_95){
objj_msgSend(_93,"willChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueUnionSetMutation,objj_msgSend(_95,"copy"));
_92(_93,_94,_95);
objj_msgSend(_93,"didChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueUnionSetMutation,objj_msgSend(_95,"copy"));
},"");
}
if(_8b){
var _96=_8b.method_imp;
class_addMethod(_50,_8a,function(_97,_98,_99){
objj_msgSend(_97,"willChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueMinusSetMutation,objj_msgSend(CPSet,"setWithObject:",_99));
_96(_97,_98,_99);
objj_msgSend(_97,"didChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueMinusSetMutation,objj_msgSend(CPSet,"setWithObject:",_99));
},"");
}
if(_8d){
var _9a=_8d.method_imp;
class_addMethod(_50,_8c,function(_9b,_9c,_9d){
objj_msgSend(_9b,"willChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueMinusSetMutation,objj_msgSend(_9d,"copy"));
_9a(_9b,_9c,_9d);
objj_msgSend(_9b,"didChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueMinusSetMutation,objj_msgSend(_9d,"copy"));
},"");
}
var _9e=sel_getUid("intersect"+_51+":"),_9f=class_getInstanceMethod(_4f,_9e);
if(_9f){
var _a0=_9f.method_imp;
class_addMethod(_50,_9e,function(_a1,_a2,_a3){
objj_msgSend(_a1,"willChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueIntersectSetMutation,objj_msgSend(_a3,"copy"));
_a0(_a1,_a2,_a3);
objj_msgSend(_a1,"didChangeValueForKey:withSetMutation:usingObjects:",_4e,CPKeyValueIntersectSetMutation,objj_msgSend(_a3,"copy"));
},"");
}
}
var _a4=objj_msgSend(objj_msgSend(_nativeClass,"keyPathsForValuesAffectingValueForKey:",_4e),"allObjects"),_a5=_a4?_a4.length:0;
if(!_a5){
return;
}
var _a6=_nativeClass[_41];
if(!_a6){
_a6={};
_nativeClass[_41]=_a6;
}
while(_a5--){
var _a7=_a4[_a5],_a8=_a6[_a7];
if(!_a8){
_a8=objj_msgSend(CPSet,"new");
_a6[_a7]=_a8;
}
objj_msgSend(_a8,"addObject:",_4e);
if(_a7.indexOf(".")!==-1){
objj_msgSend(_targetObject,"addObserver:forKeyPath:options:context:",_4c,_a7,CPKeyValueObservingOptionPrior|_40,nil);
}else{
objj_msgSend(_4c,"_replaceModifiersForKey:",_a7);
}
}
}
}),new objj_method(sel_getUid("observeValueForKeyPath:ofObject:change:context:"),function(_a9,_aa,_ab,_ac,_ad,_ae){
with(_a9){
var _af=_nativeClass[_41],_b0=objj_msgSend(_af[_ab],"allObjects");
var _b1=!!objj_msgSend(_ad,"objectForKey:",CPKeyValueChangeNotificationIsPriorKey);
for(var i=0;i<objj_msgSend(_b0,"count");i++){
var _b2=objj_msgSend(_b0,"objectAtIndex:",i);
objj_msgSend(_a9,"_sendNotificationsForKey:changeOptions:isBefore:",_b2,_ad,_b1);
}
}
}),new objj_method(sel_getUid("_addObserver:forKeyPath:options:context:"),function(_b3,_b4,_b5,_b6,_b7,_b8){
with(_b3){
if(!_b5){
return;
}
var _b9=nil;
if(_b6.indexOf(".")!==CPNotFound&&_b6.charAt(0)!=="@"){
_b9=objj_msgSend(objj_msgSend(_CPKVOForwardingObserver,"alloc"),"initWithKeyPath:object:observer:options:context:",_b6,_targetObject,_b5,_b7,_b8);
}else{
objj_msgSend(_b3,"_replaceModifiersForKey:",_b6);
}
var _ba=_observersForKey[_b6];
if(!_ba){
_ba=objj_msgSend(CPDictionary,"dictionary");
_observersForKey[_b6]=_ba;
_observersForKeyLength++;
}
objj_msgSend(_ba,"setObject:forKey:",_bb(_b5,_b7,_b8,_b9),objj_msgSend(_b5,"UID"));
if(_b7&CPKeyValueObservingOptionInitial){
var _bc=objj_msgSend(_targetObject,"valueForKeyPath:",_b6);
if(_bc===nil||_bc===undefined){
_bc=objj_msgSend(CPNull,"null");
}
var _bd=objj_msgSend(CPDictionary,"dictionaryWithObject:forKey:",_bc,CPKeyValueChangeNewKey);
objj_msgSend(_b5,"observeValueForKeyPath:ofObject:change:context:",_b6,_targetObject,_bd,_b8);
}
}
}),new objj_method(sel_getUid("_removeObserver:forKeyPath:"),function(_be,_bf,_c0,_c1){
with(_be){
var _c2=_observersForKey[_c1];
if(!_c2){
CPLog.warn("Cannot remove an observer %@ for the key path \"%@\" from %@ because it is not registered as an observer.",_targetObject,_c1,_c0);
return;
}
if(_c1.indexOf(".")!=CPNotFound){
var _c3=objj_msgSend(_c2,"objectForKey:",objj_msgSend(_c0,"UID")).forwarder;
objj_msgSend(_c3,"finalize");
}
objj_msgSend(_c2,"removeObjectForKey:",objj_msgSend(_c0,"UID"));
if(!objj_msgSend(_c2,"count")){
_observersForKeyLength--;
delete _observersForKey[_c1];
}
if(!_observersForKeyLength){
_targetObject.isa=_nativeClass;
delete _targetObject[_6];
}
}
}),new objj_method(sel_getUid("_sendNotificationsForKey:changeOptions:isBefore:"),function(_c4,_c5,_c6,_c7,_c8){
with(_c4){
var _c9=_changesForKey[_c6];
if(_c8){
if(_c9){
var _ca=_nestingForKey[_c6];
if(!_ca){
objj_msgSend(CPException,"raise:reason:",CPInternalInconsistencyException,"_changesForKey without _nestingForKey");
}
_nestingForKey[_c6]=_ca+1;
return;
}
_nestingForKey[_c6]=1;
_c9=_c7;
var _cb=objj_msgSend(_c9,"objectForKey:",CPKeyValueChangeIndexesKey),_cc=_c9[_CPKeyValueChangeSetMutationKindKey];
if(_cc){
var _cd=objj_msgSend(_c9[_CPKeyValueChangeSetMutationObjectsKey],"copy"),_ce=objj_msgSend(objj_msgSend(_targetObject,"valueForKey:",_c6),"copy");
if(_cc==CPKeyValueMinusSetMutation){
objj_msgSend(_ce,"intersectSet:",_cd);
objj_msgSend(_c9,"setValue:forKey:",_ce,CPKeyValueChangeOldKey);
}else{
if(_cc===CPKeyValueIntersectSetMutation||_cc===CPKeyValueSetSetMutation){
objj_msgSend(_ce,"minusSet:",_cd);
objj_msgSend(_c9,"setValue:forKey:",_ce,CPKeyValueChangeOldKey);
}
}
if(_cc===CPKeyValueUnionSetMutation||_cc===CPKeyValueSetSetMutation){
objj_msgSend(_cd,"minusSet:",_ce);
_c9[_CPKeyValueChangeSetMutationNewValueKey]=_cd;
}
}else{
if(_cb){
var _cf=objj_msgSend(_c9,"objectForKey:",CPKeyValueChangeKindKey);
if(_cf===CPKeyValueChangeReplacement||_cf===CPKeyValueChangeRemoval){
var _d0=objj_msgSend(objj_msgSend(_targetObject,"mutableArrayValueForKeyPath:",_c6),"objectsAtIndexes:",_cb);
objj_msgSend(_c9,"setValue:forKey:",_d0,CPKeyValueChangeOldKey);
}
}else{
var _d1=objj_msgSend(_targetObject,"valueForKey:",_c6);
if(_d1===nil||_d1===undefined){
_d1=objj_msgSend(CPNull,"null");
}
objj_msgSend(_c9,"setObject:forKey:",_d1,CPKeyValueChangeOldKey);
}
}
objj_msgSend(_c9,"setObject:forKey:",1,CPKeyValueChangeNotificationIsPriorKey);
_changesForKey[_c6]=_c9;
}else{
var _ca=_nestingForKey[_c6];
if(!_c9||!_ca){
if(_targetObject._willChangeMessageCounter&&_targetObject._willChangeMessageCounter[_c6]){
_targetObject._willChangeMessageCounter[_c6]-=1;
if(!_targetObject._willChangeMessageCounter[_c6]){
delete _targetObject._willChangeMessageCounter[_c6];
}
return;
}else{
objj_msgSend(CPException,"raise:reason:","CPKeyValueObservingException","'didChange...' message called without prior call of 'willChange...'");
}
}
_nestingForKey[_c6]=_ca-1;
if(_ca-1>0){
return;
}
delete _nestingForKey[_c6];
objj_msgSend(_c9,"removeObjectForKey:",CPKeyValueChangeNotificationIsPriorKey);
var _cb=objj_msgSend(_c9,"objectForKey:",CPKeyValueChangeIndexesKey),_cc=_c9[_CPKeyValueChangeSetMutationKindKey];
if(_cc){
var _d2=_c9[_CPKeyValueChangeSetMutationNewValueKey];
objj_msgSend(_c9,"setValue:forKey:",_d2,CPKeyValueChangeNewKey);
delete _c9[_CPKeyValueChangeSetMutationNewValueKey];
delete _c9[_CPKeyValueChangeSetMutationObjectsKey];
delete _c9[_CPKeyValueChangeSetMutationKindKey];
}else{
if(_cb){
var _cf=objj_msgSend(_c9,"objectForKey:",CPKeyValueChangeKindKey);
if(_cf==CPKeyValueChangeReplacement||_cf==CPKeyValueChangeInsertion){
var _d0=objj_msgSend(objj_msgSend(_targetObject,"mutableArrayValueForKeyPath:",_c6),"objectsAtIndexes:",_cb);
objj_msgSend(_c9,"setValue:forKey:",_d0,CPKeyValueChangeNewKey);
}
}else{
var _d2=objj_msgSend(_targetObject,"valueForKey:",_c6);
if(_d2===nil||_d2===undefined){
_d2=objj_msgSend(CPNull,"null");
}
objj_msgSend(_c9,"setObject:forKey:",_d2,CPKeyValueChangeNewKey);
}
}
delete _changesForKey[_c6];
}
var _d3=objj_msgSend(_observersForKey[_c6],"allValues"),_d4=_d3?_d3.length:0;
while(_d4--){
var _d5=_d3[_d4];
if(!_c8||(_d5.options&CPKeyValueObservingOptionPrior)){
objj_msgSend(_d5.observer,"observeValueForKeyPath:ofObject:change:context:",_c6,_targetObject,_c9,_d5.context);
}
}
var _d6=_nativeClass[_41];
if(!_d6){
return;
}
var _d7=objj_msgSend(_d6[_c6],"allObjects");
if(!_d7){
return;
}
var _d8=0,_d4=objj_msgSend(_d7,"count");
for(;_d8<_d4;++_d8){
var _d9=_d7[_d8];
objj_msgSend(_c4,"_sendNotificationsForKey:changeOptions:isBefore:",_d9,_c8?objj_msgSend(_c7,"copy"):_changesForKey[_d9],_c8);
}
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("proxyForObject:"),function(_da,_db,_dc){
with(_da){
var _dd=_dc[_6];
if(_dd){
return _dd;
}
return objj_msgSend(objj_msgSend(_da,"alloc"),"initWithTarget:",_dc);
}
})]);
var _1=objj_allocateClassPair(Nil,"_CPKVOModelSubclass"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("willChangeValueForKey:"),function(_de,_df,_e0){
with(_de){
var _e1=objj_msgSend(_de,"class"),_e2=sel_getUid("willChangeValueForKey:"),_e3=class_getMethodImplementation(_e1,_e2);
_e3(_de,_e2,_e0);
if(!_e0){
return;
}
var _e4=objj_msgSend(CPDictionary,"dictionaryWithObject:forKey:",CPKeyValueChangeSetting,CPKeyValueChangeKindKey);
objj_msgSend(objj_msgSend(_CPKVOProxy,"proxyForObject:",_de),"_sendNotificationsForKey:changeOptions:isBefore:",_e0,_e4,YES);
}
}),new objj_method(sel_getUid("didChangeValueForKey:"),function(_e5,_e6,_e7){
with(_e5){
var _e8=objj_msgSend(_e5,"class"),_e9=sel_getUid("didChangeValueForKey:"),_ea=class_getMethodImplementation(_e8,_e9);
_ea(_e5,_e9,_e7);
if(!_e7){
return;
}
objj_msgSend(objj_msgSend(_CPKVOProxy,"proxyForObject:",_e5),"_sendNotificationsForKey:changeOptions:isBefore:",_e7,nil,NO);
}
}),new objj_method(sel_getUid("willChange:valuesAtIndexes:forKey:"),function(_eb,_ec,_ed,_ee,_ef){
with(_eb){
var _f0=objj_msgSend(_eb,"class"),_f1=sel_getUid("willChange:valuesAtIndexes:forKey:"),_f2=class_getMethodImplementation(_f0,_f1);
_f2(_eb,_f1,_ed,_ee,_ef);
if(!_ef){
return;
}
var _f3=objj_msgSend(CPDictionary,"dictionaryWithObjects:forKeys:",[_ed,_ee],[CPKeyValueChangeKindKey,CPKeyValueChangeIndexesKey]);
objj_msgSend(objj_msgSend(_CPKVOProxy,"proxyForObject:",_eb),"_sendNotificationsForKey:changeOptions:isBefore:",_ef,_f3,YES);
}
}),new objj_method(sel_getUid("didChange:valuesAtIndexes:forKey:"),function(_f4,_f5,_f6,_f7,_f8){
with(_f4){
var _f9=objj_msgSend(_f4,"class"),_fa=sel_getUid("didChange:valuesAtIndexes:forKey:"),_fb=class_getMethodImplementation(_f9,_fa);
_fb(_f4,_fa,_f6,_f7,_f8);
if(!_f8){
return;
}
objj_msgSend(objj_msgSend(_CPKVOProxy,"proxyForObject:",_f4),"_sendNotificationsForKey:changeOptions:isBefore:",_f8,nil,NO);
}
}),new objj_method(sel_getUid("willChangeValueForKey:withSetMutation:usingObjects:"),function(_fc,_fd,_fe,_ff,_100){
with(_fc){
var _101=objj_msgSend(_fc,"class"),_102=sel_getUid("willChangeValueForKey:withSetMutation:usingObjects:"),_103=class_getMethodImplementation(_101,_102);
_103(_fc,_102,_fe,_ff,_100);
if(!_fe){
return;
}
var _104=_3e(_ff),_105=objj_msgSend(CPDictionary,"dictionaryWithObject:forKey:",_104,CPKeyValueChangeKindKey);
_105[_CPKeyValueChangeSetMutationObjectsKey]=_100;
_105[_CPKeyValueChangeSetMutationKindKey]=_ff;
objj_msgSend(objj_msgSend(_CPKVOProxy,"proxyForObject:",_fc),"_sendNotificationsForKey:changeOptions:isBefore:",_fe,_105,YES);
}
}),new objj_method(sel_getUid("didChangeValueForKey:withSetMutation:usingObjects:"),function(self,_106,aKey,_107,_108){
with(self){
var _109=objj_msgSend(self,"class"),_10a=sel_getUid("didChangeValueForKey:withSetMutation:usingObjects:"),_10b=class_getMethodImplementation(_109,_10a);
_10b(self,_10a,aKey,_107,_108);
if(!aKey){
return;
}
objj_msgSend(objj_msgSend(_CPKVOProxy,"proxyForObject:",self),"_sendNotificationsForKey:changeOptions:isBefore:",aKey,nil,NO);
}
}),new objj_method(sel_getUid("class"),function(self,_10c){
with(self){
return self[_6]._nativeClass;
}
}),new objj_method(sel_getUid("superclass"),function(self,_10d){
with(self){
return objj_msgSend(objj_msgSend(self,"class"),"superclass");
}
}),new objj_method(sel_getUid("isKindOfClass:"),function(self,_10e,_10f){
with(self){
return objj_msgSend(objj_msgSend(self,"class"),"isSubclassOfClass:",_10f);
}
}),new objj_method(sel_getUid("isMemberOfClass:"),function(self,_110,_111){
with(self){
return objj_msgSend(self,"class")==_111;
}
}),new objj_method(sel_getUid("className"),function(self,_112){
with(self){
return objj_msgSend(self,"class").name;
}
})]);
var _1=objj_allocateClassPair(Nil,"_CPKVOModelDictionarySubclass"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("removeAllObjects"),function(self,_113){
with(self){
var keys=objj_msgSend(self,"allKeys"),_114=objj_msgSend(keys,"count"),i=0;
for(;i<_114;i++){
objj_msgSend(self,"willChangeValueForKey:",keys[i]);
}
var _115=objj_msgSend(self,"class"),_116=sel_getUid("removeAllObjects"),_117=class_getMethodImplementation(_115,_116);
_117(self,_116);
for(i=0;i<_114;i++){
objj_msgSend(self,"didChangeValueForKey:",keys[i]);
}
}
}),new objj_method(sel_getUid("removeObjectForKey:"),function(self,_118,aKey){
with(self){
objj_msgSend(self,"willChangeValueForKey:",aKey);
var _119=objj_msgSend(self,"class"),_11a=sel_getUid("removeObjectForKey:"),_11b=class_getMethodImplementation(_119,_11a);
_11b(self,_11a,aKey);
objj_msgSend(self,"didChangeValueForKey:",aKey);
}
}),new objj_method(sel_getUid("setObject:forKey:"),function(self,_11c,_11d,aKey){
with(self){
objj_msgSend(self,"willChangeValueForKey:",aKey);
var _11e=objj_msgSend(self,"class"),_11f=sel_getUid("setObject:forKey:"),_120=class_getMethodImplementation(_11e,_11f);
_120(self,_11f,_11d,aKey);
objj_msgSend(self,"didChangeValueForKey:",aKey);
}
})]);
var _1=objj_allocateClassPair(CPObject,"_CPKVOForwardingObserver"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_object"),new objj_ivar("_observer"),new objj_ivar("_context"),new objj_ivar("_options"),new objj_ivar("_firstPart"),new objj_ivar("_secondPart"),new objj_ivar("_value")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithKeyPath:object:observer:options:context:"),function(self,_121,_122,_123,_124,_125,_126){
with(self){
self=objj_msgSendSuper({receiver:self,super_class:objj_getClass("_CPKVOForwardingObserver").super_class},"init");
_context=_126;
_observer=_124;
_object=_123;
_options=_125;
var _127=_122.indexOf(".");
if(_127===CPNotFound){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"Created _CPKVOForwardingObserver without compound key path: "+_122);
}
_firstPart=_122.substring(0,_127);
_secondPart=_122.substring(_127+1);
objj_msgSend(_object,"addObserver:forKeyPath:options:context:",self,_firstPart,_options,nil);
_value=objj_msgSend(_object,"valueForKey:",_firstPart);
if(_value){
objj_msgSend(_value,"addObserver:forKeyPath:options:context:",self,_secondPart,_options,nil);
}
return self;
}
}),new objj_method(sel_getUid("observeValueForKeyPath:ofObject:change:context:"),function(self,_128,_129,_12a,_12b,_12c){
with(self){
if(_129===_firstPart){
var _12d=objj_msgSend(_value,"valueForKeyPath:",_secondPart),_12e=objj_msgSend(_object,"valueForKeyPath:",_firstPart+"."+_secondPart),_12f=objj_msgSend(CPDictionary,"dictionaryWithObjectsAndKeys:",_12e?_12e:objj_msgSend(CPNull,"null"),CPKeyValueChangeNewKey,_12d?_12d:objj_msgSend(CPNull,"null"),CPKeyValueChangeOldKey,CPKeyValueChangeSetting,CPKeyValueChangeKindKey);
objj_msgSend(_observer,"observeValueForKeyPath:ofObject:change:context:",_firstPart+"."+_secondPart,_object,_12f,_context);
if(_value){
objj_msgSend(_value,"removeObserver:forKeyPath:",self,_secondPart);
}
_value=objj_msgSend(_object,"valueForKey:",_firstPart);
if(_value){
objj_msgSend(_value,"addObserver:forKeyPath:options:context:",self,_secondPart,_options,nil);
}
}else{
objj_msgSend(_observer,"observeValueForKeyPath:ofObject:change:context:",_firstPart+"."+_129,_object,_12b,_context);
}
}
}),new objj_method(sel_getUid("finalize"),function(self,_130){
with(self){
if(_value){
objj_msgSend(_value,"removeObserver:forKeyPath:",self,_secondPart);
}
objj_msgSend(_object,"removeObserver:forKeyPath:",self,_firstPart);
_object=nil;
_observer=nil;
_context=nil;
_value=nil;
}
})]);
var _bb=_bb=function(_131,_132,_133,_134){
return {observer:_131,options:_132,context:_133,forwarder:_134};
};
objj_executeFile("CPArray+KVO.j",YES);
objj_executeFile("CPSet+KVO.j",YES);
p;13;CPArray+KVO.jt;14489;@STATIC;1.0;i;9;CPArray.ji;8;CPNull.ji;27;_CPCollectionKVCOperators.jt;14412;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPNull.j",YES);
objj_executeFile("_CPCollectionKVCOperators.j",YES);
var _1=objj_getClass("CPObject");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPObject\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("mutableArrayValueForKey:"),function(_3,_4,_5){
with(_3){
return objj_msgSend(objj_msgSend(_CPKVCArray,"alloc"),"initWithKey:forProxyObject:",_5,_3);
}
}),new objj_method(sel_getUid("mutableArrayValueForKeyPath:"),function(_6,_7,_8){
with(_6){
var _9=_8.indexOf(".");
if(_9<0){
return objj_msgSend(_6,"mutableArrayValueForKey:",_8);
}
var _a=_8.substring(0,_9),_b=_8.substring(_9+1);
return objj_msgSend(objj_msgSend(_6,"valueForKeyPath:",_a),"mutableArrayValueForKeyPath:",_b);
}
})]);
var _1=objj_allocateClassPair(CPMutableArray,"_CPKVCArray"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_proxyObject"),new objj_ivar("_key"),new objj_ivar("_insertSEL"),new objj_ivar("_insert"),new objj_ivar("_removeSEL"),new objj_ivar("_remove"),new objj_ivar("_replaceSEL"),new objj_ivar("_replace"),new objj_ivar("_insertManySEL"),new objj_ivar("_insertMany"),new objj_ivar("_removeManySEL"),new objj_ivar("_removeMany"),new objj_ivar("_replaceManySEL"),new objj_ivar("_replaceMany"),new objj_ivar("_objectAtIndexSEL"),new objj_ivar("_objectAtIndex"),new objj_ivar("_objectsAtIndexesSEL"),new objj_ivar("_objectsAtIndexes"),new objj_ivar("_countSEL"),new objj_ivar("_count"),new objj_ivar("_accessSEL"),new objj_ivar("_access"),new objj_ivar("_setSEL"),new objj_ivar("_set")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithKey:forProxyObject:"),function(_c,_d,_e,_f){
with(_c){
_c=objj_msgSendSuper({receiver:_c,super_class:objj_getClass("_CPKVCArray").super_class},"init");
_key=_e;
_proxyObject=_f;
var _10=_key.charAt(0).toUpperCase()+_key.substring(1);
_insertSEL=sel_getName("insertObject:in"+_10+"AtIndex:");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_insertSEL)){
_insert=objj_msgSend(_proxyObject,"methodForSelector:",_insertSEL);
}
_removeSEL=sel_getName("removeObjectFrom"+_10+"AtIndex:");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_removeSEL)){
_remove=objj_msgSend(_proxyObject,"methodForSelector:",_removeSEL);
}
_replaceSEL=sel_getName("replaceObjectIn"+_10+"AtIndex:withObject:");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_replaceSEL)){
_replace=objj_msgSend(_proxyObject,"methodForSelector:",_replaceSEL);
}
_insertManySEL=sel_getName("insert"+_10+":atIndexes:");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_insertManySEL)){
_insertMany=objj_msgSend(_proxyObject,"methodForSelector:",_insertManySEL);
}
_removeManySEL=sel_getName("remove"+_10+"AtIndexes:");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_removeManySEL)){
_removeMany=objj_msgSend(_proxyObject,"methodForSelector:",_removeManySEL);
}
_replaceManySEL=sel_getName("replace"+_10+"AtIndexes:with"+_10+":");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_replaceManySEL)){
_replaceMany=objj_msgSend(_proxyObject,"methodForSelector:",_replaceManySEL);
}
_objectAtIndexSEL=sel_getName("objectIn"+_10+"AtIndex:");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_objectAtIndexSEL)){
_objectAtIndex=objj_msgSend(_proxyObject,"methodForSelector:",_objectAtIndexSEL);
}
_objectsAtIndexesSEL=sel_getName(_key+"AtIndexes:");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_objectsAtIndexesSEL)){
_objectsAtIndexes=objj_msgSend(_proxyObject,"methodForSelector:",_objectsAtIndexesSEL);
}
_countSEL=sel_getName("countOf"+_10);
if(objj_msgSend(_proxyObject,"respondsToSelector:",_countSEL)){
_count=objj_msgSend(_proxyObject,"methodForSelector:",_countSEL);
}
_accessSEL=sel_getName(_key);
if(objj_msgSend(_proxyObject,"respondsToSelector:",_accessSEL)){
_access=objj_msgSend(_proxyObject,"methodForSelector:",_accessSEL);
}
_setSEL=sel_getName("set"+_10+":");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_setSEL)){
_set=objj_msgSend(_proxyObject,"methodForSelector:",_setSEL);
}
return _c;
}
}),new objj_method(sel_getUid("copy"),function(_11,_12){
with(_11){
var i=0,_13=[],_14=objj_msgSend(_11,"count");
for(;i<_14;i++){
objj_msgSend(_13,"addObject:",objj_msgSend(_11,"objectAtIndex:",i));
}
return _13;
}
}),new objj_method(sel_getUid("_representedObject"),function(_15,_16){
with(_15){
if(_access){
return _access(_proxyObject,_accessSEL);
}
return objj_msgSend(_proxyObject,"valueForKey:",_key);
}
}),new objj_method(sel_getUid("_setRepresentedObject:"),function(_17,_18,_19){
with(_17){
if(_set){
return _set(_proxyObject,_setSEL,_19);
}
objj_msgSend(_proxyObject,"setValue:forKey:",_19,_key);
}
}),new objj_method(sel_getUid("count"),function(_1a,_1b){
with(_1a){
if(_count){
return _count(_proxyObject,_countSEL);
}
return objj_msgSend(objj_msgSend(_1a,"_representedObject"),"count");
}
}),new objj_method(sel_getUid("indexOfObject:inRange:"),function(_1c,_1d,_1e,_1f){
with(_1c){
var _20=_1f.location,_21=_1f.length,_22=!!_1e.isa;
for(;_20<_21;++_20){
var _23=objj_msgSend(_1c,"objectAtIndex:",_20);
if(_1e===_23||_22&&!!_23.isa&&objj_msgSend(_1e,"isEqual:",_23)){
return _20;
}
}
return CPNotFound;
}
}),new objj_method(sel_getUid("indexOfObject:"),function(_24,_25,_26){
with(_24){
return objj_msgSend(_24,"indexOfObject:inRange:",_26,CPMakeRange(0,objj_msgSend(_24,"count")));
}
}),new objj_method(sel_getUid("indexOfObjectIdenticalTo:inRange:"),function(_27,_28,_29,_2a){
with(_27){
var _2b=_2a.location,_2c=_2a.length;
for(;_2b<_2c;++_2b){
if(_29===objj_msgSend(_27,"objectAtIndex:",_2b)){
return _2b;
}
}
return CPNotFound;
}
}),new objj_method(sel_getUid("indexOfObjectIdenticalTo:"),function(_2d,_2e,_2f){
with(_2d){
return objj_msgSend(_2d,"indexOfObjectIdenticalTo:inRange:",_2f,CPMakeRange(0,objj_msgSend(_2d,"count")));
}
}),new objj_method(sel_getUid("objectAtIndex:"),function(_30,_31,_32){
with(_30){
return objj_msgSend(objj_msgSend(_30,"objectsAtIndexes:",objj_msgSend(CPIndexSet,"indexSetWithIndex:",_32)),"firstObject");
}
}),new objj_method(sel_getUid("objectsAtIndexes:"),function(_33,_34,_35){
with(_33){
if(_objectsAtIndexes){
return _objectsAtIndexes(_proxyObject,_objectsAtIndexesSEL,_35);
}
if(_objectAtIndex){
var _36=CPNotFound,_37=[];
while((_36=objj_msgSend(_35,"indexGreaterThanIndex:",_36))!==CPNotFound){
_37.push(_objectAtIndex(_proxyObject,_objectAtIndexSEL,_36));
}
return _37;
}
return objj_msgSend(objj_msgSend(_33,"_representedObject"),"objectsAtIndexes:",_35);
}
}),new objj_method(sel_getUid("addObject:"),function(_38,_39,_3a){
with(_38){
objj_msgSend(_38,"insertObject:atIndex:",_3a,objj_msgSend(_38,"count"));
}
}),new objj_method(sel_getUid("addObjectsFromArray:"),function(_3b,_3c,_3d){
with(_3b){
var _3e=0,_3f=objj_msgSend(_3d,"count");
objj_msgSend(_3b,"insertObjects:atIndexes:",_3d,objj_msgSend(CPIndexSet,"indexSetWithIndexesInRange:",CPMakeRange(objj_msgSend(_3b,"count"),_3f)));
}
}),new objj_method(sel_getUid("insertObject:atIndex:"),function(_40,_41,_42,_43){
with(_40){
objj_msgSend(_40,"insertObjects:atIndexes:",[_42],objj_msgSend(CPIndexSet,"indexSetWithIndex:",_43));
}
}),new objj_method(sel_getUid("insertObjects:atIndexes:"),function(_44,_45,_46,_47){
with(_44){
if(_insertMany){
_insertMany(_proxyObject,_insertManySEL,_46,_47);
}else{
if(_insert){
var _48=[];
objj_msgSend(_47,"getIndexes:maxCount:inIndexRange:",_48,-1,nil);
for(var _49=0;_49<objj_msgSend(_48,"count");_49++){
var _4a=objj_msgSend(_48,"objectAtIndex:",_49),_4b=objj_msgSend(_46,"objectAtIndex:",_49);
_insert(_proxyObject,_insertSEL,_4b,_4a);
}
}else{
var _4c=objj_msgSend(objj_msgSend(_44,"_representedObject"),"copy");
objj_msgSend(_4c,"insertObjects:atIndexes:",_46,_47);
objj_msgSend(_44,"_setRepresentedObject:",_4c);
}
}
}
}),new objj_method(sel_getUid("removeObject:"),function(_4d,_4e,_4f){
with(_4d){
objj_msgSend(_4d,"removeObject:inRange:",_4f,CPMakeRange(0,objj_msgSend(_4d,"count")));
}
}),new objj_method(sel_getUid("removeObjectsInArray:"),function(_50,_51,_52){
with(_50){
if(_removeMany){
var _53=objj_msgSend(CPIndexSet,"indexSet"),_54=objj_msgSend(_52,"count");
while(_54--){
objj_msgSend(_53,"addIndex:",objj_msgSend(_50,"indexOfObject:",objj_msgSend(_52,"objectAtIndex:",_54)));
}
_removeMany(_proxyObject,_removeManySEL,_53);
}else{
if(_remove){
var _54=objj_msgSend(_52,"count");
while(_54--){
_remove(_proxyObject,_removeSEL,objj_msgSend(_50,"indexOfObject:",objj_msgSend(_52,"objectAtIndex:",_54)));
}
}else{
var _55=objj_msgSend(objj_msgSend(_50,"_representedObject"),"copy");
objj_msgSend(_55,"removeObjectsInArray:",_52);
objj_msgSend(_50,"_setRepresentedObject:",_55);
}
}
}
}),new objj_method(sel_getUid("removeObject:inRange:"),function(_56,_57,_58,_59){
with(_56){
if(_remove){
_remove(_proxyObject,_removeSEL,objj_msgSend(_56,"indexOfObject:inRange:",_58,_59));
}else{
if(_removeMany){
var _5a=objj_msgSend(_56,"indexOfObject:inRange:",_58,_59);
_removeMany(_proxyObject,_removeManySEL,objj_msgSend(CPIndexSet,"indexSetWithIndex:",_5a));
}else{
var _5a;
while((_5a=objj_msgSend(_56,"indexOfObject:inRange:",_58,_59))!==CPNotFound){
objj_msgSend(_56,"removeObjectAtIndex:",_5a);
_59=CPIntersectionRange(CPMakeRange(_5a,length-_5a),_59);
}
}
}
}
}),new objj_method(sel_getUid("removeLastObject"),function(_5b,_5c){
with(_5b){
objj_msgSend(_5b,"removeObjectsAtIndexes:",objj_msgSend(CPIndexSet,"indexSetWithIndex:",objj_msgSend(_5b,"count")-1));
}
}),new objj_method(sel_getUid("removeObjectAtIndex:"),function(_5d,_5e,_5f){
with(_5d){
objj_msgSend(_5d,"removeObjectsAtIndexes:",objj_msgSend(CPIndexSet,"indexSetWithIndex:",_5f));
}
}),new objj_method(sel_getUid("removeObjectsAtIndexes:"),function(_60,_61,_62){
with(_60){
if(_removeMany){
_removeMany(_proxyObject,_removeManySEL,_62);
}else{
if(_remove){
var _63=objj_msgSend(_62,"lastIndex");
while(_63!==CPNotFound){
_remove(_proxyObject,_removeSEL,_63);
_63=objj_msgSend(_62,"indexLessThanIndex:",_63);
}
}else{
var _64=objj_msgSend(objj_msgSend(_60,"_representedObject"),"copy");
objj_msgSend(_64,"removeObjectsAtIndexes:",_62);
objj_msgSend(_60,"_setRepresentedObject:",_64);
}
}
}
}),new objj_method(sel_getUid("replaceObjectAtIndex:withObject:"),function(_65,_66,_67,_68){
with(_65){
objj_msgSend(_65,"replaceObjectsAtIndexes:withObjects:",objj_msgSend(CPIndexSet,"indexSetWithIndex:",_67),[_68]);
}
}),new objj_method(sel_getUid("replaceObjectsAtIndexes:withObjects:"),function(_69,_6a,_6b,_6c){
with(_69){
if(_replaceMany){
return _replaceMany(_proxyObject,_replaceManySEL,_6b,_6c);
}else{
if(_replace){
var i=0,_6d=objj_msgSend(_6b,"firstIndex");
while(_6d!==CPNotFound){
_replace(_proxyObject,_replaceSEL,_6d,objj_msgSend(_6c,"objectAtIndex:",i++));
_6d=objj_msgSend(_6b,"indexGreaterThanIndex:",_6d);
}
}else{
var _6e=objj_msgSend(objj_msgSend(_69,"_representedObject"),"copy");
objj_msgSend(_6e,"replaceObjectsAtIndexes:withObjects:",_6b,_6c);
objj_msgSend(_69,"_setRepresentedObject:",_6e);
}
}
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("alloc"),function(_6f,_70){
with(_6f){
var _71=[];
_71.isa=_6f;
var _72=class_copyIvarList(_6f),_73=_72.length;
while(_73--){
_71[ivar_getName(_72[_73])]=nil;
}
return _71;
}
})]);
var _1=objj_getClass("CPArray");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPArray\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("valueForKey:"),function(_74,_75,_76){
with(_74){
if(_76.indexOf("@")===0){
if(_76.indexOf(".")!==-1){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"called valueForKey: on an array with a complex key ("+_76+"). use valueForKeyPath:");
}
if(_76==="@count"){
return length;
}
return objj_msgSend(_74,"valueForUndefinedKey:",_76);
}else{
var _77=[],_78=objj_msgSend(_74,"objectEnumerator"),_79;
while((_79=objj_msgSend(_78,"nextObject"))!==nil){
var _7a=objj_msgSend(_79,"valueForKey:",_76);
if(_7a===nil||_7a===undefined){
_7a=objj_msgSend(CPNull,"null");
}
_77.push(_7a);
}
return _77;
}
}
}),new objj_method(sel_getUid("valueForKeyPath:"),function(_7b,_7c,_7d){
with(_7b){
if(!_7d){
objj_msgSend(_7b,"valueForUndefinedKey:","<empty path>");
}
if(_7d.charAt(0)==="@"){
var _7e=_7d.indexOf("."),_7f,_80;
if(_7e!==-1){
_7f=_7d.substring(1,_7e);
_80=_7d.substring(_7e+1);
}else{
_7f=_7d.substring(1);
}
return objj_msgSend(_CPCollectionKVCOperator,"performOperation:withCollection:propertyPath:",_7f,_7b,_80);
}else{
var _81=[],_82=objj_msgSend(_7b,"objectEnumerator"),_83;
while((_83=objj_msgSend(_82,"nextObject"))!==nil){
var _84=objj_msgSend(_83,"valueForKeyPath:",_7d);
if(_84===nil||_84===undefined){
_84=objj_msgSend(CPNull,"null");
}
_81.push(_84);
}
return _81;
}
}
}),new objj_method(sel_getUid("setValue:forKey:"),function(_85,_86,_87,_88){
with(_85){
var _89=objj_msgSend(_85,"objectEnumerator"),_8a;
while((_8a=objj_msgSend(_89,"nextObject"))!==nil){
objj_msgSend(_8a,"setValue:forKey:",_87,_88);
}
}
}),new objj_method(sel_getUid("setValue:forKeyPath:"),function(_8b,_8c,_8d,_8e){
with(_8b){
var _8f=objj_msgSend(_8b,"objectEnumerator"),_90;
while((_90=objj_msgSend(_8f,"nextObject"))!==nil){
objj_msgSend(_90,"setValue:forKeyPath:",_8d,_8e);
}
}
})]);
var _1=objj_getClass("CPArray");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPArray\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("addObserver:forKeyPath:options:context:"),function(_91,_92,_93,_94,_95,_96){
with(_91){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"[CPArray "+CPStringFromSelector(_92)+"] is not supported. Key path: "+_94);
}
}),new objj_method(sel_getUid("removeObserver:forKeyPath:"),function(_97,_98,_99,_9a){
with(_97){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,"[CPArray "+CPStringFromSelector(_98)+"] is not supported. Key path: "+_9a);
}
}),new objj_method(sel_getUid("addObserver:toObjectsAtIndexes:forKeyPath:options:context:"),function(_9b,_9c,_9d,_9e,_9f,_a0,_a1){
with(_9b){
var _a2=objj_msgSend(_9e,"firstIndex");
while(_a2>=0){
objj_msgSend(_9b[_a2],"addObserver:forKeyPath:options:context:",_9d,_9f,_a0,_a1);
_a2=objj_msgSend(_9e,"indexGreaterThanIndex:",_a2);
}
}
}),new objj_method(sel_getUid("removeObserver:fromObjectsAtIndexes:forKeyPath:"),function(_a3,_a4,_a5,_a6,_a7){
with(_a3){
var _a8=objj_msgSend(_a6,"firstIndex");
while(_a8>=0){
objj_msgSend(_a3[_a8],"removeObserver:forKeyPath:",_a5,_a7);
_a8=objj_msgSend(_a6,"indexGreaterThanIndex:",_a8);
}
}
})]);
p;27;_CPCollectionKVCOperators.jt;2360;@STATIC;1.0;t;2341;
var _1=objj_allocateClassPair(CPObject,"_CPCollectionKVCOperator"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_2,[new objj_method(sel_getUid("performOperation:withCollection:propertyPath:"),function(_3,_4,_5,_6,_7){
with(_3){
var _8=CPSelectorFromString(_5+"ForCollection:propertyPath:");
if(!objj_msgSend(_3,"respondsToSelector:",_8)){
return objj_msgSend(_6,"valueForUndefinedKey:","@"+_5);
}
return objj_msgSend(_3,"performSelector:withObject:withObject:",_8,_6,_7);
}
}),new objj_method(sel_getUid("avgForCollection:propertyPath:"),function(_9,_a,_b,_c){
with(_9){
if(!_c){
return objj_msgSend(_b,"valueForUndefinedKey:","@avg");
}
var _d=objj_msgSend(_b,"valueForKeyPath:",_c),_e=0,_f=objj_msgSend(_d,"objectEnumerator"),_10;
while((_10=objj_msgSend(_f,"nextObject"))!==nil){
_e+=objj_msgSend(_10,"doubleValue");
}
return _e/objj_msgSend(_d,"count");
}
}),new objj_method(sel_getUid("minForCollection:propertyPath:"),function(_11,_12,_13,_14){
with(_11){
if(!_14){
return objj_msgSend(_13,"valueForUndefinedKey:","@min");
}
var _15=objj_msgSend(_13,"valueForKeyPath:",_14);
if(objj_msgSend(_15,"count")===0){
return nil;
}
var _16=objj_msgSend(_15,"objectEnumerator"),min=objj_msgSend(_16,"nextObject"),_17;
while((_17=objj_msgSend(_16,"nextObject"))!==nil){
if(objj_msgSend(min,"compare:",_17)>0){
min=_17;
}
}
return min;
}
}),new objj_method(sel_getUid("maxForCollection:propertyPath:"),function(_18,_19,_1a,_1b){
with(_18){
if(!_1b){
return objj_msgSend(_1a,"valueForUndefinedKey:","@max");
}
var _1c=objj_msgSend(_1a,"valueForKeyPath:",_1b);
if(objj_msgSend(_1c,"count")===0){
return nil;
}
var _1d=objj_msgSend(_1c,"objectEnumerator"),max=objj_msgSend(_1d,"nextObject"),_1e;
while((_1e=objj_msgSend(_1d,"nextObject"))!==nil){
if(objj_msgSend(max,"compare:",_1e)<0){
max=_1e;
}
}
return max;
}
}),new objj_method(sel_getUid("sumForCollection:propertyPath:"),function(_1f,_20,_21,_22){
with(_1f){
if(!_22){
return objj_msgSend(_21,"valueForUndefinedKey:","@sum");
}
var _23=objj_msgSend(_21,"valueForKeyPath:",_22),sum=0,_24=objj_msgSend(_23,"objectEnumerator"),_25;
while((_25=objj_msgSend(_24,"nextObject"))!==nil){
sum+=objj_msgSend(_25,"doubleValue");
}
return sum;
}
}),new objj_method(sel_getUid("countForCollection:propertyPath:"),function(_26,_27,_28,_29){
with(_26){
return objj_msgSend(_28,"count");
}
})]);
p;11;CPSet+KVO.jt;10888;@STATIC;1.0;i;13;CPException.ji;10;CPObject.ji;7;CPSet.ji;27;_CPCollectionKVCOperators.jt;10792;
objj_executeFile("CPException.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPSet.j",YES);
objj_executeFile("_CPCollectionKVCOperators.j",YES);
var _1=objj_getClass("CPObject");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPObject\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("mutableSetValueForKey:"),function(_3,_4,_5){
with(_3){
return objj_msgSend(objj_msgSend(_CPKVCSet,"alloc"),"initWithKey:forProxyObject:",_5,_3);
}
}),new objj_method(sel_getUid("mutableSetValueForKeyPath:"),function(_6,_7,_8){
with(_6){
var _9=_8.indexOf(".");
if(_9<0){
return objj_msgSend(_6,"mutableSetValueForKey:",_8);
}
var _a=_8.substring(0,_9),_b=_8.substring(_9+1);
return objj_msgSend(objj_msgSend(_6,"valueForKeyPath:",_a),"mutableSetValueForKeyPath:",_b);
}
})]);
var _1=objj_allocateClassPair(CPMutableSet,"_CPKVCSet"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_proxyObject"),new objj_ivar("_key"),new objj_ivar("_accessSEL"),new objj_ivar("_access"),new objj_ivar("_setSEL"),new objj_ivar("_set"),new objj_ivar("_countSEL"),new objj_ivar("_count"),new objj_ivar("_enumeratorSEL"),new objj_ivar("_enumerator"),new objj_ivar("_memberSEL"),new objj_ivar("_member"),new objj_ivar("_addSEL"),new objj_ivar("_add"),new objj_ivar("_addManySEL"),new objj_ivar("_addMany"),new objj_ivar("_removeSEL"),new objj_ivar("_remove"),new objj_ivar("_removeManySEL"),new objj_ivar("_removeMany"),new objj_ivar("_intersectSEL"),new objj_ivar("_intersect")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithKey:forProxyObject:"),function(_c,_d,_e,_f){
with(_c){
_c=objj_msgSendSuper({receiver:_c,super_class:objj_getClass("_CPKVCSet").super_class},"init");
_key=_e;
_proxyObject=_f;
var _10=_key.charAt(0).toUpperCase()+_key.substring(1);
_accessSEL=sel_getName(_key);
if(objj_msgSend(_proxyObject,"respondsToSelector:",_accessSEL)){
_access=objj_msgSend(_proxyObject,"methodForSelector:",_accessSEL);
}
_setSEL=sel_getName("set"+_10+":");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_setSEL)){
_set=objj_msgSend(_proxyObject,"methodForSelector:",_setSEL);
}
_countSEL=sel_getName("countOf"+_10);
if(objj_msgSend(_proxyObject,"respondsToSelector:",_countSEL)){
_count=objj_msgSend(_proxyObject,"methodForSelector:",_countSEL);
}
_enumeratorSEL=sel_getName("enumeratorOf"+_10);
if(objj_msgSend(_proxyObject,"respondsToSelector:",_enumeratorSEL)){
_enumerator=objj_msgSend(_proxyObject,"methodForSelector:",_enumeratorSEL);
}
_memberSEL=sel_getName("memberOf"+_10+":");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_memberSEL)){
_member=objj_msgSend(_proxyObject,"methodForSelector:",_memberSEL);
}
_addSEL=sel_getName("add"+_10+"Object:");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_addSEL)){
_add=objj_msgSend(_proxyObject,"methodForSelector:",_addSEL);
}
_addManySEL=sel_getName("add"+_10+":");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_addManySEL)){
_addMany=objj_msgSend(_proxyObject,"methodForSelector:",_addManySEL);
}
_removeSEL=sel_getName("remove"+_10+"Object:");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_removeSEL)){
_remove=objj_msgSend(_proxyObject,"methodForSelector:",_removeSEL);
}
_removeManySEL=sel_getName("remove"+_10+":");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_removeManySEL)){
_removeMany=objj_msgSend(_proxyObject,"methodForSelector:",_removeManySEL);
}
_intersectSEL=sel_getName("intersect"+_10+":");
if(objj_msgSend(_proxyObject,"respondsToSelector:",_intersectSEL)){
_intersect=objj_msgSend(_proxyObject,"methodForSelector:",_intersectSEL);
}
return _c;
}
}),new objj_method(sel_getUid("_representedObject"),function(_11,_12){
with(_11){
if(_access){
return _access(_proxyObject,_accessSEL);
}
return objj_msgSend(_proxyObject,"valueForKey:",_key);
}
}),new objj_method(sel_getUid("_setRepresentedObject:"),function(_13,_14,_15){
with(_13){
if(_set){
return _set(_proxyObject,_setSEL,_15);
}
objj_msgSend(_proxyObject,"setValue:forKey:",_15,_key);
}
}),new objj_method(sel_getUid("count"),function(_16,_17){
with(_16){
if(_count){
return _count(_proxyObject,_countSEL);
}
return objj_msgSend(objj_msgSend(_16,"_representedObject"),"count");
}
}),new objj_method(sel_getUid("objectEnumerator"),function(_18,_19){
with(_18){
if(_enumerator){
return _enumerator(_proxyObject,_enumeratorSEL);
}
return objj_msgSend(objj_msgSend(_18,"_representedObject"),"objectEnumerator");
}
}),new objj_method(sel_getUid("member:"),function(_1a,_1b,_1c){
with(_1a){
if(_member){
return _member(_proxyObject,_memberSEL,_1c);
}
return objj_msgSend(objj_msgSend(_1a,"_representedObject"),"member:",_1c);
}
}),new objj_method(sel_getUid("addObject:"),function(_1d,_1e,_1f){
with(_1d){
if(_add){
_add(_proxyObject,_addSEL,_1f);
}else{
if(_addMany){
var _20=objj_msgSend(CPSet,"setWithObject:",_1f);
_addMany(_proxyObject,_addManySEL,_20);
}else{
var _21=objj_msgSend(objj_msgSend(_1d,"_representedObject"),"copy");
objj_msgSend(_21,"addObject:",_1f);
objj_msgSend(_1d,"_setRepresentedObject:",_21);
}
}
}
}),new objj_method(sel_getUid("addObjectsFromArray:"),function(_22,_23,_24){
with(_22){
if(_addMany){
var _25=objj_msgSend(CPSet,"setWithArray:",_24);
_addMany(_proxyObject,_addManySEL,_25);
}else{
if(_add){
var _26,_27=objj_msgSend(_24,"objectEnumerator");
while((_26=objj_msgSend(_27,"nextObject"))!==nil){
_add(_proxyObject,_addSEL,_26);
}
}else{
var _28=objj_msgSend(objj_msgSend(_22,"_representedObject"),"copy");
objj_msgSend(_28,"addObjectsFromArray:",_24);
objj_msgSend(_22,"_setRepresentedObject:",_28);
}
}
}
}),new objj_method(sel_getUid("unionSet:"),function(_29,_2a,_2b){
with(_29){
if(_addMany){
_addMany(_proxyObject,_addManySEL,_2b);
}else{
if(_add){
var _2c,_2d=objj_msgSend(_2b,"objectEnumerator");
while((_2c=objj_msgSend(_2d,"nextObject"))!==nil){
_add(_proxyObject,_addSEL,_2c);
}
}else{
var _2e=objj_msgSend(objj_msgSend(_29,"_representedObject"),"copy");
objj_msgSend(_2e,"unionSet:",_2b);
objj_msgSend(_29,"_setRepresentedObject:",_2e);
}
}
}
}),new objj_method(sel_getUid("removeObject:"),function(_2f,_30,_31){
with(_2f){
if(_remove){
_remove(_proxyObject,_removeSEL,_31);
}else{
if(_removeMany){
var _32=objj_msgSend(CPSet,"setWithObject:",_31);
_removeMany(_proxyObject,_removeManySEL,_32);
}else{
var _33=objj_msgSend(objj_msgSend(_2f,"_representedObject"),"copy");
objj_msgSend(_33,"removeObject:",_31);
objj_msgSend(_2f,"_setRepresentedObject:",_33);
}
}
}
}),new objj_method(sel_getUid("minusSet:"),function(_34,_35,_36){
with(_34){
if(_removeMany){
_removeMany(_proxyObject,_removeManySEL,_36);
}else{
if(_remove){
var _37,_38=objj_msgSend(_36,"objectEnumerator");
while((_37=objj_msgSend(_38,"nextObject"))!==nil){
_remove(_proxyObject,_removeSEL,_37);
}
}else{
var _39=objj_msgSend(objj_msgSend(_34,"_representedObject"),"copy");
objj_msgSend(_39,"minusSet:",_36);
objj_msgSend(_34,"_setRepresentedObject:",_39);
}
}
}
}),new objj_method(sel_getUid("removeObjectsInArray:"),function(_3a,_3b,_3c){
with(_3a){
if(_removeMany){
var _3d=objj_msgSend(CPSet,"setWithArray:",_3c);
_removeMany(_proxyObject,_removeManySEL,_3d);
}else{
if(_remove){
var _3e,_3f=objj_msgSend(_3c,"objectEnumerator");
while((_3e=objj_msgSend(_3f,"nextObject"))!==nil){
_remove(_proxyObject,_removeSEL,_3e);
}
}else{
var _40=objj_msgSend(objj_msgSend(_3a,"_representedObject"),"copy");
objj_msgSend(_40,"removeObjectsInArray:",_3c);
objj_msgSend(_3a,"_setRepresentedObject:",_40);
}
}
}
}),new objj_method(sel_getUid("removeAllObjects"),function(_41,_42){
with(_41){
if(_removeMany){
var _43=objj_msgSend(objj_msgSend(_41,"_representedObject"),"copy");
_removeMany(_proxyObject,_removeManySEL,_43);
}else{
if(_remove){
var _44,_45=objj_msgSend(objj_msgSend(objj_msgSend(_41,"_representedObject"),"copy"),"objectEnumerator");
while((_44=objj_msgSend(_45,"nextObject"))!==nil){
_remove(_proxyObject,_removeSEL,_44);
}
}else{
var _46=objj_msgSend(objj_msgSend(_41,"_representedObject"),"copy");
objj_msgSend(_46,"removeAllObjects");
objj_msgSend(_41,"_setRepresentedObject:",_46);
}
}
}
}),new objj_method(sel_getUid("intersectSet:"),function(_47,_48,_49){
with(_47){
if(_intersect){
_intersect(_proxyObject,_intersectSEL,_49);
}else{
var _4a=objj_msgSend(objj_msgSend(_47,"_representedObject"),"copy");
objj_msgSend(_4a,"intersectSet:",_49);
objj_msgSend(_47,"_setRepresentedObject:",_4a);
}
}
}),new objj_method(sel_getUid("setSet:"),function(_4b,_4c,set){
with(_4b){
objj_msgSend(_4b,"_setRepresentedObject:",set);
}
}),new objj_method(sel_getUid("allObjects"),function(_4d,_4e){
with(_4d){
return objj_msgSend(objj_msgSend(_4d,"_representedObject"),"allObjects");
}
}),new objj_method(sel_getUid("anyObject"),function(_4f,_50){
with(_4f){
return objj_msgSend(objj_msgSend(_4f,"_representedObject"),"anyObject");
}
}),new objj_method(sel_getUid("containsObject:"),function(_51,_52,_53){
with(_51){
return objj_msgSend(objj_msgSend(_51,"_representedObject"),"containsObject:",_53);
}
}),new objj_method(sel_getUid("intersectsSet:"),function(_54,_55,_56){
with(_54){
return objj_msgSend(objj_msgSend(_54,"_representedObject"),"intersectsSet:",_56);
}
}),new objj_method(sel_getUid("isEqualToSet:"),function(_57,_58,_59){
with(_57){
return objj_msgSend(objj_msgSend(_57,"_representedObject"),"isEqualToSet:",_59);
}
}),new objj_method(sel_getUid("copy"),function(_5a,_5b){
with(_5a){
return objj_msgSend(objj_msgSend(_5a,"_representedObject"),"copy");
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("alloc"),function(_5c,_5d){
with(_5c){
var set=objj_msgSend(CPMutableSet,"set");
set.isa=_5c;
var _5e=class_copyIvarList(_5c),_5f=_5e.length;
while(_5f--){
set[ivar_getName(_5e[_5f])]=nil;
}
return set;
}
})]);
var _1=objj_getClass("CPSet");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPSet\"");
}
var _2=_1.isa;
class_addMethods(_1,[new objj_method(sel_getUid("valueForKeyPath:"),function(_60,_61,_62){
with(_60){
if(!_62){
objj_msgSend(_60,"valueForUndefinedKey:","<empty path>");
}
if(_62.charAt(0)==="@"){
var _63=_62.indexOf("."),_64,_65;
if(_63!==-1){
_64=_62.substring(1,_63);
_65=_62.substring(_63+1);
}else{
_64=_62.substring(1);
}
return objj_msgSend(_CPCollectionKVCOperator,"performOperation:withCollection:propertyPath:",_64,_60,_65);
}else{
var _66=objj_msgSend(CPSet,"set"),_67,_68,_69=objj_msgSend(_60,"objectEnumerator");
while((_67=objj_msgSend(_69,"nextObject"))!==nil){
_68=objj_msgSend(_67,"valueForKeyPath:",_62);
if(_68===nil||_68===undefined){
_68=objj_msgSend(CPNull,"null");
}
objj_msgSend(_66,"addObject:",_68);
}
return _66;
}
}
}),new objj_method(sel_getUid("setValue:forKey:"),function(_6a,_6b,_6c,_6d){
with(_6a){
var _6e,_6f=objj_msgSend(_6a,"objectEnumerator");
while((_6e=objj_msgSend(_6f,"nextObject"))!==nil){
objj_msgSend(_6e,"setValue:forKey:",_6c,_6d);
}
}
})]);
p;29;CPPropertyListSerialization.jt;1554;@STATIC;1.0;i;13;CPException.ji;10;CPObject.jt;1502;
objj_executeFile("CPException.j",YES);
objj_executeFile("CPObject.j",YES);
CPPropertyListUnknownFormat=0;
CPPropertyListOpenStepFormat=kCFPropertyListOpenStepFormat;
CPPropertyListXMLFormat_v1_0=kCFPropertyListXMLFormat_v1_0;
CPPropertyListBinaryFormat_v1_0=kCFPropertyListBinaryFormat_v1_0;
CPPropertyList280NorthFormat_v1_0=kCFPropertyList280NorthFormat_v1_0;
var _1=objj_allocateClassPair(CPObject,"CPPropertyListSerialization"),_2=_1.isa;
objj_registerClassPair(_1);
class_addMethods(_2,[new objj_method(sel_getUid("dataFromPropertyList:format:"),function(_3,_4,_5,_6){
with(_3){
return CPPropertyListCreateData(_5,_6);
}
}),new objj_method(sel_getUid("propertyListFromData:format:"),function(_7,_8,_9,_a){
with(_7){
return CPPropertyListCreateFromData(_9,_a);
}
})]);
var _1=objj_getClass("CPPropertyListSerialization");
if(!_1){
throw new SyntaxError("*** Could not find definition for class \"CPPropertyListSerialization\"");
}
var _2=_1.isa;
class_addMethods(_2,[new objj_method(sel_getUid("dataFromPropertyList:format:errorDescription:"),function(_b,_c,_d,_e,_f){
with(_b){
_CPReportLenientDeprecation(_b,_c,sel_getUid("dataFromPropertyList:format:"));
return objj_msgSend(_b,"dataFromPropertyList:format:",_d,_e);
}
}),new objj_method(sel_getUid("propertyListFromData:format:errorDescription:"),function(_10,_11,_12,_13,_14){
with(_10){
_CPReportLenientDeprecation(_10,_11,sel_getUid("propertyListFromData:format:"));
return objj_msgSend(_10,"propertyListFromData:format:",_12,_13);
}
})]);
p;20;CPValueTransformer.jt;5085;@STATIC;1.0;i;8;CPData.ji;14;CPDictionary.ji;13;CPException.ji;17;CPKeyedArchiver.ji;19;CPKeyedUnarchiver.ji;10;CPNumber.ji;10;CPObject.jt;4941;
objj_executeFile("CPData.j",YES);
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPException.j",YES);
objj_executeFile("CPKeyedArchiver.j",YES);
objj_executeFile("CPKeyedUnarchiver.j",YES);
objj_executeFile("CPNumber.j",YES);
objj_executeFile("CPObject.j",YES);
var _1=objj_msgSend(CPDictionary,"dictionary");
var _2=objj_allocateClassPair(CPObject,"CPValueTransformer"),_3=_2.isa;
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("reverseTransformedValue:"),function(_4,_5,_6){
with(_4){
if(!objj_msgSend(objj_msgSend(_4,"class"),"allowsReverseTransformation")){
objj_msgSend(CPException,"raise:reason:",CPInvalidArgumentException,(_4+" is not reversible."));
}
return objj_msgSend(_4,"transformedValue:",_6);
}
}),new objj_method(sel_getUid("transformedValue:"),function(_7,_8,_9){
with(_7){
return nil;
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("initialize"),function(_a,_b){
with(_a){
if(_a!==objj_msgSend(CPValueTransformer,"class")){
return;
}
objj_msgSend(CPValueTransformer,"setValueTransformer:forName:",objj_msgSend(objj_msgSend(CPNegateBooleanTransformer,"alloc"),"init"),CPNegateBooleanTransformerName);
objj_msgSend(CPValueTransformer,"setValueTransformer:forName:",objj_msgSend(objj_msgSend(CPIsNilTransformer,"alloc"),"init"),CPIsNilTransformerName);
objj_msgSend(CPValueTransformer,"setValueTransformer:forName:",objj_msgSend(objj_msgSend(CPIsNotNilTransformer,"alloc"),"init"),CPIsNotNilTransformerName);
objj_msgSend(CPValueTransformer,"setValueTransformer:forName:",objj_msgSend(objj_msgSend(CPUnarchiveFromDataTransformer,"alloc"),"init"),CPUnarchiveFromDataTransformerName);
}
}),new objj_method(sel_getUid("setValueTransformer:forName:"),function(_c,_d,_e,_f){
with(_c){
objj_msgSend(_1,"setObject:forKey:",_e,_f);
}
}),new objj_method(sel_getUid("valueTransformerForName:"),function(_10,_11,_12){
with(_10){
return objj_msgSend(_1,"objectForKey:",_12);
}
}),new objj_method(sel_getUid("valueTransformerNames"),function(_13,_14){
with(_13){
return objj_msgSend(_1,"allKeys");
}
}),new objj_method(sel_getUid("allowsReverseTransformation"),function(_15,_16){
with(_15){
return NO;
}
}),new objj_method(sel_getUid("transformedValueClass"),function(_17,_18){
with(_17){
return objj_msgSend(CPObject,"class");
}
})]);
var _2=objj_allocateClassPair(CPValueTransformer,"CPNegateBooleanTransformer"),_3=_2.isa;
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("reverseTransformedValue:"),function(_19,_1a,_1b){
with(_19){
return !objj_msgSend(_1b,"boolValue");
}
}),new objj_method(sel_getUid("transformedValue:"),function(_1c,_1d,_1e){
with(_1c){
return !objj_msgSend(_1e,"boolValue");
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("allowsReverseTransformation"),function(_1f,_20){
with(_1f){
return YES;
}
}),new objj_method(sel_getUid("transformedValueClass"),function(_21,_22){
with(_21){
return objj_msgSend(CPNumber,"class");
}
})]);
var _2=objj_allocateClassPair(CPValueTransformer,"CPIsNilTransformer"),_3=_2.isa;
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("transformedValue:"),function(_23,_24,_25){
with(_23){
return _25===nil||_25===undefined;
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("allowsReverseTransformation"),function(_26,_27){
with(_26){
return NO;
}
}),new objj_method(sel_getUid("transformedValueClass"),function(_28,_29){
with(_28){
return objj_msgSend(CPNumber,"class");
}
})]);
var _2=objj_allocateClassPair(CPValueTransformer,"CPIsNotNilTransformer"),_3=_2.isa;
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("transformedValue:"),function(_2a,_2b,_2c){
with(_2a){
return _2c!==nil&&_2c!==undefined;
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("allowsReverseTransformation"),function(_2d,_2e){
with(_2d){
return NO;
}
}),new objj_method(sel_getUid("transformedValueClass"),function(_2f,_30){
with(_2f){
return objj_msgSend(CPNumber,"class");
}
})]);
var _2=objj_allocateClassPair(CPValueTransformer,"CPUnarchiveFromDataTransformer"),_3=_2.isa;
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("reverseTransformedValue:"),function(_31,_32,_33){
with(_31){
return objj_msgSend(CPKeyedArchiver,"archivedDataWithRootObject:",_33);
}
}),new objj_method(sel_getUid("transformedValue:"),function(_34,_35,_36){
with(_34){
return objj_msgSend(CPKeyedUnarchiver,"unarchiveObjectWithData:",_36);
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("allowsReverseTransformation"),function(_37,_38){
with(_37){
return YES;
}
}),new objj_method(sel_getUid("transformedValueClass"),function(_39,_3a){
with(_39){
return objj_msgSend(CPData,"class");
}
})]);
CPNegateBooleanTransformerName="CPNegateBoolean";
CPIsNilTransformerName="CPIsNil";
CPIsNotNilTransformerName="CPIsNotNil";
CPUnarchiveFromDataTransformerName="CPUnarchiveFromData";
CPKeyedUnarchiveFromDataTransformerName="CPKeyedUnarchiveFromData";
p;17;CPURLConnection.jt;5476;@STATIC;1.0;i;8;CPData.ji;10;CPObject.ji;11;CPRunLoop.ji;14;CPURLRequest.ji;15;CPURLResponse.jt;5375;
objj_executeFile("CPData.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPRunLoop.j",YES);
objj_executeFile("CPURLRequest.j",YES);
objj_executeFile("CPURLResponse.j",YES);
var _1=nil;
var _2=objj_allocateClassPair(CPObject,"CPURLConnection"),_3=_2.isa;
class_addIvars(_2,[new objj_ivar("_request"),new objj_ivar("_delegate"),new objj_ivar("_isCanceled"),new objj_ivar("_isLocalFileConnection"),new objj_ivar("_HTTPRequest")]);
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("initWithRequest:delegate:startImmediately:"),function(_4,_5,_6,_7,_8){
with(_4){
_4=objj_msgSendSuper({receiver:_4,super_class:objj_getClass("CPURLConnection").super_class},"init");
if(_4){
_request=_6;
_delegate=_7;
_isCanceled=NO;
var _9=objj_msgSend(_request,"URL"),_a=objj_msgSend(_9,"scheme");
_isLocalFileConnection=_a==="file"||((_a==="http"||_a==="https:")&&window.location&&(window.location.protocol==="file:"||window.location.protocol==="app:"));
_HTTPRequest=new CFHTTPRequest();
if(_8){
objj_msgSend(_4,"start");
}
}
return _4;
}
}),new objj_method(sel_getUid("initWithRequest:delegate:"),function(_b,_c,_d,_e){
with(_b){
return objj_msgSend(_b,"initWithRequest:delegate:startImmediately:",_d,_e,YES);
}
}),new objj_method(sel_getUid("delegate"),function(_f,_10){
with(_f){
return _delegate;
}
}),new objj_method(sel_getUid("start"),function(_11,_12){
with(_11){
_isCanceled=NO;
try{
_HTTPRequest.open(objj_msgSend(_request,"HTTPMethod"),objj_msgSend(objj_msgSend(_request,"URL"),"absoluteString"),YES);
_HTTPRequest.onreadystatechange=function(){
objj_msgSend(_11,"_readyStateDidChange");
};
var _13=objj_msgSend(_request,"allHTTPHeaderFields"),key=nil,_14=objj_msgSend(_13,"keyEnumerator");
while((key=objj_msgSend(_14,"nextObject"))!==nil){
_HTTPRequest.setRequestHeader(key,objj_msgSend(_13,"objectForKey:",key));
}
_HTTPRequest.send(objj_msgSend(_request,"HTTPBody"));
}
catch(anException){
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("connection:didFailWithError:"))){
objj_msgSend(_delegate,"connection:didFailWithError:",_11,anException);
}
}
}
}),new objj_method(sel_getUid("cancel"),function(_15,_16){
with(_15){
_isCanceled=YES;
try{
_HTTPRequest.abort();
}
catch(anException){
}
}
}),new objj_method(sel_getUid("isLocalFileConnection"),function(_17,_18){
with(_17){
return _isLocalFileConnection;
}
}),new objj_method(sel_getUid("_readyStateDidChange"),function(_19,_1a){
with(_19){
if(_HTTPRequest.readyState()===CFHTTPRequest.CompleteState){
var _1b=_HTTPRequest.status(),URL=objj_msgSend(_request,"URL");
if(_1b===401&&objj_msgSend(_1,"respondsToSelector:",sel_getUid("connectionDidReceiveAuthenticationChallenge:"))){
objj_msgSend(_1,"connectionDidReceiveAuthenticationChallenge:",_19);
}else{
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("connection:didReceiveResponse:"))){
if(_isLocalFileConnection){
objj_msgSend(_delegate,"connection:didReceiveResponse:",_19,objj_msgSend(objj_msgSend(CPURLResponse,"alloc"),"initWithURL:",URL));
}else{
var _1c=objj_msgSend(objj_msgSend(CPHTTPURLResponse,"alloc"),"initWithURL:",URL);
objj_msgSend(_1c,"_setStatusCode:",_1b);
objj_msgSend(_delegate,"connection:didReceiveResponse:",_19,_1c);
}
}
if(!_isCanceled){
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("connection:didReceiveData:"))){
objj_msgSend(_delegate,"connection:didReceiveData:",_19,_HTTPRequest.responseText());
}
if(objj_msgSend(_delegate,"respondsToSelector:",sel_getUid("connectionDidFinishLoading:"))){
objj_msgSend(_delegate,"connectionDidFinishLoading:",_19);
}
}
}
}
objj_msgSend(objj_msgSend(CPRunLoop,"currentRunLoop"),"limitDateForMode:",CPDefaultRunLoopMode);
}
}),new objj_method(sel_getUid("_HTTPRequest"),function(_1d,_1e){
with(_1d){
return _HTTPRequest;
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("setClassDelegate:"),function(_1f,_20,_21){
with(_1f){
_1=_21;
}
}),new objj_method(sel_getUid("sendSynchronousRequest:returningResponse:"),function(_22,_23,_24,_25){
with(_22){
try{
var _26=new CFHTTPRequest();
_26.open(objj_msgSend(_24,"HTTPMethod"),objj_msgSend(objj_msgSend(_24,"URL"),"absoluteString"),NO);
var _27=objj_msgSend(_24,"allHTTPHeaderFields"),key=nil,_28=objj_msgSend(_27,"keyEnumerator");
while((key=objj_msgSend(_28,"nextObject"))!==nil){
_26.setRequestHeader(key,objj_msgSend(_27,"objectForKey:",key));
}
_26.send(objj_msgSend(_24,"HTTPBody"));
return objj_msgSend(CPData,"dataWithRawString:",_26.responseText());
}
catch(anException){
}
return nil;
}
}),new objj_method(sel_getUid("connectionWithRequest:delegate:"),function(_29,_2a,_2b,_2c){
with(_29){
return objj_msgSend(objj_msgSend(_29,"alloc"),"initWithRequest:delegate:",_2b,_2c);
}
})]);
var _2=objj_getClass("CPURLConnection");
if(!_2){
throw new SyntaxError("*** Could not find definition for class \"CPURLConnection\"");
}
var _3=_2.isa;
class_addMethods(_2,[new objj_method(sel_getUid("_XMLHTTPRequest"),function(_2d,_2e){
with(_2d){
_CPReportLenientDeprecation(_2d,_2e,sel_getUid("_HTTPRequest"));
return objj_msgSend(_2d,"_HTTPRequest");
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("sendSynchronousRequest:returningResponse:error:"),function(_2f,_30,_31,_32,_33){
with(_2f){
_CPReportLenientDeprecation(_2f,_30,sel_getUid("sendSynchronousRequest:returningResponse:"));
return objj_msgSend(_2f,"sendSynchronousRequest:returningResponse:",_31,_32);
}
})]);
p;14;CPURLRequest.jt;2337;@STATIC;1.0;i;14;CPDictionary.ji;10;CPObject.ji;10;CPString.ji;7;CPURL.jt;2258;
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPString.j",YES);
objj_executeFile("CPURL.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPURLRequest"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_URL"),new objj_ivar("_HTTPBody"),new objj_ivar("_HTTPMethod"),new objj_ivar("_HTTPHeaderFields")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("init"),function(_3,_4){
with(_3){
return objj_msgSend(_3,"initWithURL:",nil);
}
}),new objj_method(sel_getUid("initWithURL:"),function(_5,_6,_7){
with(_5){
_5=objj_msgSendSuper({receiver:_5,super_class:objj_getClass("CPURLRequest").super_class},"init");
if(_5){
objj_msgSend(_5,"setURL:",_7);
_HTTPBody="";
_HTTPMethod="GET";
_HTTPHeaderFields=objj_msgSend(CPDictionary,"dictionary");
objj_msgSend(_5,"setValue:forHTTPHeaderField:","Thu, 01 Jan 1970 00:00:00 GMT","If-Modified-Since");
objj_msgSend(_5,"setValue:forHTTPHeaderField:","no-cache","Cache-Control");
objj_msgSend(_5,"setValue:forHTTPHeaderField:","XMLHttpRequest","X-Requested-With");
}
return _5;
}
}),new objj_method(sel_getUid("URL"),function(_8,_9){
with(_8){
return _URL;
}
}),new objj_method(sel_getUid("setURL:"),function(_a,_b,_c){
with(_a){
_URL=new CFURL(_c);
}
}),new objj_method(sel_getUid("setHTTPBody:"),function(_d,_e,_f){
with(_d){
_HTTPBody=_f;
}
}),new objj_method(sel_getUid("HTTPBody"),function(_10,_11){
with(_10){
return _HTTPBody;
}
}),new objj_method(sel_getUid("setHTTPMethod:"),function(_12,_13,_14){
with(_12){
_HTTPMethod=_14;
}
}),new objj_method(sel_getUid("HTTPMethod"),function(_15,_16){
with(_15){
return _HTTPMethod;
}
}),new objj_method(sel_getUid("allHTTPHeaderFields"),function(_17,_18){
with(_17){
return _HTTPHeaderFields;
}
}),new objj_method(sel_getUid("valueForHTTPHeaderField:"),function(_19,_1a,_1b){
with(_19){
return objj_msgSend(_HTTPHeaderFields,"objectForKey:",_1b);
}
}),new objj_method(sel_getUid("setValue:forHTTPHeaderField:"),function(_1c,_1d,_1e,_1f){
with(_1c){
objj_msgSend(_HTTPHeaderFields,"setObject:forKey:",_1e,_1f);
}
})]);
class_addMethods(_2,[new objj_method(sel_getUid("requestWithURL:"),function(_20,_21,_22){
with(_20){
return objj_msgSend(objj_msgSend(CPURLRequest,"alloc"),"initWithURL:",_22);
}
})]);
p;15;CPURLResponse.jt;932;@STATIC;1.0;i;10;CPObject.ji;7;CPURL.jt;888;
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPURL.j",YES);
var _1=objj_allocateClassPair(CPObject,"CPURLResponse"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_URL")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("initWithURL:"),function(_3,_4,_5){
with(_3){
_3=objj_msgSendSuper({receiver:_3,super_class:objj_getClass("CPURLResponse").super_class},"init");
if(_3){
_URL=_5;
}
return _3;
}
}),new objj_method(sel_getUid("URL"),function(_6,_7){
with(_6){
return _URL;
}
})]);
var _1=objj_allocateClassPair(CPURLResponse,"CPHTTPURLResponse"),_2=_1.isa;
class_addIvars(_1,[new objj_ivar("_statusCode")]);
objj_registerClassPair(_1);
class_addMethods(_1,[new objj_method(sel_getUid("_setStatusCode:"),function(_8,_9,_a){
with(_8){
_statusCode=_a;
}
}),new objj_method(sel_getUid("statusCode"),function(_b,_c){
with(_b){
return _statusCode;
}
})]);
p;16;CPUserDefaults.jt;14141;@STATIC;1.0;i;10;CPBundle.ji;8;CPData.ji;14;CPDictionary.ji;13;CPException.ji;17;CPKeyedArchiver.ji;19;CPKeyedUnarchiver.ji;22;CPNotificationCenter.ji;10;CPObject.ji;11;CPRunLoop.ji;10;CPString.ji;7;CPURL.ji;17;CPURLConnection.ji;14;CPURLRequest.jt;13886;
objj_executeFile("CPBundle.j",YES);
objj_executeFile("CPData.j",YES);
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPException.j",YES);
objj_executeFile("CPKeyedArchiver.j",YES);
objj_executeFile("CPKeyedUnarchiver.j",YES);
objj_executeFile("CPNotificationCenter.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPRunLoop.j",YES);
objj_executeFile("CPString.j",YES);
objj_executeFile("CPURL.j",YES);
objj_executeFile("CPURLConnection.j",YES);
objj_executeFile("CPURLRequest.j",YES);
CPArgumentDomain="CPArgumentDomain";
CPApplicationDomain=objj_msgSend(objj_msgSend(objj_msgSend(CPBundle,"mainBundle"),"infoDictionary"),"objectForKey:","CPBundleIdentifier")||"CPApplicationDomain";
CPGlobalDomain="CPGlobalDomain";
CPLocaleDomain="CPLocaleDomain";
CPRegistrationDomain="CPRegistrationDomain";
CPUserDefaultsDidChangeNotification="CPUserDefaultsDidChangeNotification";
var _1;
var _2=objj_allocateClassPair(CPObject,"CPUserDefaults"),_3=_2.isa;
class_addIvars(_2,[new objj_ivar("_domains"),new objj_ivar("_stores"),new objj_ivar("_searchList"),new objj_ivar("_searchListNeedsReload")]);
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("init"),function(_4,_5){
with(_4){
_4=objj_msgSendSuper({receiver:_4,super_class:objj_getClass("CPUserDefaults").super_class},"init");
if(_4){
_domains=objj_msgSend(CPDictionary,"dictionary");
objj_msgSend(_4,"_setupArgumentsDomain");
var _6=objj_msgSend(CPUserDefaultsLocalStore,"supportsLocalStorage")?CPUserDefaultsLocalStore:CPUserDefaultsCookieStore;
_stores=objj_msgSend(CPDictionary,"dictionary");
objj_msgSend(_4,"setPersistentStoreClass:forDomain:reloadData:",_6,CPGlobalDomain,YES);
objj_msgSend(_4,"setPersistentStoreClass:forDomain:reloadData:",_6,CPApplicationDomain,YES);
}
return _4;
}
}),new objj_method(sel_getUid("_setupArgumentsDomain"),function(_7,_8){
with(_7){
var _9=objj_msgSend(CPApp,"namedArguments"),_a=objj_msgSend(_9,"allKeys"),_b=objj_msgSend(_a,"count"),i=0;
for(;i<_b;i++){
var _c=_a[i];
objj_msgSend(_7,"setObject:forKey:inDomain:",objj_msgSend(_9,"objectForKey:",_c),_c,CPArgumentDomain);
}
}
}),new objj_method(sel_getUid("objectForKey:"),function(_d,_e,_f){
with(_d){
if(_searchListNeedsReload){
objj_msgSend(_d,"_reloadSearchList");
}
return objj_msgSend(_searchList,"objectForKey:",_f);
}
}),new objj_method(sel_getUid("setObject:forKey:"),function(_10,_11,_12,_13){
with(_10){
objj_msgSend(_10,"setObject:forKey:inDomain:",_12,_13,CPApplicationDomain);
}
}),new objj_method(sel_getUid("objectForKey:inDomain:"),function(_14,_15,_16,_17){
with(_14){
var _18=objj_msgSend(_domains,"objectForKey:",_17);
if(!_18){
return nil;
}
return objj_msgSend(_18,"objectForKey:",_16);
}
}),new objj_method(sel_getUid("setObject:forKey:inDomain:"),function(_19,_1a,_1b,_1c,_1d){
with(_19){
if(!_1c||!_1d){
return;
}
var _1e=objj_msgSend(_domains,"objectForKey:",_1d);
if(!_1e){
_1e=objj_msgSend(CPDictionary,"dictionary");
objj_msgSend(_domains,"setObject:forKey:",_1e,_1d);
}
objj_msgSend(_1e,"setObject:forKey:",_1b,_1c);
_searchListNeedsReload=YES;
objj_msgSend(_19,"domainDidChange:",_1d);
}
}),new objj_method(sel_getUid("removeObjectForKey:"),function(_1f,_20,_21){
with(_1f){
objj_msgSend(_1f,"removeObjectForKey:inDomain:",_21,CPApplicationDomain);
}
}),new objj_method(sel_getUid("removeObjectForKey:inDomain:"),function(_22,_23,_24,_25){
with(_22){
if(!_24||!_25){
return;
}
var _26=objj_msgSend(_domains,"objectForKey:",_25);
if(!_26){
return;
}
objj_msgSend(_26,"removeObjectForKey:",_24);
_searchListNeedsReload=YES;
objj_msgSend(_22,"domainDidChange:",_25);
}
}),new objj_method(sel_getUid("registerDefaults:"),function(_27,_28,_29){
with(_27){
var _2a=objj_msgSend(_29,"allKeys"),_2b=objj_msgSend(_2a,"count"),i=0;
for(;i<_2b;i++){
var key=_2a[i];
objj_msgSend(_27,"setObject:forKey:inDomain:",objj_msgSend(_29,"objectForKey:",key),key,CPRegistrationDomain);
}
}
}),new objj_method(sel_getUid("registerDefaultsFromContentsOfFile:"),function(_2c,_2d,_2e){
with(_2c){
var _2f=objj_msgSend(CPURLConnection,"sendSynchronousRequest:returningResponse:",objj_msgSend(CPURLRequest,"requestWithURL:",_2e),nil),_30=objj_msgSend(CPData,"dataWithRawString:",objj_msgSend(_2f,"rawString")),_31=objj_msgSend(_30,"plistObject");
objj_msgSend(_2c,"registerDefaults:",_31);
}
}),new objj_method(sel_getUid("_reloadSearchList"),function(_32,_33){
with(_32){
_searchListNeedsReload=NO;
var _34=[CPRegistrationDomain,CPGlobalDomain,CPApplicationDomain,CPArgumentDomain],_35=objj_msgSend(_34,"count"),i=0;
_searchList=objj_msgSend(CPDictionary,"dictionary");
for(;i<_35;i++){
var _36=objj_msgSend(_domains,"objectForKey:",_34[i]);
if(!_36){
continue;
}
var _37=objj_msgSend(_36,"allKeys"),_38=objj_msgSend(_37,"count"),j=0;
for(;j<_38;j++){
var key=_37[j];
objj_msgSend(_searchList,"setObject:forKey:",objj_msgSend(_36,"objectForKey:",key),key);
}
}
}
}),new objj_method(sel_getUid("volatileDomainNames"),function(_39,_3a){
with(_39){
return [CPArgumentDomain,CPLocaleDomain,CPRegistrationDomain];
}
}),new objj_method(sel_getUid("persistentDomainNames"),function(_3b,_3c){
with(_3b){
return [CPGlobalDomain,CPApplicationDomain];
}
}),new objj_method(sel_getUid("persistentStoreForDomain:"),function(_3d,_3e,_3f){
with(_3d){
return objj_msgSend(_stores,"objectForKey:",_3f);
}
}),new objj_method(sel_getUid("setPersistentStoreClass:forDomain:reloadData:"),function(_40,_41,_42,_43,_44){
with(_40){
var _45=objj_msgSend(_stores,"objectForKey:",_43);
if(_45&&objj_msgSend(_45,"class")===_42){
return _45;
}
var _46=objj_msgSend(objj_msgSend(_42,"alloc"),"init");
objj_msgSend(_46,"setDomain:",_43);
objj_msgSend(_stores,"setObject:forKey:",_46,_43);
if(_44){
objj_msgSend(_40,"reloadDataFromStoreForDomain:",_43);
}
return _46;
}
}),new objj_method(sel_getUid("reloadDataFromStoreForDomain:"),function(_47,_48,_49){
with(_47){
var _4a=objj_msgSend(objj_msgSend(_47,"persistentStoreForDomain:",_49),"data"),_4b=_4a?objj_msgSend(CPKeyedUnarchiver,"unarchiveObjectWithData:",_4a):nil;
objj_msgSend(_domains,"setObject:forKey:",_4b,_49);
_searchListNeedsReload=YES;
}
}),new objj_method(sel_getUid("domainDidChange:"),function(_4c,_4d,_4e){
with(_4c){
if(_4e===CPGlobalDomain||_4e===CPApplicationDomain){
objj_msgSend(objj_msgSend(CPRunLoop,"currentRunLoop"),"performSelector:target:argument:order:modes:",sel_getUid("synchronize"),_4c,nil,0,[CPDefaultRunLoopMode]);
}
objj_msgSend(objj_msgSend(CPNotificationCenter,"defaultCenter"),"postNotificationName:object:",CPUserDefaultsDidChangeNotification,_4c);
}
}),new objj_method(sel_getUid("synchronize"),function(_4f,_50){
with(_4f){
var _51=objj_msgSend(_domains,"objectForKey:",CPGlobalDomain);
if(_51){
var _52=objj_msgSend(CPKeyedArchiver,"archivedDataWithRootObject:",_51);
objj_msgSend(objj_msgSend(_4f,"persistentStoreForDomain:",CPGlobalDomain),"setData:",_52);
}
var _53=objj_msgSend(_domains,"objectForKey:",CPApplicationDomain);
if(_53){
var _52=objj_msgSend(CPKeyedArchiver,"archivedDataWithRootObject:",_53);
objj_msgSend(objj_msgSend(_4f,"persistentStoreForDomain:",CPApplicationDomain),"setData:",_52);
}
}
}),new objj_method(sel_getUid("arrayForKey:"),function(_54,_55,_56){
with(_54){
var _57=objj_msgSend(_54,"objectForKey:",_56);
if(objj_msgSend(_57,"isKindOfClass:",CPArray)){
return _57;
}
return nil;
}
}),new objj_method(sel_getUid("boolForKey:"),function(_58,_59,_5a){
with(_58){
var _5b=objj_msgSend(_58,"objectForKey:",_5a);
if(objj_msgSend(_5b,"respondsToSelector:",sel_getUid("boolValue"))){
return objj_msgSend(_5b,"boolValue");
}
return NO;
}
}),new objj_method(sel_getUid("dataForKey:"),function(_5c,_5d,_5e){
with(_5c){
var _5f=objj_msgSend(_5c,"objectForKey:",_5e);
if(objj_msgSend(_5f,"isKindOfClass:",CPData)){
return _5f;
}
return nil;
}
}),new objj_method(sel_getUid("dictionaryForKey:"),function(_60,_61,_62){
with(_60){
var _63=objj_msgSend(_60,"objectForKey:",_62);
if(objj_msgSend(_63,"isKindOfClass:",CPDictionary)){
return _63;
}
return nil;
}
}),new objj_method(sel_getUid("floatForKey:"),function(_64,_65,_66){
with(_64){
var _67=objj_msgSend(_64,"objectForKey:",_66);
if(_67===nil){
return 0;
}
if(objj_msgSend(_67,"respondsToSelector:",sel_getUid("floatValue"))){
_67=objj_msgSend(_67,"floatValue");
}
return parseFloat(_67);
}
}),new objj_method(sel_getUid("integerForKey:"),function(_68,_69,_6a){
with(_68){
var _6b=objj_msgSend(_68,"objectForKey:",_6a);
if(_6b===nil){
return 0;
}
if(objj_msgSend(_6b,"respondsToSelector:",sel_getUid("intValue"))){
_6b=objj_msgSend(_6b,"intValue");
}
return parseInt(_6b);
}
}),new objj_method(sel_getUid("doubleForKey:"),function(_6c,_6d,_6e){
with(_6c){
return objj_msgSend(_6c,"floatForKey:",_6e);
}
}),new objj_method(sel_getUid("stringForKey:"),function(_6f,_70,_71){
with(_6f){
var _72=objj_msgSend(_6f,"objectForKey:",_71);
if(objj_msgSend(_72,"isKindOfClass:",CPString)){
return _72;
}else{
if(objj_msgSend(_72,"respondsToSelector:",sel_getUid("stringValue"))){
return objj_msgSend(_72,"stringValue");
}
}
return nil;
}
}),new objj_method(sel_getUid("stringArrayForKey:"),function(_73,_74,_75){
with(_73){
var _76=objj_msgSend(_73,"objectForKey:",_75);
if(!objj_msgSend(_76,"isKindOfClass:",CPArray)){
return nil;
}
for(var i=0,_77=objj_msgSend(_76,"count");i<_77;i++){
if(!objj_msgSend(_76[i],"isKindOfClass:",CPString)){
return nil;
}
}
return _76;
}
}),new objj_method(sel_getUid("URLForKey:"),function(_78,_79,_7a){
with(_78){
var _7b=objj_msgSend(_78,"objectForKey:",_7a);
if(objj_msgSend(_7b,"isKindOfClass:",CPURL)){
return _7b;
}
if(objj_msgSend(_7b,"isKindOfClass:",CPString)){
return objj_msgSend(CPURL,"URLWithString:",_7b);
}
return nil;
}
}),new objj_method(sel_getUid("setBool:forKey:"),function(_7c,_7d,_7e,_7f){
with(_7c){
if(objj_msgSend(_7e,"respondsToSelector:",sel_getUid("boolValue"))){
objj_msgSend(_7c,"setObject:forKey:",objj_msgSend(_7e,"boolValue"),_7f);
}
}
}),new objj_method(sel_getUid("setFloat:forKey:"),function(_80,_81,_82,_83){
with(_80){
if(objj_msgSend(_82,"respondsToSelector:",sel_getUid("aValue"))){
_82=objj_msgSend(_82,"floatValue");
}
objj_msgSend(_80,"setObject:forKey:",parseFloat(_82),_83);
}
}),new objj_method(sel_getUid("setDouble:forKey:"),function(_84,_85,_86,_87){
with(_84){
objj_msgSend(_84,"setFloat:forKey:",_86,_87);
}
}),new objj_method(sel_getUid("setInteger:forKey:"),function(_88,_89,_8a,_8b){
with(_88){
if(objj_msgSend(_8a,"respondsToSelector:",sel_getUid("intValue"))){
_8a=objj_msgSend(_8a,"intValue");
}
objj_msgSend(_88,"setObject:forKey:",parseInt(_8a),_8b);
}
}),new objj_method(sel_getUid("setURL:forKey:"),function(_8c,_8d,_8e,_8f){
with(_8c){
if(objj_msgSend(_8e,"isKindOfClass:",CPString)){
_8e=objj_msgSend(CPURL,"URLWithString:",_8e);
}
objj_msgSend(_8c,"setObject:forKey:",_8e,_8f);
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("standardUserDefaults"),function(_90,_91){
with(_90){
if(!_1){
_1=objj_msgSend(objj_msgSend(CPUserDefaults,"alloc"),"init");
}
return _1;
}
}),new objj_method(sel_getUid("resetStandardUserDefaults"),function(_92,_93){
with(_92){
if(_1){
objj_msgSend(_1,"synchronize");
}
_1=nil;
}
})]);
var _2=objj_allocateClassPair(CPObject,"CPUserDefaultsStore"),_3=_2.isa;
class_addIvars(_2,[new objj_ivar("_domain")]);
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("domain"),function(_94,_95){
with(_94){
return _domain;
}
}),new objj_method(sel_getUid("setDomain:"),function(_96,_97,_98){
with(_96){
_domain=_98;
}
}),new objj_method(sel_getUid("data"),function(_99,_9a){
with(_99){
_CPRaiseInvalidAbstractInvocation(_99,_9a);
return nil;
}
}),new objj_method(sel_getUid("setData:"),function(_9b,_9c,_9d){
with(_9b){
_CPRaiseInvalidAbstractInvocation(_9b,_9c);
}
})]);
var _2=objj_allocateClassPair(CPUserDefaultsStore,"CPUserDefaultsCookieStore"),_3=_2.isa;
class_addIvars(_2,[new objj_ivar("_cookie")]);
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("setDomain:"),function(_9e,_9f,_a0){
with(_9e){
if(_domain===_a0){
return;
}
_domain=_a0;
_cookie=objj_msgSend(objj_msgSend(CPCookie,"alloc"),"initWithName:",_domain);
}
}),new objj_method(sel_getUid("data"),function(_a1,_a2){
with(_a1){
var _a3=objj_msgSend(_cookie,"value");
if(!_a3||objj_msgSend(_a3,"length")<1){
return nil;
}
return objj_msgSend(CPData,"dataWithRawString:",decodeURIComponent(_a3));
}
}),new objj_method(sel_getUid("setData:"),function(_a4,_a5,_a6){
with(_a4){
objj_msgSend(_cookie,"setValue:expires:domain:",encodeURIComponent(objj_msgSend(_a6,"rawString")),objj_msgSend(CPDate,"distantFuture"),window.location.href.hostname);
}
})]);
var _a7="9961800812587769-Cappuccino-Storage-Test";
var _2=objj_allocateClassPair(CPUserDefaultsStore,"CPUserDefaultsLocalStore"),_3=_2.isa;
objj_registerClassPair(_2);
class_addMethods(_2,[new objj_method(sel_getUid("init"),function(_a8,_a9){
with(_a8){
if(!objj_msgSend(objj_msgSend(_a8,"class"),"supportsLocalStorage")){
objj_msgSend(CPException,"raise:reason:","UnsupportedFeature","Browser does not support localStorage for CPUserDefaultsLocalStore");
return _a8=nil;
}
return _a8=objj_msgSendSuper({receiver:_a8,super_class:objj_getClass("CPUserDefaultsLocalStore").super_class},"init");
}
}),new objj_method(sel_getUid("data"),function(_aa,_ab){
with(_aa){
var _ac=localStorage.getItem(_domain);
if(!_ac||objj_msgSend(_ac,"length")<1){
return nil;
}
return objj_msgSend(CPData,"dataWithRawString:",decodeURIComponent(_ac));
}
}),new objj_method(sel_getUid("setData:"),function(_ad,_ae,_af){
with(_ad){
try{
localStorage.setItem(_domain,encodeURIComponent(objj_msgSend(_af,"rawString")));
}
catch(e){
CPLog.warn("Unable to write to local storage: "+e);
}
}
})]);
class_addMethods(_3,[new objj_method(sel_getUid("supportsLocalStorage"),function(_b0,_b1){
with(_b0){
if(!window.localStorage){
return NO;
}
try{
localStorage.setItem(_a7,"1");
if(localStorage.getItem(_a7)!="1"){
return NO;
}
localStorage.removeItem(_a7);
}
catch(e){
return NO;
}
return YES;
}
})]);
p;18;CPKeyValueCoding.jt;10687;@STATIC;1.0;i;9;CPArray.ji;14;CPDictionary.ji;13;CPException.ji;12;CPIndexSet.ji;8;CPNull.ji;10;CPObject.ji;7;CPSet.ji;21;CPKeyValueObserving.jt;10536;
objj_executeFile("CPArray.j",YES);
objj_executeFile("CPDictionary.j",YES);
objj_executeFile("CPException.j",YES);
objj_executeFile("CPIndexSet.j",YES);
objj_executeFile("CPNull.j",YES);
objj_executeFile("CPObject.j",YES);
objj_executeFile("CPSet.j",YES);
CPUndefinedKeyException="CPUndefinedKeyException";
CPTargetObjectUserInfoKey="CPTargetObjectUserInfoKey";
CPUnknownUserInfoKey="CPUnknownUserInfoKey";
var _1="$CPObjectAccessorsForClassKey",_2="$CPObjectModifiersForClassKey";
var _3=objj_getClass("CPObject");
if(!_3){
throw new SyntaxError("*** Could not find definition for class \"CPObject\"");
}
var _4=_3.isa;
class_addMethods(_3,[new objj_method(sel_getUid("valueForKey:"),function(_5,_6,_7){
with(_5){
var _8=objj_msgSend(_5,"class"),_9=nil,_a=_8[_1];
if(!_a){
_a=_8[_1]={};
}
if(_a.hasOwnProperty(_7)){
_9=_a[_7];
}else{
var _b=nil,_c=_7.charAt(0).toUpperCase()+_7.substr(1),_d=nil,_e=nil;
if(objj_msgSend(_8,"instancesRespondToSelector:",_b=sel_getUid("get"+_c))||objj_msgSend(_8,"instancesRespondToSelector:",_b=sel_getUid(_7))||objj_msgSend(_8,"instancesRespondToSelector:",_b=sel_getUid((_e="is"+_c)))||objj_msgSend(_8,"instancesRespondToSelector:",_b=sel_getUid("_get"+_c))||objj_msgSend(_8,"instancesRespondToSelector:",_b=sel_getUid((_d="_"+_7)))||objj_msgSend(_8,"instancesRespondToSelector:",_b=sel_getUid("_"+_e))){
_9=_a[_7]=[0,_b];
}else{
if(objj_msgSend(_8,"instancesRespondToSelector:",sel_getUid("countOf"+_c))){
if(objj_msgSend(_8,"instancesRespondToSelector:",sel_getUid("objectIn"+_c+"AtIndex:"))||objj_msgSend(_8,"instancesRespondToSelector:",sel_getUid(_7+"AtIndexes:"))){
_9=_a[_7]=[1];
}else{
if(objj_msgSend(_8,"instancesRespondToSelector:",sel_getUid("enumeratorOf"+_c))&&objj_msgSend(_8,"instancesRespondToSelector:",sel_getUid("memberOf"+_c+":"))){
_9=_a[_7]=[2];
}
}
}
}
if(!_9){
if(class_getInstanceVariable(_8,_b=_d)||class_getInstanceVariable(_8,_b="_"+_e)||class_getInstanceVariable(_8,_b=_7)||class_getInstanceVariable(_8,_b=_e)){
_9=_a[_7]=[3,_b];
}else{
_9=_a[_7]=[];
}
}
}
switch(_9[0]){
case 0:
return objj_msgSend(_5,_9[1]);
case 1:
return objj_msgSend(objj_msgSend(_CPKeyValueCodingArray,"alloc"),"initWithTarget:key:",_5,_7);
case 2:
return objj_msgSend(objj_msgSend(_CPKeyValueCodingSet,"alloc"),"initWithTarget:key:",_5,_7);
case 3:
if(objj_msgSend(_8,"accessInstanceVariablesDirectly")){
return _5[_9[1]];
}
}
return objj_msgSend(_5,"valueForUndefinedKey:",_7);
}
}),new objj_method(sel_getUid("valueForKeyPath:"),function(_f,_10,_11){
with(_f){
var _12=_11.indexOf(".");
if(_12===CPNotFound){
return objj_msgSend(_f,"valueForKey:",_11);
}
var _13=_11.substring(0,_12),_14=_11.substring(_12+1),_15=objj_msgSend(_f,"valueForKey:",_13);
return objj_msgSend(_15,"valueForKeyPath:",_14);
}
}),new objj_method(sel_getUid("dictionaryWithValuesForKeys:"),function(_16,_17,_18){
with(_16){
var _19=0,_1a=_18.length,_1b=objj_msgSend(CPDictionary,"dictionary");
for(;_19<_1a;++_19){
var key=_18[_19],_1c=objj_msgSend(_16,"valueForKey:",key);
if(_1c===nil){
objj_msgSend(_1b,"setObject:forKey:",objj_msgSend(CPNull,"null"),key);
}else{
objj_msgSend(_1b,"setObject:forKey:",_1c,key);
}
}
return _1b;
}
}),new objj_method(sel_getUid("valueForUndefinedKey:"),function(_1d,_1e,_1f){
with(_1d){
objj_msgSend(objj_msgSend(CPException,"exceptionWithName:reason:userInfo:",CPUndefinedKeyException,objj_msgSend(_1d,"_objectDescription")+" is not key value coding-compliant for the key "+_1f,objj_msgSend(CPDictionary,"dictionaryWithObjects:forKeys:",[_1d,_1f],[CPTargetObjectUserInfoKey,CPUnknownUserInfoKey])),"raise");
}
}),new objj_method(sel_getUid("setValue:forKeyPath:"),function(_20,_21,_22,_23){
with(_20){
if(!_23){
_23="self";
}
var _24=_23.indexOf(".");
if(_24===CPNotFound){
return objj_msgSend(_20,"setValue:forKey:",_22,_23);
}
var _25=_23.substring(0,_24),_26=_23.substring(_24+1),_27=objj_msgSend(_20,"valueForKey:",_25);
return objj_msgSend(_27,"setValue:forKeyPath:",_22,_26);
}
}),new objj_method(sel_getUid("setValue:forKey:"),function(_28,_29,_2a,_2b){
with(_28){
var _2c=objj_msgSend(_28,"class"),_2d=nil,_2e=_2c[_2];
if(!_2e){
_2e=_2c[_2]={};
}
if(_2e.hasOwnProperty(_2b)){
_2d=_2e[_2b];
}else{
var _2f=nil,_30=_2b.charAt(0).toUpperCase()+_2b.substr(1),_31=nil;
if(objj_msgSend(_2c,"instancesRespondToSelector:",_2f=sel_getUid("set"+_30+":"))||objj_msgSend(_2c,"instancesRespondToSelector:",_2f=sel_getUid("_set"+_30+":"))){
_2d=_2e[_2b]=[0,_2f];
}else{
if(class_getInstanceVariable(_2c,_2f="_"+_2b)||class_getInstanceVariable(_2c,_2f="_"+(_31="is"+_30))||class_getInstanceVariable(_2c,_2f=_2b)||class_getInstanceVariable(_2c,_2f=_31)){
_2d=_2e[_2b]=[1,_2f];
}else{
_2d=_2e[_2b]=[];
}
}
}
switch(_2d[0]){
case 0:
return objj_msgSend(_28,_2d[1],_2a);
case 1:
if(objj_msgSend(_2c,"accessInstanceVariablesDirectly")){
objj_msgSend(_28,"willChangeValueForKey:",_2b);
_28[_2d[1]]=_2a;
return objj_msgSend(_28,"didChangeValueForKey:",_2b);
}
}
return objj_msgSend(_28,"setValue:forUndefinedKey:",_2a,_2b);
}
}),new objj_method(sel_getUid("setValuesForKeysWithDictionary:"),function(_32,_33,_34){
with(_32){
var _35,key,_36=objj_msgSend(_34,"keyEnumerator");
while((key=objj_msgSend(_36,"nextObject"))!==nil){
_35=objj_msgSend(_34,"objectForKey:",key);
if(_35===objj_msgSend(CPNull,"null")){
objj_msgSend(_32,"setValue:forKey:",nil,key);
}else{
objj_msgSend(_32,"setValue:forKey:",_35,key);
}
}
}
}),new objj_method(sel_getUid("setValue:forUndefinedKey:"),function(_37,_38,_39,_3a){
with(_37){
objj_msgSend(objj_msgSend(CPException,"exceptionWithName:reason:userInfo:",CPUndefinedKeyException,objj_msgSend(_37,"_objectDescription")+" is not key value coding-compliant for the key "+_3a,objj_msgSend(CPDictionary,"dictionaryWithObjects:forKeys:",[_37,_3a],[CPTargetObjectUserInfoKey,CPUnknownUserInfoKey])),"raise");
}
}),new objj_method(sel_getUid("_objectDescription"),function(_3b,_3c){
with(_3b){
return "<"+objj_msgSend(_3b,"className")+" 0x"+objj_msgSend(CPString,"stringWithHash:",objj_msgSend(_3b,"UID"))+">";
}
})]);
class_addMethods(_4,[new objj_method(sel_getUid("accessInstanceVariablesDirectly"),function(_3d,_3e){
with(_3d){
return YES;
}
})]);
var _3=objj_getClass("CPDictionary");
if(!_3){
throw new SyntaxError("*** Could not find definition for class \"CPDictionary\"");
}
var _4=_3.isa;
class_addMethods(_3,[new objj_method(sel_getUid("valueForKey:"),function(_3f,_40,_41){
with(_3f){
if(objj_msgSend(_41,"hasPrefix:","@")){
return objj_msgSendSuper({receiver:_3f,super_class:objj_getClass("CPDictionary").super_class},"valueForKey:",_41.substr(1));
}
return objj_msgSend(_3f,"objectForKey:",_41);
}
}),new objj_method(sel_getUid("setValue:forKey:"),function(_42,_43,_44,_45){
with(_42){
if(_44!==nil){
objj_msgSend(_42,"setObject:forKey:",_44,_45);
}else{
objj_msgSend(_42,"removeObjectForKey:",_45);
}
}
})]);
var _3=objj_getClass("CPNull");
if(!_3){
throw new SyntaxError("*** Could not find definition for class \"CPNull\"");
}
var _4=_3.isa;
class_addMethods(_3,[new objj_method(sel_getUid("valueForKey:"),function(_46,_47,_48){
with(_46){
return _46;
}
})]);
var _3=objj_allocateClassPair(CPArray,"_CPKeyValueCodingArray"),_4=_3.isa;
class_addIvars(_3,[new objj_ivar("_target"),new objj_ivar("_countOfSelector"),new objj_ivar("_objectInAtIndexSelector"),new objj_ivar("_atIndexesSelector")]);
objj_registerClassPair(_3);
class_addMethods(_3,[new objj_method(sel_getUid("initWithTarget:key:"),function(_49,_4a,_4b,_4c){
with(_49){
_49=objj_msgSendSuper({receiver:_49,super_class:objj_getClass("_CPKeyValueCodingArray").super_class},"init");
if(_49){
var _4d=_4c.charAt(0).toUpperCase()+_4c.substr(1);
_target=_4b;
_countOfSelector=CPSelectorFromString("countOf"+_4d);
_objectInAtIndexSelector=CPSelectorFromString("objectIn"+_4d+"AtIndex:");
if(!objj_msgSend(_target,"respondsToSelector:",_objectInAtIndexSelector)){
_objectInAtIndexSelector=nil;
}
_atIndexesSelector=CPSelectorFromString(_4c+"AtIndexes:");
if(!objj_msgSend(_target,"respondsToSelector:",_atIndexesSelector)){
_atIndexesSelector=nil;
}
}
return _49;
}
}),new objj_method(sel_getUid("count"),function(_4e,_4f){
with(_4e){
return objj_msgSend(_target,_countOfSelector);
}
}),new objj_method(sel_getUid("objectAtIndex:"),function(_50,_51,_52){
with(_50){
if(_objectInAtIndexSelector){
return objj_msgSend(_target,_objectInAtIndexSelector,_52);
}
return objj_msgSend(_target,_atIndexesSelector,objj_msgSend(CPIndexSet,"indexSetWithIndex:",_52))[0];
}
}),new objj_method(sel_getUid("objectsAtIndexes:"),function(_53,_54,_55){
with(_53){
if(_atIndexesSelector){
return objj_msgSend(_target,_atIndexesSelector,_55);
}
return objj_msgSendSuper({receiver:_53,super_class:objj_getClass("_CPKeyValueCodingArray").super_class},"objectsAtIndexes:",_55);
}
}),new objj_method(sel_getUid("classForCoder"),function(_56,_57){
with(_56){
return objj_msgSend(CPArray,"class");
}
}),new objj_method(sel_getUid("copy"),function(_58,_59){
with(_58){
return objj_msgSend(CPArray,"arrayWithArray:",_58);
}
})]);
var _3=objj_allocateClassPair(CPSet,"_CPKeyValueCodingSet"),_4=_3.isa;
class_addIvars(_3,[new objj_ivar("_target"),new objj_ivar("_countOfSelector"),new objj_ivar("_enumeratorOfSelector"),new objj_ivar("_memberOfSelector")]);
objj_registerClassPair(_3);
class_addMethods(_3,[new objj_method(sel_getUid("initWithObjects:count:"),function(_5a,_5b,_5c,_5d){
with(_5a){
return objj_msgSend(objj_msgSend(CPSet,"alloc"),"initWithObjects:count:",_5c,_5d);
}
}),new objj_method(sel_getUid("initWithTarget:key:"),function(_5e,_5f,_60,_61){
with(_5e){
_5e=objj_msgSendSuper({receiver:_5e,super_class:objj_getClass("_CPKeyValueCodingSet").super_class},"initWithObjects:count:",nil,0);
if(_5e){
var _62=_61.charAt(0).toUpperCase()+_61.substr(1);
_target=_60;
_countOfSelector=CPSelectorFromString("countOf"+_62);
_enumeratorOfSelector=CPSelectorFromString("enumeratorOf"+_62);
_memberOfSelector=CPSelectorFromString("memberOf"+_62+":");
}
return _5e;
}
}),new objj_method(sel_getUid("count"),function(_63,_64){
with(_63){
return objj_msgSend(_target,_countOfSelector);
}
}),new objj_method(sel_getUid("objectEnumerator"),function(_65,_66){
with(_65){
return objj_msgSend(_target,_enumeratorOfSelector);
}
}),new objj_method(sel_getUid("member:"),function(_67,_68,_69){
with(_67){
return objj_msgSend(_target,_memberOfSelector,_69);
}
}),new objj_method(sel_getUid("classForCoder"),function(_6a,_6b){
with(_6a){
return objj_msgSend(CPSet,"class");
}
}),new objj_method(sel_getUid("copy"),function(_6c,_6d){
with(_6c){
return objj_msgSend(CPSet,"setWithSet:",_6c);
}
})]);
objj_executeFile("CPKeyValueObserving.j",YES);
