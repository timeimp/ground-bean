/*
 *  GCLoginModel.j
 *  Defines the relationship between the webapp and php-db backend
 *
 */


@import <Foundation/CPObject.j>
@import <Foundation/CPURLRequest.j>
@import <Foundation/CPURLConnection.j>

@implementation GCAuthenticationObject : CPObject{
	CPURLConnection authConnection;
	CPURLRequest	authRequest;
	id delegate @accessors;
}
-(id)init{
	self = [super init];
	if(self){
			console.info("New GCAuthenticationModel created");
	}
	return self;
}


-(BOOL)authenticateWithUser:(CPString)aUser andPassword:(CPString)aPassword{
	authRequest = [[CPURLRequest alloc] initWithURL:@"Server/Authentication/authenticate.php"];
	[authRequest setHTTPMethod:@"POST"];
	var payloadJSON = {"user" : aUser, "pass" : aPassword };
	var content = [CPString JSONFromObject:payloadJSON];
	[authRequest setHTTPBody:content];
	console.info("GCAuthenticationObject: connecting to server...");
	authServer = [[CPURLConnection alloc] initWithRequest:authRequest delegate:self];
}

-(void)connection:(CPURLConnection)authServer didFailWithError:(id)error{
	console.warn("GCAuthenticationObject: an error occurred with details: "+error);
	[delegate didFailAuthenticationWithError:@"Server Failed to Authenticate"];
}
-(void)connection:(CPURLConnection)authServer didReceiveData:(CPString)data{
	var theData = [data objectFromJSON];
	console.info("GCAuthenticationObject: finished loading initSession URL.")
	if(theData.status == "SUCCESS"){
		[delegate didSucceedAuthenticating:@"Successfully Authenticated User..."];
	} else {
		[delegate didFailAuthenticationWithError:theData.errorMessage];
	}
}
@end
