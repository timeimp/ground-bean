/*
 * GCServerModel.j
 * GroundCoffee
 *
 * Created by James Wilson (timeimp) 2012
 * Copyright 2012, timeimp inc. All rights reserved.
 */
 

@implementation GCServerMeta : CPObject<CPCoding>{
    
}
 
 -(void)encodeWithCoder:(CPCoder)aCoder{
     [coder encodeObject:self.serverName forKey:@"ServerName"];
     [coder encodeObject:self.serverIP   forKey:@"ServerIP"];
     [coder encodeObject:self.serverHash forKey:@"ServerHash"];
     [coder encodeObject:self.serverKey  forKey:@"ServerKey"];
     
 }
 
 -(id)initWithCoder:(NSCoder *)coder {
	if ((self = [super init]))
	{
          self.serverName = [coder decodeObjectForKey:@"ServerName"];
          self.serverIP   = [coder decodeObjectForKey:@"ServerIP"];  
          self.serverHash = [coder decodeObjectForKey:@"ServerHash"];
          self.serverKey  = [coder decodeObjectForKey:@"ServerKey"];
 	}
	return self;
}
 
@end
@implementation GCServerModel : CPObject{
    CPString        serverName;
    CPString        serverIP;
    CPString        serverHash;
    CPString        serverKey;
    CPDictionary    minecraftInformation;
    CPURL           serverURL;
    CPURLConnection serverConnection;
    id              delegate    @accessors;
}

-(id)init:(id)sender{
    alert("!)");
    if(self = [super init]){
        return self;
    }
}

-(void)retrieveInformation{
    
    var serverURL = [[CPURL alloc] initWithString:@"Server/Authentication/getAuthMetaData.php"];
    [serverURL setResourceValue:@"A" forKey:@"challenge"];
    
    serverConnection = [[CPURLConnection alloc] initWithRequest:[CPURLRequest requestWithURL:serverURL] delegate:self startImmediately:YES];
    [[CPNotificationCenter defaultCenter] postNotificationName:@"GCServerInformationRetrieved" object:nil];
}
/*
    +(void)retrieveInformationWithURL:(CPString)aURL andKey:(CPString)aKey{
    var serverURL = [[CPURL alloc] initWithString:aURL];
    [serverURL setResourceValue:aKey forKey:@"challenge"];
    
    
    serverConnection = [[CPURLConnection alloc] initWithRequest:serverURL delegate:self startImmediately:YES];
    [[CPNotificationCenter defaultCenter] postNotificationName:@"GCServerInformationRetrieved" object:nil];
        
    }
*/
-(void)connection:(CPURLConnection)connection didReceiveData:(CPString)data{
    console.log("Data retrieved");
    var result  = [data objectFromJSON];
    console.log(result);
   /* serverName  = [[CPString alloc] initWithString:result.name];
    serverIP    = [[CPString alloc] initWithString:result.IP];
    serverHash  = [[CPString alloc] initWithString:result.Hash];
    
    var info    = result.information.properties;
    minecraftInformation = [[CPDictionary alloc] initWithObjectsAndKeys:
                                                                        result.version, @"version",
                                                                        result.mods,  @"mods"];
    for(key in info){
        [minecraftInformation setObject:info[key] forKey:key];
        console.log("Added: [",key,"] ==> ",info[key]);
    }*/
    
    [self validateFileSettings:result.meta_file_info.validation_str];
    [[CPNotificationCenter defaultCenter] postNotificationName:@"GCLoginAuditSuccess" object:nil];
}

-(void)validateFileSettings:(id)aFile{
    [delegate changeString:aFile];

    for(key in aFile){
        if(aFile[key] === nil)
            return;
    }
}

@end;